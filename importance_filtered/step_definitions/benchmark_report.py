from utils import *


@step('I am on the "Benchmark Report" page')
def verify_page(step):
    time.sleep(2)
    assert get_browser().is_text_present("Benchmark Report")
    assert 'benchmark' in get_browser().url


@step(u'there is no Unfavorable score for question 1')
def there_is_no_unfavorable_score_for_question_1(step):
    assert True


@step(u'there is no Unfavorable score on top for question 1')
def there_is_no_unfavorable_score_on_top_for_question_1(step):
    assert True


@step(u'there is no Unfavorable score on bottom for question 1')
def there_is_no_unfavorable_score_on_bottom_for_question_1(step):
    assert True
