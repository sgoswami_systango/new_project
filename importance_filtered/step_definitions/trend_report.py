from utils import *


def add_space(s1, s2):
    text = ' '.join([s1, s2])
    return '' if text == ' ' else text


def get_value_for_element_and_column(element, column, row=None):
    if column == "2014":
        return add_space(element[0].text, element[1].text)
    if column == "2013":
        return add_space(element[2].text, element[3].text)
    if column == "2012":
        if row == 'question 1' or get_user() == 'qa2':
            return element[4].text
        elif get_user() == "qa3" or get_user() == "qa4" or get_user() == "qa5":
            return element[4].text
        else:
            return add_space(element[4].text, element[5].text)
    if column == "2011":
        return element[6].text


def get_score_for(row, column):
    browser = get_browser()
    rows = browser.find_by_css('.data.standalone')
    if row == "question 1":
        # import pdb;pdb.set_trace()
        element = rows[0].find_by_css('td')
        data = get_value_for_element_and_column(element, column, row)
        if not data:
            element = browser.find_by_css('.data.sub')[0].find_by_css('td')
            data = get_value_for_element_and_column(element, column, row)
            if not data:
                element = rows[33].find_by_css('td')
                data = get_value_for_element_and_column(element, column, row)
                if not data:
                    element = browser.find_by_css(
                        '.data.sub')[51].find_by_css('td')
                    data = get_value_for_element_and_column(
                        element, column, row)
        return data
    if row == "over_all":
        # import pdb; pdb.set_trace()
        element = rows[31].find_by_css('td')
        data = get_value_for_element_and_column(element, column)
        if not data:
            element = rows[32].find_by_css('td')
            data = get_value_for_element_and_column(element, column)
            if not data:
                element = rows[64].find_by_css('td')
                data = get_value_for_element_and_column(element, column)
                if not data:
                    element = rows[65].find_by_css('td')
                    data = get_value_for_element_and_column(element, column)
        return data
    if row == "Clarity of Direction":
        rows = browser.find_by_css('.data.category')
        element = rows[0].find_by_css('td')
        data = get_value_for_element_and_column(element, column)
        if not data:
            element = rows[12].find_by_css('td')
            data = get_value_for_element_and_column(element, column)
        return data


@step('I am on the "Trend Report" page')
def verify_page(step):
    time.sleep(2)
    assert get_browser().is_text_present("Trend Report")
    assert 'trend_report' in get_browser().url


@step(u'the score for "([^"]*)" in the "([^"]*)" column is "([^"]*)"')
def the_score_for_group1_in_the_group2_column_is_group3(step, group1, group2, group3):
    assert get_score_for(group1, group2) == group3


@step(u'the Overall Average for "([^"]*)" is "([^"]*)"')
def the_overall_average_for_group1_is_group2(step, group1, group2):
    assert get_score_for('over_all', group1) == group2


@step(u'the score for the "([^"]*)" category for "([^"]*)" is "([^"]*)"')
def the_score_for_the_group1_category_for_group2_is_group3(step, group1, group2, group3):
    assert get_score_for(group1, group2) == group3


@step(u'And I am on the "Favorability" tab')
def i_am_on_the_favorability(step):
    get_browser().find_link_by_partial_text('Favorability')[1].click()
    time.sleep(3)


@step(u'I am on the Categories tab')
def i_am_on_the_categories(step):
    get_browser().find_link_by_text('Categories')[1].click()
    time.sleep(3)


@step(u'the Overall Average at the bottom for "([^"]*)" is "([^"]*)"')
def the_overall_average_at_the_bottom_for_group1_is_group2(step, group1, group2):
    assert get_score_for('over_all', group1) == group2


@step(u'the score for the "([^"]*)" category for "([^"]*)" is "([^"]*)"')
def the_score_for_the_group1_category_for_group2_is_group3(step, group1, group2, group3):
    assert get_score_for(group1, group2) == group3


@step(u'the "([^"]*)" for the "([^"]*)" column is "([^"]*)"')
def the_count_for_group1_is_group2(step, new, group1, group2):
    rows = get_browser().find_by_css('.report-table .data .value.count')
    if get_tab_link() != 'Categories':
        if group1 == '2014':
            if get_user() == 'qa5':
                if rows[0].text:
                    value = rows[0].text
                else:
                    value = rows[6].text
            else:
                if rows[0].text:
                    value = rows[0].text
                elif get_user() == 'qa2' or get_user() == 'qa3' or get_user() == 'qa4':
                    value = rows[6].text
                else:
                    value = rows[8].text
        elif group1 == '2013':
            if get_user() == 'qa5':
                if rows[1].text:
                    value = rows[1].text
                else:
                    value = rows[7].text
            else:
                if rows[1].text:
                    value = rows[1].text
                elif get_user() == 'qa2' or get_user() == 'qa3' or get_user() == 'qa4':
                    value = rows[7].text
                else:
                    value = rows[9].text
        elif group1 == '2012':
            if get_user()=='qa1':
                if rows[2].text:
                    value = rows[2].text
                else:
                    value = rows[10].text
            else:    
                if rows[2].text:
                    value = rows[2].text
                else:
                    value = rows[8].text
        elif group1 == '2011':
            # import pdb;pdb.set_trace()
            if rows[3].text:
                value = rows[3].text
            else:
                value = rows[11].text
    else:
        if group1 == '2014':
            if get_user() == 'qa1':
                if rows[4].text:
                    value = rows[4].text
                elif rows[8].text:
                    value = rows[8].text
                elif rows[12].text:
                    value = rows[12].text
            else:
                if rows[3].text:
                    value = rows[3].text
                else:
                    value = rows[9].text
        elif group1 == '2013':
            if get_user() == 'qa1':
                if rows[5].text:
                    value = rows[5].text
                elif rows[9].text:
                    value = rows[9].text
                elif rows[13].text:
                    value = rows[13].text
            else:
                if rows[4].text:
                    value = rows[4].text
                else:
                    value = rows[10].text
        elif group1 == '2012':
            if get_user() == 'qa2' or get_user() == 'qa3'or get_user() == 'qa4'or get_user() == 'qa5':
                value = rows[5].text
                if not value:
                    value = rows[11].text
            else:
                value = rows[6].text
                if not value:
                    value = rows[10].text
                    if not value:
                        value = rows[14].text
        elif group1 == '2011':
            value = rows[7].text
            if not value:
                value = rows[11].text
                if not value:
                    value = rows[15].text

        else:
            if rows[5].text:
                value = rows[5].text
            else:
                value = rows[11].text
    assert value == group2


@step(u'there is no score for question 1 in the "([^"]*)" column')
def there_is_no_score_for_question_1_in_the_group1_column(step, group1):
    assert True


@step(u'there is no score for "([^"]*)" in the "([^"]*)" column')
def there_is_no_score_for_group1_in_the_group2_column(step, group1, group2):
    assert True


@step(u'there is no score for question 1 in the 2011 column')
def there_is_no_score_for_question_1_in_the_2011_column(step):
    assert True
