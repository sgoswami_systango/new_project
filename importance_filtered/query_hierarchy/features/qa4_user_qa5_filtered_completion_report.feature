Feature: Check filtered Completion Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa5"
	And I click on the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Male" in the "Invitees" column is "21"
	And the figure for "Male" in the "Respondents" column is "19(90.5%)"
	And the figure for "Male" in the "Completions" column is "19(90.5%)"
	And the figure for the "Totals" row in the "Invitees" column is "39"
	And the figure for the "Totals" row in the "Respondents" column is "35(89.7%)"
	And the figure for the "Totals" row in the "Completions" column is "35(89.7%)"

	When I am on the "By Time" tab
	Then the figure for "Monday, September 15, 2014" in the "Respondents" column is "10"
	And the figure for "Monday, September 15, 2014" in the "% Responded" column is "28.6%"
	And the figure for "Monday, September 15, 2014" in the "% Invited" column is "25.6%"
	And the figure for the "Totals" row in the "Respondents" column is "35"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "89.7%"
	
	When I click on "Monday, September 15, 2014"
	Then the figure for "12:00 AM" in the "Respondents" column is "2"
	And the figure for "12:00 AM" in the "% Responded" column is "5.7%"
	And the figure for "12:00 AM" in the "% Invited" column is "5.1%"
	
	Now I go to "Functions" > "Logout"