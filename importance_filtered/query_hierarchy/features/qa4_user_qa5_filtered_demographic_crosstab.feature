Feature: Check filtered Demographic Crosstab for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa5"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(19)"
	And the first number under "Female" is "(16)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.3"
	And the score for question 1 in the "Female" column is "4.5"
	And the Weighted Average on the right for question 1 is "4.4"
	And the Group Average at the bottom for the "Male" column is "3.8"
	And the Group Average at the bottom for the "Female" column is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.32"
	And the score for question 1 in the "Female" column is "4.50"
	And the Weighted Average on the right for question 1 is "4.40"
	And the Group Average at the bottom for the "Male" column is "3.77"
	And the Group Average at the bottom for the "Female" column is "4.08"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "95"
	And the score for question 1 in the "Female" column is "88"
	And the Weighted Average on the right for question 1 is "91"
	And the Group Average at the bottom for the "Male" column is "70"
	And the Group Average at the bottom for the "Female" column is "82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "94.7"
	And the score for question 1 in the "Female" column is "87.5"
	And the Weighted Average on the right for question 1 is "91.4"
	And the Group Average at the bottom for the "Male" column is "69.9"
	And the Group Average at the bottom for the "Female" column is "82.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "94.74"
	And the score for question 1 in the "Female" column is "87.50"
	And the Weighted Average on the right for question 1 is "91.43"
	And the Group Average at the bottom for the "Male" column is "69.92"
	And the Group Average at the bottom for the "Female" column is "82.14"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "0"
	And the Weighted Average on the right for question 1 is "0"
	And the Group Average at the bottom for the "Male" column is "11"
	And the Group Average at the bottom for the "Female" column is "8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "0.0"
	And the Weighted Average on the right for question 1 is "0.0"
	And the Group Average at the bottom for the "Male" column is "11.0"
	And the Group Average at the bottom for the "Female" column is "8.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "0.00"
	And the Weighted Average on the right for question 1 is "0.00"
	And the Group Average at the bottom for the "Male" column is "10.98"
	And the Group Average at the bottom for the "Female" column is "8.21"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.8"
	And the score for question 1 in the "Female" column is "4.4"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.2"
	And the Group Average at the bottom for the "Female" column is "4.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.84"
	And the score for question 1 in the "Female" column is "4.38"
	And the Weighted Average on the right for question 1 is "4.09"
	And the Group Average at the bottom for the "Male" column is "4.18"
	And the Group Average at the bottom for the "Female" column is "4.41"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "63"
	And the score for question 1 in the "Female" column is "88"
	And the Weighted Average on the right for question 1 is "74"
	And the Group Average at the bottom for the "Male" column is "85"
	And the Group Average at the bottom for the "Female" column is "93"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "63.2"
	And the score for question 1 in the "Female" column is "87.5"
	And the Weighted Average on the right for question 1 is "74.3"
	And the Group Average at the bottom for the "Male" column is "85.3"
	And the Group Average at the bottom for the "Female" column is "92.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "63.16"
	And the score for question 1 in the "Female" column is "87.50"
	And the Weighted Average on the right for question 1 is "74.29"
	And the Group Average at the bottom for the "Male" column is "85.26"
	And the Group Average at the bottom for the "Female" column is "92.68"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-9"
	And the score for question 1 in the "Female" column is "-3"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-9.2"
	And the score for question 1 in the "Female" column is "-2.6"
	And the Weighted Average on the right for question 1 is "-3.1"
	And the Group Average at the bottom for the "Male" column is "8.6"
	And the Group Average at the bottom for the "Female" column is "7.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-9.22"
	And the score for question 1 in the "Female" column is "-2.63"
	And the Weighted Average on the right for question 1 is "-3.13"
	And the Group Average at the bottom for the "Male" column is "8.57"
	And the Group Average at the bottom for the "Female" column is "7.28"


# Categories
@wip
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(19)"
	And the number directly below "Female" is "(16)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "3.8"
	And the Group Average at the bottom for the "Female" column is "4.1"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.3"
	And the score for question 1 in the "Female" column is "4.5"
	And the Weighted Average on the right for question 1 is "4.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.95"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.06"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.00"
	And the Group Average at the bottom for the "Male" column is "3.77"
	And the Group Average at the bottom for the "Female" column is "4.08"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.32"
	And the score for question 1 in the "Female" column is "4.50"
	And the Weighted Average on the right for question 1 is "4.40"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76"
	And the score for the "Clarity of Direction" category in the "Female" column is "78"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77"
	And the Group Average at the bottom for the "Male" column is "70"
	And the Group Average at the bottom for the "Female" column is "82"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "95"
	And the score for question 1 in the "Female" column is "88"
	And the Weighted Average on the right for question 1 is "91"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "78.1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77.1"
	And the Group Average at the bottom for the "Male" column is "69.9"
	And the Group Average at the bottom for the "Female" column is "82.1"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "94.7"
	And the score for question 1 in the "Female" column is "87.5"
	And the Weighted Average on the right for question 1 is "91.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76.32"
	And the score for the "Clarity of Direction" category in the "Female" column is "78.12"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77.14"
	And the Group Average at the bottom for the "Male" column is "69.92"
	And the Group Average at the bottom for the "Female" column is "82.14"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "94.74"
	And the score for question 1 in the "Female" column is "87.50"
	And the Weighted Average on the right for question 1 is "91.43"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9"
	And the score for the "Clarity of Direction" category in the "Female" column is "9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Male" column is "11"
	And the Group Average at the bottom for the "Female" column is "8"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "0"
	And the Weighted Average on the right for question 1 is "0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.2"
	And the score for the "Clarity of Direction" category in the "Female" column is "9.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9.3"
	And the Group Average at the bottom for the "Male" column is "11.0"
	And the Group Average at the bottom for the "Female" column is "8.2"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "0.0"
	And the Weighted Average on the right for question 1 is "0.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.21"
	And the score for the "Clarity of Direction" category in the "Female" column is "9.38"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9.29"
	And the Group Average at the bottom for the "Male" column is "10.98"
	And the Group Average at the bottom for the "Female" column is "8.21"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "0.00"
	And the Weighted Average on the right for question 1 is "0.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.1"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.2"
	And the Group Average at the bottom for the "Female" column is "4.4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.8"
	And the score for question 1 in the "Female" column is "4.4"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.13"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.39"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.25"
	And the Group Average at the bottom for the "Male" column is "4.18"
	And the Group Average at the bottom for the "Female" column is "4.41"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.84"
	And the score for question 1 in the "Female" column is "4.38"
	And the Weighted Average on the right for question 1 is "4.09"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "84"
	And the score for the "Clarity of Direction" category in the "Female" column is "92"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "85"
	And the Group Average at the bottom for the "Female" column is "93"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "63"
	And the score for question 1 in the "Female" column is "88"
	And the Weighted Average on the right for question 1 is "74"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "84.2"
	And the score for the "Clarity of Direction" category in the "Female" column is "92.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.9"
	And the Group Average at the bottom for the "Male" column is "85.3"
	And the Group Average at the bottom for the "Female" column is "92.7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "63.2"
	And the score for question 1 in the "Female" column is "87.5"
	And the Weighted Average on the right for question 1 is "74.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "84.21"
	And the score for the "Clarity of Direction" category in the "Female" column is "92.19"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.86"
	And the Group Average at the bottom for the "Male" column is "85.26"
	And the Group Average at the bottom for the "Female" column is "92.68"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "63.16"
	And the score for question 1 in the "Female" column is "87.50"
	And the Weighted Average on the right for question 1 is "74.29"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-9"
	And the score for question 1 in the "Female" column is "-3"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.7"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.7"
	And the Group Average at the bottom for the "Male" column is "8.6"
	And the Group Average at the bottom for the "Female" column is "7.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-9.2"
	And the score for question 1 in the "Female" column is "-2.6"
	And the Weighted Average on the right for question 1 is "-3.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.72"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.24"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.73"
	And the Group Average at the bottom for the "Male" column is "8.57"
	And the Group Average at the bottom for the "Female" column is "7.28"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-9.22"
	And the score for question 1 in the "Female" column is "-2.63"
	And the Weighted Average on the right for question 1 is "-3.13"



# Questions + trend

Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.3 (4.2)"
	And the score for question 1 in the "Female" column is "4.5 (4.1)"
	And the Weighted Average on the right for question 1 is "4.4"
	And the Group Average at the bottom for the "Male" column is "3.8 (3.9)"
	And the Group Average at the bottom for the "Female" column is "4.1 (3.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.32 (4.15)"
	And the score for question 1 in the "Female" column is "4.50 (4.13)"
	And the Weighted Average on the right for question 1 is "4.40"
	And the Group Average at the bottom for the "Male" column is "3.77 (3.94)"
	And the Group Average at the bottom for the "Female" column is "4.08 (3.91)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "95 (88)"
	And the score for question 1 in the "Female" column is "88 (87)"
	And the Weighted Average on the right for question 1 is "91"
	And the Group Average at the bottom for the "Male" column is "70 (76)"
	And the Group Average at the bottom for the "Female" column is "82 (75)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "94.7 (87.7)"
	And the score for question 1 in the "Female" column is "87.5 (86.7)"
	And the Weighted Average on the right for question 1 is "91.4"
	And the Group Average at the bottom for the "Male" column is "69.9 (75.6)"
	And the Group Average at the bottom for the "Female" column is "82.1 (75.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "94.74 (87.69)"
	And the score for question 1 in the "Female" column is "87.50 (86.67)"
	And the Weighted Average on the right for question 1 is "91.43"
	And the Group Average at the bottom for the "Male" column is "69.92 (75.63)"
	And the Group Average at the bottom for the "Female" column is "82.14 (75.31)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0 (0)"
	And the score for question 1 in the "Female" column is "0 (4)"
	And the Weighted Average on the right for question 1 is "0"
	And the Group Average at the bottom for the "Male" column is "11 (8)"
	And the Group Average at the bottom for the "Female" column is "8 (9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0 (0.0)"
	And the score for question 1 in the "Female" column is "0.0 (4.0)"
	And the Weighted Average on the right for question 1 is "0.0"
	And the Group Average at the bottom for the "Male" column is "11.0 (7.9)"
	And the Group Average at the bottom for the "Female" column is "8.2 (9.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00 (0.00)"
	And the score for question 1 in the "Female" column is "0.00 (4.00)"
	And the Weighted Average on the right for question 1 is "0.00"
	And the Group Average at the bottom for the "Male" column is "10.98 (7.89)"
	And the Group Average at the bottom for the "Female" column is "8.21 (9.16)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.8 (3.9)"
	And the score for question 1 in the "Female" column is "4.4 (4.1)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.2 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.4 (4.4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.84 (3.88)"
	And the score for question 1 in the "Female" column is "4.38 (4.11)"
	And the Weighted Average on the right for question 1 is "4.09"
	And the Group Average at the bottom for the "Male" column is "4.18 (4.31)"
	And the Group Average at the bottom for the "Female" column is "4.41 (4.39)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "63 (72)"
	And the score for question 1 in the "Female" column is "88 (81)"
	And the Weighted Average on the right for question 1 is "74"
	And the Group Average at the bottom for the "Male" column is "85 (91)"
	And the Group Average at the bottom for the "Female" column is "93 (93)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "63.2 (72.3)"
	And the score for question 1 in the "Female" column is "87.5 (81.3)"
	And the Weighted Average on the right for question 1 is "74.3"
	And the Group Average at the bottom for the "Male" column is "85.3 (91.1)"
	And the Group Average at the bottom for the "Female" column is "92.7 (92.7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "63.16 (72.31)"
	And the score for question 1 in the "Female" column is "87.50 (81.33)"
	And the Weighted Average on the right for question 1 is "74.29"
	And the Group Average at the bottom for the "Male" column is "85.26 (91.12)"
	And the Group Average at the bottom for the "Female" column is "92.68 (92.73)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-9 (-5)"
	And the score for question 1 in the "Female" column is "-3 (0)"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "9 (8)"
	And the Group Average at the bottom for the "Female" column is "7 (11)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-9.2 (-5.2)"
	And the score for question 1 in the "Female" column is "-2.6 (-0.4)"
	And the Weighted Average on the right for question 1 is "-3.1"
	And the Group Average at the bottom for the "Male" column is "8.6 (8.0)"
	And the Group Average at the bottom for the "Female" column is "7.3 (10.5)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-9.22 (-5.24)"
	And the score for question 1 in the "Female" column is "-2.63 (-0.41)"
	And the Weighted Average on the right for question 1 is "-3.13"
	And the Group Average at the bottom for the "Male" column is "8.57 (7.97)"
	And the Group Average at the bottom for the "Female" column is "7.28 (10.54)"


# Categories + trend

Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9 (4.0)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1 (3.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "3.8 (3.9)"
	And the Group Average at the bottom for the "Female" column is "4.1 (3.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.3 (4.2)"
	And the score for question 1 in the "Female" column is "4.5 (4.1)"
	And the Weighted Average on the right for question 1 is "4.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.95 (4.05)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.06 (3.94)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.00"
	And the Group Average at the bottom for the "Male" column is "3.77 (3.94)"
	And the Group Average at the bottom for the "Female" column is "4.08 (3.91)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.32 (4.15)"
	And the score for question 1 in the "Female" column is "4.50 (4.13)"
	And the Weighted Average on the right for question 1 is "4.40"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76 (85)"
	And the score for the "Clarity of Direction" category in the "Female" column is "78 (80)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77"
	And the Group Average at the bottom for the "Male" column is "70 (76)"
	And the Group Average at the bottom for the "Female" column is "82 (75)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "95 (88)"
	And the score for question 1 in the "Female" column is "88 (87)"
	And the Weighted Average on the right for question 1 is "91"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76.3 (84.6)"
	And the score for the "Clarity of Direction" category in the "Female" column is "78.1 (80.0)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77.1"
	And the Group Average at the bottom for the "Male" column is "69.9 (75.6)"
	And the Group Average at the bottom for the "Female" column is "82.1 (75.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "94.7 (87.7)"
	And the score for question 1 in the "Female" column is "87.5 (86.7)"
	And the Weighted Average on the right for question 1 is "91.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76.32 (84.62)"
	And the score for the "Clarity of Direction" category in the "Female" column is "78.12 (80.00)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77.14"
	And the Group Average at the bottom for the "Male" column is "69.92 (75.63)"
	And the Group Average at the bottom for the "Female" column is "82.14 (75.31)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "94.74 (87.69)"
	And the score for question 1 in the "Female" column is "87.50 (86.67)"
	And the Weighted Average on the right for question 1 is "91.43"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "9 (8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Male" column is "11 (8)"
	And the Group Average at the bottom for the "Female" column is "8 (9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0 (0)"
	And the score for question 1 in the "Female" column is "0 (4)"
	And the Weighted Average on the right for question 1 is "0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.2 (3.8)"
	And the score for the "Clarity of Direction" category in the "Female" column is "9.4 (7.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9.3"
	And the Group Average at the bottom for the "Male" column is "11.0 (7.9)"
	And the Group Average at the bottom for the "Female" column is "8.2 (9.2)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0 (0.0)"
	And the score for question 1 in the "Female" column is "0.0 (4.0)"
	And the Weighted Average on the right for question 1 is "0.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.21 (3.85)"
	And the score for the "Clarity of Direction" category in the "Female" column is "9.38 (7.67)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9.29"
	And the Group Average at the bottom for the "Male" column is "10.98 (7.89)"
	And the Group Average at the bottom for the "Female" column is "8.21 (9.16)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00 (0.00)"
	And the score for question 1 in the "Female" column is "0.00 (4.00)"
	And the Weighted Average on the right for question 1 is "0.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.1 (4.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.4 (4.3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.2 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.4 (4.4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.8 (3.9)"
	And the score for question 1 in the "Female" column is "4.4 (4.1)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.13 (4.21)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.39 (4.29)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.25"
	And the Group Average at the bottom for the "Male" column is "4.18 (4.31)"
	And the Group Average at the bottom for the "Female" column is "4.41 (4.39)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.84 (3.88)"
	And the score for question 1 in the "Female" column is "4.38 (4.11)"
	And the Weighted Average on the right for question 1 is "4.09"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "84 (87)"
	And the score for the "Clarity of Direction" category in the "Female" column is "92 (89)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "85 (91)"
	And the Group Average at the bottom for the "Female" column is "93 (93)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "63 (72)"
	And the score for question 1 in the "Female" column is "88 (81)"
	And the Weighted Average on the right for question 1 is "74"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "84.2 (87.3)"
	And the score for the "Clarity of Direction" category in the "Female" column is "92.2 (89.0)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.9"
	And the Group Average at the bottom for the "Male" column is "85.3 (91.1)"
	And the Group Average at the bottom for the "Female" column is "92.7 (92.7)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "63.2 (72.3)"
	And the score for question 1 in the "Female" column is "87.5 (81.3)"
	And the Weighted Average on the right for question 1 is "74.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "84.21 (87.31)"
	And the score for the "Clarity of Direction" category in the "Female" column is "92.19 (89.00)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.86"
	And the Group Average at the bottom for the "Male" column is "85.26 (91.12)"
	And the Group Average at the bottom for the "Female" column is "92.68 (92.73)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "63.16 (72.31)"
	And the score for question 1 in the "Female" column is "87.50 (81.33)"
	And the Weighted Average on the right for question 1 is "74.29"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (3)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7 (8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Male" column is "9 (8)"
	And the Group Average at the bottom for the "Female" column is "7 (11)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-9 (-5)"
	And the score for question 1 in the "Female" column is "-3 (0)"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.7 (3.4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.2 (7.6)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.7"
	And the Group Average at the bottom for the "Male" column is "8.6 (8.0)"
	And the Group Average at the bottom for the "Female" column is "7.3 (10.5)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-9.2 (-5.2)"
	And the score for question 1 in the "Female" column is "-2.6 (-0.4)"
	And the Weighted Average on the right for question 1 is "-3.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.72 (3.37)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.24 (7.51)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.73"
	And the Group Average at the bottom for the "Male" column is "8.57 (7.97)"
	And the Group Average at the bottom for the "Female" column is "7.28 (10.54)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-9.22 (-5.24)"
	And the score for question 1 in the "Female" column is "-2.63 (-0.41)"
	And the Weighted Average on the right for question 1 is "-3.13"



	Now I go to "Functions" > "Logout"