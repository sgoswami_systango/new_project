Feature: Check filtered Gap Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa5"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the figure for "Filter Count:" in the upper right is "35"
	Then the Eval Avg for question 1 is "4.40"
	And the Importance for question 1 is "4.09"
	And the Gap for question 1 is "-0.31"
	And the Weighted Gap for question 1 is "-6.34"
	And the Overall Average at the bottom has an Eval Avg of "3.91"
	And the Overall Average has a Importance of "4.28"
	And the Overall Average has a Gap of "0.37"
	And the Overall Average has a Weighted Gap of "7.92"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "4.00"
	And the Importance for the "Clarity of Direction" category is "4.25"
	And the Gap for the "Clarity of Direction" category is "0.25"
	And the Weighted Gap for the "Clarity of Direction" category is "5.31"
	And the Overall Average at the bottom has an Eval Avg of "3.91"
	And the Overall Average has a Importance of "4.28"
	And the Overall Average has a Gap of "0.37"
	And the Overall Average has a Weighted Gap of "7.92"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.40"
	And the Importance for question 1 is "4.09"
	And the Gap for question 1 is "-0.31"
	And the Weighted Gap for question 1 is "-6.34"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the number for "Filter" in the upper right is "(35)"
	And the number for "Remainder" in the upper right is "(10)"
	And the Eval Avg for question 1 is "4.40 (+0.30)"
	And the Importance for question 1 is "4.09 (-0.41)"
	And the Gap for question 1 is "-0.31 (-0.71)"
	And the Weighted Gap for question 1 is "-6.34 (-15.34)"
	And the Overall Average at the bottom has an Eval Avg of "3.91 (+0.04)"
	And the Overall Average has a Importance of "4.28 (-0.25)"
	And the Overall Average has a Gap of "0.37 (-0.29)"
	And the Overall Average has a Weighted Gap of "7.92 (-7.03)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the Eval Avg for the "Clarity of Direction" category is "4.00 (+0.04)"
	And the Importance for the "Clarity of Direction" category is "4.25 (-0.01)"
	And the Gap for the "Clarity of Direction" category is "0.25 (-0.05)"
	And the Weighted Gap for the "Clarity of Direction" category is "5.31 (-1.08)"
	And the Overall Average at the bottom has an Eval Avg of "3.91 (-0.04)"
	And the Overall Average has a Importance of "4.28 (-0.05)"
	And the Overall Average has a Gap of "0.37 (-0.01)"
	And the Overall Average has a Weighted Gap of "7.92 (-0.31)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.40 (+0.28)"
	And the Importance for question 1 is "4.09 (-0.01)"
	And the Gap for question 1 is "-0.31 (-0.29)"
	And the Weighted Gap for question 1 is "-6.34 (-5.93)"
	
	Now I go to "Functions" > "Logout"