Feature: Check filtered Trend Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa5"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(35)"
	And the "Count" for the "2013" column is "(143)"
	And the "Count" for the "2012" column is "(165)"	
	And the score for "question 1" in the "2014" column is "4.40 (+0.26)"
	And the score for "question 1" in the "2013" column is "4.14 (-0.05)"
	And the score for "question 1" in the "2012" column is "4.19"
	And the Overall Average at the bottom for "2014" is "3.96 (+0.04)"
	And the Overall Average for "2013" is "3.92 (-0.02)"
	And the Overall Average for "2012" is "3.94"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(35)"
	And the "Count" for the "2013" column is "(143)"
	And the "Count" for the "2012" column is "(165)"
	And the score for the "Clarity of Direction" category for "2014" is "4.00 (+0.01)"
	And the score for the "Clarity of Direction" category for "2013" is "3.99 (-0.08)"
	And the score for the "Clarity of Direction" category for "2012" is "4.07"
	And the Overall Average at the bottom for "2014" is "3.96 (+0.04)"
	And the Overall Average for "2013" is "3.92 (-0.02)"
	And the Overall Average for "2012" is "3.94"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.40 (+0.26)"
	And the score for "question 1" in the "2013" column is "4.14 (-0.05)"
	And the score for "question 1" in the "2012" column is "4.19"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(35)"
	And the "Count" for the "2013" column is "(143)"
	And the "Count" for the "2012" column is "(165)"
	And the score for "question 1" in the "2014" column is "91.4 (+4.3)"
	And the score for "question 1" in the "2013" column is "87.1 (-0.6)"
	And the score for "question 1" in the "2012" column is "87.7"
	And the Overall Average at the bottom for "2014" is "77.5 (+2.0)"
	And the Overall Average for "2013" is "75.5 (-1.0)"
	And the Overall Average for "2012" is "76.5"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(35)"
	And the "Count" for the "2013" column is "(143)"
	And the "Count" for the "2012" column is "(165)"
	And the score for the "Clarity of Direction" category for "2014" is "77.1 (-5.0)"
	And the score for the "Clarity of Direction" category for "2013" is "82.1 (-2.6)"
	And the score for the "Clarity of Direction" category for "2012" is "84.7"
	And the Overall Average at the bottom for "2014" is "77.5 (+2.0)"
	And the Overall Average for "2013" is "75.5 (-1.0)"
	And the Overall Average for "2012" is "76.5"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "91.4 (+4.3)"
	And the score for "question 1" in the "2013" column is "87.1 (-0.6)"
	And the score for "question 1" in the "2012" column is "87.7"
	And there is no score for question 1 in the 2011 column

	Now I go to "Functions" > "Logout"