Feature: Check filtered Favorability Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa5"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter" at the top right is "(35)"
	And the Favorable score for question 1 is "91.4%"
	And the Neutral score for question 1 is "8.6%"
	And there is no Unfavorable score for question 1
	And the Overall Average at the bottom has a Favorable score of "75.5%"
	And the Overall Average Neutral score is "14.8%"
	And the Overall Average Unfavorable score is "9.7%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "77.1%"
	And the Neutral score for the "Clarity of Direction" category is "13.6%"
	And the Unfavorable score for the "Clarity of Direction" category is "9.3%"
	And the Overall Average at the bottom has a Favorable score of "75.5%"
	And the Overall Average Neutral score is "14.8%"
	And the Overall Average Unfavorable score is "9.7%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score for question 1 is "91.4%"
	And the Neutral score for question 1 is "8.6%"
	And there is no Unfavorable score for question 1
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the number for "Filter" is "(35)"
	And the number for "Remainder" is "(10)"
	And the Favorable score on top for question 1 is "91.4%"
	And the Neutral score on top for question 1 is "8.6%"
	And there is no Unfavorable score on top for question 1
	And the Favorable score on bottom for question 1 is "80.0%"
	And the Neutral score on bottom for question 1 is "20.0%"
	And there is no Unfavorable score on bottom for question 1
	And the Overall Average at the bottom Favorable score on top is "75.5%"
	And the Overall Average Neutral score on top is "14.8%"
	And the Overall Average Unfavorable score on top is "9.7%"
	And the Overall Average Favorable score on bottom is "69.7%"
	And the Overall Average Neutral score on bottom is "18.0%"
	And the Overall Average Unfavorable score on bottom is "12.3%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the Favorable score on top for the "Clarity of Direction" category is "77.1%"
	And the Neutral score on top for the "Clarity of Direction" category is "13.6%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "9.3%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "77.5%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "15.0%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "7.5%"
	And the Overall Average at the bottom Favorable score on top is "75.5%"
	And the Overall Average Neutral score on top is "14.8%"
	And the Overall Average Unfavorable score on top is "9.7%"
	And the Overall Average Favorable score on bottom is "69.7%"
	And the Overall Average Neutral score on bottom is "18.0%"
	And the Overall Average Unfavorable score on bottom is "12.3%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score on top for question 1 is "91.4%"
	And the Neutral score on top for question 1 is "8.6%"
	And there is no Unfavorable score on top for question 1
	And the Favorable score on bottom for question 1 is "80.0%"
	And the Neutral score on bottom for question 1 is "20.0%"
	And there is no Unfavorable score on bottom for question 1
	
	Now I go to "Functions" > "Logout"
