from lettuce_env import *


def login_to_url(url, username, password):
    browser = get_browser()
    browser.visit(url)
    assert url in browser.url
    if not browser.find_link_by_partial_text('Logout'):
        for text in ['Username', 'Password']:
            assert browser.is_text_present(text)
        browser.fill('username', username)
        browser.fill('password', password)
        browser.find_by_name('Submit').click()


@step('I am logged into "([^"]*)" as user "([^"]*)"')
def login_to_perceptyx(step, url, username):
    password = username  # Password and username are same
    login_to_url(url, username, password)
    assert get_browser().is_text_present(
        'Click Here to Access Manager Toolkit')
    set_user(username)
    set_filtered(False)
    time.sleep(5)


@step('I click on the "([^"]*)" link under "([^"]*)"')
def click_link(step, link, menu_name):
    browser = get_browser()
    assert browser.find_link_by_text(
        menu_name), "%s menu is not found" % (menu_name)
    browser.find_link_by_text(menu_name).click()
    assert browser.find_link_by_partial_text(
        link), "%s link is not found" % (link)
    browser.find_link_by_partial_text(link).click()
    time.sleep(3)


@step('there is a "([^"]*)" tab in top left corner')
def verify_tab(step, tab_name):
    assert get_browser().find_link_by_text(tab_name)


@step('I am on the "([^"]*)" tab')
def when_i_am_on_the_group_tab(step, group):
    tabs = get_browser().find_link_by_partial_text(group)
    if group == 'Categories':
        set_tab_link(group)
    elif group == 'Questions':
        set_tab_link(group)
    set_clarity_of_direction_checked_value(False)
    for tab in tabs:
        if tab.text == group:
            tab.click()
    time.sleep(3)


@step('there is a "([^"]*)" tab in top right corner')
def verify_tab(step, tab_name):
    assert get_browser().find_link_by_partial_text(tab_name)


@step(u'Now I go to "([^"]*)" > "([^"]*)"')
def now_i_go_to_group1_group2(step, group1, group2):
    browser = get_browser()
    assert browser.find_link_by_text(
        group1), "%s menu is not found" % (group1)
    browser.find_link_by_text(group1).click()
    assert browser.find_link_by_partial_text(
        group2), "%s link is not found" % (group2)
    browser.find_link_by_partial_text(group2).click()


@step('I click the "([^"]*)" link')
def click_on_link(step, menu):
    browser = get_browser()
    assert browser.find_link_by_text(menu), "%s menu is not found" % (menu)
    browser.find_link_by_text(menu).click()


@step('the score for question 2 in the "([^"]*)" column is "([^"]*)"')
def verify_score_for_question_2_in_the_group(step, group, score):
    # import pdb;pdb.set_trace()
    browser = get_browser()
    questions_tab_row = browser.find_by_css(
        '.data.standalone.q_00_02 .value')
    categories_tab_row = browser.find_by_css('.data.sub.q_00_02 .value')
    if group == 'My View':
        if get_user() == "qa2" or get_user() == "qa3" or get_user() == "qa4" or get_user() == "qa5":
            score1_found = questions_tab_row[1].text
            score2_found = categories_tab_row[1].text
        else:
            score1_found = questions_tab_row.first.text
            score2_found = categories_tab_row.first.text
        assert score1_found == score or score2_found == score
    elif group == 'Mitchell':
        if get_user() == "qa1":
            score1_found = questions_tab_row.first.text
            score2_found = categories_tab_row.first.text
        elif get_user() == "qa2" or get_user() == "qa3" or get_user() == "qa4" or get_user() == "qa5":
            score1_found = questions_tab_row[2].text
            score2_found = categories_tab_row[2].text
        else:
            score1_found = questions_tab_row[1].text
            score2_found = categories_tab_row[1].text
        assert score1_found == score or score2_found == score
    elif group == 'My Filter':
        if get_user() == "qa2" or get_user() == "qa3" or get_user() == "qa4" or get_user() == "qa5":
            score1_found = questions_tab_row[0].text
            score2_found = categories_tab_row[0].text
        else:
            score1_found = questions_tab_row[1].text
            score2_found = categories_tab_row[1].text
        assert score1_found == score or score2_found == score
    elif group == "Perceptyx overall benchmark":
        if get_user() == "qa2" or get_user() == "qa3" or get_user() == "qa4" or get_user() == "qa5":
            score1_found = questions_tab_row[3].text
            score2_found = categories_tab_row[3].text
        else:
            score1_found = questions_tab_row[2].text
            # score2_found represents score at Categories tab
            score2_found = categories_tab_row[2].text
        assert score1_found == score or score2_found == score
    elif group == "Perceptyx software/technology benchmark":
        if get_user() == "qa2" or get_user() == "qa3" or get_user() == "qa4" or get_user() == "qa5":
            score1_found = questions_tab_row[4].text
            score2_found = categories_tab_row[4].text
        else:
            score1_found = questions_tab_row[3].text
            # score2_found represents score at Categories tab
            score2_found = categories_tab_row[3].text
        assert score1_found == score or score2_found == score

# @step('the score for question 2 in the "([^"]*)" column is "([^"]*)"')
# def verify_score_for_question_2_in_the_group(step, group, score):
#     # import pdb;pdb.set_trace()
#     browser = get_browser()
#     questions_tab_row = browser.find_by_css(
#         '.data.standalone.q_00_02 .value.centred')
#     categories_tab_row = browser.find_by_css('.data.sub.q_00_02 .value.centred')
#     if group == 'My View':
#         score1_found = questions_tab_row.first.text
#         score2_found = categories_tab_row.first.text
#         assert score1_found == score or score2_found == score
#     elif group == 'Mitchell':
#         # import pdb;pdb.set_trace()
#         if get_user() == "qa1":
#             score1_found = questions_tab_row.first.text
#             score2_found = categories_tab_row.first.text
#         else:
#             score1_found = questions_tab_row[1].text
#             score2_found = categories_tab_row[1].text
#         assert score1_found == score or score2_found == score
#     elif group == "Perceptyx overall benchmark":
#         if get_user() == 'qa1':
#             # score1_found represents score at Questions tab
#             score1_found = questions_tab_row[1].text
#             # score2_found represents score at Categories tab
#             score2_found = categories_tab_row[1].text
#         else:
#             score1_found = questions_tab_row[2].text
#             # score2_found represents score at Categories tab
#             score2_found = categories_tab_row[2].text
#         assert score1_found == score or score2_found == score
#     elif group == "Perceptyx software/technology benchmark":
#         if get_user() == 'qa1':
#             score1_found = questions_tab_row[2].text
#             score2_found = categories_tab_row[2].text
#         else:
#             score1_found = questions_tab_row[3].text
#             score2_found = categories_tab_row[3].text
#         assert score1_found == score or score2_found == score


def get_the_score_for_the_clarity_of_direction_category_in_the_group1_column(group1):
    if group1 == 'My View':
        if get_user() == 'qa3' or get_user() == 'qa4'or get_user() == 'qa5':
            if get_tab_link() != 'Categories':
                for i in range(0, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .data.standalone.average .%s' % (i, id_number)).first.text
                    except Exception, e:
                        continue
                    if data:
                        return data
            else:   
                for i in range(1, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .category .calc-value' % i)[1].text
                    except Exception, e:
                        continue
                    if data:
                        return data
        else:
            if get_tab_link() != 'Categories':
                for i in range(0, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .data.standalone.average .%s' % (i, id_number)).first.text
                    except Exception, e:
                        continue
                    if data:
                        return data
            else:   
                for i in range(0, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .category .calc-value' % i).first.text
                    except Exception, e:
                        continue
                    if data:
                        return data

    elif group1 == 'Mitchell':
        if get_user() == 'qa1':
            if get_tab_link() != 'Categories':
                for i in range(0, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .data.standalone.average .%s' % (i, id_number)).first.text
                    except Exception, e:
                        continue
                    if data:
                        return data
            else:
                for i in range(0, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .category .calc-value' % i).first.text
                    except Exception, e:
                        continue
                    if data:
                        return data
        elif get_user() == 'qa2' or get_user() == "qa3" or get_user() == 'qa4'or get_user() == 'qa5':
            if get_tab_link() != 'Categories':
                for i in range(1, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .data.standalone.average .%s' % (i, id_number))[2].text
                    except Exception, e:
                        continue
                    if data:
                        return data
            else:
                for i in range(1, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .category .calc-value' % i)[2].text
                    except Exception, e:
                        continue
                    if data:
                        return data
        else:
            if get_tab_link() != 'Categories':
                for i in range(0, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .data.standalone.average .%s' % (i, id_number))[1].text
                    except Exception, e:
                        continue
                    if data:
                        return data
            else:
                for i in range(1, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .category .calc-value' % i)[1].text
                    except Exception, e:
                        continue
                    if data:
                        return data
    elif group1 == 'My Filter':
        if get_user() == 'qa3' or get_user() == 'qa4'or get_user() == 'qa5':
            if get_tab_link() != 'Categories':
                for i in range(0, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .data.standalone.average .%s' % (i, id_number))[1].text
                    except Exception, e:
                        continue
                    if data:
                        return data
            else:
                for i in range(1, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .category .calc-value' % i)[0].text
                    except Exception, e:
                        continue
                    if data:
                        return data
        else:
            if get_tab_link() != 'Categories':
                for i in range(0, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .data.standalone.average .%s' % (i, id_number))[1].text
                    except Exception, e:
                        continue
                    if data:
                        return data
            else:
                for i in range(1, 30):
                    try:
                        data = get_browser().find_by_css(
                            '#DataTables_Table_%s .category .calc-value' % i)[1].text
                    except Exception, e:
                        continue
                    if data:
                        return data


@step(u'the score for the Clarity of Direction category in the "([^"]*)" column is "([^"]*)"')
def the_score_for_the_clarity_of_direction_category_in_the_female_column_is_group1(step, group1, group2):
    # import pdb;pdb.set_trace()
    value = get_the_score_for_the_clarity_of_direction_category_in_the_group1_column(
        group1)
    assert value == group2


@step(u'the number for Organization at the top right is "([^"]*)"')
def the_number_for_organization_at_the_top_right_is_group1(step, group1):
    elements = get_browser().find_by_css('.legend .description')
    value = elements[0].find_by_tag('span').first.text
    assert value == group1


# @step(u'I click on the "([^"]*)" category')
# def i_click_on_the_group1_category(step, group1):
#     # import pdb;pdb.set_trace()
#     elements = get_browser().find_by_css('.category .average .name')
#     for element in elements:
#         if element.text == 'Clarity of Direction':
#             element.click()
#             break
#     if not elements:
#         for i in range(1, 38):
#             try:
#                 elements = get_browser().find_by_css(
#                     '#DataTables_Table_%s .category .name' % (i))
#                 if elements[0].text:
#                     elements[0].click()
#                     set_clarity_of_direction_checked_value(True)
#                     break

#             except Exception, e:
#                 continue
#     else:
#         set_clarity_of_direction_checked_value(True)

@step(u'I click on the "([^"]*)" category')
def i_click_on_the_group1_category(step, group1):
    time.sleep(2)
    elements = get_browser().find_by_css('.category .average .name')
    for element in elements:
        if element.text == 'Clarity of Direction':
            element.click()
            break
    if not elements:
        elements=get_browser().find_by_css('.category .name')
        for element in elements:
            if element.text == 'Clarity of Direction':
                element.click()
                break
    if not elements:
        for i in range(1, 45):
            try:
                elements = get_browser().find_by_css(
                    '#DataTables_Table_%s .category .name' % (i))
                if elements[0].text:
                    elements[0].click()
                    set_clarity_of_direction_checked_value(True)
                    break

            except Exception, e:
                continue
    else:
        set_clarity_of_direction_checked_value(True)



@step(u'the number for "([^"]*)" is "([^"]*)"')
def the_number_for_group1_is_group2(step, group1, group2):
    if group1 == 'Your View':
        elements = get_browser().find_by_css('.legend .bar .description')
        assert elements[2].find_by_tag('span').first.text == group2
    elif group1 == 'Organization':
        elements = get_browser().find_by_css('.legend .sub-bar .description')
        if elements:
            assert elements[0].find_by_tag('span').first.text == group2
        else:
            elements = get_browser().find_by_css('.legend .description')
            if get_user() == "qa3" or get_user() == 'qa4' or get_user() == 'qa5':
                assert elements[3].find_by_tag('span').first.text == group2
            else:
                if elements[1].text:
                    assert elements[1].find_by_tag('span').first.text == group2
                else:
                    assert elements[3].find_by_tag('span').first.text == group2
    elif group1 == 'Filter':
        # import pdb;pdb.set_trace()
        time.sleep(2)
        elements = get_browser().find_by_css('.legend .description')
        if get_user() == "qa3" or get_user() == 'qa4' or get_user() == 'qa5':
            assert elements[2].find_by_tag('span').first.text == group2
        else:
            if elements[0].text:
                assert elements[0].find_by_tag('span').first.text == group2
            else:
                assert elements[2].find_by_tag('span').first.text == group2


@step(u'the number for "([^"]*)" in the upper right is "([^"]*)"')
def the_number_for_group1_in_the_upper_right_is_group2(step, group1, group2):
    elements = get_browser().find_by_css('.legend .description')
    if group1 == 'Your view':
        assert elements[0].find_by_tag('span').first.text == group2
    elif group1 == 'Organization':
        if get_user() == 'qa2' or get_user()=='qa4' or get_user() == 'qa5':
            assert elements[3].find_by_tag('span').first.text == group2
        else:
            assert elements[1].find_by_tag('span').first.text == group2
    elif group1 == 'Filter':
        if get_user() == 'qa2' or get_user() == 'qa3' or get_user() == 'qa4' or get_user() == 'qa5':
            assert elements[2].find_by_tag('span').first.text == group2
        else:
            if elements[1].text:
                assert elements[1].find_by_tag('span').first.text == group2
            else:
                assert elements[2].find_by_tag('span').first.text == group2

@step('I click on "([^"]*)" > "([^"]*)"')
def click_on_link_filter(step, menu_name, link):
    time.sleep(2)
    browser = get_browser()
    assert browser.find_link_by_text(
        "Filter"), "%s menu is not found" % ("Filter")
    browser.find_link_by_text("Filter").click()
    assert browser.find_link_by_partial_text(
        link), "%s link is not found" % (link)
    browser.find_link_by_partial_text(link).click()

    set_filtered(True)
    time.sleep(2)


@step('select "([^"]*)" from the Select a Demographic: popdown')
def select_group1_from_the_Select_a_Demographic_popdown(step, group1):
    time.sleep(2)
    browser = get_browser()
    # assert browser.is_text_present(group2)
    elements = browser.find_by_name('q').find_by_tag("option")
    for i in range(1, 18):
        if elements[i].text == group1:
            elements[i].click()
            break
    time.sleep(2)


@step('I click "Submit Filter"')
def click_submit_filter(step):
    get_browser().find_by_css('.ui-dialog-buttonset .submit_btn').first.click()
    time.sleep(2)


@step(u'I select "([^"]*)" from the "([^"]*)" popdown')
def select_group1_from_the_group2_popdown_in_the_top_right(step, group1, group2):
    if group2 == "Demographic Selection":
        element = get_browser().find_by_name('resp_select').find_by_tag("option")
        if group1 == 'Gender':
            element[16].click()
        elif group1 == 'Business Group':
            element[3].click()
    else:
        element = get_browser().find_by_name('compared_to').find_by_tag("option")
        if element[0].text == group1:
            element[0].click()
        elif element[1].text == group1:
            element[1].click()
        elif element[2].text == group1:
            element[2].click()
        elif element[3].text == group1:
            element[3].click()
    time.sleep(3)


@step('select "([^"]*)" from the "Select Your Option" popdown')
def select_group1_from_the_Select_Your_Option_popdown(step, group1):
    browser = get_browser()
    # import pdb;pdb.set_trace()
    elements = browser.find_by_name('q0')
    if browser.find_by_name('q')[0].value == '01_03_07':
        elements[1].click()
    elif get_user() == 'qa3' or get_user() == 'qa4':
        elements[1].click()
    elif get_user() == 'qa5':
        elements[0].click()
    elif browser.find_by_name('q')[0].value =='01_07_07':
        elements[3].click()
    else:
        elements[16].click()
    time.sleep(3)

@step('the page finishes loading')
def the_page_finishes_loading(step):
    time.sleep(1)
    for i in range(1, 5):
        if get_browser().is_text_present("Filter:") == True:
            break
        time.sleep(1)
    time.sleep(2)


@step(u'select "([^"]*)" from the "Compared to:" popdown')
def select_group1_from_the_group2_popdown(step, group1):
    elements = get_browser().find_by_name('compared_to')
    # import pdb;pdb.set_trace()
    if group1 == 'Organization':
        if get_user() == 'qa2' :
            elements[0].find_by_tag('option')[2].click()
        elif get_user() == 'qa3' or get_user() == 'qa4' or get_user() == 'qa5':
            elements[0].find_by_tag('option')[0].click()
        else:
            elements[0].find_by_tag('option')[1].click()
    else:
        elements[0].find_by_tag('option')[2].click()