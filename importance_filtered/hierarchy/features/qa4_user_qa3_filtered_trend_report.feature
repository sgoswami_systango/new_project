Feature: Check filtered Trend Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(106)"
	And the "Count" for the "2013" column is "(99)"
	And the "Count" for the "2012" column is "(98)"
	And the score for "question 1" in the "2014" column is "4.12 (-0.10)"
	And the score for "question 1" in the "2013" column is "4.22 (+0.16)"
	And the score for "question 1" in the "2012" column is "4.06"
	And the Overall Average at the bottom for "2014" is "4.04 (-0.03)"
	And the Overall Average for "2013" is "4.07 (+0.04)"
	And the Overall Average for "2012" is "4.03"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(106)"
	And the "Count" for the "2013" column is "(99)"
	And the "Count" for the "2012" column is "(98)"
	And the score for the "Clarity of Direction" category for "2014" is "3.96 (-0.14)"
	And the score for the "Clarity of Direction" category for "2013" is "4.10 (+0.12)"
	And the score for the "Clarity of Direction" category for "2012" is "3.98"
	And the Overall Average at the bottom for "2014" is "4.04 (-0.03)"
	And the Overall Average for "2013" is "4.07 (+0.04)"
	And the Overall Average for "2012" is "4.03"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.12 (-0.10)"
	And the score for "question 1" in the "2013" column is "4.22 (+0.16)"
	And the score for "question 1" in the "2012" column is "4.06"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(106)"
	And the "Count" for the "2013" column is "(99)"
	And the "Count" for the "2012" column is "(98)"
	And the score for "question 1" in the "2014" column is "85.7 (-2.2)"
	And the score for "question 1" in the "2013" column is "87.9 (+3.4)"
	And the score for "question 1" in the "2012" column is "84.5"
	And the Overall Average at the bottom for "2014" is "79.6 (-1.4)"
	And the Overall Average for "2013" is "81.0 (+0.9)"
	And the Overall Average for "2012" is "80.1"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(106)"
	And the "Count" for the "2013" column is "(99)"
	And the "Count" for the "2012" column is "(98)"
	And the score for the "Clarity of Direction" category for "2014" is "79.5 (-4.8)"
	And the score for the "Clarity of Direction" category for "2013" is "84.3 (+4.1)"
	And the score for the "Clarity of Direction" category for "2012" is "80.2"
	And the Overall Average at the bottom for "2014" is "79.6 (-1.4)"
	And the Overall Average for "2013" is "81.0 (+0.9)"
	And the Overall Average for "2012" is "80.1"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "85.7 (-2.2)"
	And the score for "question 1" in the "2013" column is "87.9 (+3.4)"
	And the score for "question 1" in the "2012" column is "84.5"

	Now I go to "Functions" > "Logout"