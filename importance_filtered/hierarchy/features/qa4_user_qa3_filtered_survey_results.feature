Feature: Check filtered Survey Results for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click on the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "1"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.0%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "3"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "2.9%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "11"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "10.5%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "57"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "54.3%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "33"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "31.4%"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 is "4.12 (±0.78)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Number" column is "105"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Percent" column is "100.0%"
	And the figure under question 1 for "1. Not Important" in the "Number" column is "1"
	And the figure under question 1 for "1. Not Important" in the "Percent" column is "1.0%"
	And the figure under question 1 for "2. Little Importance" in the "Number" column is "8"
	And the figure under question 1 for "2. Little Importance" in the "Percent" column is "7.6%"
	And the figure under question 1 for "3. Somewhat Important" in the "Number" column is "11"
	And the figure under question 1 for "3. Somewhat Important" in the "Percent" column is "10.5%"
	And the figure under question 1 for "4. Very Important" in the "Number" column is "57"
	And the figure under question 1 for "4. Very Important" in the "Percent" column is "54.3%"
	And the figure under question 1 for "5. Extremely Important" in the "Number" column is "28"
	And the figure under question 1 for "5. Extremely Important" in the "Percent" column is "26.7%"
	And the "Imp Mean" under question 1 is "3.98 (±0.87)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Number" column is "105"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Percent" column is "100.0%"


	When I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the number for "Filter" is "(106)"
	And the number for "Organization" is "(1136)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "1(11)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.0%(1.0%)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Segment Density" column is "9.1%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "3(40)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "2.9%(3.6%)"
	And the figure under question 1 for "2. Disagree" in the "Segment Density" column is "7.5%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "11(112)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "10.5%(10.1%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Segment Density" column is "9.8%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "57(596)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "54.3%(53.5%)"
	And the figure under question 1 for "4. Agree" in the "Segment Density" column is "9.6%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "33(355)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "31.4%(31.9%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Segment Density" column is "9.3%"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 is "4.12 (±0.78)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Number" column is "105(1114)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Percent" column is "100.0%(100.0%)"
	And the figure under question 1 for "1. Not Important" in the "Number" column is "1(3)"
	And the figure under question 1 for "1. Not Important" in the "Percent" column is "1.0%(0.3%)"
	And the figure under question 1 for "1. Not Important" in the "Segment Density" column is "33.3%"
	And the figure under question 1 for "2. Little Importance" in the "Number" column is "8(28)"
	And the figure under question 1 for "2. Little Importance" in the "Percent" column is "7.6%(2.5%)"
	And the figure under question 1 for "2. Little Importance" in the "Segment Density" column is "28.6%"
	And the figure under question 1 for "3. Somewhat Important" in the "Number" column is "11(166)"
	And the figure under question 1 for "3. Somewhat Important" in the "Percent" column is "10.5%(14.9%)"
	And the figure under question 1 for "3. Somewhat Important" in the "Segment Density" column is "6.6%"
	And the figure under question 1 for "4. Very Important" in the "Number" column is "57(575)"
	And the figure under question 1 for "4. Very Important" in the "Percent" column is "54.3%(51.6%)"
	And the figure under question 1 for "4. Very Important" in the "Segment Density" column is "9.9%"
	And the figure under question 1 for "5. Extremely Important" in the "Number" column is "28(342)"
	And the figure under question 1 for "5. Extremely Important" in the "Percent" column is "26.7%(30.7%)"
	And the figure under question 1 for "5. Extremely Important" in the "Segment Density" column is "8.2%"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 is "3.98 (±0.87)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Number" column is "105(1114)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"