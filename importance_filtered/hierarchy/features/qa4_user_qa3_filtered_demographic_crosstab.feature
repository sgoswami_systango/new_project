Feature: Check filtered Demographic Crosstab for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(82)"
	And the first number under "Female" is "(24)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.3"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.0"
	And the Group Average at the bottom for the "Female" column is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.06"
	And the score for question 1 in the "Female" column is "4.33"
	And the Weighted Average on the right for question 1 is "4.12"
	And the Group Average at the bottom for the "Male" column is "3.96"
	And the Group Average at the bottom for the "Female" column is "4.16"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81"
	And the score for question 1 in the "Female" column is "100"
	And the Weighted Average on the right for question 1 is "86"
	And the Group Average at the bottom for the "Male" column is "76"
	And the Group Average at the bottom for the "Female" column is "86"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81.5"
	And the score for question 1 in the "Female" column is "100.0"
	And the Weighted Average on the right for question 1 is "85.7"
	And the Group Average at the bottom for the "Male" column is "76.3"
	And the Group Average at the bottom for the "Female" column is "86.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81.48"
	And the score for question 1 in the "Female" column is "100.00"
	And the Weighted Average on the right for question 1 is "85.71"
	And the Group Average at the bottom for the "Male" column is "76.30"
	And the Group Average at the bottom for the "Female" column is "86.31"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5"
	And the score for question 1 in the "Female" column is "0"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.9"
	And the score for question 1 in the "Female" column is "0.0"
	And the Weighted Average on the right for question 1 is "3.8"
	And the Group Average at the bottom for the "Male" column is "9.3"
	And the Group Average at the bottom for the "Female" column is "4.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.94"
	And the score for question 1 in the "Female" column is "0.00"
	And the Weighted Average on the right for question 1 is "3.81"
	And the Group Average at the bottom for the "Male" column is "9.31"
	And the Group Average at the bottom for the "Female" column is "4.64"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.0"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.95"
	And the score for question 1 in the "Female" column is "4.08"
	And the Weighted Average on the right for question 1 is "3.98"
	And the Group Average at the bottom for the "Male" column is "4.33"
	And the Group Average at the bottom for the "Female" column is "4.38"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80"
	And the score for question 1 in the "Female" column is "83"
	And the Weighted Average on the right for question 1 is "81"
	And the Group Average at the bottom for the "Male" column is "91"
	And the Group Average at the bottom for the "Female" column is "92"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80.2"
	And the score for question 1 in the "Female" column is "83.3"
	And the Weighted Average on the right for question 1 is "81.0"
	And the Group Average at the bottom for the "Male" column is "90.9"
	And the Group Average at the bottom for the "Female" column is "91.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80.25"
	And the score for question 1 in the "Female" column is "83.33"
	And the Weighted Average on the right for question 1 is "80.95"
	And the Group Average at the bottom for the "Male" column is "90.90"
	And the Group Average at the bottom for the "Female" column is "91.55"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2"
	And the score for question 1 in the "Female" column is "-5"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "8"
	And the Group Average at the bottom for the "Female" column is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2.2"
	And the score for question 1 in the "Female" column is "-5.1"
	And the Weighted Average on the right for question 1 is "-2.8"
	And the Group Average at the bottom for the "Male" column is "8.0"
	And the Group Average at the bottom for the "Female" column is "4.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2.17"
	And the score for question 1 in the "Female" column is "-5.10"
	And the Weighted Average on the right for question 1 is "-2.80"
	And the Group Average at the bottom for the "Male" column is "8.01"
	And the Group Average at the bottom for the "Female" column is "4.82"


# Categories
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(82)"
	And the number directly below "Female" is "(24)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.0"
	And the Group Average at the bottom for the "Female" column is "4.2"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.3"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.92"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.07"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.96"
	And the Group Average at the bottom for the "Male" column is "3.96"
	And the Group Average at the bottom for the "Female" column is "4.16"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.06"
	And the score for question 1 in the "Female" column is "4.33"
	And the Weighted Average on the right for question 1 is "4.12"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "77"
	And the score for the "Clarity of Direction" category in the "Female" column is "89"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80"
	And the Group Average at the bottom for the "Male" column is "76"
	And the Group Average at the bottom for the "Female" column is "86"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81"
	And the score for question 1 in the "Female" column is "100"
	And the Weighted Average on the right for question 1 is "86"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76.9"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79.5"
	And the Group Average at the bottom for the "Male" column is "76.3"
	And the Group Average at the bottom for the "Female" column is "86.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81.5"
	And the score for question 1 in the "Female" column is "100.0"
	And the Weighted Average on the right for question 1 is "85.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76.85"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.54"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79.52"
	And the Group Average at the bottom for the "Male" column is "76.30"
	And the Group Average at the bottom for the "Female" column is "86.31"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81.48"
	And the score for question 1 in the "Female" column is "100.00"
	And the Weighted Average on the right for question 1 is "85.71"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "10"
	And the score for the "Clarity of Direction" category in the "Female" column is "7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "5"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5"
	And the score for question 1 in the "Female" column is "0"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.6"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.3"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9.0"
	And the Group Average at the bottom for the "Male" column is "9.3"
	And the Group Average at the bottom for the "Female" column is "4.6"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.9"
	And the score for question 1 in the "Female" column is "0.0"
	And the Weighted Average on the right for question 1 is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.57"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.29"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9.05"
	And the Group Average at the bottom for the "Male" column is "9.31"
	And the Group Average at the bottom for the "Female" column is "4.64"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.94"
	And the score for question 1 in the "Female" column is "0.00"
	And the Weighted Average on the right for question 1 is "3.81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.2"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.3"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.0"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.20"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.31"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.22"
	And the Group Average at the bottom for the "Male" column is "4.33"
	And the Group Average at the bottom for the "Female" column is "4.38"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.95"
	And the score for question 1 in the "Female" column is "4.08"
	And the Weighted Average on the right for question 1 is "3.98"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86"
	And the score for the "Clarity of Direction" category in the "Female" column is "91"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87"
	And the Group Average at the bottom for the "Male" column is "91"
	And the Group Average at the bottom for the "Female" column is "92"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80"
	And the score for question 1 in the "Female" column is "83"
	And the Weighted Average on the right for question 1 is "81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.4"
	And the score for the "Clarity of Direction" category in the "Female" column is "90.6"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.4"
	And the Group Average at the bottom for the "Male" column is "90.9"
	And the Group Average at the bottom for the "Female" column is "91.5"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80.2"
	And the score for question 1 in the "Female" column is "83.3"
	And the Weighted Average on the right for question 1 is "81.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.42"
	And the score for the "Clarity of Direction" category in the "Female" column is "90.62"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.38"
	And the Group Average at the bottom for the "Male" column is "90.90"
	And the Group Average at the bottom for the "Female" column is "91.55"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80.25"
	And the score for question 1 in the "Female" column is "83.33"
	And the Weighted Average on the right for question 1 is "80.95"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "6"
	And the score for the "Clarity of Direction" category in the "Female" column is "5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5"
	And the Group Average at the bottom for the "Male" column is "8"
	And the Group Average at the bottom for the "Female" column is "5"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2"
	And the score for question 1 in the "Female" column is "-5"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.9"
	And the score for the "Clarity of Direction" category in the "Female" column is "5.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.9"
	And the Group Average at the bottom for the "Male" column is "8.0"
	And the Group Average at the bottom for the "Female" column is "4.8"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2.2"
	And the score for question 1 in the "Female" column is "-5.1"
	And the Weighted Average on the right for question 1 is "-2.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.88"
	And the score for the "Clarity of Direction" category in the "Female" column is "5.17"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.85"
	And the Group Average at the bottom for the "Male" column is "8.01"
	And the Group Average at the bottom for the "Female" column is "4.82"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2.17"
	And the score for question 1 in the "Female" column is "-5.10"
	And the Weighted Average on the right for question 1 is "-2.80"



# Questions + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1 (4.2)"
	And the score for question 1 in the "Female" column is "4.3 (4.2)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.0 (4.1)"
	And the Group Average at the bottom for the "Female" column is "4.2 (4.1)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.06 (4.24)"
	And the score for question 1 in the "Female" column is "4.33 (4.17)"
	And the Weighted Average on the right for question 1 is "4.12"
	And the Group Average at the bottom for the "Male" column is "3.96 (4.06)"
	And the Group Average at the bottom for the "Female" column is "4.16 (4.12)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81 (88)"
	And the score for question 1 in the "Female" column is "100 (88)"
	And the Weighted Average on the right for question 1 is "86"
	And the Group Average at the bottom for the "Male" column is "76 (80)"
	And the Group Average at the bottom for the "Female" column is "86 (85)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81.5 (88.0)"
	And the score for question 1 in the "Female" column is "100.0 (87.5)"
	And the Weighted Average on the right for question 1 is "85.7"
	And the Group Average at the bottom for the "Male" column is "76.3 (79.6)"
	And the Group Average at the bottom for the "Female" column is "86.3 (85.5)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81.48 (88.00)"
	And the score for question 1 in the "Female" column is "100.00 (87.50)"
	And the Weighted Average on the right for question 1 is "85.71"
	And the Group Average at the bottom for the "Male" column is "76.30 (79.61)"
	And the Group Average at the bottom for the "Female" column is "86.31 (85.48)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5 (3)"
	And the score for question 1 in the "Female" column is "0 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "9 (7)"
	And the Group Average at the bottom for the "Female" column is "5 (5)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.9 (2.7)"
	And the score for question 1 in the "Female" column is "0.0 (4.2)"
	And the Weighted Average on the right for question 1 is "3.8"
	And the Group Average at the bottom for the "Male" column is "9.3 (6.8)"
	And the Group Average at the bottom for the "Female" column is "4.6 (4.6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.94 (2.67)"
	And the score for question 1 in the "Female" column is "0.00 (4.17)"
	And the Weighted Average on the right for question 1 is "3.81"
	And the Group Average at the bottom for the "Male" column is "9.31 (6.80)"
	And the Group Average at the bottom for the "Female" column is "4.64 (4.57)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.0 (3.9)"
	And the score for question 1 in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.4 (4.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.95 (3.95)"
	And the score for question 1 in the "Female" column is "4.08 (4.08)"
	And the Weighted Average on the right for question 1 is "3.98"
	And the Group Average at the bottom for the "Male" column is "4.33 (4.31)"
	And the Group Average at the bottom for the "Female" column is "4.38 (4.30)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80 (76)"
	And the score for question 1 in the "Female" column is "83 (88)"
	And the Weighted Average on the right for question 1 is "81"
	And the Group Average at the bottom for the "Male" column is "91 (90)"
	And the Group Average at the bottom for the "Female" column is "92 (90)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80.2 (76.0)"
	And the score for question 1 in the "Female" column is "83.3 (87.5)"
	And the Weighted Average on the right for question 1 is "81.0"
	And the Group Average at the bottom for the "Male" column is "90.9 (90.2)"
	And the Group Average at the bottom for the "Female" column is "91.5 (89.5)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80.25 (76.00)"
	And the score for question 1 in the "Female" column is "83.33 (87.50)"
	And the Weighted Average on the right for question 1 is "80.95"
	And the Group Average at the bottom for the "Male" column is "90.90 (90.17)"
	And the Group Average at the bottom for the "Female" column is "91.55 (89.52)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2 (-6)"
	And the score for question 1 in the "Female" column is "-5 (-2)"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "8 (5)"
	And the Group Average at the bottom for the "Female" column is "5 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2.2 (-5.7)"
	And the score for question 1 in the "Female" column is "-5.1 (-1.8)"
	And the Weighted Average on the right for question 1 is "-2.8"
	And the Group Average at the bottom for the "Male" column is "8.0 (5.4)"
	And the Group Average at the bottom for the "Female" column is "4.8 (3.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2.17 (-5.73)"
	And the score for question 1 in the "Female" column is "-5.10 (-1.84)"
	And the Weighted Average on the right for question 1 is "-2.80"
	And the Group Average at the bottom for the "Male" column is "8.01 (5.39)"
	And the Group Average at the bottom for the "Female" column is "4.82 (3.87)"


# Categories + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9 (4.1)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.0 (4.1)"
	And the Group Average at the bottom for the "Female" column is "4.2 (4.1)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1 (4.2)"
	And the score for question 1 in the "Female" column is "4.3 (4.2)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.92 (4.09)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.07 (4.11)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.96"
	And the Group Average at the bottom for the "Male" column is "3.96 (4.06)"
	And the Group Average at the bottom for the "Female" column is "4.16 (4.12)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.06 (4.24)"
	And the score for question 1 in the "Female" column is "4.33 (4.17)"
	And the Weighted Average on the right for question 1 is "4.12"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "77 (83)"
	And the score for the "Clarity of Direction" category in the "Female" column is "89 (88)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80"
	And the Group Average at the bottom for the "Male" column is "76 (80)"
	And the Group Average at the bottom for the "Female" column is "86 (85)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81 (88)"
	And the score for question 1 in the "Female" column is "100 (88)"
	And the Weighted Average on the right for question 1 is "86"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76.9 (83.3)"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.5 (87.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79.5"
	And the Group Average at the bottom for the "Male" column is "76.3 (79.6)"
	And the Group Average at the bottom for the "Female" column is "86.3 (85.5)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81.5 (88.0)"
	And the score for question 1 in the "Female" column is "100.0 (87.5)"
	And the Weighted Average on the right for question 1 is "85.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76.85 (83.33)"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.54 (87.50)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79.52"
	And the Group Average at the bottom for the "Male" column is "76.30 (79.61)"
	And the Group Average at the bottom for the "Female" column is "86.31 (85.48)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81.48 (88.00)"
	And the score for question 1 in the "Female" column is "100.00 (87.50)"
	And the Weighted Average on the right for question 1 is "85.71"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "10 (6)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7 (3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Male" column is "9 (7)"
	And the Group Average at the bottom for the "Female" column is "5 (5)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5 (3)"
	And the score for question 1 in the "Female" column is "0 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.6 (5.7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.3 (3.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9.0"
	And the Group Average at the bottom for the "Male" column is "9.3 (6.8)"
	And the Group Average at the bottom for the "Female" column is "4.6 (4.6)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.9 (2.7)"
	And the score for question 1 in the "Female" column is "0.0 (4.2)"
	And the Weighted Average on the right for question 1 is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.57 (5.67)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.29 (3.12)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9.05"
	And the Group Average at the bottom for the "Male" column is "9.31 (6.80)"
	And the Group Average at the bottom for the "Female" column is "4.64 (4.57)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.94 (2.67)"
	And the score for question 1 in the "Female" column is "0.00 (4.17)"
	And the Weighted Average on the right for question 1 is "3.81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.2 (4.1)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.3 (4.2)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.4 (4.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.0 (3.9)"
	And the score for question 1 in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.20 (4.15)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.31 (4.23)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.22"
	And the Group Average at the bottom for the "Male" column is "4.33 (4.31)"
	And the Group Average at the bottom for the "Female" column is "4.38 (4.30)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.95 (3.95)"
	And the score for question 1 in the "Female" column is "4.08 (4.08)"
	And the Weighted Average on the right for question 1 is "3.98"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86 (82)"
	And the score for the "Clarity of Direction" category in the "Female" column is "91 (93)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87"
	And the Group Average at the bottom for the "Male" column is "91 (90)"
	And the Group Average at the bottom for the "Female" column is "92 (90)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80 (76)"
	And the score for question 1 in the "Female" column is "83 (88)"
	And the Weighted Average on the right for question 1 is "81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.4 (82.3)"
	And the score for the "Clarity of Direction" category in the "Female" column is "90.6 (92.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.4"
	And the Group Average at the bottom for the "Male" column is "90.9 (90.2)"
	And the Group Average at the bottom for the "Female" column is "91.5 (89.5)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80.2 (76.0)"
	And the score for question 1 in the "Female" column is "83.3 (87.5)"
	And the Weighted Average on the right for question 1 is "81.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.42 (82.33)"
	And the score for the "Clarity of Direction" category in the "Female" column is "90.62 (92.71)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.38"
	And the Group Average at the bottom for the "Male" column is "90.90 (90.17)"
	And the Group Average at the bottom for the "Female" column is "91.55 (89.52)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80.25 (76.00)"
	And the score for question 1 in the "Female" column is "83.33 (87.50)"
	And the Weighted Average on the right for question 1 is "80.95"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "6 (1)"
	And the score for the "Clarity of Direction" category in the "Female" column is "5 (2)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5"
	And the Group Average at the bottom for the "Male" column is "8 (5)"
	And the Group Average at the bottom for the "Female" column is "5 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2 (-6)"
	And the score for question 1 in the "Female" column is "-5 (-2)"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.9 (1.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "5.2 (2.4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.9"
	And the Group Average at the bottom for the "Male" column is "8.0 (5.4)"
	And the Group Average at the bottom for the "Female" column is "4.8 (3.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2.2 (-5.7)"
	And the score for question 1 in the "Female" column is "-5.1 (-1.8)"
	And the Weighted Average on the right for question 1 is "-2.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.88 (1.25)"
	And the score for the "Clarity of Direction" category in the "Female" column is "5.17 (2.54)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.85"
	And the Group Average at the bottom for the "Male" column is "8.01 (5.39)"
	And the Group Average at the bottom for the "Female" column is "4.82 (3.87)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2.17 (-5.73)"
	And the score for question 1 in the "Female" column is "-5.10 (-1.84)"
	And the Weighted Average on the right for question 1 is "-2.80"


	Now I go to "Functions" > "Logout"