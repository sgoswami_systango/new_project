Feature: Check filtered Gap Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the figure for "Filter Count:" in the upper right is "106"
	And the Eval Avg for question 1 is "4.12"
	And the Importance for question 1 is "3.98"
	And the Gap for question 1 is "-0.14"
	And the Weighted Gap for question 1 is "-2.79"
	And the Overall Average at the bottom has an Eval Avg of "4.01"
	And the Overall Average has a Importance of "4.34"
	And the Overall Average has a Gap of "0.33"
	And the Overall Average has a Weighted Gap of "7.16"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.96"
	And the Importance for the "Clarity of Direction" category is "4.22"
	And the Gap for the "Clarity of Direction" category is "0.26"
	And the Weighted Gap for the "Clarity of Direction" category is "5.49"
	And the Overall Average at the bottom has an Eval Avg of "4.01"
	And the Overall Average has a Importance of "4.34"
	And the Overall Average has a Gap of "0.33"
	And the Overall Average has a Weighted Gap of "7.16"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.12"
	And the Importance for question 1 is "3.98"
	And the Gap for question 1 is "-0.14"
	And the Weighted Gap for question 1 is "-2.79"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the number for "Filter" in the upper right is "(106)"
	And the number for "Remainder" in the upper right is "(13)"
	And the Eval Avg for question 1 is "4.12 (-0.11)"
	And the Importance for question 1 is "3.98 (-0.17)"
	And the Gap for question 1 is "-0.14 (-0.06)"
	And the Weighted Gap for question 1 is "-2.79 (-1.13)"
	And the Overall Average at the bottom has an Eval Avg of "4.01 (-0.16)"
	And the Overall Average has a Importance of "4.34 (+0.04)"
	And the Overall Average has a Gap of "0.33 (+0.20)"
	And the Overall Average has a Weighted Gap of "7.16 (+4.37)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the Eval Avg for the "Clarity of Direction" category is "3.96 (0.00)"
	And the Importance for the "Clarity of Direction" category is "4.22 (-0.04)"
	And the Gap for the "Clarity of Direction" category is "0.26 (-0.04)"
	And the Weighted Gap for the "Clarity of Direction" category is "5.49 (-0.90)"
	And the Overall Average at the bottom has an Eval Avg of "4.01 (+0.06)"
	And the Overall Average has a Importance of "4.34 (+0.01)"
	And the Overall Average has a Gap of "0.33 (-0.05)"
	And the Overall Average has a Weighted Gap of "7.16 (-1.07)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.12 (0.00)"
	And the Importance for question 1 is "3.98 (-0.12)"
	And the Gap for question 1 is "-0.14 (-0.12)"
	And the Weighted Gap for question 1 is "-2.79 (-2.38)"
	
	Now I go to "Functions" > "Logout"

