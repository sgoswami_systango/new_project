Feature: Check filtered Favorability Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Your view" at the top right is "(106)"
	And the Favorable score for question 1 is "85.7%"
	And the Neutral score for question 1 is "10.5%"
	And the Unfavorable score for question 1 is "3.8%"
	And the Overall Average at the bottom has a Favorable score of "78.6%"
	And the Overall Average Neutral score is "13.2%"
	And the Overall Average Unfavorable score is "8.2%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "79.5%"
	And the Neutral score for the "Clarity of Direction" category is "11.4%"
	And the Unfavorable score for the "Clarity of Direction" category is "9.0%"
	And the Overall Average at the bottom has a Favorable score of "78.6%"
	And the Overall Average Neutral score is "13.2%"
	And the Overall Average Unfavorable score is "8.2%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score for question 1 is "85.7%"
	And the Neutral score for question 1 is "10.5%"
	And the Unfavorable score for question 1 is "3.8%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the number for "Filter" is "(106)"
	And the number for "Remainder" is "(13)"
	And the Favorable score on top for question 1 is "85.7%"
	And the Neutral score on top for question 1 is "10.5%"
	And the Unfavorable score on top for question 1 is "3.8%"
	And the Favorable score on bottom for question 1 is "92.3%"
	And the Unfavorable score on bottom for question 1 is "7.7%"
	And the Overall Average at the bottom Favorable score on top is "78.6%"
	And the Overall Average Neutral score on top is "13.2%"
	And the Overall Average Unfavorable score on top is "8.2%"
	And the Overall Average Favorable score on bottom is "85.9%"
	And the Overall Average Neutral score on bottom is "11.4%"
	And the Overall Average Unfavorable score on bottom is "2.6%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the Favorable score on top for the "Clarity of Direction" category is "79.5%"
	And the Neutral score on top for the "Clarity of Direction" category is "11.4%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "9.0%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "88.5%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "9.6%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "1.9%"
	And the Overall Average at the bottom Favorable score on top is "78.6%"
	And the Overall Average Neutral score on top is "13.2%"
	And the Overall Average Unfavorable score on top is "8.2%"
	And the Overall Average Favorable score on bottom is "85.9%"
	And the Overall Average Neutral score on bottom is "11.4%"
	And the Overall Average Unfavorable score on bottom is "2.6%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score on top for question 1 is "85.7%"
	And the Neutral score on top for question 1 is "10.5%"
	And the Unfavorable score on top for question 1 is "3.8%"
	And the Favorable score on bottom for question 1 is "92.3%"
	And the Unfavorable score on bottom for question 1 is "7.7%"
	
	Now I go to "Functions" > "Logout"
