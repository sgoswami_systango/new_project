Feature: Check filtered Demographic Crosstab for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(25)"
	And the first number under "Female" is "(40)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.2"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "3.9"
	And the Group Average at the bottom for the "Female" column is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.20"
	And the score for question 1 in the "Female" column is "4.00"
	And the Weighted Average on the right for question 1 is "4.08"
	And the Group Average at the bottom for the "Male" column is "3.93"
	And the Group Average at the bottom for the "Female" column is "3.81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96"
	And the score for question 1 in the "Female" column is "82"
	And the Weighted Average on the right for question 1 is "88"
	And the Group Average at the bottom for the "Male" column is "77"
	And the Group Average at the bottom for the "Female" column is "71"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96.0"
	And the score for question 1 in the "Female" column is "82.5"
	And the Weighted Average on the right for question 1 is "87.7"
	And the Group Average at the bottom for the "Male" column is "77.3"
	And the Group Average at the bottom for the "Female" column is "71.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96.00"
	And the score for question 1 in the "Female" column is "82.50"
	And the Weighted Average on the right for question 1 is "87.69"
	And the Group Average at the bottom for the "Male" column is "77.26"
	And the Group Average at the bottom for the "Female" column is "71.29"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "2"
	And the Weighted Average on the right for question 1 is "2"
	And the Group Average at the bottom for the "Male" column is "7"
	And the Group Average at the bottom for the "Female" column is "10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "2.5"
	And the Weighted Average on the right for question 1 is "1.5"
	And the Group Average at the bottom for the "Male" column is "7.2"
	And the Group Average at the bottom for the "Female" column is "10.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "2.50"
	And the Weighted Average on the right for question 1 is "1.54"
	And the Group Average at the bottom for the "Male" column is "7.20"
	And the Group Average at the bottom for the "Female" column is "10.50"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.8"
	And the score for question 1 in the "Female" column is "3.8"
	And the Weighted Average on the right for question 1 is "3.8"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.80"
	And the score for question 1 in the "Female" column is "3.83"
	And the Weighted Average on the right for question 1 is "3.82"
	And the Group Average at the bottom for the "Male" column is "4.31"
	And the Group Average at the bottom for the "Female" column is "4.20"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "72"
	And the score for question 1 in the "Female" column is "70"
	And the Weighted Average on the right for question 1 is "71"
	And the Group Average at the bottom for the "Male" column is "90"
	And the Group Average at the bottom for the "Female" column is "85"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "72.0"
	And the score for question 1 in the "Female" column is "70.0"
	And the Weighted Average on the right for question 1 is "70.8"
	And the Group Average at the bottom for the "Male" column is "90.1"
	And the Group Average at the bottom for the "Female" column is "84.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "72.00"
	And the score for question 1 in the "Female" column is "70.00"
	And the Weighted Average on the right for question 1 is "70.77"
	And the Group Average at the bottom for the "Male" column is "90.06"
	And the Group Average at the bottom for the "Female" column is "84.71"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-8"
	And the score for question 1 in the "Female" column is "-3"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "8"
	And the Group Average at the bottom for the "Female" column is "8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-7.6"
	And the score for question 1 in the "Female" column is "-3.3"
	And the Weighted Average on the right for question 1 is "-3.4"
	And the Group Average at the bottom for the "Male" column is "8.2"
	And the Group Average at the bottom for the "Female" column is "8.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-7.60"
	And the score for question 1 in the "Female" column is "-3.26"
	And the Weighted Average on the right for question 1 is "-3.44"
	And the Group Average at the bottom for the "Male" column is "8.19"
	And the Group Average at the bottom for the "Female" column is "8.19"


# Categories
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(25)"
	And the number directly below "Female" is "(40)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.9"
	And the Group Average at the bottom for the "Male" column is "3.9"
	And the Group Average at the bottom for the "Female" column is "3.8"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.2"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.98"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.91"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.94"
	And the Group Average at the bottom for the "Male" column is "3.93"
	And the Group Average at the bottom for the "Female" column is "3.81"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.20"
	And the score for question 1 in the "Female" column is "4.00"
	And the Weighted Average on the right for question 1 is "4.08"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "85"
	And the score for the "Clarity of Direction" category in the "Female" column is "79"
	And the Weighted Average on the right for the "Clarity of Direction" category is "82"
	And the Group Average at the bottom for the "Male" column is "77"
	And the Group Average at the bottom for the "Female" column is "71"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96"
	And the score for question 1 in the "Female" column is "82"
	And the Weighted Average on the right for question 1 is "88"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "85.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "79.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "81.5"
	And the Group Average at the bottom for the "Male" column is "77.3"
	And the Group Average at the bottom for the "Female" column is "71.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96.0"
	And the score for question 1 in the "Female" column is "82.5"
	And the Weighted Average on the right for question 1 is "87.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "85.00"
	And the score for the "Clarity of Direction" category in the "Female" column is "79.38"
	And the Weighted Average on the right for the "Clarity of Direction" category is "81.54"
	And the Group Average at the bottom for the "Male" column is "77.26"
	And the Group Average at the bottom for the "Female" column is "71.29"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96.00"
	And the score for question 1 in the "Female" column is "82.50"
	And the Weighted Average on the right for question 1 is "87.69"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5"
	And the score for the "Clarity of Direction" category in the "Female" column is "7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6"
	And the Group Average at the bottom for the "Male" column is "7"
	And the Group Average at the bottom for the "Female" column is "10"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "2"
	And the Weighted Average on the right for question 1 is "2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.2"
	And the Group Average at the bottom for the "Male" column is "7.2"
	And the Group Average at the bottom for the "Female" column is "10.5"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "2.5"
	And the Weighted Average on the right for question 1 is "1.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.00"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.88"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.15"
	And the Group Average at the bottom for the "Male" column is "7.20"
	And the Group Average at the bottom for the "Female" column is "10.50"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "2.50"
	And the Weighted Average on the right for question 1 is "1.54"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.2"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.8"
	And the score for question 1 in the "Female" column is "3.8"
	And the Weighted Average on the right for question 1 is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.96"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.98"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.97"
	And the Group Average at the bottom for the "Male" column is "4.31"
	And the Group Average at the bottom for the "Female" column is "4.20"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.80"
	And the score for question 1 in the "Female" column is "3.83"
	And the Weighted Average on the right for question 1 is "3.82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "79"
	And the score for the "Clarity of Direction" category in the "Female" column is "80"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80"
	And the Group Average at the bottom for the "Male" column is "90"
	And the Group Average at the bottom for the "Female" column is "85"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "72"
	And the score for question 1 in the "Female" column is "70"
	And the Weighted Average on the right for question 1 is "71"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "79.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "80.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79.6"
	And the Group Average at the bottom for the "Male" column is "90.1"
	And the Group Average at the bottom for the "Female" column is "84.7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "72.0"
	And the score for question 1 in the "Female" column is "70.0"
	And the Weighted Average on the right for question 1 is "70.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "79.00"
	And the score for the "Clarity of Direction" category in the "Female" column is "80.00"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79.62"
	And the Group Average at the bottom for the "Male" column is "90.06"
	And the Group Average at the bottom for the "Female" column is "84.71"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "72.00"
	And the score for question 1 in the "Female" column is "70.00"
	And the Weighted Average on the right for question 1 is "70.77"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "0"
	And the score for the "Clarity of Direction" category in the "Female" column is "1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1"
	And the Group Average at the bottom for the "Male" column is "8"
	And the Group Average at the bottom for the "Female" column is "8"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-8"
	And the score for question 1 in the "Female" column is "-3"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "-0.4"
	And the score for the "Clarity of Direction" category in the "Female" column is "1.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1.0"
	And the Group Average at the bottom for the "Male" column is "8.2"
	And the Group Average at the bottom for the "Female" column is "8.2"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-7.6"
	And the score for question 1 in the "Female" column is "-3.3"
	And the Weighted Average on the right for question 1 is "-3.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "-0.40"
	And the score for the "Clarity of Direction" category in the "Female" column is "1.39"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1.04"
	And the Group Average at the bottom for the "Male" column is "8.19"
	And the Group Average at the bottom for the "Female" column is "8.19"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-7.60"
	And the score for question 1 in the "Female" column is "-3.26"
	And the Weighted Average on the right for question 1 is "-3.44"



# Questions + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.2 (4.0)"
	And the score for question 1 in the "Female" column is "4.0 (3.9)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "3.9 (3.9)"
	And the Group Average at the bottom for the "Female" column is "3.8 (3.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.20 (4.00)"
	And the score for question 1 in the "Female" column is "4.00 (3.89)"
	And the Weighted Average on the right for question 1 is "4.08"
	And the Group Average at the bottom for the "Male" column is "3.93 (3.90)"
	And the Group Average at the bottom for the "Female" column is "3.81 (3.87)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96 (89)"
	And the score for question 1 in the "Female" column is "82 (89)"
	And the Weighted Average on the right for question 1 is "88"
	And the Group Average at the bottom for the "Male" column is "77 (77)"
	And the Group Average at the bottom for the "Female" column is "71 (73)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96.0 (88.9)"
	And the score for question 1 in the "Female" column is "82.5 (88.9)"
	And the Weighted Average on the right for question 1 is "87.7"
	And the Group Average at the bottom for the "Male" column is "77.3 (76.9)"
	And the Group Average at the bottom for the "Female" column is "71.3 (72.8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96.00 (88.89)"
	And the score for question 1 in the "Female" column is "82.50 (88.89)"
	And the Weighted Average on the right for question 1 is "87.69"
	And the Group Average at the bottom for the "Male" column is "77.26 (76.88)"
	And the Group Average at the bottom for the "Female" column is "71.29 (72.76)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0 (0)"
	And the score for question 1 in the "Female" column is "2 (0)"
	And the Weighted Average on the right for question 1 is "2"
	And the Group Average at the bottom for the "Male" column is "7 (6)"
	And the Group Average at the bottom for the "Female" column is "10 (6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0 (0.0)"
	And the score for question 1 in the "Female" column is "2.5 (0.0)"
	And the Weighted Average on the right for question 1 is "1.5"
	And the Group Average at the bottom for the "Male" column is "7.2 (6.3)"
	And the Group Average at the bottom for the "Female" column is "10.5 (5.7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00 (0.00)"
	And the score for question 1 in the "Female" column is "2.50 (0.00)"
	And the Weighted Average on the right for question 1 is "1.54"
	And the Group Average at the bottom for the "Male" column is "7.20 (6.27)"
	And the Group Average at the bottom for the "Female" column is "10.50 (5.73)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.8 (3.6)"
	And the score for question 1 in the "Female" column is "3.8 (3.6)"
	And the Weighted Average on the right for question 1 is "3.8"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.2)"
	And the Group Average at the bottom for the "Female" column is "4.2 (4.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.80 (3.61)"
	And the score for question 1 in the "Female" column is "3.83 (3.56)"
	And the Weighted Average on the right for question 1 is "3.82"
	And the Group Average at the bottom for the "Male" column is "4.31 (4.21)"
	And the Group Average at the bottom for the "Female" column is "4.20 (4.28)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "72 (61)"
	And the score for question 1 in the "Female" column is "70 (56)"
	And the Weighted Average on the right for question 1 is "71"
	And the Group Average at the bottom for the "Male" column is "90 (87)"
	And the Group Average at the bottom for the "Female" column is "85 (89)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "72.0 (61.1)"
	And the score for question 1 in the "Female" column is "70.0 (55.6)"
	And the Weighted Average on the right for question 1 is "70.8"
	And the Group Average at the bottom for the "Male" column is "90.1 (87.1)"
	And the Group Average at the bottom for the "Female" column is "84.7 (88.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "72.00 (61.11)"
	And the score for question 1 in the "Female" column is "70.00 (55.56)"
	And the Weighted Average on the right for question 1 is "70.77"
	And the Group Average at the bottom for the "Male" column is "90.06 (87.10)"
	And the Group Average at the bottom for the "Female" column is "84.71 (88.89)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-8 (-7)"
	And the score for question 1 in the "Female" column is "-3 (-6)"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "8 (7)"
	And the Group Average at the bottom for the "Female" column is "8 (9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-7.6 (-7.0)"
	And the score for question 1 in the "Female" column is "-3.3 (-5.9)"
	And the Weighted Average on the right for question 1 is "-3.4"
	And the Group Average at the bottom for the "Male" column is "8.2 (6.5)"
	And the Group Average at the bottom for the "Female" column is "8.2 (8.8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-7.60 (-7.04)"
	And the score for question 1 in the "Female" column is "-3.26 (-5.87)"
	And the Weighted Average on the right for question 1 is "-3.44"
	And the Group Average at the bottom for the "Male" column is "8.19 (6.53)"
	And the Group Average at the bottom for the "Female" column is "8.19 (8.77)"


# Categories + trend
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.0 (3.8)"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.9 (3.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.9"
	And the Group Average at the bottom for the "Male" column is "3.9 (3.9)"
	And the Group Average at the bottom for the "Female" column is "3.8 (3.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.2 (4.0)"
	And the score for question 1 in the "Female" column is "4.0 (3.9)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.98 (3.82)"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.91 (3.67)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.94"
	And the Group Average at the bottom for the "Male" column is "3.93 (3.90)"
	And the Group Average at the bottom for the "Female" column is "3.81 (3.87)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.20 (4.00)"
	And the score for question 1 in the "Female" column is "4.00 (3.89)"
	And the Weighted Average on the right for question 1 is "4.08"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "85 (78)"
	And the score for the "Clarity of Direction" category in the "Female" column is "79 (67)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "82"
	And the Group Average at the bottom for the "Male" column is "77 (77)"
	And the Group Average at the bottom for the "Female" column is "71 (73)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96 (89)"
	And the score for question 1 in the "Female" column is "82 (89)"
	And the Weighted Average on the right for question 1 is "88"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "85.0 (77.8)"
	And the score for the "Clarity of Direction" category in the "Female" column is "79.4 (66.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "81.5"
	And the Group Average at the bottom for the "Male" column is "77.3 (76.9)"
	And the Group Average at the bottom for the "Female" column is "71.3 (72.8)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96.0 (88.9)"
	And the score for question 1 in the "Female" column is "82.5 (88.9)"
	And the Weighted Average on the right for question 1 is "87.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "85.00 (77.78)"
	And the score for the "Clarity of Direction" category in the "Female" column is "79.38 (66.67)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "81.54"
	And the Group Average at the bottom for the "Male" column is "77.26 (76.88)"
	And the Group Average at the bottom for the "Female" column is "71.29 (72.76)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96.00 (88.89)"
	And the score for question 1 in the "Female" column is "82.50 (88.89)"
	And the Weighted Average on the right for question 1 is "87.69"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5 (7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7 (8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6"
	And the Group Average at the bottom for the "Male" column is "7 (6)"
	And the Group Average at the bottom for the "Female" column is "10 (6)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0 (0)"
	And the score for question 1 in the "Female" column is "2 (0)"
	And the Weighted Average on the right for question 1 is "2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.0 (6.9)"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.9 (8.3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.2"
	And the Group Average at the bottom for the "Male" column is "7.2 (6.3)"
	And the Group Average at the bottom for the "Female" column is "10.5 (5.7)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0 (0.0)"
	And the score for question 1 in the "Female" column is "2.5 (0.0)"
	And the Weighted Average on the right for question 1 is "1.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.00 (6.94)"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.88 (8.33)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.15"
	And the Group Average at the bottom for the "Male" column is "7.20 (6.27)"
	And the Group Average at the bottom for the "Female" column is "10.50 (5.73)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00 (0.00)"
	And the score for question 1 in the "Female" column is "2.50 (0.00)"
	And the Weighted Average on the right for question 1 is "1.54"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.0 (3.9)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.0 (4.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.2)"
	And the Group Average at the bottom for the "Female" column is "4.2 (4.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.8 (3.6)"
	And the score for question 1 in the "Female" column is "3.8 (3.6)"
	And the Weighted Average on the right for question 1 is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.96 (3.93)"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.98 (4.06)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.97"
	And the Group Average at the bottom for the "Male" column is "4.31 (4.21)"
	And the Group Average at the bottom for the "Female" column is "4.20 (4.28)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.80 (3.61)"
	And the score for question 1 in the "Female" column is "3.83 (3.56)"
	And the Weighted Average on the right for question 1 is "3.82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "79 (72)"
	And the score for the "Clarity of Direction" category in the "Female" column is "80 (83)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80"
	And the Group Average at the bottom for the "Male" column is "90 (87)"
	And the Group Average at the bottom for the "Female" column is "85 (89)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "72 (61)"
	And the score for question 1 in the "Female" column is "70 (56)"
	And the Weighted Average on the right for question 1 is "71"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "79.0 (72.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "80.0 (83.3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79.6"
	And the Group Average at the bottom for the "Male" column is "90.1 (87.1)"
	And the Group Average at the bottom for the "Female" column is "84.7 (88.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "72.0 (61.1)"
	And the score for question 1 in the "Female" column is "70.0 (55.6)"
	And the Weighted Average on the right for question 1 is "70.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "79.00 (72.22)"
	And the score for the "Clarity of Direction" category in the "Female" column is "80.00 (83.33)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79.62"
	And the Group Average at the bottom for the "Male" column is "90.06 (87.10)"
	And the Group Average at the bottom for the "Female" column is "84.71 (88.89)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "72.00 (61.11)"
	And the score for question 1 in the "Female" column is "70.00 (55.56)"
	And the Weighted Average on the right for question 1 is "70.77"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "0 (2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "1 (8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1"
	And the Group Average at the bottom for the "Male" column is "8 (7)"
	And the Group Average at the bottom for the "Female" column is "8 (9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-8 (-7)"
	And the score for question 1 in the "Female" column is "-3 (-6)"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "-0.4 (2.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "1.4 (7.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1.0"
	And the Group Average at the bottom for the "Male" column is "8.2 (6.5)"
	And the Group Average at the bottom for the "Female" column is "8.2 (8.8)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-7.6 (-7.0)"
	And the score for question 1 in the "Female" column is "-3.3 (-5.9)"
	And the Weighted Average on the right for question 1 is "-3.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "-0.40 (2.16)"
	And the score for the "Clarity of Direction" category in the "Female" column is "1.39 (7.92)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1.04"
	And the Group Average at the bottom for the "Male" column is "8.19 (6.53)"
	And the Group Average at the bottom for the "Female" column is "8.19 (8.77)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-7.60 (-7.04)"
	And the score for question 1 in the "Female" column is "-3.26 (-5.87)"
	And the Weighted Average on the right for question 1 is "-3.44"



	Now I go to "Functions" > "Logout"