Feature: Check filtered Gap Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the figure for "Filter Count:" in the upper right is "65"
	And the Eval Avg for question 1 is "4.08"
	And the Importance for question 1 is "3.82"
	And the Gap for question 1 is "-0.26"
	And the Weighted Gap for question 1 is "-4.97"
	And the Overall Average at the bottom has an Eval Avg of "3.86"
	And the Overall Average has a Importance of "4.25"
	And the Overall Average has a Gap of "0.39"
	And the Overall Average has a Weighted Gap of "8.29"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.94"
	And the Importance for the "Clarity of Direction" category is "3.97"
	And the Gap for the "Clarity of Direction" category is "0.03"
	And the Weighted Gap for the "Clarity of Direction" category is "0.60"
	And the Overall Average at the bottom has an Eval Avg of "3.86"
	And the Overall Average has a Importance of "4.25"
	And the Overall Average has a Gap of "0.39"
	And the Overall Average has a Weighted Gap of "8.29"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.08"
	And the Importance for question 1 is "3.82"
	And the Gap for question 1 is "-0.26"
	And the Weighted Gap for question 1 is "-4.97"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the number for "Filter" in the upper right is "(65)"
	And the number for "Organization" in the upper right is "(53)"
	And the Eval Avg for question 1 is "4.08 (-0.32)"
	And the Importance for question 1 is "3.82 (-0.52)"
	And the Gap for question 1 is "-0.26 (-0.20)"
	And the Weighted Gap for question 1 is "-4.97 (-3.67)"
	And the Overall Average at the bottom has an Eval Avg of "3.86 (-0.28)"
	And the Overall Average has a Importance of "4.25 (-0.13)"
	And the Overall Average has a Gap of "0.39 (+0.15)"
	And the Overall Average has a Weighted Gap of "8.29 (+3.03)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the Eval Avg for the "Clarity of Direction" category is "3.94 (-0.02)"
	And the Importance for the "Clarity of Direction" category is "3.97 (-0.29)"
	And the Gap for the "Clarity of Direction" category is "0.03 (-0.27)"
	And the Weighted Gap for the "Clarity of Direction" category is "0.60 (-5.79)"
	And the Overall Average at the bottom has an Eval Avg of "3.86 (-0.09)"
	And the Overall Average has a Importance of "4.25 (-0.08)"
	And the Overall Average has a Gap of "0.39 (+0.01)"
	And the Overall Average has a Weighted Gap of "8.29 (+0.06)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.08 (-0.04)"
	And the Importance for question 1 is "3.82 (-0.28)"
	And the Gap for question 1 is "-0.26 (-0.24)"
	And the Weighted Gap for question 1 is "-4.97 (-4.56)"
	
	Now I go to "Functions" > "Logout"