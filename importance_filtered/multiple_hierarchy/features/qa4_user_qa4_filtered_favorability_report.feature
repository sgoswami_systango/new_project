Feature: Check filtered Favorability Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter" at the top right is "(65)"
	And the Favorable score for question 1 is "87.7%"
	And the Neutral score for question 1 is "10.8%"
	And the Unfavorable score for question 1 is "1.5%"
	And the Overall Average at the bottom has a Favorable score of "73.6%"
	And the Overall Average Neutral score is "17.2%"
	And the Overall Average Unfavorable score is "9.2%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "81.5%"
	And the Neutral score for the "Clarity of Direction" category is "12.3%"
	And the Unfavorable score for the "Clarity of Direction" category is "6.2%"
	And the Overall Average at the bottom has a Favorable score of "73.6%"
	And the Overall Average Neutral score is "17.2%"
	And the Overall Average Unfavorable score is "9.2%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score for question 1 is "87.7%"
	And the Neutral score for question 1 is "10.8%"
	And the Unfavorable score for question 1 is "1.5%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the number for "Filter" is "(65)"
	And the number for "Remainder" is "(53)"
	And the Favorable score on top for question 1 is "87.7%"
	And the Neutral score on top for question 1 is "10.8%"
	And the Unfavorable score on top for question 1 is "1.5%"
	And the Favorable score on bottom for question 1 is "98.1%"
	# And there is no Neutral score on bottom for question 1
	And the Unfavorable score on bottom for question 1 is "1.9%"
	And the Overall Average at the bottom Favorable score on top is "73.6%"
	And the Overall Average Neutral score on top is "17.2%"
	And the Overall Average Unfavorable score on top is "9.2%"
	And the Overall Average Favorable score on bottom is "85.5%"
	And the Overall Average Neutral score on bottom is "8.2%"
	And the Overall Average Unfavorable score on bottom is "6.3%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the Favorable score on top for the "Clarity of Direction" category is "81.5%"
	And the Neutral score on top for the "Clarity of Direction" category is "12.3%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "6.2%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "95.8%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "0.9%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "3.3%"
	And the Overall Average at the bottom Favorable score on top is "73.6%"
	And the Overall Average Neutral score on top is "17.2%"
	And the Overall Average Unfavorable score on top is "9.2%"
	And the Overall Average Favorable score on bottom is "85.5%"
	And the Overall Average Neutral score on bottom is "8.2%"
	And the Overall Average Unfavorable score on bottom is "6.3%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score on top for question 1 is "87.7%"
	And the Neutral score on top for question 1 is "10.8%"
	And the Unfavorable score on top for question 1 is "1.5%"
	And the Favorable score on bottom for question 1 is "98.1%"
	# And there is no Neutral score on bottom for question 1
	And the Unfavorable score on bottom for question 1 is "1.9%"
	
	Now I go to "Functions" > "Logout"
