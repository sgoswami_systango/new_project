Feature: Check filtered Trend Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(65)"
	And the "Count" for the "2013" column is "(28)"
	And the "Count" for the "2012" column is "(18)"
	And the score for "question 1" in the "2014" column is "4.08 (+0.12)"
	And the score for "question 1" in the "2013" column is "3.96 (-0.10)"
	And the score for "question 1" in the "2012" column is "4.06"
	And the Overall Average at the bottom for "2014" is "3.87 (-0.02)"
	And the Overall Average for "2013" is "3.89 (-0.08)"
	And the Overall Average for "2012" is "3.97"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(65)"
	And the "Count" for the "2013" column is "(28)"
	And the "Count" for the "2012" column is "(18)"
	And the score for the "Clarity of Direction" category for "2014" is "3.94 (+0.17)"
	And the score for the "Clarity of Direction" category for "2013" is "3.77 (-0.19)"
	And the score for the "Clarity of Direction" category for "2012" is "3.96"
	And the Overall Average at the bottom for "2014" is "3.87 (-0.02)"
	And the Overall Average for "2013" is "3.89 (-0.08)"
	And the Overall Average for "2012" is "3.97"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.08 (+0.12)"
	And the score for "question 1" in the "2013" column is "3.96 (-0.10)"
	And the score for "question 1" in the "2012" column is "4.06"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(65)"
	And the "Count" for the "2013" column is "(28)"
	And the "Count" for the "2012" column is "(18)"
	And the score for "question 1" in the "2014" column is "87.7 (-1.2)"
	And the score for "question 1" in the "2013" column is "88.9 (-5.5)"
	And the score for "question 1" in the "2012" column is "94.4"
	And the Overall Average at the bottom for "2014" is "74.3 (-1.2)"
	And the Overall Average for "2013" is "75.5 (-4.8)"
	And the Overall Average for "2012" is "80.3"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(65)"
	And the "Count" for the "2013" column is "(28)"
	And the "Count" for the "2012" column is "(18)"
	And the score for the "Clarity of Direction" category for "2014" is "81.5 (+7.4)"
	And the score for the "Clarity of Direction" category for "2013" is "74.1 (-10.6)"
	And the score for the "Clarity of Direction" category for "2012" is "84.7"
	And the Overall Average at the bottom for "2014" is "74.3 (-1.2)"
	And the Overall Average for "2013" is "75.5 (-4.8)"
	And the Overall Average for "2012" is "80.3"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "87.7 (-1.2)"
	And the score for "question 1" in the "2013" column is "88.9 (-5.5)"
	And the score for "question 1" in the "2012" column is "94.4"

	Now I go to "Functions" > "Logout"