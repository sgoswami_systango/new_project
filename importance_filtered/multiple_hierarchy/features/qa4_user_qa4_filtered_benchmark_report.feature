Feature: Check filtered Benchmark Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click on the "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 2 in the "My Filter" column is "76.9%"
	And the score for question 2 in the "My View" column is "83.1%(6.2)"
	And the score for question 2 in the "Mitchell" column is "77.2%(0.3)"
	And the score for question 2 in the "Perceptyx overall benchmark" column is "73.6%(-3.3)"
	And the score for question 2 in the "Perceptyx software/technology benchmark" column is "75.4%(-1.5)"

	When I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the "My Filter" column is "81.5%"
	And the score for the Clarity of Direction category in the "My View" column is "87.9%"
	And the score for the Clarity of Direction category in the "Mitchell" column is "78.1%"
	And the score for question 2 in the "My Filter" column is "76.9%"
	And the score for question 2 in the "My View" column is "83.1%(6.2)"
	And the score for question 2 in the "Mitchell" column is "77.2%(0.3)"
	And the score for question 2 in the "Perceptyx overall benchmark" column is "73.6%(-3.3)"
	And the score for question 2 in the "Perceptyx software/technology benchmark" column is "75.4%(-1.5)"

	Now I go to "Functions" > "Logout"