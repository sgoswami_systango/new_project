import os
import pdb
import yaml
import datetime
import time
from lettuce import world
from consensus_filtered import application_path
from lettuce import *
from splinter import Browser
from pyvirtualdisplay import Display
from selenium import webdriver

browser = None
headless = True
tab_link = None
clarity_of_direction_checked_value = False
user = None 
filtered = False

@before.all
def open_browser():
    if headless:
        display = Display(visible=0)
        display.start()

    global browser
    if not browser:
        browser = Browser()
        browser.driver.set_window_size(1640, 1000)
        browser.driver.set_page_load_timeout(30)
        world.browser = browser
        world.tab_link = ''


@after.all
def close_browser(scenario):
    browser.quit()


@after.each_scenario
def capture_screenshot(scenario):
    for step in scenario.steps:
        if step.failed:
            now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S-%f')
            error_screenshots_path = os.path.join(
                application_path, 'error_screenshots')
            sentence = step.sentence.replace('\"', "").replace("\'", "")
            get_browser().driver.get_screenshot_as_file(
                error_screenshots_path + '/%s%s.png' % (sentence, now))
            print "Snapshot is taken"
            print "#===========================================================#"
            print step.sentence + ' step failed'
            print "#===========================================================#"


def get_browser():
    return world.browser


def get_tab_link():
    return world.tab_link


def set_tab_link(value):
    world.tab_link = value


def set_clarity_of_direction_checked_value(value):
    world.clarity_of_direction_checked_value = value


def get_clarity_of_direction_checked_value():
    return world.clarity_of_direction_checked_value

def get_user():
    return world.user


def set_user(value):
    world.user = value

def get_filtered():
    return world.filtered


def set_filtered(value):
    world.filtered = value

print "================================"
