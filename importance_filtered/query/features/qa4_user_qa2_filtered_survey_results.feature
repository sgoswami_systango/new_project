Feature: Check filtered Survey Results for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click on the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "4"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.5%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "10"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "3.7%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "30"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "11.1%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "146"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "54.1%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "80"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "29.6%"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 is "4.07 (±0.83)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Number" column is "270"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Percent" column is "100.0%"
	And the figure under question 1 for "1. Not Important" in the "Number" column is "1"
	And the figure under question 1 for "1. Not Important" in the "Percent" column is "0.4%"
	And the figure under question 1 for "2. Little Importance" in the "Number" column is "10"
	And the figure under question 1 for "2. Little Importance" in the "Percent" column is "3.7%"
	And the figure under question 1 for "3. Somewhat Important" in the "Number" column is "44"
	And the figure under question 1 for "3. Somewhat Important" in the "Percent" column is "16.3%"
	And the figure under question 1 for "4. Very Important" in the "Number" column is "129"
	And the figure under question 1 for "4. Very Important" in the "Percent" column is "47.8%"
	And the figure under question 1 for "5. Extremely Important" in the "Number" column is "86"
	And the figure under question 1 for "5. Extremely Important" in the "Percent" column is "31.9%"
	And the "Imp Mean" under question 1 is "4.07 (±0.81)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Number" column is "270"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Percent" column is "100.0%"


	When I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the number for "Filter" is "(276)"
	And the number for "Organization" is "(188)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "4(2)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.5%(1.1%)"
	# And the figure under question 1 for "1. Strongly Disagree" in the "Segment Density" column is "36.4%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "10(9)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "3.7%(4.8%)"
	# And the figure under question 1 for "2. Disagree" in the "Segment Density" column is "25.0%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "30(20)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "11.1%(10.8%)"
	# And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Segment Density" column is "26.8%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "146(108)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "54.1%(58.1%)"
	# And the figure under question 1 for "4. Agree" in the "Segment Density" column is "24.5%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "80(47)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "29.6%(25.3%)"
	# And the figure under question 1 for "5. Strongly Agree" in the "Segment Density" column is "22.5%"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 is "4.07 (±0.83)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Number" column is "270(186)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Percent" column is "100.0%(100.0%)"
	And the figure under question 1 for "1. Not Important" in the "Number" column is "1(0)"
	And the figure under question 1 for "1. Not Important" in the "Percent" column is "0.4%(0.0%)"
	# And the figure under question 1 for "1. Not Important" in the "Percent" column is "33.3%"
	And the figure under question 1 for "2. Little Importance" in the "Number" column is "10(0)"
	And the figure under question 1 for "2. Little Importance" in the "Percent" column is "3.7%(0.0%)"
	# And the figure under question 1 for "2. Little Importance" in the "Segment Density" column is "35.7%"
	And the figure under question 1 for "3. Somewhat Important" in the "Number" column is "44(29)"
	And the figure under question 1 for "3. Somewhat Important" in the "Percent" column is "16.3%(15.6%)"
	# And the figure under question 1 for "3. Somewhat Important" in the "Segment Density" column is "26.5%"
	And the figure under question 1 for "4. Very Important" in the "Number" column is "129(102)"
	And the figure under question 1 for "4. Very Important" in the "Percent" column is "47.8%(54.8%)"
	# And the figure under question 1 for "4. Very Important" in the "Segment Density" column is "22.4%"
	And the figure under question 1 for "5. Extremely Important" in the "Number" column is "86(55)"
	And the figure under question 1 for "5. Extremely Important" in the "Percent" column is "31.9%(29.6%)"
	# And the figure under question 1 for "5. Extremely Important" in the "Segment Density" column is "25.1%"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 is "4.07 (±0.81)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Number" column is "270(186)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"