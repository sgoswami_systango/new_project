Feature: Check filtered Trend Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip
Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(276)"
	And the "Count" for the "2013" column is "(320)"
	And the "Count" for the "2012" column is "(304)"
	And the score for "question 1" in the "2014" column is "4.07 (-0.04)"
	And the score for "question 1" in the "2013" column is "4.11 (+0.03)"
	And the score for "question 1" in the "2012" column is "4.08"
	And the Overall Average at the bottom for "2014" is "3.98 (+0.04)"
	And the Overall Average for "2013" is "3.94 (+0.06)"
	And the Overall Average for "2012" is "3.88"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(276)"
	And the "Count" for the "2013" column is "(320)"
	And the "Count" for the "2012" column is "(304)"
	And the score for the "Clarity of Direction" category for "2014" is "3.88 (-0.09)"
	And the score for the "Clarity of Direction" category for "2013" is "3.97 (+0.12)"
	And the score for the "Clarity of Direction" category for "2012" is "3.85"
	And the Overall Average at the bottom for "2014" is "3.98 (+0.04)"
	And the Overall Average for "2013" is "3.94 (+0.06)"
	And the Overall Average for "2012" is "3.88"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.07 (-0.04)"
	And the score for "question 1" in the "2013" column is "4.11 (+0.03)"
	And the score for "question 1" in the "2012" column is "4.08"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(276)"
	And the "Count" for the "2013" column is "(320)"
	And the "Count" for the "2012" column is "(304)"
	And the score for "question 1" in the "2014" column is "83.7 (-1.4)"
	And the score for "question 1" in the "2013" column is "85.1 (-0.4)"
	And the score for "question 1" in the "2012" column is "85.5"
	And the Overall Average at the bottom for "2014" is "77.5 (+1.4)"
	And the Overall Average for "2013" is "76.1 (+2.0)"
	And the Overall Average for "2012" is "74.1"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(276)"
	And the "Count" for the "2013" column is "(320)"
	And the "Count" for the "2012" column is "(304)"
	And the score for the "Clarity of Direction" category for "2014" is "74.6 (-4.4)"
	And the score for the "Clarity of Direction" category for "2013" is "79.0 (+4.0)"
	And the score for the "Clarity of Direction" category for "2012" is "75.0"
	And the Overall Average at the bottom for "2014" is "77.5 (+1.4)"
	And the Overall Average for "2013" is "76.1 (+2.0)"
	And the Overall Average for "2012" is "74.1"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "83.7 (-1.4)"
	And the score for "question 1" in the "2013" column is "85.1 (-0.4)"
	And the score for "question 1" in the "2012" column is "85.5"

	Now I go to "Functions" > "Logout"