Feature: Check filtered Demographic Crosstab for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(187)"
	And the first number under "Female" is "(89)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "3.9"
	And the Group Average at the bottom for the "Female" column is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.08"
	And the score for question 1 in the "Female" column is "4.05"
	And the Weighted Average on the right for question 1 is "4.07"
	And the Group Average at the bottom for the "Male" column is "3.93"
	And the Group Average at the bottom for the "Female" column is "3.96"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83"
	And the score for question 1 in the "Female" column is "85"
	And the Weighted Average on the right for question 1 is "84"
	And the Group Average at the bottom for the "Male" column is "75"
	And the Group Average at the bottom for the "Female" column is "76"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.2"
	And the score for question 1 in the "Female" column is "84.9"
	And the Weighted Average on the right for question 1 is "83.7"
	And the Group Average at the bottom for the "Male" column is "75.3"
	And the Group Average at the bottom for the "Female" column is "75.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.15"
	And the score for question 1 in the "Female" column is "84.88"
	And the Weighted Average on the right for question 1 is "83.70"
	And the Group Average at the bottom for the "Male" column is "75.26"
	And the Group Average at the bottom for the "Female" column is "75.69"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5"
	And the score for question 1 in the "Female" column is "5"
	And the Weighted Average on the right for question 1 is "5"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.4"
	And the score for question 1 in the "Female" column is "4.7"
	And the Weighted Average on the right for question 1 is "5.2"
	And the Group Average at the bottom for the "Male" column is "8.7"
	And the Group Average at the bottom for the "Female" column is "6.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.43"
	And the score for question 1 in the "Female" column is "4.65"
	And the Weighted Average on the right for question 1 is "5.19"
	And the Group Average at the bottom for the "Male" column is "8.73"
	And the Group Average at the bottom for the "Female" column is "6.58"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.4"
	And the Group Average at the bottom for the "Female" column is "4.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.10"
	And the score for question 1 in the "Female" column is "4.01"
	And the Weighted Average on the right for question 1 is "4.07"
	And the Group Average at the bottom for the "Male" column is "4.35"
	And the Group Average at the bottom for the "Female" column is "4.35"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80"
	And the score for question 1 in the "Female" column is "79"
	And the Weighted Average on the right for question 1 is "80"
	And the Group Average at the bottom for the "Male" column is "90"
	And the Group Average at the bottom for the "Female" column is "90"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "79.9"
	And the score for question 1 in the "Female" column is "79.1"
	And the Weighted Average on the right for question 1 is "79.6"
	And the Group Average at the bottom for the "Male" column is "90.4"
	And the Group Average at the bottom for the "Female" column is "90.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "79.89"
	And the score for question 1 in the "Female" column is "79.07"
	And the Weighted Average on the right for question 1 is "79.63"
	And the Group Average at the bottom for the "Male" column is "90.43"
	And the Group Average at the bottom for the "Female" column is "90.30"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "-1"
	And the Weighted Average on the right for question 1 is "1"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.4"
	And the score for question 1 in the "Female" column is "-0.8"
	And the Weighted Average on the right for question 1 is "1.0"
	And the Group Average at the bottom for the "Male" column is "9.1"
	And the Group Average at the bottom for the "Female" column is "8.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.41"
	And the score for question 1 in the "Female" column is "-0.80"
	And the Weighted Average on the right for question 1 is "1.02"
	And the Group Average at the bottom for the "Male" column is "9.13"
	And the Group Average at the bottom for the "Female" column is "8.48"


# Categories
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(187)"
	And the number directly below "Female" is "(89)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.9"
	And the Group Average at the bottom for the "Male" column is "3.9"
	And the Group Average at the bottom for the "Female" column is "4.0"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.88"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.86"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.88"
	And the Group Average at the bottom for the "Male" column is "3.93"
	And the Group Average at the bottom for the "Female" column is "3.96"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.08"
	And the score for question 1 in the "Female" column is "4.05"
	And the Weighted Average on the right for question 1 is "4.07"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75"
	And the score for the "Clarity of Direction" category in the "Female" column is "75"
	And the Weighted Average on the right for the "Clarity of Direction" category is "75"
	And the Group Average at the bottom for the "Male" column is "75"
	And the Group Average at the bottom for the "Female" column is "76"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83"
	And the score for question 1 in the "Female" column is "85"
	And the Weighted Average on the right for question 1 is "84"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "74.6"
	And the score for the "Clarity of Direction" category in the "Female" column is "74.7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "74.6"
	And the Group Average at the bottom for the "Male" column is "75.3"
	And the Group Average at the bottom for the "Female" column is "75.7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.2"
	And the score for question 1 in the "Female" column is "84.9"
	And the Weighted Average on the right for question 1 is "83.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "74.59"
	And the score for the "Clarity of Direction" category in the "Female" column is "74.71"
	And the Weighted Average on the right for the "Clarity of Direction" category is "74.63"
	And the Group Average at the bottom for the "Male" column is "75.26"
	And the Group Average at the bottom for the "Female" column is "75.69"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.15"
	And the score for question 1 in the "Female" column is "84.88"
	And the Weighted Average on the right for question 1 is "83.70"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9"
	And the score for the "Clarity of Direction" category in the "Female" column is "8"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5"
	And the score for question 1 in the "Female" column is "5"
	And the Weighted Average on the right for question 1 is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.2"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.6"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.7"
	And the Group Average at the bottom for the "Male" column is "8.7"
	And the Group Average at the bottom for the "Female" column is "6.6"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.4"
	And the score for question 1 in the "Female" column is "4.7"
	And the Weighted Average on the right for question 1 is "5.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.24"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.56"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.70"
	And the Group Average at the bottom for the "Male" column is "8.73"
	And the Group Average at the bottom for the "Female" column is "6.58"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.43"
	And the score for question 1 in the "Female" column is "4.65"
	And the Weighted Average on the right for question 1 is "5.19"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.3"
	And the Group Average at the bottom for the "Male" column is "4.4"
	And the Group Average at the bottom for the "Female" column is "4.4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.27"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.22"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.25"
	And the Group Average at the bottom for the "Male" column is "4.35"
	And the Group Average at the bottom for the "Female" column is "4.35"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.10"
	And the score for question 1 in the "Female" column is "4.01"
	And the Weighted Average on the right for question 1 is "4.07"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87"
	And the score for the "Clarity of Direction" category in the "Female" column is "86"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86"
	And the Group Average at the bottom for the "Male" column is "90"
	And the Group Average at the bottom for the "Female" column is "90"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80"
	And the score for question 1 in the "Female" column is "79"
	And the Weighted Average on the right for question 1 is "80"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.5"
	And the score for the "Clarity of Direction" category in the "Female" column is "86.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86.4"
	And the Group Average at the bottom for the "Male" column is "90.4"
	And the Group Average at the bottom for the "Female" column is "90.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "79.9"
	And the score for question 1 in the "Female" column is "79.1"
	And the Weighted Average on the right for question 1 is "79.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.55"
	And the score for the "Clarity of Direction" category in the "Female" column is "86.05"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86.39"
	And the Group Average at the bottom for the "Male" column is "90.43"
	And the Group Average at the bottom for the "Female" column is "90.30"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "79.89"
	And the score for question 1 in the "Female" column is "79.07"
	And the Weighted Average on the right for question 1 is "79.63"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8"
	And the score for the "Clarity of Direction" category in the "Female" column is "8"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "8"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "-1"
	And the Weighted Average on the right for question 1 is "1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.6"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.2"
	And the Group Average at the bottom for the "Male" column is "9.1"
	And the Group Average at the bottom for the "Female" column is "8.5"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.4"
	And the score for question 1 in the "Female" column is "-0.8"
	And the Weighted Average on the right for question 1 is "1.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8.33"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.60"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.25"
	And the Group Average at the bottom for the "Male" column is "9.13"
	And the Group Average at the bottom for the "Female" column is "8.48"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.41"
	And the score for question 1 in the "Female" column is "-0.80"
	And the Weighted Average on the right for question 1 is "1.02"



# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1 (4.2)"
	And the score for question 1 in the "Female" column is "4.0 (4.0)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "3.9 (4.0)"
	And the Group Average at the bottom for the "Female" column is "4.0 (3.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.08 (4.16)"
	And the score for question 1 in the "Female" column is "4.05 (3.99)"
	And the Weighted Average on the right for question 1 is "4.07"
	And the Group Average at the bottom for the "Male" column is "3.93 (3.97)"
	And the Group Average at the bottom for the "Female" column is "3.96 (3.88)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83 (86)"
	And the score for question 1 in the "Female" column is "85 (82)"
	And the Weighted Average on the right for question 1 is "84"
	And the Group Average at the bottom for the "Male" column is "75 (77)"
	And the Group Average at the bottom for the "Female" column is "76 (74)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.2 (86.5)"
	And the score for question 1 in the "Female" column is "84.9 (81.9)"
	And the Weighted Average on the right for question 1 is "83.7"
	And the Group Average at the bottom for the "Male" column is "75.3 (77.1)"
	And the Group Average at the bottom for the "Female" column is "75.7 (73.6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.15 (86.49)"
	And the score for question 1 in the "Female" column is "84.88 (81.91)"
	And the Weighted Average on the right for question 1 is "83.70"
	And the Group Average at the bottom for the "Male" column is "75.26 (77.11)"
	And the Group Average at the bottom for the "Female" column is "75.69 (73.58)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5 (4)"
	And the score for question 1 in the "Female" column is "5 (3)"
	And the Weighted Average on the right for question 1 is "5"
	And the Group Average at the bottom for the "Male" column is "9 (7)"
	And the Group Average at the bottom for the "Female" column is "7 (7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.4 (3.6)"
	And the score for question 1 in the "Female" column is "4.7 (3.2)"
	And the Weighted Average on the right for question 1 is "5.2"
	And the Group Average at the bottom for the "Male" column is "8.7 (7.4)"
	And the Group Average at the bottom for the "Female" column is "6.6 (6.7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.43 (3.60)"
	And the score for question 1 in the "Female" column is "4.65 (3.19)"
	And the Weighted Average on the right for question 1 is "5.19"
	And the Group Average at the bottom for the "Male" column is "8.73 (7.41)"
	And the Group Average at the bottom for the "Female" column is "6.58 (6.69)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.0 (4.0)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.4 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.4 (4.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.10 (4.08)"
	And the score for question 1 in the "Female" column is "4.01 (4.03)"
	And the Weighted Average on the right for question 1 is "4.07"
	And the Group Average at the bottom for the "Male" column is "4.35 (4.28)"
	And the Group Average at the bottom for the "Female" column is "4.35 (4.25)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80 (81)"
	And the score for question 1 in the "Female" column is "79 (82)"
	And the Weighted Average on the right for question 1 is "80"
	And the Group Average at the bottom for the "Male" column is "90 (89)"
	And the Group Average at the bottom for the "Female" column is "90 (88)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "79.9 (80.6)"
	And the score for question 1 in the "Female" column is "79.1 (81.9)"
	And the Weighted Average on the right for question 1 is "79.6"
	And the Group Average at the bottom for the "Male" column is "90.4 (89.0)"
	And the Group Average at the bottom for the "Female" column is "90.3 (88.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "79.89 (80.63)"
	And the score for question 1 in the "Female" column is "79.07 (81.91)"
	And the Weighted Average on the right for question 1 is "79.63"
	And the Group Average at the bottom for the "Male" column is "90.43 (89.00)"
	And the Group Average at the bottom for the "Female" column is "90.30 (88.19)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0 (-2)"
	And the score for question 1 in the "Female" column is "-1 (1)"
	And the Weighted Average on the right for question 1 is "1"
	And the Group Average at the bottom for the "Male" column is "9 (7)"
	And the Group Average at the bottom for the "Female" column is "8 (8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.4 (-1.6)"
	And the score for question 1 in the "Female" column is "-0.8 (0.8)"
	And the Weighted Average on the right for question 1 is "1.0"
	And the Group Average at the bottom for the "Male" column is "9.1 (6.6)"
	And the Group Average at the bottom for the "Female" column is "8.5 (7.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.41 (-1.63)"
	And the score for question 1 in the "Female" column is "-0.80 (0.81)"
	And the Weighted Average on the right for question 1 is "1.02"
	And the Group Average at the bottom for the "Male" column is "9.13 (6.63)"
	And the Group Average at the bottom for the "Female" column is "8.48 (7.86)"


# Categories + trend
@wip
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9 (4.0)"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.9 (3.8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.9"
	And the Group Average at the bottom for the "Male" column is "3.9 (4.0)"
	And the Group Average at the bottom for the "Female" column is "4.0 (3.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1 (4.2)"
	And the score for question 1 in the "Female" column is "4.0 (4.0)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.88 (4.03)"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.86 (3.84)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.88"
	And the Group Average at the bottom for the "Male" column is "3.93 (3.97)"
	And the Group Average at the bottom for the "Female" column is "3.96 (3.88)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.08 (4.16)"
	And the score for question 1 in the "Female" column is "4.05 (3.99)"
	And the Weighted Average on the right for question 1 is "4.07"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75 (81)"
	And the score for the "Clarity of Direction" category in the "Female" column is "75 (75)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "75"
	And the Group Average at the bottom for the "Male" column is "75 (77)"
	And the Group Average at the bottom for the "Female" column is "76 (74)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83 (86)"
	And the score for question 1 in the "Female" column is "85 (82)"
	And the Weighted Average on the right for question 1 is "84"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "74.6 (80.7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "74.7 (74.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "74.6"
	And the Group Average at the bottom for the "Male" column is "75.3 (77.1)"
	And the Group Average at the bottom for the "Female" column is "75.7 (73.6)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.2 (86.5)"
	And the score for question 1 in the "Female" column is "84.9 (81.9)"
	And the Weighted Average on the right for question 1 is "83.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "74.59 (80.74)"
	And the score for the "Clarity of Direction" category in the "Female" column is "74.71 (74.73)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "74.63"
	And the Group Average at the bottom for the "Male" column is "75.26 (77.11)"
	And the Group Average at the bottom for the "Female" column is "75.69 (73.58)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.15 (86.49)"
	And the score for question 1 in the "Female" column is "84.88 (81.91)"
	And the Weighted Average on the right for question 1 is "83.70"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9 (7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "8 (7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Male" column is "9 (7)"
	And the Group Average at the bottom for the "Female" column is "7 (7)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5 (4)"
	And the score for question 1 in the "Female" column is "5 (3)"
	And the Weighted Average on the right for question 1 is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.2 (6.5)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.6 (7.4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.7"
	And the Group Average at the bottom for the "Male" column is "8.7 (7.4)"
	And the Group Average at the bottom for the "Female" column is "6.6 (6.7)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.4 (3.6)"
	And the score for question 1 in the "Female" column is "4.7 (3.2)"
	And the Weighted Average on the right for question 1 is "5.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.24 (6.53)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.56 (7.45)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.70"
	And the Group Average at the bottom for the "Male" column is "8.73 (7.41)"
	And the Group Average at the bottom for the "Female" column is "6.58 (6.69)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.43 (3.60)"
	And the score for question 1 in the "Female" column is "4.65 (3.19)"
	And the Weighted Average on the right for question 1 is "5.19"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.3 (4.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.2 (4.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.3"
	And the Group Average at the bottom for the "Male" column is "4.4 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.4 (4.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.0 (4.0)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.27 (4.23)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.22 (4.14)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.25"
	And the Group Average at the bottom for the "Male" column is "4.35 (4.28)"
	And the Group Average at the bottom for the "Female" column is "4.35 (4.25)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.10 (4.08)"
	And the score for question 1 in the "Female" column is "4.01 (4.03)"
	And the Weighted Average on the right for question 1 is "4.07"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87 (87)"
	And the score for the "Clarity of Direction" category in the "Female" column is "86 (87)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86"
	And the Group Average at the bottom for the "Male" column is "90 (89)"
	And the Group Average at the bottom for the "Female" column is "90 (88)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80 (81)"
	And the score for question 1 in the "Female" column is "79 (82)"
	And the Weighted Average on the right for question 1 is "80"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.5 (86.9)"
	And the score for the "Clarity of Direction" category in the "Female" column is "86.0 (86.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86.4"
	And the Group Average at the bottom for the "Male" column is "90.4 (89.0)"
	And the Group Average at the bottom for the "Female" column is "90.3 (88.2)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "79.9 (80.6)"
	And the score for question 1 in the "Female" column is "79.1 (81.9)"
	And the Weighted Average on the right for question 1 is "79.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.55 (86.94)"
	And the score for the "Clarity of Direction" category in the "Female" column is "86.05 (86.70)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86.39"
	And the Group Average at the bottom for the "Male" column is "90.43 (89.00)"
	And the Group Average at the bottom for the "Female" column is "90.30 (88.19)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "79.89 (80.63)"
	And the score for question 1 in the "Female" column is "79.07 (81.91)"
	And the Weighted Average on the right for question 1 is "79.63"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "8 (6)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10"
	And the Group Average at the bottom for the "Male" column is "9 (7)"
	And the Group Average at the bottom for the "Female" column is "8 (8)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0 (-2)"
	And the score for question 1 in the "Female" column is "-1 (1)"
	And the Weighted Average on the right for question 1 is "1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8.3 (4.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.6 (6.2)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.2"
	And the Group Average at the bottom for the "Male" column is "9.1 (6.6)"
	And the Group Average at the bottom for the "Female" column is "8.5 (7.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.4 (-1.6)"
	And the score for question 1 in the "Female" column is "-0.8 (0.8)"
	And the Weighted Average on the right for question 1 is "1.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8.33 (4.23)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7.60 (6.21)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.25"
	And the Group Average at the bottom for the "Male" column is "9.13 (6.63)"
	And the Group Average at the bottom for the "Female" column is "8.48 (7.86)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.41 (-1.63)"
	And the score for question 1 in the "Female" column is "-0.80 (0.81)"
	And the Weighted Average on the right for question 1 is "1.02"



	Now I go to "Functions" > "Logout"