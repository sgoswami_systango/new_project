Feature: Check filtered Favorability Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter" at the top right is "(276)"
	And the Favorable score for question 1 is "83.7%"
	And the Neutral score for question 1 is "11.1%"
	And the Unfavorable score for question 1 is "5.2%"
	And the Overall Average at the bottom has a Favorable score of "75.4%"
	And the Overall Average Neutral score is "16.5%"
	And the Overall Average Unfavorable score is "8.1%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "74.6%"
	And the Neutral score for the "Clarity of Direction" category is "16.7%"
	And the Unfavorable score for the "Clarity of Direction" category is "8.7%"
	And the Overall Average at the bottom has a Favorable score of "75.4%"
	And the Overall Average Neutral score is "16.5%"
	And the Overall Average Unfavorable score is "8.1%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score for question 1 is "83.7%"
	And the Neutral score for question 1 is "11.1%"
	And the Unfavorable score for question 1 is "5.2%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the number for "Filter" is "(276)"
	And the number for "Organization" is "(188)"
	And the Favorable score on top for question 1 is "83.7%"
	And the Neutral score on top for question 1 is "11.1%"
	And the Unfavorable score on top for question 1 is "5.2%"
	And the Favorable score on bottom for question 1 is "83.3%"
	And the Neutral score on bottom for question 1 is "10.8%"
	And the Unfavorable score on bottom for question 1 is "5.9%"
	And the Overall Average at the bottom Favorable score on top is "75.4%"
	And the Overall Average Neutral score on top is "16.5%"
	And the Overall Average Unfavorable score on top is "8.1%"
	And the Overall Average Favorable score on bottom is "68.9%"
	And the Overall Average Neutral score on bottom is "17.9%"
	And the Overall Average Unfavorable score on bottom is "13.2%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the Favorable score on top for the "Clarity of Direction" category is "74.6%"
	And the Neutral score on top for the "Clarity of Direction" category is "16.7%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "8.7%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "65.6%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "19.0%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "15.5%"
	And the Overall Average at the bottom Favorable score on top is "75.4%"
	And the Overall Average Neutral score on top is "16.5%"
	And the Overall Average Unfavorable score on top is "8.1%"
	And the Overall Average Favorable score on bottom is "68.9%"
	And the Overall Average Neutral score on bottom is "17.9%"
	And the Overall Average Unfavorable score on bottom is "13.2%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score on top for question 1 is "83.7%"
	And the Neutral score on top for question 1 is "11.1%"
	And the Unfavorable score on top for question 1 is "5.2%"
	And the Favorable score on bottom for question 1 is "83.3%"
	And the Neutral score on bottom for question 1 is "10.8%"
	And the Unfavorable score on bottom for question 1 is "5.9%"
	
	Now I go to "Functions" > "Logout"
