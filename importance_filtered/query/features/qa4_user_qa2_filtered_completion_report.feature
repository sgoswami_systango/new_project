Feature: Check filtered Completion Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click on the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Male" in the "Invitees" column is "211"
	And the figure for "Male" in the "Respondents" column is "187(88.6%)"
	And the figure for "Male" in the "Completions" column is "182(86.3%)"
	And the figure for the "Totals" row in the "Invitees" column is "309"
	And the figure for the "Totals" row in the "Respondents" column is "276(89.3%)"
	And the figure for the "Totals" row in the "Completions" column is "265(85.8%)"

	When I am on the "By Time" tab
	Then the figure for "Monday, September 15, 2014" in the "Respondents" column is "30"
	And the figure for "Monday, September 15, 2014" in the "% Responded" column is "10.9%"
	And the figure for "Monday, September 15, 2014" in the "% Invited" column is "9.7%"
	And the figure for the "Totals" row in the "Respondents" column is "276"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "89.3%"
	
	When I click on "Monday, September 15, 2014"
	Then the figure for "08:00 PM" in the "Respondents" column is "1"
	And the figure for "08:00 PM" in the "% Responded" column is "0.4%"
	And the figure for "08:00 PM" in the "% Invited" column is "0.3%"
	
	Now I go to "Functions" > "Logout"