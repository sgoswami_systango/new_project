Feature: Check filtered Gap Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the Select a Demographic: popdown
	And select "Headquarters" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip 	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter Count" in the upper right is "276"
	And the Eval Avg for question 1 is "4.07"
	And the Importance for question 1 is "4.07"
	And the Gap for question 1 is "0.00"
	And the Weighted Gap for question 1 is "0.00"
	And the Overall Average at the bottom has an Eval Avg of "3.94"
	And the Overall Average has a Importance of "4.35"
	And the Overall Average has a Gap of "0.41"
	And the Overall Average has a Weighted Gap of "8.92"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.88"
	And the Importance for the "Clarity of Direction" category is "4.25"
	And the Gap for the "Clarity of Direction" category is "0.37"
	And the Weighted Gap for the "Clarity of Direction" category is "7.86"
	And the Overall Average at the bottom has an Eval Avg of "3.94"
	And the Overall Average has a Importance of "4.35"
	And the Overall Average has a Gap of "0.41"
	And the Overall Average has a Weighted Gap of "8.92"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.07"
	And the Importance for question 1 is "4.07"
	And the Gap for question 1 is "0.00"
	And the Weighted Gap for question 1 is "0.00"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the number for "Filter" in the upper right is "(276)"
	And the number for "Organization" in the upper right is "(188)"
	And the Eval Avg for question 1 is "4.07 (+0.05)"
	And the Importance for question 1 is "4.07 (-0.07)"
	And the Gap for question 1 is "0.00 (-0.12)"
	And the Weighted Gap for question 1 is "0.00 (-2.48)"
	And the Overall Average at the bottom has an Eval Avg of "3.94 (+0.15)"
	And the Overall Average has a Importance of "4.35 (+0.02)"
	And the Overall Average has a Gap of "0.41 (-0.13)"
	And the Overall Average has a Weighted Gap of "8.92 (-2.77)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown
	Then the Eval Avg for the "Clarity of Direction" category is "3.88 (+0.21)"
	And the Importance for the "Clarity of Direction" category is "4.25 (-0.06)"
	And the Gap for the "Clarity of Direction" category is "0.37 (-0.27)"
	And the Weighted Gap for the "Clarity of Direction" category is "7.86 (-5.93)"
	And the Overall Average at the bottom has an Eval Avg of "3.94 (+0.15)"
	And the Overall Average has a Importance of "4.35 (+0.02)"
	And the Overall Average has a Gap of "0.41 (-0.13)"
	And the Overall Average has a Weighted Gap of "8.92 (-2.77)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.07 (+0.05)"
	And the Importance for question 1 is "4.07 (-0.07)"
	And the Gap for question 1 is "0.00 (-0.12)"
	And the Weighted Gap for question 1 is "0.00 (-2.48)"
	
	Now I go to "Functions" > "Logout"