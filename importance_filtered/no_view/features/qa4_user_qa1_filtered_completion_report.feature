Feature: Check filtered Completion Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa1"
	And I click on the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Business Unit" from the Select a Demographic: popdown
	And select "Auto Physical Damage" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Male" in the "Invitees" column is "379"
	And the figure for "Male" in the "Respondents" column is "332(87.6%)"
	And the figure for "Male" in the "Completions" column is "324(85.5%)"
	And the figure for the "Totals" row in the "Invitees" column is "525"
	And the figure for the "Totals" row in the "Respondents" column is "464(88.4%)"
	And the figure for the "Totals" row in the "Completions" column is "449(85.5%)"

	When I am on the "By Time" tab
	Then the figure for "Monday, September 15, 2014" in the "Respondents" column is "66"
	And the figure for "Monday, September 15, 2014" in the "% Responded" column is "14.2%"
	And the figure for "Monday, September 15, 2014" in the "% Invited" column is "12.6%"
	And the figure for the "Totals" row in the "Respondents" column is "464"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "88.4%"
	
	When I click on "Monday, September 15, 2014"
	Then the figure for "08:00 PM" in the "Respondents" column is "9"
	And the figure for "08:00 PM" in the "% Responded" column is "1.9%"
	And the figure for "08:00 PM" in the "% Invited" column is "1.7%"
	
	Now I go to "Functions" > "Logout"