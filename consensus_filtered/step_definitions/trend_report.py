from utils import *


def add_space(s1, s2):
    text = ' '.join([s1, s2])
    return '' if text == ' ' else text


def get_value_for_element_and_column(element, column):
    if get_user() == 'qa1' or get_user() == 'qa2':
        if column == "2014":
            return add_space(element[0].text, element[1].text)
        if column == "2013":
            return add_space(element[2].text, element[3].text)
        if column == "2012":
            return element[4].text
    else:
        if column == "2014":
            return add_space(element[0].text, element[1].text)
        if column == "2013":
            return element[2].text


def get_score_for(row, column):
    browser = get_browser()
    rows = browser.find_by_css('.data.standalone')
    if row == "question 1":
        element = rows[0].find_by_css('td')
        data = get_value_for_element_and_column(element, column)
        if not data:
            element = browser.find_by_css('.data.sub')[0].find_by_css('td')
            data = get_value_for_element_and_column(element, column)
            if not data:
                element = rows[52].find_by_css('td')
                data = get_value_for_element_and_column(element, column)
                if not data:
                    element = browser.find_by_css(
                        '.data.sub')[66].find_by_css('td')
                    data = get_value_for_element_and_column(element, column)
        return data
    if row == "over_all":
        # import pdb; pdb.set_trace()
        element = rows[50].find_by_css('td')
        data = get_value_for_element_and_column(element, column)
        if not data:
            element = rows[51].find_by_css('td')
            data = get_value_for_element_and_column(element, column)
            if not data:
                element = rows[102].find_by_css('td')
                data = get_value_for_element_and_column(element, column)
                if not data:
                    element = rows[103].find_by_css('td')
                    data = get_value_for_element_and_column(element, column)
        return data
    if row == "Clarity of Direction":
        rows = browser.find_by_css('.data.category')
        element = rows[0].find_by_css('td')
        data = get_value_for_element_and_column(element, column)
        if not data:
            element = rows[12].find_by_css('td')
            data = get_value_for_element_and_column(element, column)
        return data


@step('I am on the "Trend Report" page')
def verify_page(step):
    time.sleep(2)
    assert get_browser().is_text_present("Trend Report")
    assert 'trend_report' in get_browser().url


@step(u'the score for "([^"]*)" in the "([^"]*)" column is "([^"]*)"')
def the_score_for_group1_in_the_group2_column_is_group3(step, group1, group2, group3):
    assert get_score_for(group1, group2) == group3


@step(u'the Overall Average for "([^"]*)" is "([^"]*)"')
def the_overall_average_for_group1_is_group2(step, group1, group2):
    assert get_score_for('over_all', group1) == group2


@step(u'the score for the "([^"]*)" category for "([^"]*)" is "([^"]*)"')
def the_score_for_the_group1_category_for_group2_is_group3(step, group1, group2, group3):
    assert get_score_for(group1, group2) == group3


@step(u'And I am on the "Favorability" tab')
def i_am_on_the_favorability(step):
    get_browser().find_link_by_partial_text('Favorability')[1].click()
    time.sleep(3)


@step(u'I am on the Categories tab')
def i_am_on_the_categories(step):
    get_browser().find_link_by_text('Categories')[1].click()
    time.sleep(3)


@step(u'the Overall Average at the bottom for "([^"]*)" is "([^"]*)"')
def the_overall_average_at_the_bottom_for_group1_is_group2(step, group1, group2):
    assert get_score_for('over_all', group1) == group2


@step(u'the score for the "([^"]*)" category for "([^"]*)" is "([^"]*)"')
def the_score_for_the_group1_category_for_group2_is_group3(step, group1, group2, group3):
    assert get_score_for(group1, group2) == group3


@step(u'the Count for "([^"]*)" is "([^"]*)"')
def the_count_for_group1_is_group2(step, group1, group2):
    rows = get_browser().find_by_css('.report-table .data .value.count')
    if get_tab_link() != 'Categories':
        if group1 == '2014':
            if get_user() == 'qa3' or get_user() == 'qa4' or get_user() == 'qa5':
                if rows[0].text:
                    value = rows[0].text
                else:
                    value = rows[4].text
            else:
                if rows[0].text:
                    value = rows[0].text
                else:
                    value = rows[6].text
        elif group1 == '2013':
            if get_user() == 'qa3' or get_user() == 'qa4' or get_user() == 'qa5':
                if rows[1].text:
                    value = rows[1].text
                else:
                    value = rows[5].text

            else:
                if rows[1].text:
                    value = rows[1].text
                else:
                    value = rows[7].text
        else:
            if rows[2].text:
                value = rows[2].text
            else:
                value = rows[8].text
    else:
        if group1 == '2014':
            if get_user() == 'qa3' or get_user() == 'qa4' or get_user() == 'qa5':
                if rows[2].text:
                    value = rows[2].text
                else:
                    value = rows[6].text
            else:
                if rows[3].text:
                    value = rows[3].text
                else:
                    value = rows[9].text
        elif group1 == '2013':
            if get_user() == 'qa3' or get_user() == 'qa4' or get_user() == 'qa5':
                if rows[3].text:
                    value = rows[3].text
                else:
                    value = rows[7].text
            else:
                if rows[4].text:
                    value = rows[4].text
                else:
                    value = rows[10].text
        else:
            if rows[5].text:
                value = rows[5].text
            else:
                value = rows[11].text
    assert value == group2


@step(u'I click on the "Clarity of Direction" category')
def i_click_on_the_category(step):
    elements = get_browser().find_by_css('.sort-name.name.average')
    if elements[1].text:
        elements[1].click()
    elif elements[81].text:
        elements[81].click()
