from utils import *


def get_consensus_text_for_questions_On_Compared_to_tab(id_number):

    value = get_browser().find_by_css(
        '.%s.qid-00_01 .value .calc-value' % id_number)[5].text
    if value:
        return get_browser().find_by_css('.%s.qid-00_01 .value .calc-value' % id_number)[
            5].text + ' (' + get_browser().find_by_css(".%s.qid-00_01 .comparison .number" % id_number)[1].text + ')'
    else:
        return get_browser().find_by_css('.%s.qid-00_01 .value .calc-value' % id_number)[
            9].text + ' (' + get_browser().find_by_css(".%s.qid-00_01 .comparison .number" % id_number)[5].text + ')'


def get_eval_avg_text_for_question_On_Compared_to_tab(id_number):
    value = get_browser().find_by_css(
        '.%s.qid-00_01 .value .calc-value' % id_number)[4].text
    if value:
        return get_browser().find_by_css('.%s.qid-00_01 .value .calc-value' % id_number)[
            4].text + ' (' + get_browser().find_by_css(".%s.qid-00_01 .comparison .number" % id_number)[0].text + ')'
    else:
        return get_browser().find_by_css('.%s.qid-00_01 .value .calc-value' % id_number)[
            8].text + ' (' + get_browser().find_by_css(".%s.qid-00_01 .comparison .number" % id_number)[4].text + ')'


def get_gap_score_text_for_questions_On_Compared_to_tab(id_number):
    value = get_browser().find_by_css(
        '.%s.qid-00_01 .value .calc-value' % id_number)[6].text
    if value:
        return get_browser().find_by_css('.%s.qid-00_01 .value .calc-value' % id_number)[
            6].text + ' (' + get_browser().find_by_css(".%s.qid-00_01 .comparison .number" % id_number)[2].text + ')'
    else:
        return get_browser().find_by_css('.%s.qid-00_01 .value .calc-value' % id_number)[
            10].text + ' (' + get_browser().find_by_css(".%s.qid-00_01 .comparison .number" % id_number)[6].text + ')'


def get_weighted_gap_text_for_questions_On_Compared_to_tab(id_number):
    value = get_browser().find_by_css(
        '.%s.qid-00_01 .value .calc-value' % id_number)[7].text
    if value:
        return get_browser().find_by_css('.%s.qid-00_01 .value .calc-value' % id_number)[
            7].text + ' (' + get_browser().find_by_css(".%s.qid-00_01 .comparison .number" % id_number)[3].text + ')'
    else:
        return get_browser().find_by_css('.%s.qid-00_01 .value .calc-value' % id_number)[
            11].text + ' (' + get_browser().find_by_css(".%s.qid-00_01 .comparison .number" % id_number)[7].text + ')'


def get_eval_avg_text_for_question(id_number):
    if get_tab_link() != 'Categories' or id_number != '0_01':
        return get_browser().find_by_css(
            '.data.standalone.qid-0%s .value.centred  .sort-eval.number.calc-value' % id_number).first.text
    else:
        return get_browser().find_by_css(
            '.data.sub.qid-0%s .value.centred .sort-eval.number.calc-value' % id_number).first.text


def get_consensus_text_for_questions(id_number):
    if get_tab_link() != 'Categories' or id_number != '0_01':
        return get_browser().find_by_css('.data.standalone.qid-0%s .value.centred  .sort-imp.number.calc-value' % id_number).first.text
    else:
        return get_browser().find_by_css(
            '.data.sub.qid-0%s .value.centred .sort-imp.number.calc-value' % id_number).first.text


def get_gap_score_text_for_questions(id_number):
    if get_tab_link() != 'Categories' or id_number != '0_01':
        return get_browser().find_by_css('.data.standalone.qid-0%s .value.centred  .sort-gap.number.calc-value' % id_number).first.text
    else:
        return get_browser().find_by_css(
            '.data.sub.qid-0%s .value.centred .sort-gap.number.calc-value' % id_number).first.text


def get_weighted_gap_text_for_questions(id_number):
    if get_tab_link() != 'Categories' or id_number != '0_01':
        return get_browser().find_by_css('.data.standalone.qid-0%s .value.centred  .sort-wgi.number.calc-value' % id_number).first.text
    elif get_clarity_of_direction_checked_value() == False:
        return get_browser().find_by_css('.data.category  .sort-wgi.number.calc-value').first.text
    else:
        return get_browser().find_by_css('.data.sub.qid-0%s .value.centred  .sort-wgi.number.calc-value' % id_number).first.text


@step('I am on the "Gap Report" page')
def verify_page(step):
    time.sleep(2)
    assert get_browser().is_text_present("Gap Report")
    assert 'gap_report' in get_browser().url


@step(u'the Eval Avg for question 1 is "([^"]*)"')
def the_eval_avg_for_question_1_is_group1(step, group1):
    value = get_eval_avg_text_for_question('0_01')
    if value:
        assert value == group1

    elif get_clarity_of_direction_checked_value() == False:
        value = get_eval_avg_text_for_question_On_Compared_to_tab('standalone')
        assert value == group1
    else:
        value = get_eval_avg_text_for_question_On_Compared_to_tab('sub')

        if value:
            assert value == group1
        else:
            element = get_browser().find_by_css("sub qid-00_01")[2]
            value = element.find_by_css(
                '.value')[0].text + ' ' + element.find_by_css('.comp-eval')[0].text
            assert value == group1


@step(u'the Consensus for question 1 is "([^"]*)"')
def the_consensus_for_question_1_is_group1(step, group1):
    value = get_consensus_text_for_questions("0_01")
    if value:
        pass
    elif get_clarity_of_direction_checked_value() == False:
        value = get_consensus_text_for_questions_On_Compared_to_tab(
            'standalone')
    else:
        value = get_consensus_text_for_questions_On_Compared_to_tab('sub')
    assert value == group1


@step(u'the Gap for question 1 is "([^"]*)"')
def the_gap_for_question_1_is_group1(step, group1):
    value = get_gap_score_text_for_questions("0_01")
    if value:
        pass
    elif get_clarity_of_direction_checked_value() == False:
        value = get_gap_score_text_for_questions_On_Compared_to_tab(
            'standalone')
    else:
        value = get_gap_score_text_for_questions_On_Compared_to_tab('sub')
    assert value == group1


@step(u'the Weighted Gap for question 1 is "([^"]*)"')
def the_weighted_gap_for_question_1_is_group1(step, group1):
    value = get_weighted_gap_text_for_questions("0_01")
    if value:
        assert value == group1
    elif get_clarity_of_direction_checked_value() == False:
        value = get_weighted_gap_text_for_questions_On_Compared_to_tab(
            "standalone")
    else:
        value = get_weighted_gap_text_for_questions_On_Compared_to_tab('sub')
    assert value == group1


@step(u'the Overall Average at the bottom has an Eval Avg of "([^"]*)"')
def the_overall_average_has_a_eval_avg_of_group1(step, group1):
    value = get_eval_avg_text_for_question("4_07.average")
    if value:
        assert value == group1
    else:
        value = get_eval_avg_text_for_question("3_04.average")
        if value:
            assert value == group1


@step(u'the Overall Average has a Consensus of "([^"]*)"')
def the_overall_average_has_a_consensus_of_group1(step, group1):
    value = get_consensus_text_for_questions("4_07.average")
    if value:
        assert value == group1
    else:
        value = get_consensus_text_for_questions("3_04.average")
        if value:
            assert value == group1


@step(u'the Overall Average has a Gap of "([^"]*)"')
def the_overall_average_has_a_gap_of_group1(step, group1):
    value = get_gap_score_text_for_questions("4_07.average")
    if value:
        assert value == group1
    else:
        value = get_gap_score_text_for_questions("3_04.average")
        if value:
            assert value == group1


@step(u'the Overall Average has a Weighted Gap of "([^"]*)"')
def the_overall_average_has_a_weighted_gap_of_group1(step, group1):
    value = get_weighted_gap_text_for_questions("4_07.average")
    if value:
        assert value == group1
    else:
        value = get_weighted_gap_text_for_questions("3_04.average")
        if value:
            assert value == group1


@step(u'the Eval Avg for the Clarity of Direction category is "([^"]*)"')
def the_eval_avg_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.qid-1 .value.centred .sort-eval.number.calc-value').first.text
    if value:
        assert value == group1
    else:
        element = get_browser().find_by_css('.data.category.qid-1')[1]
        value = element.find_by_css(
            '.value')[0].text + ' ' + element.find_by_css('.comp-eval')[0].text
        assert value == group1


@step(u'the Consensus for the Clarity of Direction category is "([^"]*)"')
def the_consensus_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.qid-1 .value.centred .sort-imp.number.calc-value').first.text
    if value:
        assert value == group1
    else:
        element = get_browser().find_by_css('.data.category.qid-1')[1]
        value = element.find_by_css(
            '.value')[1].text + ' ' + element.find_by_css('.comp-imp')[0].text
        assert value == group1


@step(u'the Gap for the Clarity of Direction category is "([^"]*)"')
def the_gap_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.qid-1 .value.centred .sort-gap.number.calc-value').first.text
    if value:
        assert value == group1
    else:
        element = get_browser().find_by_css('.data.category.qid-1')[1]
        value = element.find_by_css(
            '.value')[2].text + ' ' + element.find_by_css('.comp-gap')[0].text
        assert value == group1


@step(u'the Weighted Gap for the Clarity of Direction category is "([^"]*)"')
def the_weighted_gap_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.qid-1 .value.centred .sort-wgi.number.calc-value').first.text
    if value:
        assert value == group1
    else:
        element = get_browser().find_by_css('.data.category.qid-1')[1]
        value = element.find_by_css(
            '.value')[3].text + ' ' + element.find_by_css('.comp-wgi')[0].text
        assert value == group1


@step(u'the Eval Avg for question 2 is "([^"]*)"')
def the_eval_avg_for_question_2_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.sub.qid-00_02 .value.centred .sort-eval.number.calc-value').first.text
    assert value == group1


@step(u'the Consensus for question 2 is "([^"]*)"')
def the_consensus_for_question_2_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.sub.qid-00_02 .value.centred .sort-imp.number.calc-value').first.text
    assert value == group1


@step(u'the Gap for question 2 is "([^"]*)"')
def the_gap_for_question_2_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.sub.qid-00_02 .value.centred .sort-gap.number.calc-value').first.text
    assert value == group1


@step(u'the Weighted Gap for question 2 is "([^"]*)"')
def the_weighted_gap_for_question_2_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.sub.qid-00_02 .value.centred .sort-wgi.number.calc-value').first.text
    assert value == group1


@step(u'the Eval Avg for question 3 is "([^"]*)"')
def the_eval_avg_for_question_3_is_group1(step, group1):
    element = get_browser().find_by_css('.data.standalone.qid-00_03')[1]
    value = element.find_by_css(
        '.value')[0].text + ' ' + element.find_by_css('.comp-eval')[0].text
    assert value == group1


@step(u'the Consensus for question 3 is "([^"]*)"')
def the_consensus_for_question_3_is_group1(step, group1):
    element = get_browser().find_by_css('.data.standalone.qid-00_03')[1]
    value = element.find_by_css(
        '.value')[1].text + ' ' + element.find_by_css('.comp-imp')[0].text
    assert value == group1


@step(u'the Gap for question 3 is "([^"]*)"')
def the_gap_for_question_3_is_group1(step, group1):
    element = get_browser().find_by_css('.data.standalone.qid-00_03')[1]
    value = element.find_by_css(
        '.value')[2].text + ' ' + element.find_by_css('.comp-gap')[0].text
    assert value == group1


@step(u'the Weighted Gap for question 3 is "([^"]*)"')
def the_weighted_gap_for_question_3_is_group1(step, group1):
    element = get_browser().find_by_css('.data.standalone.qid-00_03')[1]
    value = element.find_by_css(
        '.value')[3].text + ' ' + element.find_by_css('.comp-wgi')[0].text
    assert value == group1


@step(u'the Eval Avg for question 4 is "([^"]*)"')
def the_eval_avg_for_question_4_is_group1(step, group1):
    element = get_browser().find_by_css('.data.sub.qid-00_04')[2]
    value = element.find_by_css(
        '.value')[0].text + ' ' + element.find_by_css('.comp-eval')[0].text
    assert value == group1


@step(u'the Consensus for question 4 is "([^"]*)"')
def the_consensus_for_question_4_is_group1(step, group1):
    element = get_browser().find_by_css('.data.sub.qid-00_04')[2]
    value = element.find_by_css(
        '.value')[1].text + ' ' + element.find_by_css('.comp-imp')[0].text
    assert value == group1


@step(u'the Gap for question 4 is "([^"]*)"')
def the_gap_for_question_4_is_group1(step, group1):
    element = get_browser().find_by_css('.data.sub.qid-00_04')[2]
    value = element.find_by_css(
        '.value')[2].text + ' ' + element.find_by_css('.comp-gap')[0].text
    assert value == group1


@step(u'the Weighted Gap for question 4 is "([^"]*)"')
def the_weighted_gap_for_question_4_is_group1(step, group1):
    element = get_browser().find_by_css('.data.sub.qid-00_04')[2]
    value = element.find_by_css(
        '.value')[3].text + ' ' + element.find_by_css('.comp-wgi')[0].text
    assert value == group1


@step(u'the Eval Avg for the "Clarity of Direction" category is "([^"]*)"')
def the_eval_avg_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.qid-1 .value.centred .sort-eval.number.calc-value').first.text
    if value:
        assert value == group1
    else:
        elements = get_browser().find_by_css('.data.category.qid-1')
        if elements[1].text:
            value = elements[1].find_by_css(
                '.value')[0].text + ' ' + elements[1].find_by_css('.comp-eval')[0].text
            assert value == group1
        else:
            value = elements[2].find_by_css(
                '.value')[0].text + ' ' + elements[2].find_by_css('.comp-eval')[0].text
            assert value == group1


@step(u'the Consensus for the "Clarity of Direction" category is "([^"]*)"')
def the_consensus_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.qid-1 .value.centred .sort-imp.number.calc-value').first.text
    if value:
        assert value == group1
    else:
        elements = get_browser().find_by_css('.data.category.qid-1')
        if elements[1].text:
            value = elements[1].find_by_css(
                '.value')[1].text + ' ' + elements[1].find_by_css('.comp-imp')[0].text
            assert value == group1
        else:
            value = elements[2].find_by_css(
                '.value')[1].text + ' ' + elements[2].find_by_css('.comp-imp')[0].text
            assert value == group1


@step(u'the Gap for the "Clarity of Direction" category is "([^"]*)"')
def the_gap_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.qid-1 .value.centred .sort-gap.number.calc-value').first.text
    if value:
        assert value == group1
    else:
        elements = get_browser().find_by_css('.data.category.qid-1')
        if elements[1].text:
            value = elements[1].find_by_css(
                '.value')[2].text + ' ' + elements[1].find_by_css('.comp-gap')[0].text
            assert value == group1
        else:
            value = elements[2].find_by_css(
                '.value')[2].text + ' ' + elements[2].find_by_css('.comp-gap')[0].text
            assert value == group1


@step(u'the Weighted Gap for the "Clarity of Direction" category is "([^"]*)"')
def the_weighted_gap_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.qid-1 .value.centred .sort-wgi.number.calc-value').first.text
    if value:
        assert value == group1
    else:
        elements = get_browser().find_by_css('.data.category.qid-1')
        if elements[1].text:
            value = elements[1].find_by_css(
                '.value')[3].text + ' ' + elements[1].find_by_css('.comp-wgi')[0].text
            assert value == group1
        else:
            value = elements[2].find_by_css(
                '.value')[3].text + ' ' + elements[2].find_by_css('.comp-wgi')[0].text
            assert value == group1
