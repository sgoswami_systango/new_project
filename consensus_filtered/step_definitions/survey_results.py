from utils import *


def get_value_for_element_and_column(row, column):
    columns = row.find_by_css('.value')
    text = ''
    if column == "Number":
        text = columns[0].text
    if column == "Percent":
        text = columns[1].text
    return ''.join(text.splitlines())


def get_figure_under_question_1_for(row, column):
    browser = get_browser()
    rows = browser.find_by_css('.report-table .data')
    if row == "1. Strongly Disagree":
        return get_value_for_element_and_column(rows[0], column)


def get_value_for_element_and_column(row, column):
    columns = row.find_by_css('.value')
    text = ''
    if column == "Number":
        text = columns[0].text
    if column == "Percent":
        text = columns[1].text
    if column == "Segment Density":
        text = columns[2].text
    return ''.join(text.splitlines())


def get_figure_under_question_1_for(row, column):
    browser = get_browser()
    if get_user() == 'qa1':
        rows = browser.find_by_css('.report-table .data')
    else:
        rows = browser.find_by_css('.cmp_by_no .report-table .data')
        if rows.text == '':
            rows = browser.find_by_css('.cmp_by_org .report-table .data')
            if rows:
                if rows.text == '':
                    rows = browser.find_by_css('.cmp_by_trend .report-table .data')
            else:
                rows = browser.find_by_css('.cmp_by_remainder .report-table .data')            
    if row == "1. Strongly Disagree":
        return get_value_for_element_and_column(rows[0], column)
    if row == "2. Disagree":
        return get_value_for_element_and_column(rows[1], column)
    if row == "3. Neither under Agree Nor Disagree":
        return get_value_for_element_and_column(rows[2], column)
    if row == "4. Agree":
        return get_value_for_element_and_column(rows[3], column)
    if row == "5. Strongly Agree":
        return get_value_for_element_and_column(rows[4], column)
    if row == "Mean":
        return get_value_for_element_and_column(rows[5], column)


@step('I am on the "Survey Results" page')
def verify_page(step):
    time.sleep(2)
    assert get_browser().is_text_present("Survey Results")
    assert 'survey_result' in get_browser().url
    time.sleep(2)

@step(u'the figure under question 1 for "([^"]*)" in the "([^"]*)" column is "([^"]*)"')
def the_figure_under_question_1_for_group1_in_the_group2_column_is_group3(step, group1, group2, group3):
    assert get_figure_under_question_1_for(group1, group2) == group3


@step(u'the "([^"]*)" under question 1 is "([^"]*)"')
def and_the_group1_under_question_1_is_group2(step, group1, group2):
    mean_row = get_browser().find_by_css('.report-table .data')[5]
    string = group1 + " " + group2
    if mean_row.find_by_css('.name').text:
        assert mean_row.find_by_css('.name').text == string
    else:
        mean_row = get_browser().find_by_css(
            '.cmp_by_trend .report-table .data')[5]
        assert mean_row.find_by_css('.name').text == string


@step(u'the figure for "([^"]*)" in the upper right is "([^"]*)"')
def the_figure_for_group1_in_the_upper_right_is_group2(step, group1, group2):
    if group1 == 'Filter':
        elements = get_browser().find_by_css('.legend .description')
        value = elements[0].find_by_tag('span').first.text[1:-1]
        assert value == group2
    else:
        elements = get_browser().find_by_css('.legend .description')
        value = elements[1].find_by_tag('span').first.text[1:-1]
        assert value == group2