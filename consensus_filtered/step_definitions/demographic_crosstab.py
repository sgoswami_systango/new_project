from utils import *


def get_weighted_average_on_the_right_for_question_1():
    if get_clarity_of_direction_checked_value() == True:
        for i in range(0, 30):
            try:
                data = get_browser().find_by_css(
                    '#DataTables_Table_%s .sub .w_average.value' % i).first.text
            except Exception, e:
                continue
            if data:
                return data
    else:
        # import pdb;pdb.set_trace()
        for i in range(0, 30):
            try:
                data = get_browser().find_by_css(
                    '#DataTables_Table_%s .data.standalone .w_average.value' % i).first.text
            except Exception, e:
                continue
            if data:
                return data


def get_the_score_for_the_clarity_of_direction_category_in_the_group1_column(id_number):
    if get_tab_link() != 'Categories':
        for i in range(0, 30):
            try:
                data = get_browser().find_by_css(
                    '#DataTables_Table_%s .data.standalone.average .%s' % (i, id_number)).first.text
            except Exception, e:
                continue
            if data:
                return data
    else:
        for i in range(0, 30):
            try:
                data = get_browser().find_by_css(
                    '#DataTables_Table_%s .category .%s' % (i, id_number)).first.text
            except Exception, e:
                continue
            if data:
                return data


def get_the_score_for_the_clarity_of_direction_category_in_the_female_column():

    if get_tab_link() != 'Categories':
        for i in range(0, 30):
            try:
                data = get_browser().find_by_css(
                    '#DataTables_Table_%s .data.standalone.average .v_1' % i).first.text
            except Exception, e:
                continue
            if data:
                return data
    else:
        for i in range(0, 30):
            try:
                data = get_browser().find_by_css(
                    '#DataTables_Table_%s .category .v_1' % i).first.text
            except Exception, e:
                continue
            if data:
                return data


def get_the_score_for_the_clarity_of_direction_category_in_the_male_column():
    if get_tab_link() != 'Categories':
        for i in range(0, 30):
            try:
                data = get_browser().find_by_css(
                    '#DataTables_Table_%s .data.standalone.average .v_2' % i).first.text
            except Exception, e:
                continue
            if data:
                return data
    else:
        for i in range(0, 30):
            try:
                data = get_browser().find_by_css(
                    '#DataTables_Table_%s .category .v_2' % i).first.text
            except Exception, e:
                continue
            if data:
                return data


def get_value_Group_Average_at_the_bottom_for_the_Female_column():
    for i in range(0, 30):
        try:
            data = get_browser().find_by_css(
                '#DataTables_Table_%s .data.standalone.average .v_1' % i).first.text
        except Exception, e:
            continue
        if data:
            return data


def get_value_Group_Average_at_the_bottom_for_the_Male_column():
    for i in range(0, 30):
        try:
            data = get_browser().find_by_css(
                '#DataTables_Table_%s .data.standalone.average .v_2' % i).first.text
        except Exception, e:
            continue
        if data:
            return data


def get_weighted_average_on_the_right_for_the_clarity_of_direction_category():
    for i in range(0, 30):
        try:
            data = get_browser().find_by_css(
                '#DataTables_Table_%s .category .w_average' % i).first.text
        except Exception, e:
            continue
        if data:
            return data


@step('I am on the "Demographic Crosstab" page')
def verify_page(step):
    time.sleep(2)
    assert get_browser().is_text_present("Demographic Crosstab")
    assert 'dcomp' in get_browser().url


@step(u'I select "([^"]*)" from the "Demographic Selection" popdown in the top left corner')
def i_select_gender_from_the_Demographic_Selection_popdown_in_the_top_left_corner(step, group1):
    elements = get_browser().find_by_name('resp_select').find_by_tag("option")
    for element in elements:
        if element.text == group1:
            element.click()
    # if group1 == 'Gender':
    #     element[8].click()
    # elif group1 == 'Business Group':
    #     element[3].click()


@step(u'select "([^"]*)" from the "Type of Score:" popdown in the top middle')
def select_evaluation_by_mean_form_the_type_of_score_popdown(step, group1):
    element = get_browser().find_by_name('score_type').find_by_tag("option")
    for i in range(0, 6):
        if element[i].text == group1:
            element[i].click()
    time.sleep(3)


@step(u'select "([^"]*)" from the "Number of Decimal Places:" popdown in the top middle')
def select_0_from_the_Number_of_Decimal_Places_popdown_in_the_top_middle(step, group1):
    element = get_browser().find_by_name(
        'score_precision').find_by_tag("option")
    element[int(group1)].click()
    time.sleep(3)


@step(u'the Weighted Average on the right for question 1 is "([^"]*)"')
def the_Weighted_Average_on_the_right_for_question_1_is_group1(step, group1):
    time.sleep(3)
    value = get_weighted_average_on_the_right_for_question_1()
    assert value == group1


@step(u'the Group Average at the bottom for the "([^"]*)" column is "([^"]*)"')
def the_Group_Average_at_the_bottom_for_the_Female_column_is_group1(step, group1, group2):
    time.sleep(3)
    if get_user() == 'qa5':
        if group1 == 'Male' or group1 == 'C&CS - Governance, Risk & Compliance' or group1 == 'Below Manager':
            value = get_value_Group_Average_at_the_bottom_for_the_Female_column()
        else:
            value = get_value_Group_Average_at_the_bottom_for_the_Male_column()
    else:    
        if group1 == 'Female' or group1 == 'C&CS - Governance, Risk & Compliance' or group1 == 'Below Manager':
            value = get_value_Group_Average_at_the_bottom_for_the_Female_column()
        elif group1 == 'Male' or group1 == 'Client Support Services':
            value = get_value_Group_Average_at_the_bottom_for_the_Male_column()
    assert value == group2


@step(u'the Group Average at the bottom for the "Male" column is "([^"]*)"')
def the_Group_Average_at_the_bottom_for_the_Male_column_is_group1(step, group1):
    value = get_value_Group_Average_at_the_bottom_for_the_Male_column()
    assert value == group1


@step(u'the score for the Clarity of Direction category in the Female column is "([^"]*)"')
def the_score_for_the_clarity_of_direction_category_in_the_female_column_is_group1(step, group1):
    value = get_the_score_for_the_clarity_of_direction_category_in_the_female_column()
    assert value == group1


@step(u'the score for the Clarity of Direction category in the Male column is "([^"]*)"')
def the_score_for_the_clarity_of_direction_category_in_the_male_column_is_group1(step, group1):
    value = get_the_score_for_the_clarity_of_direction_category_in_the_male_column()
    assert value == group1


@step(u'the Weighted Average on the right for the Clarity of Direction category is "([^"]*)"')
def the_weighted_average_on_the_right_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_weighted_average_on_the_right_for_the_clarity_of_direction_category()
    assert value == group1


@step(u'check "([^"]*)" in the top middle')
def check_group1_in_the_top_middle(step, group1):
    element = get_browser().find_by_name('crosstab_trend')[0]
    if element.checked == False:
        element.click()
    time.sleep(5)


@step(u'the score for the "Clarity of Direction" category in the "([^"]*)" column is "([^"]*)"')
def the_score_for_the_clarity_of_direction_category_in_the_female_column_is_group1(step, group1, group2):
    if get_user() == 'qa5':
        if group1 == 'Male' or group1 == 'C&CS - Governance, Risk & Compliance':
            value = get_the_score_for_the_clarity_of_direction_category_in_the_group1_column(
                'v_1')
            assert value == group2
        elif group1 == 'Male' or group1 == 'Client Support Services':
            value = get_the_score_for_the_clarity_of_direction_category_in_the_group1_column(
                'v_2')
            assert value == group2

    else:
        if group1 == 'Female' or group1 == 'C&CS - Governance, Risk & Compliance':
            value = get_the_score_for_the_clarity_of_direction_category_in_the_group1_column(
                'v_1')
            assert value == group2
        elif group1 == 'Male' or group1 == 'Client Support Services':
            value = get_the_score_for_the_clarity_of_direction_category_in_the_group1_column(
                'v_2')
            assert value == group2


@step(u'the Weighted Average on the right for the "Clarity of Direction" category is "([^"]*)"')
def the_weighted_average_on_the_right_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_weighted_average_on_the_right_for_the_clarity_of_direction_category()
    assert value == group1


@step(u'select "([^"]*)" from the "Type of Score:" popdown in the top middle')
def select_evaluation_by_mean_form_the_type_of_score_popdown(step, group1):
    element = get_browser().find_by_name('score_type').find_by_tag("option")
    for i in range(0, 6):
        if element[i].text == group1:
            element[i].click()
    time.sleep(3)


@step(u'select "([^"]*)" from the "Number of Decimal Places:" popdown in the top middle')
def select_0_from_the_Number_of_Decimal_Places_popdown_in_the_top_middle(step, group1):
    element = get_browser().find_by_name(
        'score_precision').find_by_tag("option")
    element[int(group1)].click()
    time.sleep(3)



@step(u'the first number under "([^"]*)" is "([^"]*)"')
def the_first_number_under_group1_is_group2(step, group1, group2):
    if get_user() != 'qa5':
        if group1 == 'Female' or group1 == 'Below Manager':
            elements = get_browser().find_by_css('.report-table.dataTable .header .count.v_1')
        else:
            elements = get_browser().find_by_css('.report-table.dataTable .header .count.v_2')
    else:
        if group1 == 'Male' or group1 == 'Below Manager':
            elements = get_browser().find_by_css('.report-table.dataTable .header .count.v_1')
        else:
            elements = get_browser().find_by_css('.report-table.dataTable .header .count.v_2')
    for i in range(0,16):
        if elements[i].text:
            assert elements[i].text == group2
            break