from utils import *


def get_favorable_score_for_question(id_number):
    # import pdb;pdb.set_trace()
    if get_user() == 'qa1'and get_filtered() == False:
        return get_browser().find_by_css('#fav-questions .data.standalone.%s .sub-bar .value' % id_number)[0].text
    else:
        if get_tab_link() != 'Categories' or id_number == 'q_last':
            return get_browser().find_by_css('#fav-questions .data.standalone.%s .sub-bar .value' % id_number)[0].text
        else:
            return get_browser().find_by_css('.data.sub.%s .sub-bar .value' % id_number)[0].text


def get_neutral_score_for_question(id_number):
    if get_user() == 'qa1'and get_filtered() == False:
        return get_browser().find_by_css('#fav-questions .data.standalone.%s .sub-bar .value' % id_number)[1].text
    else:
        if get_tab_link() != 'Categories' or id_number == 'q_last':
            return get_browser().find_by_css('#fav-questions .data.standalone.%s .sub-bar .value' % id_number)[1].text
        else:
            return get_browser().find_by_css('.data.sub.%s .sub-bar .value' % id_number)[1].text


def get_unfavorable_score_for_question(id_number):
    if get_user() == 'qa1'and get_filtered() == False:
        return get_browser().find_by_css('#fav-questions .data.standalone.%s .sub-bar .value' % id_number)[2].text
    else:
        if get_tab_link() != 'Categories' or id_number == 'q_last':
            return get_browser().find_by_css('#fav-questions .data.standalone.%s .sub-bar .value' % id_number)[2].text
        else:
            return get_browser().find_by_css('.data.sub.%s .sub-bar .value' % id_number)[2].text


@step('I am on the "Favorability Report" page')
def verify_page(step):
    time.sleep(2)
    assert get_browser().is_text_present("Favorability Report")
    assert 'fav_report' in get_browser().url


@step(u'the Favorable score for question 1 is "([^"]*)"')
def the_favorable_score_for_question_1_is_group1(step, group1):
    assert get_favorable_score_for_question("q_00_01") == group1


@step(u'the Neutral score for question 1 is "([^"]*)"')
def the_neutral_score_for_question_1_is_group1(step, group1):
    assert get_neutral_score_for_question("q_00_01") == group1


@step(u'the Unfavorable score for question 1 is "([^"]*)"')
def the_unfavorable_score_for_question_1_is_group1(step, group1):
    assert get_unfavorable_score_for_question("q_00_01") == group1


@step(u'the Overall Average at the bottom has a Favorable score of "([^"]*)"')
def the_overall_average_at_the_bottom_has_a_favorable_score_of_group1(step, group1):
    value = get_favorable_score_for_question("q_last")
    if value:
        assert value == group1
    else:
        value = get_browser().find_by_css(
            '#fav-categories .data.standalone.q_last .sub-bar .calc-value')[0].text
        assert value == group1


@step(u'the Overall Average Neutral score is "([^"]*)"')
def the_overall_average_neutral_score_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.standalone.average.q_last .calc-value')[1].text
    if value:
        assert value == group1
    else:
        value = get_neutral_score_for_question("q_last")
        if value:
            assert value == group1
        else:
            value = get_browser().find_by_css(
                '#fav-categories .data.standalone.q_last .sub-bar .calc-value')[1].text
            assert value == group1


@step(u'the Overall Average Unfavorable score is "([^"]*)"')
def the_overall_average_unfavorable_score_is_group1(step, group1):
    value = get_unfavorable_score_for_question("q_last")
    if value:
        assert value == group1
    else:
        value = get_browser().find_by_css(
            '#fav-categories .data.standalone.q_last .sub-bar .calc-value')[2].text
        assert value == group1


@step(u'the Favorable score for the Clarity of Direction category is "([^"]*)"')
def the_favorable_score_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.q_1 .sub-bar .calc-value')[0].text
    if value:
        assert value == group1


@step(u'the Neutral score for the Clarity of Direction category is "([^"]*)"')
def the_neutral_score_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.q_1 .sub-bar .calc-value')[1].text
    if value:
        assert value == group1


@step(u'the Unfavorable score for the Clarity of Direction category is "([^"]*)"')
def the_unfavorable_score_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.q_1 .sub-bar .calc-value')[2].text
    if value:
        assert value == group1


@step(u'I click on the Effective Management category')
def click_on_the_effective_management_category(step):
    elements = get_browser().find_by_css(
        '.data.category.q_2 .name.average .name')
    if elements[0].text:
        elements[0].click()
    elif elements[1].text:
        elements[1].click()


@step(u'the Favorable score for question 6 is "([^"]*)"')
def the_favorable_score_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.sub.q_00_08 .sub-bar .value')[0].text
    assert value == group1


@step(u'the Neutral score for question 6 is "([^"]*)"')
def the_neutral_score_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.sub.q_00_08 .sub-bar .value')[1].text
    assert value == group1


@step(u'the Unfavorable score for question 6 is "([^"]*)"')
def the_unfavorable_score_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.sub.q_00_08 .sub-bar .value')[2].text
    assert value == group1


@step(u'the Favorable score on top for question 1 is "([^"]*)"')
def the_favorable_score_on_top_for_question_1_is_group1(step, group1):
    if get_tab_link() != 'Categories':
        elements = get_browser().find_by_css(
            '.data.standalone.q_00_01 .sub-bar .value')
        if elements[3].text:
            value = elements[3].text
        elif elements[6].text:
            value = elements[6].text
        assert value == group1
    else:
        elements = get_browser().find_by_css(
            '.data.sub.q_00_01 .sub-bar .value')
        if elements[3].text:
            value = elements[3].text
        elif elements[6].text:
            value = elements[6].text
        assert value == group1


@step(u'the Neutral score on top for question 1 is "([^"]*)"')
def the_neutral_score_on_top_for_question_1_is_group1(step, group1):
    if get_tab_link() != 'Categories':
        elements = get_browser().find_by_css(
            '.data.standalone.q_00_01 .sub-bar .value')
        if elements[4].text:
            value = elements[4].text
        elif elements[7].text:
            value = elements[7].text
        assert value == group1
    else:
        elements = get_browser().find_by_css(
            '.data.sub.q_00_01 .sub-bar .value')
        if elements[4].text:
            value = elements[4].text
        elif elements[7].text:
            value = elements[7].text
        assert value == group1


@step(u'the Unfavorable score on top for question 1 is "([^"]*)"')
def the_unfavorable_score_on_top_for_question_1_is_group1(step, group1):
    if get_tab_link() != 'Categories':
        elements = get_browser().find_by_css(
            '.data.standalone.q_00_01 .sub-bar .value')
        if elements[5].text:
            value = elements[5].text
        elif elements[8].text:
            value = elements[8].text
        assert value == group1
    else:
        elements = get_browser().find_by_css(
            '.data.sub.q_00_01 .sub-bar .value')
        if elements[5].text:
            value = elements[5].text
        elif elements[8].text:
            value = elements[8].text
        assert value == group1


@step(u'the Favorable score on bottom for question 1 is "([^"]*)"')
def the_favorable_score_on_bottom_for_question_1_is_group1(step, group1):
    if get_tab_link() != 'Categories':
        elements = get_browser().find_by_css(
            '.data.standalone.q_00_01 .comparison')
        if elements[0].text:
            value = elements[0].text
        elif elements[3].text:
            value = elements[3].text
        assert value == group1
    else:
        elements = get_browser().find_by_css('.data.q_00_01 .comparison')
        if get_user() == 'qa5':
            if elements[2].text:
                value = elements[2].text
            elif elements[9].text:
                value = elements[9].text
        else:
            if elements[3].text:
                value = elements[3].text
            elif elements[9].text:
                value = elements[9].text
        assert value == group1


@step(u'the Neutral score on bottom for question 1 is "([^"]*)"')
def the_neutral_score_on_bottom_for_question_1_is_group1(step, group1):
    if get_tab_link() != 'Categories':
        elements = get_browser().find_by_css(
            '.data.standalone.q_00_01 .comparison')
        if elements[1].text:
            value = elements[1].text
        elif elements[4].text:
            value = elements[4].text

        assert value == group1
    else:
        elements = get_browser().find_by_css('.data.q_00_01 .comparison')
        if get_user() == 'qa5':
            if elements[3].text:
                value = elements[3].text
            elif elements[10].text:
                value = elements[10].text
        else:
            if elements[4].text:
                value = elements[4].text
            elif elements[10].text:
                value = elements[10].text
        assert value == group1


@step(u'the Unfavorable score on bottom for question 1 is "([^"]*)"')
def the_unfavorable_score_on_bottom_for_question_1_is_group1(step, group1):
    if get_tab_link() != 'Categories':
        elements = get_browser().find_by_css(
            '.data.standalone.q_00_01 .comparison')
        if elements[2].text:
            value = elements[2].text
        elif elements[5].text:
            value = elements[5].text

        assert value == group1
    else:
        elements = get_browser().find_by_css('.data.q_00_01 .comparison')
        if elements[5].text:
            value = elements[5].text
        elif elements[11].text:
            value = elements[11].text
        assert value == group1


@step(u'the Overall Average at the bottom Favorable score on top is "([^"]*)"')
def the_overall_average_at_the_bottom_favorable_score_on_top_is_group1(step, group1):
    elements = get_browser().find_by_css(
        '.data.standalone.q_last .calc-value')
    if elements[6].text:
        assert elements[6].text == group1
    else:
        value = get_browser().find_by_css(
            '.data.standalone.q_last .number')[12].text
        if value:
            assert value == group1
        elif elements[12].text:
            assert elements[12].text == group1


@step(u'And the Overall Average Neutral score on top is "([^"]*)"')
def and_the_overall_average_neutral_score_on_top_is_group1(step, group1):
    elements = get_browser().find_by_css(
        '.data.standalone.q_last .calc-value')
    if elements[7].text:
        assert elements[7].text == group1
    else:
        value = get_browser().find_by_css(
            '.data.standalone.q_last .number')[13].text
        if value:
            assert value == group1
        elif elements[13].text:
            assert elements[13].text == group1


@step(u'And the Overall Average Unfavorable score on top is "([^"]*)"')
def and_the_overall_average_unfavorable_score_on_top_is_group1(step, group1):
    elements = get_browser().find_by_css(
        '.data.standalone.q_last .calc-value')
    if elements[8].text:
        assert elements[8].text == group1
    else:
        value = get_browser().find_by_css(
            '.data.standalone.q_last .number')[14].text
        if value:
            assert value == group1
        elif elements[14].text:
            assert elements[14].text == group1


@step(u'And the Overall Average Favorable score on bottom is "([^"]*)"')
def and_the_overall_average_favorable_score_on_bottom_is_group1(step, group1):
    elements = get_browser().find_by_css(
        '.data.standalone.q_last .comparison')
    if elements[0].text:
        assert elements[0].text == group1
    else:
        value = get_browser().find_by_css(
            '.data.standalone.q_last .comparison')[3].text
        if value:
            assert value == group1
        elif elements[6].text:
            assert elements[6].text == group1


@step(u'And the Overall Average Neutral score on bottom is "([^"]*)"')
def and_the_overall_average_neutral_score_on_bottom_is_group1(step, group1):
    elements = get_browser().find_by_css(
        '.data.standalone.q_last .comparison')
    if elements[1].text:
        assert elements[1].text == group1
    else:
        value = get_browser().find_by_css(
            '.data.standalone.q_last .comparison')[4].text
        if value:
            assert value == group1
        elif elements[7].text:
            assert elements[7].text == group1


@step(u'And the Overall Average Unfavorable score on bottom is "([^"]*)"')
def and_the_overall_average_unfavorable_score_on_bottom_is_group1(step, group1):
    elements = get_browser().find_by_css(
        '.data.standalone.q_last .comparison')
    if elements[2].text:
        assert elements[2].text == group1
    else:
        value = get_browser().find_by_css(
            '.data.standalone.q_last .comparison')[5].text
        if value:
            assert value == group1
        elif elements[8].text:
            assert elements[8].text == group1


@step(u'the Favorable score on top for the "Clarity of Direction" category is "([^"]*)"')
def then_the_favorable_score_on_top_for_the_clarity_of_direction_category_is_group1(step, group1):
    elements = get_browser().find_by_css('.data.category.q_1 .calc-value')
    if elements[3].text:
        value = elements[3].text
    elif elements[6].text:
        value = elements[6].text
    assert value == group1


@step(u'the Neutral score on top for the "Clarity of Direction" category is "([^"]*)"')
def and_the_neutral_score_on_top_for_the_clarity_of_direction_category_is_group1(step, group1):
    elements = get_browser().find_by_css('.data.category.q_1 .calc-value')
    if elements[4].text:
        value = elements[4].text
    elif elements[7].text:
        value = elements[7].text
    assert value == group1


@step(u'the Unfavorable score on top for the "Clarity of Direction" category is "([^"]*)"')
def the_unfavorable_score_on_top_for_the_clarity_of_direction_category_is_group1(step, group1):
    elements = get_browser().find_by_css('.data.category.q_1 .calc-value')
    if elements[5].text:
        value = elements[5].text
    elif elements[8].text:
        value = elements[8].text
    assert value == group1


@step(u'the Favorable score on bottom for the "Clarity of Direction" category is "([^"]*)"')
def the_favorable_score_on_bottom_for_the_clarity_of_direction_category_is_group1(step, group1):
    elements = get_browser().find_by_css('.data.category.q_1 .comparison')
    if elements[0].text:
        value = elements[0].text
    elif elements[3].text:
        value = elements[3].text
    assert value == group1


@step(u'the Neutral score on bottom for the "Clarity of Direction" category is "([^"]*)"')
def the_neutral_score_on_bottom_for_the_clarity_of_direction_category_is_group1(step, group1):
    elements = get_browser().find_by_css('.data.category.q_1 .comparison')
    if elements[1].text:
        value = elements[1].text
    elif elements[4].text:
        value = elements[4].text
    assert value == group1


@step(u'the Unfavorable score on bottom for the "Clarity of Direction" category is "([^"]*)"')
def the_unfavorable_score_on_bottom_for_the_clarity_of_direction_category_is_group1(step, group1):
    elements = get_browser().find_by_css('.data.category.q_1 .comparison')
    if elements[2].text:
        value = elements[2].text
    elif elements[5].text:
        value = elements[5].text
    assert value == group1


@step(u'Then the Favorable score on top for question 6 is "([^"]*)"')
def then_the_favorable_score_on_top_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css('.data.q_00_08 .calc-value')[9].text
    assert value == group1


@step(u'And the Neutral score on top for question 6 is "([^"]*)"')
def and_the_neutral_score_on_top_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css('.data.q_00_08 .calc-value')[10].text
    assert value == group1


@step(u'And the Unfavorable score on top for question 6 is "([^"]*)"')
def and_the_unfavorable_score_on_top_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css('.data.q_00_08 .calc-value')[11].text
    assert value == group1


@step(u'And the Favorable score on bottom for question 6 is "([^"]*)"')
def and_the_favorable_score_on_bottom_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css('.data.q_00_08 .comparison')[3].text
    assert value == group1


@step(u'And the Neutral score on bottom for question 6 is "([^"]*)"')
def and_the_neutral_score_on_bottom_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css('.data.q_00_08 .comparison')[4].text
    assert value == group1


@step(u'And the Unfavorable score on bottom for question 6 is "([^"]*)"')
def and_the_unfavorable_score_on_bottom_for_question_6_is_group1(step, group1):
    value = get_browser().find_by_css('.data.q_00_08 .comparison')[5].text
    assert value == group1


@step(u'the Favorable score for the "Clarity of Direction" category is "([^"]*)"')
def the_favorable_score_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.q_1 .sub-bar .calc-value')[0].text
    if value:
        assert value == group1


@step(u'the Neutral score for the "Clarity of Direction" category is "([^"]*)"')
def the_neutral_score_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.q_1 .sub-bar .calc-value')[1].text
    if value:
        assert value == group1


@step(u'the Unfavorable score for the "Clarity of Direction" category is "([^"]*)"')
def the_unfavorable_score_for_the_clarity_of_direction_category_is_group1(step, group1):
    value = get_browser().find_by_css(
        '.data.category.q_1 .sub-bar .calc-value')[2].text
    if value:
        assert value == group1


@step(u'there is no Unfavorable score on bottom for question 1')
def there_is_no_unfavorable_score_on_bottom_for_question_1(step):
    if get_tab_link() != 'Categories':
        elements = get_browser().find_by_css(
            '.data.standalone.q_00_01 .comparison')
        if len(elements)==2:
            assert True
        elif len(elements)==5:
            assert True
    else:
        elements = get_browser().find_by_css('.data.q_00_01 .comparison')
        if len(elements)==5:
            assert True
        elif len(elements)==11:
            assert True
