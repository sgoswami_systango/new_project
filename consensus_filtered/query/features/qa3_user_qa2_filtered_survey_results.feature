Feature: Check filtered Survey Results for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "2"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "4.9%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "10"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "24.4%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "26"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "63.4%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "3"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "7.3%"
	And the "Mean" under question 1 is "3.73 (±0.66)"
	And the figure under question 1 for "Mean" in the "Number" column is "41"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the figure for "Filter" in the upper right is "41"
	And the figure for "Remainder" in the upper right is "23"
	And the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0(0)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%(0.0%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "2(6)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "4.9%(26.1%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "10(10)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "24.4%(43.5%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "26(6)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "63.4%(26.1%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "3(1)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "7.3%(4.3%)"
	And the "Mean" under question 1 is "3.73 (±0.66)"
	And the figure under question 1 for "Mean" in the "Number" column is "41(23)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"