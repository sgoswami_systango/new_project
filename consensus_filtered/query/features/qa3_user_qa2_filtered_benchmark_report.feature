Feature: Check filtered Benchmark Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 1 in the "My Filter" column is "70.7%"
	And the score for question 1 in the "My View" column is "56.2%(-14.5)"
	And the score for question 1 in the "LPL Financial" column is "75.5%(4.8)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-2.1)"

	When I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "My Filter" column is "73.2%"
	And the score for the "Clarity of Direction" category in the "My View" column is "65.6%"
	And the score for the "Clarity of Direction" category in the "LPL Financial" column is "80.2%"
	And the score for question 1 in the "My Filter" column is "70.7%"
	And the score for question 1 in the "My View" column is "56.2%(-14.5)"
	And the score for question 1 in the "LPL Financial" column is "75.5%(4.8)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-2.1)"
	
	Now I go to "Functions" > "Logout"