Feature: Check filtered Demographic Crosstab for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Female" is "(17)"
	And the first number under "Male" is "(24)"
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.0"
	And the score for question 1 in the "Male" column is "3.5"
	And the Weighted Average on the right for question 1 is "3.7"
	And the Group Average at the bottom for the "Female" column is "4.1"
	And the Group Average at the bottom for the "Male" column is "3.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.00"
	And the score for question 1 in the "Male" column is "3.54"
	And the Weighted Average on the right for question 1 is "3.73"
	And the Group Average at the bottom for the "Female" column is "4.08"
	And the Group Average at the bottom for the "Male" column is "3.50"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "82"
	And the score for question 1 in the "Male" column is "62"
	And the Weighted Average on the right for question 1 is "71"
	And the Group Average at the bottom for the "Female" column is "81"
	And the Group Average at the bottom for the "Male" column is "57"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "82.4"
	And the score for question 1 in the "Male" column is "62.5"
	And the Weighted Average on the right for question 1 is "70.7"
	And the Group Average at the bottom for the "Female" column is "81.1"
	And the Group Average at the bottom for the "Male" column is "57.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "82.35"
	And the score for question 1 in the "Male" column is "62.50"
	And the Weighted Average on the right for question 1 is "70.73"
	And the Group Average at the bottom for the "Female" column is "81.06"
	And the Group Average at the bottom for the "Male" column is "57.42"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0"
	And the score for question 1 in the "Male" column is "8"
	And the Weighted Average on the right for question 1 is "5"
	And the Group Average at the bottom for the "Female" column is "6"
	And the Group Average at the bottom for the "Male" column is "15"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.0"
	And the score for question 1 in the "Male" column is "8.3"
	And the Weighted Average on the right for question 1 is "4.9"
	And the Group Average at the bottom for the "Female" column is "6.1"
	And the Group Average at the bottom for the "Male" column is "14.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.00"
	And the score for question 1 in the "Male" column is "8.33"
	And the Weighted Average on the right for question 1 is "4.88"
	And the Group Average at the bottom for the "Female" column is "6.12"
	And the Group Average at the bottom for the "Male" column is "14.92"

	Now I go to "Functions" > "Logout"

# Categories
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the first number under "Female" is "(17)"
	And the first number under "Male" is "(24)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.0"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.8"
	And the Group Average at the bottom for the "Female" column is "4.1"
	And the Group Average at the bottom for the "Male" column is "3.5"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.0"
	And the score for question 1 in the "Male" column is "3.5"
	And the Weighted Average on the right for question 1 is "3.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.04"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.71"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.84"
	And the Group Average at the bottom for the "Female" column is "4.08"
	And the Group Average at the bottom for the "Male" column is "3.50"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.00"
	And the score for question 1 in the "Male" column is "3.54"
	And the Weighted Average on the right for question 1 is "3.73"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "78"
	And the score for the "Clarity of Direction" category in the "Male" column is "70"
	And the Weighted Average on the right for the "Clarity of Direction" category is "73"
	And the Group Average at the bottom for the "Female" column is "81"
	And the Group Average at the bottom for the "Male" column is "57"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "82"
	And the score for question 1 in the "Male" column is "62"
	And the Weighted Average on the right for question 1 is "71"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "77.6"
	And the score for the "Clarity of Direction" category in the "Male" column is "70.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "73.2"
	And the Group Average at the bottom for the "Female" column is "81.1"
	And the Group Average at the bottom for the "Male" column is "57.4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "82.4"
	And the score for question 1 in the "Male" column is "62.5"
	And the Weighted Average on the right for question 1 is "70.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "77.65"
	And the score for the "Clarity of Direction" category in the "Male" column is "70.00"
	And the Weighted Average on the right for the "Clarity of Direction" category is "73.17"
	And the Group Average at the bottom for the "Female" column is "81.06"
	And the Group Average at the bottom for the "Male" column is "57.42"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "82.35"
	And the score for question 1 in the "Male" column is "62.50"
	And the Weighted Average on the right for question 1 is "70.73"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "10"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Female" column is "6"
	And the Group Average at the bottom for the "Male" column is "15"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0"
	And the score for question 1 in the "Male" column is "8"
	And the Weighted Average on the right for question 1 is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.5"
	And the score for the "Clarity of Direction" category in the "Male" column is "10.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7.3"
	And the Group Average at the bottom for the "Female" column is "6.1"
	And the Group Average at the bottom for the "Male" column is "14.9"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.0"
	And the score for question 1 in the "Male" column is "8.3"
	And the Weighted Average on the right for question 1 is "4.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.53"
	And the score for the "Clarity of Direction" category in the "Male" column is "10.00"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7.32"
	And the Group Average at the bottom for the "Female" column is "6.12"
	And the Group Average at the bottom for the "Male" column is "14.92"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.00"
	And the score for question 1 in the "Male" column is "8.33"
	And the Weighted Average on the right for question 1 is "4.88"

	Now I go to "Functions" > "Logout"

# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.0 (4.0)"
	And the score for question 1 in the "Male" column is "3.5 (3.8)"
	And the Weighted Average on the right for question 1 is "3.7"
	And the Group Average at the bottom for the "Female" column is "4.1 (4.0)"
	And the Group Average at the bottom for the "Male" column is "3.5 (3.8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.00 (4.05)"
	And the score for question 1 in the "Male" column is "3.54 (3.79)"
	And the Weighted Average on the right for question 1 is "3.73"
	And the Group Average at the bottom for the "Female" column is "4.08 (4.00)"
	And the Group Average at the bottom for the "Male" column is "3.50 (3.78)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "82 (90)"
	And the score for question 1 in the "Male" column is "62 (76)"
	And the Weighted Average on the right for question 1 is "71"
	And the Group Average at the bottom for the "Female" column is "81 (78)"
	And the Group Average at the bottom for the "Male" column is "57 (74)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "82.4 (90.0)"
	And the score for question 1 in the "Male" column is "62.5 (75.9)"
	And the Weighted Average on the right for question 1 is "70.7"
	And the Group Average at the bottom for the "Female" column is "81.1 (78.0)"
	And the Group Average at the bottom for the "Male" column is "57.4 (73.8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "82.35 (90.00)"
	And the score for question 1 in the "Male" column is "62.50 (75.86)"
	And the Weighted Average on the right for question 1 is "70.73"
	And the Group Average at the bottom for the "Female" column is "81.06 (78.03)"
	And the Group Average at the bottom for the "Male" column is "57.42 (73.79)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0 (0)"
	And the score for question 1 in the "Male" column is "8 (3)"
	And the Weighted Average on the right for question 1 is "5"
	And the Group Average at the bottom for the "Female" column is "6 (5)"
	And the Group Average at the bottom for the "Male" column is "15 (10)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.0 (0.0)"
	And the score for question 1 in the "Male" column is "8.3 (3.4)"
	And the Weighted Average on the right for question 1 is "4.9"
	And the Group Average at the bottom for the "Female" column is "6.1 (5.0)"
	And the Group Average at the bottom for the "Male" column is "14.9 (10.0)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.00 (0.00)"
	And the score for question 1 in the "Male" column is "8.33 (3.45)"
	And the Weighted Average on the right for question 1 is "4.88"
	And the Group Average at the bottom for the "Female" column is "6.12 (4.97)"
	And the Group Average at the bottom for the "Male" column is "14.92 (10.04)"

	Now I go to "Functions" > "Logout"

# Categories + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.0 (4.1)"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.7 (3.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.8"
	And the Group Average at the bottom for the "Female" column is "4.1 (4.0)"
	And the Group Average at the bottom for the "Male" column is "3.5 (3.8)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.0 (4.0)"
	And the score for question 1 in the "Male" column is "3.5 (3.8)"
	And the Weighted Average on the right for question 1 is "3.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.04 (4.10)"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.71 (3.86)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.84"
	And the Group Average at the bottom for the "Female" column is "4.08 (4.00)"
	And the Group Average at the bottom for the "Male" column is "3.50 (3.78)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.00 (4.05)"
	And the score for question 1 in the "Male" column is "3.54 (3.79)"
	And the Weighted Average on the right for question 1 is "3.73"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "78 (86)"
	And the score for the "Clarity of Direction" category in the "Male" column is "70 (78)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "73"
	And the Group Average at the bottom for the "Female" column is "81 (78)"
	And the Group Average at the bottom for the "Male" column is "57 (74)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "82 (90)"
	And the score for question 1 in the "Male" column is "62 (76)"
	And the Weighted Average on the right for question 1 is "71"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "77.6 (86.0)"
	And the score for the "Clarity of Direction" category in the "Male" column is "70.0 (77.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "73.2"
	And the Group Average at the bottom for the "Female" column is "81.1 (78.0)"
	And the Group Average at the bottom for the "Male" column is "57.4 (73.8)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "82.4 (90.0)"
	And the score for question 1 in the "Male" column is "62.5 (75.9)"
	And the Weighted Average on the right for question 1 is "70.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "77.65 (86.00)"
	And the score for the "Clarity of Direction" category in the "Male" column is "70.00 (77.93)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "73.17"
	And the Group Average at the bottom for the "Female" column is "81.06 (78.03)"
	And the Group Average at the bottom for the "Male" column is "57.42 (73.79)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "82.35 (90.00)"
	And the score for question 1 in the "Male" column is "62.50 (75.86)"
	And the Weighted Average on the right for question 1 is "70.73"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4 (1)"
	And the score for the "Clarity of Direction" category in the "Male" column is "10 (6)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Female" column is "6 (5)"
	And the Group Average at the bottom for the "Male" column is "15 (10)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0 (0)"
	And the score for question 1 in the "Male" column is "8 (3)"
	And the Weighted Average on the right for question 1 is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.5 (1.0)"
	And the score for the "Clarity of Direction" category in the "Male" column is "10.0 (5.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7.3"
	And the Group Average at the bottom for the "Female" column is "6.1 (5.0)"
	And the Group Average at the bottom for the "Male" column is "14.9 (10.0)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.0 (0.0)"
	And the score for question 1 in the "Male" column is "8.3 (3.4)"
	And the Weighted Average on the right for question 1 is "4.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.53 (1.00)"
	And the score for the "Clarity of Direction" category in the "Male" column is "10.00 (5.52)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7.32"
	And the Group Average at the bottom for the "Female" column is "6.12 (4.97)"
	And the Group Average at the bottom for the "Male" column is "14.92 (10.04)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.00 (0.00)"
	And the score for question 1 in the "Male" column is "8.33 (3.45)"
	And the Weighted Average on the right for question 1 is "4.88"

	Now I go to "Functions" > "Logout"