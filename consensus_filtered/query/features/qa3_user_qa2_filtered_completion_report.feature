Feature: Check filtered Completion Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Female" in the "Invitees" column is "26"
	And the figure for "Female" in the "Respondents" column is "17(65.4%)"
	And the figure for "Female" in the "Completions" column is "17(65.4%)"
	And the figure for the "Totals" row in the "Invitees" column is "63"
	And the figure for the "Totals" row in the "Respondents" column is "41(65.1%)"
	And the figure for the "Totals" row in the "Completions" column is "41(65.1%)"

	When I am on the "By Time" tab
	Then the figure for "Wednesday, September 3, 2014" in the "Respondents" column is "2"
	And the figure for "Wednesday, September 3, 2014" in the "% Responded" column is "4.9%"
	And the figure for "Wednesday, September 3, 2014" in the "% Invited" column is "3.2%"
	And the figure for the "Totals" row in the "Respondents" column is "41"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "65.1%"
	
	When I click on "Wednesday, September 3, 2014"
	Then the figure for "1:00 PM" in the "Respondents" column is "1"
	And the figure for "1:00 PM" in the "% Responded" column is "2.4%"
	And the figure for "1:00 PM" in the "% Invited" column is "1.6%"
	
	Now I go to "Functions" > "Logout"