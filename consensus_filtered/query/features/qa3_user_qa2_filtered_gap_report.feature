Feature: Check filtered Gap Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter Count" in the top right is "41"
	And the Eval Avg for question 1 is "3.73"
	And the Consensus for question 1 is "4.34"
	And the Gap for question 1 is "0.61"
	And the Weighted Gap for question 1 is "13.24"
	And the Overall Average at the bottom has an Eval Avg of "3.74"
	And the Overall Average has a Consensus of "4.02"
	And the Overall Average has a Gap of "0.28"
	And the Overall Average has a Weighted Gap of "5.63"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.84"
	And the Consensus for the "Clarity of Direction" category is "4.19"
	And the Gap for the "Clarity of Direction" category is "0.35"
	And the Weighted Gap for the "Clarity of Direction" category is "7.33"
	And the Overall Average at the bottom has an Eval Avg of "3.74"
	And the Overall Average has a Consensus of "4.02"
	And the Overall Average has a Gap of "0.28"
	And the Overall Average has a Weighted Gap of "5.63"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.73"
	And the Consensus for question 1 is "4.34"
	And the Gap for question 1 is "0.61"
	And the Weighted Gap for question 1 is "13.24"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the number for "Filter" in the top right is "41"
	And the number for "Remainder" in the top right is "23"
	And the Eval Avg for question 1 is "3.73 (+0.64)"
	And the Consensus for question 1 is "4.34 (+0.17)"
	And the Gap for question 1 is "0.61 (-0.47)"
	And the Weighted Gap for question 1 is "13.24 (-9.28)"
	And the Overall Average at the bottom has an Eval Avg of "3.74 (+0.30)"
	And the Overall Average has a Consensus of "4.02 (-0.07)"
	And the Overall Average has a Gap of "0.28 (-0.37)"
	And the Overall Average has a Weighted Gap of "5.63 (-7.66)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "3.84 (+0.42)"
	And the Consensus for the "Clarity of Direction" category is "4.19 (+0.03)"
	And the Gap for the "Clarity of Direction" category is "0.35 (-0.39)"
	And the Weighted Gap for the "Clarity of Direction" category is "7.33 (-8.06)"
	And the Overall Average at the bottom has an Eval Avg of "3.74 (+0.30)"
	And the Overall Average has a Consensus of "4.02 (-0.07)"
	And the Overall Average has a Gap of "0.28 (-0.37)"
	And the Overall Average has a Weighted Gap of "5.63 (-7.66)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.73 (+0.64)"
	And the Consensus for question 1 is "4.34 (+0.17)"
	And the Gap for question 1 is "0.61 (-0.47)"
	And the Weighted Gap for question 1 is "13.24 (-9.28)"
	
	Now I go to "Functions" > "Logout"