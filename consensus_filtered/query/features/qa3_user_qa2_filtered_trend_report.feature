Feature: Check filtered Trend Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(41)"
	And the Count for "2013" is "(50)"
	And the Count for "2012" is "(43)"
	And the score for question 1 in the "2014" column is "3.73 (-0.17)"
	And the score for question 1 in the "2013" column is "3.90 (+0.04)"
	And the score for question 1 in the "2012" column is "3.86"
	And the Overall Average at the bottom for "2014" is "3.74 (-0.13)"
	And the Overall Average for "2013" is "3.87 (+0.02)"
	And the Overall Average for "2012" is "3.85"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(41)"
	And the Count for "2013" is "(50)"
	And the Count for "2012" is "(43)"
	And the score for the "Clarity of Direction" category for "2014" is "3.84 (-0.12)"
	And the score for the "Clarity of Direction" category for "2013" is "3.96 (-0.01)"
	And the score for the "Clarity of Direction" category for "2012" is "3.97"
	And the Overall Average at the bottom for "2014" is "3.74 (-0.13)"
	And the Overall Average for "2013" is "3.87 (+0.02)"
	And the Overall Average for "2012" is "3.85"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "3.73 (-0.17)"
	And the score for question 1 in the "2013" column is "3.90 (+0.04)"
	And the score for question 1 in the "2012" column is "3.86"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(41)"
	And the Count for "2013" is "(50)"
	And the Count for "2012" is "(43)"
	And the score for question 1 in the "2014" column is "70.7 (-10.9)"
	And the score for question 1 in the "2013" column is "81.6 (+4.9)"
	And the score for question 1 in the "2012" column is "76.7"
	And the Overall Average at the bottom for "2014" is "67.2 (-8.3)"
	And the Overall Average for "2013" is "75.5 (+3.0)"
	And the Overall Average for "2012" is "72.5"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(41)"
	And the Count for "2013" is "(50)"
	And the Count for "2012" is "(43)"
	And the score for the "Clarity of Direction" category for "2014" is "73.2 (-8.0)"
	And the score for the "Clarity of Direction" category for "2013" is "81.2 (+2.1)"
	And the score for the "Clarity of Direction" category for "2012" is "79.1"
	And the Overall Average at the bottom for "2014" is "67.2 (-8.3)"
	And the Overall Average for "2013" is "75.5 (+3.0)"
	And the Overall Average for "2012" is "72.5"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "70.7 (-10.9)"
	And the score for question 1 in the "2013" column is "81.6 (+4.9)"
	And the score for question 1 in the "2012" column is "76.7"

	Now I go to "Functions" > "Logout"