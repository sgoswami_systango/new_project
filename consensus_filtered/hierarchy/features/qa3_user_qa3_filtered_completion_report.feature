Feature: Check filtered Completion Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Female" in the "Invitees" column is "50"
	And the figure for "Female" in the "Respondents" column is "46(92.0%)"
	And the figure for "Female" in the "Completions" column is "44(88.0%)"
	And the figure for the "Totals" row in the "Invitees" column is "85"
	And the figure for the "Totals" row in the "Respondents" column is "77(90.6%)"
	And the figure for the "Totals" row in the "Completions" column is "74(87.1%)"

	When I am on the "By Time" tab
	Then the figure for "Wednesday, September 3, 2014" in the "Respondents" column is "5"
	And the figure for "Wednesday, September 3, 2014" in the "% Responded" column is "6.5%"
	And the figure for "Wednesday, September 3, 2014" in the "% Invited" column is "5.9%"
	And the figure for the "Totals" row in the "Respondents" column is "77"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "90.6%"
	
	When I click on "Wednesday, September 3, 2014"
	Then the figure for "12:00 PM" in the "Respondents" column is "1"
	And the figure for "12:00 PM" in the "% Responded" column is "1.3%"
	And the figure for "12:00 PM" in the "% Invited" column is "1.2%"
	
	Now I go to "Functions" > "Logout"