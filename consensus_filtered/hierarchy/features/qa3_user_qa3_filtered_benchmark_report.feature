Feature: Check filtered Trend Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 1 in the "My Filter" column is "86.7%"
	And the score for question 1 in the "My View" column is "83.7%(-3)"
	And the score for question 1 in the "LPL Financial" column is "75.5%(-11.2)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-18.1)"

	When I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "My Filter" column is "87.7%"
	And the score for the "Clarity of Direction" category in the "My View" column is "87.0%"
	And the score for the "Clarity of Direction" category in the "LPL Financial" column is "80.2%"
	Then the score for question 1 in the "My Filter" column is "86.7%"
	And the score for question 1 in the "My View" column is "83.7%(-3)"
	And the score for question 1 in the "LPL Financial" column is "75.5%(-11.2)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-18.1)"
	
	Now I go to "Functions" > "Logout"