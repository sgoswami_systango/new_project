Feature: Check filtered Favorability Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter" in the top right is "77"
	And the Favorable score for question 1 is "86.7%"
	And the Neutral score for question 1 is "6.7%"
	And the Unfavorable score for question 1 is "6.7%"
	And the Overall Average at the bottom has a Favorable score of "75.0%"
	And the Overall Average Neutral score is "15.7%"
	And the Overall Average Unfavorable score is "9.3%"

	When I am on the "Categories" tab
	And  I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "87.7%"
	And the Neutral score for the "Clarity of Direction" category is "8.8%"
	And the Unfavorable score for the "Clarity of Direction" category is "3.5%"
	And the Overall Average at the bottom has a Favorable score of "75.0%"
	And the Overall Average Neutral score is "15.7%"
	And the Overall Average Unfavorable score is "9.3%"

	When I click on the Clarity of Direction category
	Then the Favorable score for question 1 is "86.7%"
	And the Neutral score for question 1 is "6.7%"
	And the Unfavorable score for question 1 is "6.7%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	
	Then the number for "Filter" in the top right is "77"
	And the number for "Remainder" in the top right is "60"
	And the Favorable score on top for question 1 is "86.7%"
	And the Neutral score on top for question 1 is "6.7%"
	And the Unfavorable score on top for question 1 is "6.7%"
	And the Favorable score on bottom for question 1 is "80.0%"
	And the Neutral score on bottom for question 1 is "13.3%"
	And the Unfavorable score on bottom for question 1 is "6.7%"
	And the Overall Average at the bottom Favorable score on top is "75.0%"
	And the Overall Average Neutral score on top is "15.7%"
	And the Overall Average Unfavorable score on top is "9.3%"
	And the Overall Average Favorable score on bottom is "79.2%"
	And the Overall Average Neutral score on bottom is "11.4%"
	And the Overall Average Unfavorable score on bottom is "9.4%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "87.7%"
	And the Neutral score on top for the "Clarity of Direction" category is "8.8%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "3.5%"
	And the Favorable score on bottom for the "Clarity of Direction" category is "86.0%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "9.0%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "5.0%"
	And the Overall Average at the bottom Favorable score on top is "75.0%"
	And the Overall Average Neutral score on top is "15.7%"
	And the Overall Average Unfavorable score on top is "9.3%"
	And the Overall Average Favorable score on bottom is "79.2%"
	And the Overall Average Neutral score on bottom is "11.4%"
	And the Overall Average Unfavorable score on bottom is "9.4%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "86.7%"
	And the Neutral score on top for question 1 is "6.7%"
	And the Unfavorable score on top for question 1 is "6.7%"
	And the Favorable score on bottom for question 1 is "80.0%"
	And the Neutral score on bottom for question 1 is "13.3%"
	And the Unfavorable score on bottom for question 1 is "6.7%"
	
	Now I go to "Functions" > "Logout"