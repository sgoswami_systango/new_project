Feature: Check filtered Gap Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter Count" in the top right is "77"
	And the Eval Avg for question 1 is "4.00"
	And the Consensus for question 1 is "4.22"
	And the Gap for question 1 is "0.22"
	And the Weighted Gap for question 1 is "4.64"
	And the Overall Average at the bottom has an Eval Avg of "3.91"
	And the Overall Average has a Consensus of "4.07"
	And the Overall Average has a Gap of "0.16"
	And the Overall Average has a Weighted Gap of "3.26"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "4.12"
	And the Consensus for the "Clarity of Direction" category is "4.27"
	And the Gap for the "Clarity of Direction" category is "0.15"
	And the Weighted Gap for the "Clarity of Direction" category is "3.20"
	And the Overall Average at the bottom has an Eval Avg of "3.91"
	And the Overall Average has a Consensus of "4.07"
	And the Overall Average has a Gap of "0.16"
	And the Overall Average has a Weighted Gap of "3.26"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "4.00"
	And the Consensus for question 1 is "4.22"
	And the Gap for question 1 is "0.22"
	And the Weighted Gap for question 1 is "4.64"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the number for "Filter" in the top right is "77"
	And the number for "Remainder" in the top right is "60"
	And the Eval Avg for question 1 is "4.00 (+0.07)"
	And the Consensus for question 1 is "4.22 (+0.05)"
	And the Gap for question 1 is "0.22 (-0.02)"
	And the Weighted Gap for question 1 is "4.64 (-0.36)"
	And the Overall Average at the bottom has an Eval Avg of "3.91 (-0.14)"
	And the Overall Average has a Consensus of "4.07 (+0.07)"
	And the Overall Average has a Gap of "0.16 (+0.21)"
	And the Overall Average has a Weighted Gap of "3.26 (+4.26)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "4.12 (-0.12)"
	And the Consensus for the "Clarity of Direction" category is "4.27 (+0.13)"
	And the Gap for the "Clarity of Direction" category is "0.15 (+0.25)"
	And the Weighted Gap for the "Clarity of Direction" category is "3.20 (+5.27)"
	And the Overall Average at the bottom has an Eval Avg of "3.91 (-0.14)"
	And the Overall Average has a Consensus of "4.07 (+0.07)"
	And the Overall Average has a Gap of "0.16 (+0.21)"
	And the Overall Average has a Weighted Gap of "3.26 (+4.26)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "4.00 (+0.07)"
	And the Consensus for question 1 is "4.22 (+0.05)"
	And the Gap for question 1 is "0.22 (-0.02)"
	And the Weighted Gap for question 1 is "4.64 (-0.36)"
	
	Now I go to "Functions" > "Logout"