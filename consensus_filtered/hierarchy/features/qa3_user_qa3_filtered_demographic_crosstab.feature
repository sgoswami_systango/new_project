Feature: Check filtered Demographic Crosstab for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Female" is "(46)"
	And the first number under "Male" is "(31)"
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.0"
	And the score for question 1 in the "Male" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.98"
	And the score for question 1 in the "Male" column is "4.03"
	And the Weighted Average on the right for question 1 is "4.00"
	And the Group Average at the bottom for the "Female" column is "3.88"
	And the Group Average at the bottom for the "Male" column is "3.95"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "87"
	And the score for question 1 in the "Male" column is "87"
	And the Weighted Average on the right for question 1 is "87"
	And the Group Average at the bottom for the "Female" column is "74"
	And the Group Average at the bottom for the "Male" column is "77"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86.7"
	And the score for question 1 in the "Male" column is "86.7"
	And the Weighted Average on the right for question 1 is "86.7"
	And the Group Average at the bottom for the "Female" column is "73.9"
	And the Group Average at the bottom for the "Male" column is "76.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86.67"
	And the score for question 1 in the "Male" column is "86.67"
	And the Weighted Average on the right for question 1 is "86.67"
	And the Group Average at the bottom for the "Female" column is "73.95"
	And the Group Average at the bottom for the "Male" column is "76.60"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "7"
	And the score for question 1 in the "Male" column is "7"
	And the Weighted Average on the right for question 1 is "7"
	And the Group Average at the bottom for the "Female" column is "9"
	And the Group Average at the bottom for the "Male" column is "9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "6.7"
	And the score for question 1 in the "Male" column is "6.7"
	And the Weighted Average on the right for question 1 is "6.7"
	And the Group Average at the bottom for the "Female" column is "9.5"
	And the Group Average at the bottom for the "Male" column is "8.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "6.67"
	And the score for question 1 in the "Male" column is "6.67"
	And the Weighted Average on the right for question 1 is "6.67"
	And the Group Average at the bottom for the "Female" column is "9.50"
	And the Group Average at the bottom for the "Male" column is "8.93"

	Now I go to "Functions" > "Logout"

# Categories
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the first number under "Female" is "(46)"
	And the first number under "Male" is "(31)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.2"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "4.0"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.0"
	And the score for question 1 in the "Male" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.18"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.04"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.12"
	And the Group Average at the bottom for the "Female" column is "3.88"
	And the Group Average at the bottom for the "Male" column is "3.95"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.98"
	And the score for question 1 in the "Male" column is "4.03"
	And the Weighted Average on the right for question 1 is "4.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "92"
	And the score for the "Clarity of Direction" category in the "Male" column is "82"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Female" column is "74"
	And the Group Average at the bottom for the "Male" column is "77"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "87"
	And the score for question 1 in the "Male" column is "87"
	And the Weighted Average on the right for question 1 is "87"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "91.6"
	And the score for the "Clarity of Direction" category in the "Male" column is "82.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.7"
	And the Group Average at the bottom for the "Female" column is "73.9"
	And the Group Average at the bottom for the "Male" column is "76.6"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86.7"
	And the score for question 1 in the "Male" column is "86.7"
	And the Weighted Average on the right for question 1 is "86.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "91.56"
	And the score for the "Clarity of Direction" category in the "Male" column is "82.00"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.73"
	And the Group Average at the bottom for the "Female" column is "73.95"
	And the Group Average at the bottom for the "Male" column is "76.60"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86.67"
	And the score for question 1 in the "Male" column is "86.67"
	And the Weighted Average on the right for question 1 is "86.67"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3"
	And the score for the "Clarity of Direction" category in the "Male" column is "5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3"
	And the Group Average at the bottom for the "Female" column is "9"
	And the Group Average at the bottom for the "Male" column is "9"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "7"
	And the score for question 1 in the "Male" column is "7"
	And the Weighted Average on the right for question 1 is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "2.7"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.5"
	And the Group Average at the bottom for the "Female" column is "9.5"
	And the Group Average at the bottom for the "Male" column is "8.9"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "6.7"
	And the score for question 1 in the "Male" column is "6.7"
	And the Weighted Average on the right for question 1 is "6.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "2.67"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.67"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.47"
	And the Group Average at the bottom for the "Female" column is "9.50"
	And the Group Average at the bottom for the "Male" column is "8.93"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "6.67"
	And the score for question 1 in the "Male" column is "6.67"
	And the Weighted Average on the right for question 1 is "6.67"

	Now I go to "Functions" > "Logout"

# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.0"
	And the score for question 1 in the "Male" column is "4.0 (3.7)"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "4.0 (3.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.98"
	And the score for question 1 in the "Male" column is "4.03 (3.73)"
	And the Weighted Average on the right for question 1 is "4.00"
	And the Group Average at the bottom for the "Female" column is "3.88"
	And the Group Average at the bottom for the "Male" column is "3.95 (3.94)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "87"
	And the score for question 1 in the "Male" column is "87 (73)"
	And the Weighted Average on the right for question 1 is "87"
	And the Group Average at the bottom for the "Female" column is "74"
	And the Group Average at the bottom for the "Male" column is "77 (76)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86.7"
	And the score for question 1 in the "Male" column is "86.7 (73.3)"
	And the Weighted Average on the right for question 1 is "86.7"
	And the Group Average at the bottom for the "Female" column is "73.9"
	And the Group Average at the bottom for the "Male" column is "76.6 (76.5)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86.67"
	And the score for question 1 in the "Male" column is "86.67 (73.33)"
	And the Weighted Average on the right for question 1 is "86.67"
	And the Group Average at the bottom for the "Female" column is "73.95"
	And the Group Average at the bottom for the "Male" column is "76.60 (76.47)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "7"
	And the score for question 1 in the "Male" column is "7 (13)"
	And the Weighted Average on the right for question 1 is "7"
	And the Group Average at the bottom for the "Female" column is "9"
	And the Group Average at the bottom for the "Male" column is "9 (10)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "6.7"
	And the score for question 1 in the "Male" column is "6.7 (13.3)"
	And the Weighted Average on the right for question 1 is "6.7"
	And the Group Average at the bottom for the "Female" column is "9.5"
	And the Group Average at the bottom for the "Male" column is "8.9 (9.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "6.67"
	And the score for question 1 in the "Male" column is "6.67 (13.33)"
	And the Weighted Average on the right for question 1 is "6.67"
	And the Group Average at the bottom for the "Female" column is "9.50"
	And the Group Average at the bottom for the "Male" column is "8.93 (9.91)"

	Now I go to "Functions" > "Logout"

# Categories + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.2"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.0 (4.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "4.0 (3.9)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.0"
	And the score for question 1 in the "Male" column is "4.0 (3.7)"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.18"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.04 (4.05)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.12"
	And the Group Average at the bottom for the "Female" column is "3.88"
	And the Group Average at the bottom for the "Male" column is "3.95 (3.94)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.98"
	And the score for question 1 in the "Male" column is "4.03 (3.73)"
	And the Weighted Average on the right for question 1 is "4.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "92"
	And the score for the "Clarity of Direction" category in the "Male" column is "82 (85)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Female" column is "74"
	And the Group Average at the bottom for the "Male" column is "77 (76)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "87"
	And the score for question 1 in the "Male" column is "87 (73)"
	And the Weighted Average on the right for question 1 is "87"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "91.6"
	And the score for the "Clarity of Direction" category in the "Male" column is "82.0 (85.3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.7"
	And the Group Average at the bottom for the "Female" column is "73.9"
	And the Group Average at the bottom for the "Male" column is "76.6 (76.5)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86.7"
	And the score for question 1 in the "Male" column is "86.7 (73.3)"
	And the Weighted Average on the right for question 1 is "86.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "91.56"
	And the score for the "Clarity of Direction" category in the "Male" column is "82.00 (85.33)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.73"
	And the Group Average at the bottom for the "Female" column is "73.95"
	And the Group Average at the bottom for the "Male" column is "76.60 (76.47)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86.67"
	And the score for question 1 in the "Male" column is "86.67 (73.33)"
	And the Weighted Average on the right for question 1 is "86.67"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3"
	And the score for the "Clarity of Direction" category in the "Male" column is "5 (5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3"
	And the Group Average at the bottom for the "Female" column is "9"
	And the Group Average at the bottom for the "Male" column is "9 (10)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "7"
	And the score for question 1 in the "Male" column is "7 (13)"
	And the Weighted Average on the right for question 1 is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "2.7"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.7 (5.3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.5"
	And the Group Average at the bottom for the "Female" column is "9.5"
	And the Group Average at the bottom for the "Male" column is "8.9 (9.9)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "6.7"
	And the score for question 1 in the "Male" column is "6.7 (13.3)"
	And the Weighted Average on the right for question 1 is "6.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "2.67"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.67 (5.33)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.47"
	And the Group Average at the bottom for the "Female" column is "9.50"
	And the Group Average at the bottom for the "Male" column is "8.93 (9.91)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "6.67"
	And the score for question 1 in the "Male" column is "6.67 (13.33)"
	And the Weighted Average on the right for question 1 is "6.67"

	Now I go to "Functions" > "Logout"