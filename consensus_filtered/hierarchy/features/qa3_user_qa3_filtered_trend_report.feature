Feature: Check filtered Trend Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(77)"
	And the Count for "2013" is "(19)"
	And the score for question 1 in the "2014" column is "4.00 (+0.32)"
	And the score for question 1 in the "2013" column is "3.68"
	And the Overall Average at the bottom for "2014" is "3.91 (-0.06)"
	And the Overall Average for "2013" is "3.97"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(77)"
	And the Count for "2013" is "(19)"
	And the score for the "Clarity of Direction" category for "2014" is "4.12 (+0.14)"
	And the score for the "Clarity of Direction" category for "2013" is "3.98"
	And the Overall Average at the bottom for "2014" is "3.91 (-0.06)"
	And the Overall Average for "2013" is "3.97"
	
	When I click on the Clarity of Direction category
	Then the score for question 1 in the "2014" column is "4.00 (+0.32)"
	And the score for question 1 in the "2013" column is "3.68"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(77)"
	And the Count for "2013" is "(19)"
	And the score for question 1 in the "2014" column is "86.7 (+18.3)"
	And the score for question 1 in the "2013" column is "68.4"
	And the Overall Average at the bottom for "2014" is "75.0 (-3.8)"
	And the Overall Average for "2013" is "78.8"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(77)"
	And the Count for "2013" is "(19)"
	And the score for the "Clarity of Direction" category for "2014" is "87.7 (+6.6)"
	And the score for the "Clarity of Direction" category for "2013" is "81.1"
	And the Overall Average at the bottom for "2014" is "75.0 (-3.8)"
	And the Overall Average for "2013" is "78.8"
	
	When I click on the Clarity of Direction category
	Then the score for question 1 in the "2014" column is "86.7	(+18.3)"
	And the score for question 1 in the "2013" column is "68.4"

	Now I go to "Functions" > "Logout"