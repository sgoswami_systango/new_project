Feature: Check filtered Survey Results for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "1"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.3%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "4"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "5.3%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "5"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "6.7%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "49"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "65.3%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "16"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "21.3%"
	And the "Mean" under question 1 is "4.00 (±0.78)"
	And the figure under question 1 for "Mean" in the "Number" column is "75"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
		Then the figure for "Filter" in the upper right is "77"
	And the figure for "Remainder" in the upper right is "60"
	And the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "1(1)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.3%(1.7%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "4(3)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "5.3%(5.0%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "5(8)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "6.7%(13.3%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "49(35)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "65.3%(58.3%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "16(13)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "21.3%(21.7%)"
	And the "Mean" under question 1 is "4.00 (±0.78)"
	And the figure under question 1 for "Mean" in the "Number" column is "75(60)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"