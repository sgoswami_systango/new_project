Feature: Check filtered Gap Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter Count" in the top right is "1450"
	And the Eval Avg for question 1 is "3.81"
	And the Consensus for question 1 is "4.15"
	And the Gap for question 1 is "0.34"
	And the Weighted Gap for question 1 is "7.06"
	And the Overall Average at the bottom has an Eval Avg of "3.84"
	And the Overall Average has a Consensus of "4.00"
	And the Overall Average has a Gap of "0.16"
	And the Overall Average has a Weighted Gap of "3.20"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.97"
	And the Consensus for the "Clarity of Direction" category is "4.13"
	And the Gap for the "Clarity of Direction" category is "0.16"
	And the Weighted Gap for the "Clarity of Direction" category is "3.30"
	And the Overall Average at the bottom has an Eval Avg of "3.84"
	And the Overall Average has a Consensus of "4.00"
	And the Overall Average has a Gap of "0.16"
	And the Overall Average has a Weighted Gap of "3.20"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.81"
	And the Consensus for question 1 is "4.15"
	And the Gap for question 1 is "0.34"
	And the Weighted Gap for question 1 is "7.06"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the number for "Filter" in the top right is "1450"
	And the number for "Remainder" in the top right is "1592"
	And the Eval Avg for question 1 is "3.81 (-0.02)"
	And the Consensus for question 1 is "4.15 (-0.01)"
	And the Gap for question 1 is "0.34 (+0.01)"
	And the Weighted Gap for question 1 is "7.06 (+0.20)"
	And the Overall Average at the bottom has an Eval Avg of "3.84 (-0.03)"
	And the Overall Average has a Consensus of "4.00 (-0.01)"
	And the Overall Average has a Gap of "0.16 (+0.02)"
	And the Overall Average has a Weighted Gap of "3.20 (+0.39)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "3.97 (-0.07)"
	And the Consensus for the "Clarity of Direction" category is "4.13 (-0.04)"
	And the Gap for the "Clarity of Direction" category is "0.16 (+0.03)"
	And the Weighted Gap for the "Clarity of Direction" category is "3.30 (+0.59)"
	And the Overall Average at the bottom has an Eval Avg of "3.84 (-0.03)"
	And the Overall Average has a Consensus of "4.00 (-0.01)"
	And the Overall Average has a Gap of "0.16 (+0.02)"
	And the Overall Average has a Weighted Gap of "3.20 (+0.39)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.81 (-0.02)"
	And the Consensus for question 1 is "4.15 (-0.01)"
	And the Gap for question 1 is "0.34 (+0.01)"
	And the Weighted Gap for question 1 is "7.06 (+0.20)"
	
	Now I go to "Functions" > "Logout"