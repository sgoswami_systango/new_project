Feature: Check filtered Favorability Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter" in the top right is "1450"
	And the Favorable score for question 1 is "74.8%"
	And the Neutral score for question 1 is "16.2%"
	And the Unfavorable score for question 1 is "9.0%"
	And the Overall Average at the bottom has a Favorable score of "71.5%"
	And the Overall Average Neutral score is "17.8%"
	And the Overall Average Unfavorable score is "10.7%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "78.5%"
	And the Neutral score for the "Clarity of Direction" category is "14.6%"
	And the Unfavorable score for the "Clarity of Direction" category is "6.9%"
	And the Overall Average at the bottom has a Favorable score of "71.5%"
	And the Overall Average Neutral score is "17.8%"
	And the Overall Average Unfavorable score is "10.7%"

	When I click on the Clarity of Direction category
	Then the Favorable score for question 1 is "74.8%"
	And the Neutral score for question 1 is "16.2%"
	And the Unfavorable score for question 1 is "9.0%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	
	Then the number for "Filter" in the top right is "1450"
	And the number for "Remainder" in the top right is "1592"
	And the Favorable score on top for question 1 is "74.8%"
	And the Neutral score on top for question 1 is "16.2%"
	And the Unfavorable score on top for question 1 is "9.0%"
	And the Favorable score on bottom for question 1 is "76.2%"
	And the Neutral score on bottom for question 1 is "14.6%"
	And the Unfavorable score on bottom for question 1 is "9.2%"
	And the Overall Average at the bottom Favorable score on top is "71.5%"
	And the Overall Average Neutral score on top is "17.8%"
	And the Overall Average Unfavorable score on top is "10.7%"
	And the Overall Average Favorable score on bottom is "73.0%"
	And the Overall Average Neutral score on bottom is "16.4%"
	And the Overall Average Unfavorable score on bottom is "10.6%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "78.5%"
	And the Neutral score on top for the "Clarity of Direction" category is "14.6%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "6.9%"
	And the Favorable score on bottom for the "Clarity of Direction" category is "81.8%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "12.7%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "5.5%"
	And the Overall Average at the bottom Favorable score on top is "71.5%"
	And the Overall Average Neutral score on top is "17.8%"
	And the Overall Average Unfavorable score on top is "10.7%"
	And the Overall Average Favorable score on bottom is "73.0%"
	And the Overall Average Neutral score on bottom is "16.4%"
	And the Overall Average Unfavorable score on bottom is "10.6%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "74.8%"
	And the Neutral score on top for question 1 is "16.2%"
	And the Unfavorable score on top for question 1 is "9.0%"
	And the Favorable score on bottom for question 1 is "76.2%"
	And the Neutral score on bottom for question 1 is "14.6%"
	And the Unfavorable score on bottom for question 1 is "9.2%"
	
	Now I go to "Functions" > "Logout"
