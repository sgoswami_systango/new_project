Feature: Check filtered Completion Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Female" in the "Invitees" column is "713"
	And the figure for "Female" in the "Respondents" column is "644(90.3%)"
	And the figure for "Female" in the "Completions" column is "633(88.8%)"
	And the figure for the "Totals" row in the "Invitees" column is "1,611"
	And the figure for the "Totals" row in the "Respondents" column is "1,450(90.0%)"
	And the figure for the "Totals" row in the "Completions" column is "1,433(89.0%)"

	When I am on the "By Time" tab
	Then the figure for "Wednesday, September 3, 2014" in the "Respondents" column is "135"
	And the figure for "Wednesday, September 3, 2014" in the "% Responded" column is "9.3%"
	And the figure for "Wednesday, September 3, 2014" in the "% Invited" column is "8.4%"
	And the figure for the "Totals" row in the "Respondents" column is "1,450"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "90.0%"
	
	When I click on "Wednesday, September 3, 2014"
	Then the figure for "07:00 PM" in the "Respondents" column is "3"
	And the figure for "07:00 PM" in the "% Responded" column is "0.2%"
	And the figure for "07:00 PM" in the "% Invited" column is "0.2%"
	
	Now I go to "Functions" > "Logout"