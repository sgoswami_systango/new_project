Feature: Check filtered Survey Results for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "22"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.5%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "108"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "7.5%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "234"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "16.2%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "838"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "58.1%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "241"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "16.7%"
	And the "Mean" under question 1 is "3.81 (±0.85)"
	And the figure under question 1 for "Mean" in the "Number" column is "1443"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the number for "Filter" in the top right is "1450"
	And the figure for "Remainder" in the upper right is "1592"
	And the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "22(16)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.5%(1.0%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "108(130)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "7.5%(8.2%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "234(232)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "16.2%(14.6%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "838(931)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "58.1%(58.7%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "241(277)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "16.7%(17.5%)"
	And the "Mean" under question 1 is "3.81 (±0.85)"
	And the figure under question 1 for "Mean" in the "Number" column is "1443(1586)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"