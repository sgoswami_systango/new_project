Feature: Check filtered Demographic Crosstab for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Female" is "(644)"
	And the first number under "Male" is "(806)"
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.8"
	And the score for question 1 in the "Male" column is "3.8"
	And the Weighted Average on the right for question 1 is "3.8"
	And the Group Average at the bottom for the "Female" column is "3.8"
	And the Group Average at the bottom for the "Male" column is "3.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.78"
	And the score for question 1 in the "Male" column is "3.83"
	And the Weighted Average on the right for question 1 is "3.81"
	And the Group Average at the bottom for the "Female" column is "3.82"
	And the Group Average at the bottom for the "Male" column is "3.86"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "74"
	And the score for question 1 in the "Male" column is "76"
	And the Weighted Average on the right for question 1 is "75"
	And the Group Average at the bottom for the "Female" column is "71"
	And the Group Average at the bottom for the "Male" column is "72"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "73.6"
	And the score for question 1 in the "Male" column is "75.7"
	And the Weighted Average on the right for question 1 is "74.8"
	And the Group Average at the bottom for the "Female" column is "70.8"
	And the Group Average at the bottom for the "Male" column is "72.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "73.55"
	And the score for question 1 in the "Male" column is "75.75"
	And the Weighted Average on the right for question 1 is "74.77"
	And the Group Average at the bottom for the "Female" column is "70.80"
	And the Group Average at the bottom for the "Male" column is "72.09"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "10"
	And the score for question 1 in the "Male" column is "8"
	And the Weighted Average on the right for question 1 is "9"
	And the Group Average at the bottom for the "Female" column is "11"
	And the Group Average at the bottom for the "Male" column is "10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "9.7"
	And the score for question 1 in the "Male" column is "8.5"
	And the Weighted Average on the right for question 1 is "9.0"
	And the Group Average at the bottom for the "Female" column is "11.0"
	And the Group Average at the bottom for the "Male" column is "10.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "9.70"
	And the score for question 1 in the "Male" column is "8.46"
	And the Weighted Average on the right for question 1 is "9.01"
	And the Group Average at the bottom for the "Female" column is "11.04"
	And the Group Average at the bottom for the "Male" column is "10.44"

	Now I go to "Functions" > "Logout"

# Categories
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the first number under "Female" is "(644)"
	And the first number under "Male" is "(806)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the Clarity of Direction category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.9"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.0"
	And the Weighted Average on the right for the Clarity of Direction category is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.8"
	And the Group Average at the bottom for the "Male" column is "3.9"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.8"
	And the score for question 1 in the "Male" column is "3.8"
	And the Weighted Average on the right for question 1 is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.95"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.99"
	And the Weighted Average on the right for the Clarity of Direction category is "3.97"
	And the Group Average at the bottom for the "Female" column is "3.82"
	And the Group Average at the bottom for the "Male" column is "3.86"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.78"
	And the score for question 1 in the "Male" column is "3.83"
	And the Weighted Average on the right for question 1 is "3.81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "78"
	And the score for the "Clarity of Direction" category in the "Male" column is "79"
	And the Weighted Average on the right for the Clarity of Direction category is "79"
	And the Group Average at the bottom for the "Female" column is "71"
	And the Group Average at the bottom for the "Male" column is "72"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "74"
	And the score for question 1 in the "Male" column is "76"
	And the Weighted Average on the right for question 1 is "75"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "77.8"
	And the score for the "Clarity of Direction" category in the "Male" column is "79.1"
	And the Weighted Average on the right for the Clarity of Direction category is "78.5"
	And the Group Average at the bottom for the "Female" column is "70.8"
	And the Group Average at the bottom for the "Male" column is "72.1"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "73.6"
	And the score for question 1 in the "Male" column is "75.7"
	And the Weighted Average on the right for question 1 is "74.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "77.81"
	And the score for the "Clarity of Direction" category in the "Male" column is "79.08"
	And the Weighted Average on the right for the Clarity of Direction category is "78.52"
	And the Group Average at the bottom for the "Female" column is "70.80"
	And the Group Average at the bottom for the "Male" column is "72.09"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "73.55"
	And the score for question 1 in the "Male" column is "75.75"
	And the Weighted Average on the right for question 1 is "74.77"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "7"
	And the score for the "Clarity of Direction" category in the "Male" column is "7"
	And the Weighted Average on the right for the Clarity of Direction category is "7"
	And the Group Average at the bottom for the "Female" column is "11"
	And the Group Average at the bottom for the "Male" column is "10"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "10"
	And the score for question 1 in the "Male" column is "8"
	And the Weighted Average on the right for question 1 is "9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "7.1"
	And the score for the "Clarity of Direction" category in the "Male" column is "6.7"
	And the Weighted Average on the right for the Clarity of Direction category is "6.9"
	And the Group Average at the bottom for the "Female" column is "11.0"
	And the Group Average at the bottom for the "Male" column is "10.4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "9.7"
	And the score for question 1 in the "Male" column is "8.5"
	And the Weighted Average on the right for question 1 is "9.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "7.14"
	And the score for the "Clarity of Direction" category in the "Male" column is "6.72"
	And the Weighted Average on the right for the Clarity of Direction category is "6.90"
	And the Group Average at the bottom for the "Female" column is "11.04"
	And the Group Average at the bottom for the "Male" column is "10.44"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "9.70"
	And the score for question 1 in the "Male" column is "8.46"
	And the Weighted Average on the right for question 1 is "9.01"

	Now I go to "Functions" > "Logout"

# Questions + trend
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.8 (3.6)"
	And the score for question 1 in the "Male" column is "3.8 (3.8)"
	And the Weighted Average on the right for question 1 is "3.8"
	And the Group Average at the bottom for the "Female" column is "3.8 (3.7)"
	And the Group Average at the bottom for the "Male" column is "3.9 (3.7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.78 (3.60)"
	And the score for question 1 in the "Male" column is "3.83 (3.77)"
	And the Weighted Average on the right for question 1 is "3.81"
	And the Group Average at the bottom for the "Female" column is "3.82 (3.67)"
	And the Group Average at the bottom for the "Male" column is "3.86 (3.74)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "74 (64)"
	And the score for question 1 in the "Male" column is "76 (72)"
	And the Weighted Average on the right for question 1 is "75"
	And the Group Average at the bottom for the "Female" column is "71 (65)"
	And the Group Average at the bottom for the "Male" column is "72 (67)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "73.6 (63.8)"
	And the score for question 1 in the "Male" column is "75.7 (72.2)"
	And the Weighted Average on the right for question 1 is "74.8"
	And the Group Average at the bottom for the "Female" column is "70.8 (64.9)"
	And the Group Average at the bottom for the "Male" column is "72.1 (67.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "73.55 (63.85)"
	And the score for question 1 in the "Male" column is "75.75 (72.19)"
	And the Weighted Average on the right for question 1 is "74.77"
	And the Group Average at the bottom for the "Female" column is "70.80 (64.86)"
	And the Group Average at the bottom for the "Male" column is "72.09 (67.15)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "10 (13)"
	And the score for question 1 in the "Male" column is "8 (11)"
	And the Weighted Average on the right for question 1 is "9"
	And the Group Average at the bottom for the "Female" column is "11 (14)"
	And the Group Average at the bottom for the "Male" column is "10 (13)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "9.7 (12.8)"
	And the score for question 1 in the "Male" column is "8.5 (10.6)"
	And the Weighted Average on the right for question 1 is "9.0"
	And the Group Average at the bottom for the "Female" column is "11.0 (14.3)"
	And the Group Average at the bottom for the "Male" column is "10.4 (13.4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "9.70 (12.77)"
	And the score for question 1 in the "Male" column is "8.46 (10.60)"
	And the Weighted Average on the right for question 1 is "9.01"
	And the Group Average at the bottom for the "Female" column is "11.04 (14.33)"
	And the Group Average at the bottom for the "Male" column is "10.44 (13.42)"

	Now I go to "Functions" > "Logout"

# Categories + trend
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.9 (3.8)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.0 (3.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.8 (3.7)"
	And the Group Average at the bottom for the "Male" column is "3.9 (3.7)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.8 (3.6)"
	And the score for question 1 in the "Male" column is "3.8 (3.8)"
	And the Weighted Average on the right for question 1 is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.95 (3.81)"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.99 (3.93)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.97"
	And the Group Average at the bottom for the "Female" column is "3.82 (3.67)"
	And the Group Average at the bottom for the "Male" column is "3.86 (3.74)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.78 (3.60)"
	And the score for question 1 in the "Male" column is "3.83 (3.77)"
	And the Weighted Average on the right for question 1 is "3.81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "78 (72)"
	And the score for the "Clarity of Direction" category in the "Male" column is "79 (76)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79"
	And the Group Average at the bottom for the "Female" column is "71 (65)"
	And the Group Average at the bottom for the "Male" column is "72 (67)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "74 (64)"
	And the score for question 1 in the "Male" column is "76 (72)"
	And the Weighted Average on the right for question 1 is "75"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "77.8 (72.3)"
	And the score for the "Clarity of Direction" category in the "Male" column is "79.1 (76.4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.5"
	And the Group Average at the bottom for the "Female" column is "70.8 (64.9)"
	And the Group Average at the bottom for the "Male" column is "72.1 (67.2)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "73.6 (63.8)"
	And the score for question 1 in the "Male" column is "75.7 (72.2)"
	And the Weighted Average on the right for question 1 is "74.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "77.81 (72.31)"
	And the score for the "Clarity of Direction" category in the "Male" column is "79.08 (76.36)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.52"
	And the Group Average at the bottom for the "Female" column is "70.80 (64.86)"
	And the Group Average at the bottom for the "Male" column is "72.09 (67.15)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "73.55 (63.85)"
	And the score for question 1 in the "Male" column is "75.75 (72.19)"
	And the Weighted Average on the right for question 1 is "74.77"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "7 (9)"
	And the score for the "Clarity of Direction" category in the "Male" column is "7 (9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Female" column is "11 (14)"
	And the Group Average at the bottom for the "Male" column is "10 (13)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "10 (13)"
	And the score for question 1 in the "Male" column is "8 (11)"
	And the Weighted Average on the right for question 1 is "9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "7.1 (9.0)"
	And the score for the "Clarity of Direction" category in the "Male" column is "6.7 (8.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.9"
	And the Group Average at the bottom for the "Female" column is "11.0 (14.3)"
	And the Group Average at the bottom for the "Male" column is "10.4 (13.4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "9.7 (12.8)"
	And the score for question 1 in the "Male" column is "8.5 (10.6)"
	And the Weighted Average on the right for question 1 is "9.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "7.14 (9.05)"
	And the score for the "Clarity of Direction" category in the "Male" column is "6.72 (8.50)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.90"
	And the Group Average at the bottom for the "Female" column is "11.04 (14.33)"
	And the Group Average at the bottom for the "Male" column is "10.44 (13.42)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "9.70 (12.77)"
	And the score for question 1 in the "Male" column is "8.46 (10.60)"
	And the Weighted Average on the right for question 1 is "9.01"

	Now I go to "Functions" > "Logout"