Feature: Check filtered Trend Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	# And select "SD" from the "Select Your Option(s):" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(1450)"
	And the Count for "2013" is "(1456)"
	And the Count for "2012" is "(1446)"
	And the score for question 1 in the "2014" column is "3.81 (+0.12)"
	And the score for question 1 in the "2013" column is "3.69 (+0.31)"
	And the score for question 1 in the "2012" column is "3.38"
	And the Overall Average at the bottom for "2014" is "3.84 (+0.13)"
	And the Overall Average for "2013" is "3.71 (+0.12)"
	And the Overall Average for "2012" is "3.59"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(1450)"
	And the Count for "2013" is "(1456)"
	And the Count for "2012" is "(1446)"
	And the score for the "Clarity of Direction" category for "2014" is "3.97 (+0.09)"
	And the score for the "Clarity of Direction" category for "2013" is "3.88 (+0.22)"
	And the score for the "Clarity of Direction" category for "2012" is "3.66"
	And the Overall Average at the bottom for "2014" is "3.84 (+0.13)"
	And the Overall Average for "2013" is "3.71 (+0.12)"
	And the Overall Average for "2012" is "3.59"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "3.81 (+0.12)"
	And the score for question 1 in the "2013" column is "3.69 (+0.31)"
	And the score for question 1 in the "2012" column is "3.38"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(1450)"
	And the Count for "2013" is "(1456)"
	And the Count for "2012" is "(1446)"
	And the score for question 1 in the "2014" column is "74.8(+6.3)"
	And the score for question 1 in the "2013" column is "68.5(+14.6)"
	And the score for question 1 in the "2012" column is "53.9"
	And the Overall Average at the bottom for "2014" is "71.5 (+5.4)"
	And the Overall Average for "2013" is "66.1 (+3.7)"
	And the Overall Average for "2012" is "62.4"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(1450)"
	And the Count for "2013" is "(1456)"
	And the Count for "2012" is "(1446)"
	And the score for the "Clarity of Direction" category for "2014" is "78.5 (+4.0)"
	And the score for the "Clarity of Direction" category for "2013" is "74.5 (+9.4)"
	And the score for the "Clarity of Direction" category for "2012" is "65.1"
	And the Overall Average at the bottom for "2014" is "71.5 (+5.4)"
	And the Overall Average for "2013" is "66.1 (+3.7)"
	And the Overall Average for "2012" is "62.4"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "74.8 (+6.3)"
	And the score for question 1 in the "2013" column is "68.5 (+14.6)"
	And the score for question 1 in the "2012" column is "53.9"

	Now I go to "Functions" > "Logout"