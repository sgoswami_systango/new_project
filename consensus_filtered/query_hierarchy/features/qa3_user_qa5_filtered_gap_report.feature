Feature: Check filtered Gap Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter Count" in the top right is "15"
	And the Eval Avg for question 1 is "3.60"
	And the Consensus for question 1 is "4.20"
	And the Gap for question 1 is "0.60"
	And the Weighted Gap for question 1 is "12.60"
	And the Overall Average at the bottom has an Eval Avg of "3.57"
	And the Overall Average has a Consensus of "3.80"
	And the Overall Average has a Gap of "0.23"
	And the Overall Average has a Weighted Gap of "4.37"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.77"
	And the Consensus for the "Clarity of Direction" category is "3.97"
	And the Gap for the "Clarity of Direction" category is "0.20"
	And the Weighted Gap for the "Clarity of Direction" category is "3.97"
	And the Overall Average at the bottom has an Eval Avg of "3.57"
	And the Overall Average has a Consensus of "3.80"
	And the Overall Average has a Gap of "0.23"
	And the Overall Average has a Weighted Gap of "4.37"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.60"
	And the Consensus for question 1 is "4.20"
	And the Gap for question 1 is "0.60"
	And the Weighted Gap for question 1 is "12.60"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the number for "Filter" in the top right is "15"
	And the number for "Remainder" in the top right is "24"
	And the Eval Avg for question 1 is "3.60 (-0.57)"
	And the Consensus for question 1 is "4.20 (-0.05)"
	And the Gap for question 1 is "0.60 (+0.52)"
	And the Weighted Gap for question 1 is "12.60 (+10.90)"
	And the Overall Average at the bottom has an Eval Avg of "3.57 (-0.37)"
	And the Overall Average has a Consensus of "3.80 (-0.22)"
	And the Overall Average has a Gap of "0.23 (+0.15)"
	And the Overall Average has a Weighted Gap of "4.37 (+2.76)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "3.77 (-0.46)"
	And the Consensus for the "Clarity of Direction" category is "3.97 (-0.21)"
	And the Gap for the "Clarity of Direction" category is "0.20 (+0.25)"
	And the Weighted Gap for the "Clarity of Direction" category is "3.97 (+5.01)"
	And the Overall Average at the bottom has an Eval Avg of "3.57 (-0.37)"
	And the Overall Average has a Consensus of "3.80 (-0.22)"
	And the Overall Average has a Gap of "0.23 (+0.15)"
	And the Overall Average has a Weighted Gap of "4.37 (+2.76)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.60 (-0.57)"
	And the Consensus for question 1 is "4.20 (-0.05)"
	And the Gap for question 1 is "0.60 (+0.52)"
	And the Weighted Gap for question 1 is "12.60 (+10.90)"
	
	Now I go to "Functions" > "Logout"