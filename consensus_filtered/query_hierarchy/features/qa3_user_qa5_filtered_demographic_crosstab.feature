Feature: Check filtered Demographic Crosstab for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the first number under "Male" is "(8)"
	And the score for question 1 in the "Male" column is "3"
	And the Weighted Average on the right for question 1 is "3"
	And the Group Average at the bottom for the "Male" column is "3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "3.4"
	And the Weighted Average on the right for question 1 is "3.4"
	And the Group Average at the bottom for the "Male" column is "3.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "3.38"
	And the Weighted Average on the right for question 1 is "3.38"
	And the Group Average at the bottom for the "Male" column is "3.15"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "62"
	And the Weighted Average on the right for question 1 is "62"
	And the Group Average at the bottom for the "Male" column is "46"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "62.5"
	And the Weighted Average on the right for question 1 is "62.5"
	And the Group Average at the bottom for the "Male" column is "45.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "62.50"
	And the Weighted Average on the right for question 1 is "62.50"
	And the Group Average at the bottom for the "Male" column is "45.75"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "12"
	And the Weighted Average on the right for question 1 is "12"
	And the Group Average at the bottom for the "Male" column is "33"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "12.5"
	And the Weighted Average on the right for question 1 is "12.5"
	And the Group Average at the bottom for the "Male" column is "32.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "12.50"
	And the Weighted Average on the right for question 1 is "12.50"
	And the Group Average at the bottom for the "Male" column is "32.75"

	Now I go to "Functions" > "Logout"

# Categories
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the first number under "Male" is "(8)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "3"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "3"
	And the Weighted Average on the right for question 1 is "3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "3.5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.5"
	And the Group Average at the bottom for the "Male" column is "3.1"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "3.4"
	And the Weighted Average on the right for question 1 is "3.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "3.50"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.50"
	And the Group Average at the bottom for the "Male" column is "3.15"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "3.38"
	And the Weighted Average on the right for question 1 is "3.38"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "60"
	And the Weighted Average on the right for the "Clarity of Direction" category is "60"
	And the Group Average at the bottom for the "Male" column is "46"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "62"
	And the Weighted Average on the right for question 1 is "62"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "60.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "60.0"
	And the Group Average at the bottom for the "Male" column is "45.8"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "62.5"
	And the Weighted Average on the right for question 1 is "62.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "60.00"
	And the Weighted Average on the right for the "Clarity of Direction" category is "60.00"
	And the Group Average at the bottom for the "Male" column is "45.75"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "62.50"
	And the Weighted Average on the right for question 1 is "62.50"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "18"
	And the Weighted Average on the right for the "Clarity of Direction" category is "18"
	And the Group Average at the bottom for the "Male" column is "33"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "12"
	And the Weighted Average on the right for question 1 is "12"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "17.5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "17.5"
	And the Group Average at the bottom for the "Male" column is "32.8"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "12.5"
	And the Weighted Average on the right for question 1 is "12.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "17.50"
	And the Weighted Average on the right for the "Clarity of Direction" category is "17.50"
	And the Group Average at the bottom for the "Male" column is "32.75"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "12.50"
	And the Weighted Average on the right for question 1 is "12.50"

	Now I go to "Functions" > "Logout"

# Questions + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "3 (3)"
	And the Weighted Average on the right for question 1 is "3"
	And the Group Average at the bottom for the "Male" column is "3 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "3.4 (3.5)"
	And the Weighted Average on the right for question 1 is "3.4"
	And the Group Average at the bottom for the "Male" column is "3.1 (3.6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "3.38 (3.45)"
	And the Weighted Average on the right for question 1 is "3.38"
	And the Group Average at the bottom for the "Male" column is "3.15 (3.65)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "62 (55)"
	And the Weighted Average on the right for question 1 is "62"
	And the Group Average at the bottom for the "Male" column is "46 (64)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "62.5 (54.5)"
	And the Weighted Average on the right for question 1 is "62.5"
	And the Group Average at the bottom for the "Male" column is "45.8 (63.8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "62.50 (54.55)"
	And the Weighted Average on the right for question 1 is "62.50"
	And the Group Average at the bottom for the "Male" column is "45.75 (63.84)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "12 (18)"
	And the Weighted Average on the right for question 1 is "12"
	And the Group Average at the bottom for the "Male" column is "33 (19)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "12.5 (18.2)"
	And the Weighted Average on the right for question 1 is "12.5"
	And the Group Average at the bottom for the "Male" column is "32.8 (19.0)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	And the score for question 1 in the "Male" column is "12.50 (18.18)"
	And the Weighted Average on the right for question 1 is "12.50"
	And the Group Average at the bottom for the "Male" column is "32.75 (18.99)"

	Now I go to "Functions" > "Logout"

# Categories + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "3 (4)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "3 (3)"
	And the Weighted Average on the right for question 1 is "3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "3.5 (3.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.5"
	And the Group Average at the bottom for the "Male" column is "3.1 (3.6)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "3.4 (3.5)"
	And the Weighted Average on the right for question 1 is "3.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "3.50 (3.91)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.50"
	And the Group Average at the bottom for the "Male" column is "3.15 (3.65)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "3.38 (3.45)"
	And the Weighted Average on the right for question 1 is "3.38"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "60 (76)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "60"
	And the Group Average at the bottom for the "Male" column is "46 (64)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "62 (55)"
	And the Weighted Average on the right for question 1 is "62"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "60.0 (76.4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "60.0"
	And the Group Average at the bottom for the "Male" column is "45.8 (63.8)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "62.5 (54.5)"
	And the Weighted Average on the right for question 1 is "62.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "60.00 (76.36)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "60.00"
	And the Group Average at the bottom for the "Male" column is "45.75 (63.84)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "62.50 (54.55)"
	And the Weighted Average on the right for question 1 is "62.50"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "18 (11)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "18"
	And the Group Average at the bottom for the "Male" column is "33 (19)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "12 (18)"
	And the Weighted Average on the right for question 1 is "12"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "17.5 (10.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "17.5"
	And the Group Average at the bottom for the "Male" column is "32.8 (19.0)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "12.5 (18.2)"
	And the Weighted Average on the right for question 1 is "12.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	And the score for the "Clarity of Direction" category in the "Male" column is "17.50 (10.91)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "17.50"
	And the Group Average at the bottom for the "Male" column is "32.75 (18.99)"

	When I click on the Clarity of Direction category
	And the score for question 1 in the "Male" column is "12.50 (18.18)"
	And the Weighted Average on the right for question 1 is "12.50"

	Now I go to "Functions" > "Logout"