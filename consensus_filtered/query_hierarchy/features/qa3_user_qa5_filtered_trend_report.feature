Feature: Check filtered Trend Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip
Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(15)"
	And the Count for "2013" is "(22)"
	And the score for question 1 in the "2014" column is "3.60 (+0.15)"
	And the score for question 1 in the "2013" column is "3.45"
	And the Overall Average at the bottom for "2014" is "3.57 (+0.14)"
	And the Overall Average for "2013" is "3.43"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(15)"
	And the Count for "2013" is "(22)"
	And the score for the "Clarity of Direction" category for "2014" is "3.77 (+0.04)"
	And the score for the "Clarity of Direction" category for "2013" is "3.73"
	And the Overall Average at the bottom for "2014" is "3.57 (+0.14)"
	And the Overall Average for "2013" is "3.43"
	
	When I click on the Clarity of Direction category
	Then the score for question 1 in the "2014" column is "3.60 (+0.15)"
	And the score for question 1 in the "2013" column is "3.45h"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(15)"
	And the Count for "2013" is "(22)"
	And the score for question 1 in the "2014" column is "73.3 (+18.8)"
	And the score for question 1 in the "2013" column is "54.5"
	And the Overall Average at the bottom for "2014" is "64.0 (+11.2)"
	And the Overall Average for "2013" is "52.8"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(15)"
	And the Count for "2013" is "(22)"
	And the score for the "Clarity of Direction" category for "2014" is "74.7 (+8.3)"
	And the score for the "Clarity of Direction" category for "2013" is "66.4"
	And the Overall Average at the bottom for "2014" is "64.0 (+11.2)"
	And the Overall Average for "2013" is "52.8"
	
	When I click on the Clarity of Direction category
	Then the score for question 1 in the "2014" column is "73.3 (+18.8)"
	And the score for question 1 in the "2013" column is "54.5"

	Now I go to "Functions" > "Logout"