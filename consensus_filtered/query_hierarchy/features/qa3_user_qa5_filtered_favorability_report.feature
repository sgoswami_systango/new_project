Feature: Check filtered Favorability Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter" in the top right is "15"
	And the Favorable score for question 1 is "73.3%"
	And the Neutral score for question 1 is "20.0%"
	And the Unfavorable score for question 1 is "6.7%"
	And the Overall Average at the bottom has a Favorable score of "64.0%"
	And the Overall Average Neutral score is "15.7%"
	And the Overall Average Unfavorable score is "20.3%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "74.7%"
	And the Neutral score for the "Clarity of Direction" category is "14.7%"
	And the Unfavorable score for the "Clarity of Direction" category is "10.7%"
	And the Overall Average at the bottom has a Favorable score of "64.0%"
	And the Overall Average Neutral score is "15.7%"
	And the Overall Average Unfavorable score is "20.3%"

	When I click on the Clarity of Direction category
	Then The Favorable score for question 1 is "73.3%"
	And the Neutral score for question 1 is "20.0%"
	And the Unfavorable score for question 1 is "6.7%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	
	Then the number for "Filter" in the top right is "15"
	And the number for "Remainder" in the top right is "24"
	And the Favorable score on top for question 1 is "73.3%"
	And the Neutral score on top for question 1 is "20.0%"
	And the Unfavorable score on top for question 1 is "6.7%"
	And the Favorable score on bottom for question 1 is "79.2%"
	And the Neutral score on bottom for question 1 is "20.8%"
	And there is no Unfavorable score on bottom for question 1
	And the Overall Average at the bottom Favorable score on top is "64.0%"
	And the Overall Average Neutral score on top is "15.7%"
	And the Overall Average Unfavorable score on top is "20.3%"
	And the Overall Average Favorable score on bottom is "74.6%"
	And the Overall Average Neutral score on bottom is "14.9%"
	And the Overall Average Unfavorable score on bottom is "10.5%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "74.7%"
	And the Neutral score on top for the "Clarity of Direction" category is "14.7%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "10.7%"
	And the Favorable score on bottom for the "Clarity of Direction" category is "88.3%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "7.5%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "4.2%"
	And the Overall Average at the bottom Favorable score on top is "64.0%"
	And the Overall Average Neutral score on top is "15.7%"
	And the Overall Average Unfavorable score on top is "20.3%"
	And the Overall Average Favorable score on bottom is "74.6%"
	And the Overall Average Neutral score on bottom is "14.9%"
	And the Overall Average Unfavorable score on bottom is "10.5%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "73.3%"
	And the Neutral score on top for question 1 is "20.0%"
	And the Unfavorable score on top for question 1 is "6.7%"
	And the Favorable score on bottom for question 1 is "79.2%"
	And the Neutral score on bottom for question 1 is "20.8%"
	And there is no Unfavorable score on bottom for question 1
	
	Now I go to "Functions" > "Logout"
