Feature: Check filtered Completion Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Female" in the "Invitees" column is "11"
	And the figure for "Female" in the "Respondents" column is "7(63.6%)"
	And the figure for "Female" in the "Completions" column is "7(63.6%)"
	And the figure for the "Totals" row in the "Invitees" column is "19"
	And the figure for the "Totals" row in the "Respondents" column is "15(78.9%)"
	And the figure for the "Totals" row in the "Completions" column is "15(78.9%)"

	When I am on the "By Time" tab
	Then the figure for "Wednesday, September 3, 2014" in the "Respondents" column is "3"
	And the figure for "Wednesday, September 3, 2014" in the "% Responded" column is "20.0%"
	And the figure for "Wednesday, September 3, 2014" in the "% Invited" column is "15.8%"
	And the figure for the "Totals" row in the "Respondents" column is "15"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "78.9%"
	
	When I click on "Wednesday, September 3, 2014"
	Then the figure for "07:00 PM" in the "Respondents" column is "1"
	And the figure for "07:00 PM" in the "% Responded" column is "6.7%"
	And the figure for "07:00 PM" in the "% Invited" column is "5.3%"
	
	Now I go to "Functions" > "Logout"