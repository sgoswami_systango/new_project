Feature: Check filtered Survey Results for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "1"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "6.7%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "0"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "0.0%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "3"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "20.0%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "11"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "73.3%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "0"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "0.0%"
	And the "Mean" under question 1 is "3.60 (±0.80)"
	And the figure under question 1 for "Mean" in the "Number" column is "15"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the figure for "Filter" in the upper right is "15"
	And the figure for "Remainder" in the upper right is "24"
	And the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "1(0)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "6.7%(0.0%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "0(0)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "0.0%(0.0%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "3(5)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "20.0%(20.8%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "11(10)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "73.3%(41.7%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "0(9)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "0.0%(37.5%)"
	And the "Mean" under question 1 is "3.60 (±0.80)"
	And the figure under question 1 for "Mean" in the "Number" column is "15(24)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"