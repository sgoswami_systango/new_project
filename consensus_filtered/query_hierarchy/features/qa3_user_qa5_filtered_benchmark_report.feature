Feature: Check filtered Trend Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 1 in the "My Filter" column is "73.3%"
	And the score for question 1 in the "My View" column is "76.9%(3.6)"
	And the score for question 1 in the "LPL Financial" column is "75.5%(2.2)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-4.7)"

	When I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "My Filter" column is "74.7%"
	And the score for the "Clarity of Direction" category in the "My View" column is "83.1%"
	And the score for the "Clarity of Direction" category in the "LPL Financial" column is "80.2%"
	Then the score for question 1 in the "My Filter" column is "73.3%"
	And the score for question 1 in the "My View" column is "76.9%(3.6)"
	And the score for question 1 in the "LPL Financial" column is "75.5%(2.2)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-4.7)"
	
	Now I go to "Functions" > "Logout"