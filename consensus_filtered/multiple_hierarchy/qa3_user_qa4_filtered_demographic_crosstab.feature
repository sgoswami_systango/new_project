Feature: Check filtered Demographic Crosstab for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Below Manager" is "(14)"
	Then the score for question 1 in the "Below Manager" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Below Manager" column is "4"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Below Manager" column is "3.6"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "4.00"
	And the Weighted Average on the right for question 1 is "4.00"
	And the Group Average at the bottom for the "Below Manager" column is "3.64"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "79"
	And the Weighted Average on the right for question 1 is "79"
	And the Group Average at the bottom for the "Below Manager" column is "65"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "78.6"
	And the Weighted Average on the right for question 1 is "78.6"
	And the Group Average at the bottom for the "Below Manager" column is "65.0"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "78.57"
	And the Weighted Average on the right for question 1 is "78.57"
	And the Group Average at the bottom for the "Below Manager" column is "65.00"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "7"
	And the Weighted Average on the right for question 1 is "7"
	And the Group Average at the bottom for the "Below Manager" column is "14"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "7.1"
	And the Weighted Average on the right for question 1 is "7.1"
	And the Group Average at the bottom for the "Below Manager" column is "13.9"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "7.14"
	And the Weighted Average on the right for question 1 is "7.14"
	And the Group Average at the bottom for the "Below Manager" column is "13.86"

	Now I go to "Functions" > "Logout"

# Categories
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner


	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the first number under "Below Manager" is "(14)"
	And the score for the "Clarity of Direction" category in the "Below Manager" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Below Manager" column is "4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "3.9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.9"
	And the Group Average at the bottom for the "Below Manager" column is "3.6"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "3.91"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.91"
	And the Group Average at the bottom for the "Below Manager" column is "3.64"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "4.00"
	And the Weighted Average on the right for question 1 is "4.00"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "79"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79"
	And the Group Average at the bottom for the "Below Manager" column is "65"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "79"
	And the Weighted Average on the right for question 1 is "79"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "78.6"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.6"
	And the Group Average at the bottom for the "Below Manager" column is "65.0"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "78.6"
	And the Weighted Average on the right for question 1 is "78.6"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "78.57"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.57"
	And the Group Average at the bottom for the "Below Manager" column is "65.00"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "78.57"
	And the Weighted Average on the right for question 1 is "78.57"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Below Manager" column is "14"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "7"
	And the Weighted Average on the right for question 1 is "7"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "8.6"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.6"
	And the Group Average at the bottom for the "Below Manager" column is "13.9"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "7.1"
	And the Weighted Average on the right for question 1 is "7.1"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "8.57"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.57"
	And the Group Average at the bottom for the "Below Manager" column is "13.86"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "7.14"
	And the Weighted Average on the right for question 1 is "7.14"

	Now I go to "Functions" > "Logout"

# Questions + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner


	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "4 (3)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Below Manager" column is "4 (4)"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "4.0 (3.0)"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Below Manager" column is "3.6 (3.5)"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "4.00 (3.00)"
	And the Weighted Average on the right for question 1 is "4.00"
	And the Group Average at the bottom for the "Below Manager" column is "3.64 (3.52)"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "79 (33)"
	And the Weighted Average on the right for question 1 is "79"
	And the Group Average at the bottom for the "Below Manager" column is "65 (60)"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "78.6 (33.3)"
	And the Weighted Average on the right for question 1 is "78.6"
	And the Group Average at the bottom for the "Below Manager" column is "65.0 (60.5)"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "78.57 (33.33)"
	And the Weighted Average on the right for question 1 is "78.57"
	And the Group Average at the bottom for the "Below Manager" column is "65.00 (60.49)"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "7 (22)"
	And the Weighted Average on the right for question 1 is "7"
	And the Group Average at the bottom for the "Below Manager" column is "14 (14)"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "7.1 (22.2)"
	And the Weighted Average on the right for question 1 is "7.1"
	And the Group Average at the bottom for the "Below Manager" column is "13.9 (13.8)"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Below Manager" column is "7.14 (22.22)"
	And the Weighted Average on the right for question 1 is "7.14"
	And the Group Average at the bottom for the "Below Manager" column is "13.86 (13.83)"

	Now I go to "Functions" > "Logout"

# Categories + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner


	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "4 (3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Below Manager" column is "4 (4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "4 (3)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "3.9 (3.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.9"
	And the Group Average at the bottom for the "Below Manager" column is "3.6 (3.5)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "4.0 (3.0)"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "3.91 (3.11)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.91"
	And the Group Average at the bottom for the "Below Manager" column is "3.64 (3.52)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "4.00 (3.00)"
	And the Weighted Average on the right for question 1 is "4.00"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "79 (47)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "79"
	And the Group Average at the bottom for the "Below Manager" column is "65 (60)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "79 (33)"
	And the Weighted Average on the right for question 1 is "79"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "78.6 (46.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.6"
	And the Group Average at the bottom for the "Below Manager" column is "65.0 (60.5)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "78.6 (33.3)"
	And the Weighted Average on the right for question 1 is "78.6"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "78.57 (46.67)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.57"
	And the Group Average at the bottom for the "Below Manager" column is "65.00 (60.49)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "78.57 (33.33)"
	And the Weighted Average on the right for question 1 is "78.57"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "9 (22)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Below Manager" column is "14 (14)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "7 (22)"
	And the Weighted Average on the right for question 1 is "7"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "8.6 (22.2)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.6"
	And the Group Average at the bottom for the "Below Manager" column is "13.9 (13.8)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "7.1 (22.2)"
	And the Weighted Average on the right for question 1 is "7.1"

	When I select "Custom Management Categories" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Below Manager" column is "8.57 (22.22)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.57"
	And the Group Average at the bottom for the "Below Manager" column is "13.86 (13.83)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Below Manager" column is "7.14 (22.22)"
	And the Weighted Average on the right for question 1 is "7.14"

	Now I go to "Functions" > "Logout"