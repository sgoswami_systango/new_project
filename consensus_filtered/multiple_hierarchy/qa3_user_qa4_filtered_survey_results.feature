Feature: Check filtered Survey Results for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "1"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "7.1%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "2"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "14.3%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "7"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "50.0%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "4"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "28.6%"
	And the "Mean" under question 1 is "4.00 (±0.85)"
	And the figure under question 1 for "Mean" in the "Number" column is "14"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the figure for "Filter" in the upper right is "14"
	And the figure for "Remainder" in the upper right is "16"
	And the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0(0)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%(0.0%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "1(1)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "7.1%(6.2%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "2(1)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "14.3%(6.2%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "7(13)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "50.0%(81.2%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "4(1)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "28.6%(6.2%)"
	And the "Mean" under question 1 is "4.00 (±0.85)"
	And the figure under question 1 for "Mean" in the "Number" column is "14(16)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"