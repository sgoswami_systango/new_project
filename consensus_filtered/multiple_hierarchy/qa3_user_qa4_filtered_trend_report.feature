Feature: Check filtered Trend Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(14)"
	And the Count for "2013" is "(9)"
	And the score for question 1 in the "2014" column is "4.00 (+1.00)"
	And the score for question 1 in the "2013" column is "3.00"
	And the Overall Average at the bottom for "2014" is "3.64 (+0.12)"
	And the Overall Average for "2013" is "3.52"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the Count for "2014" is "(14)"
	And the Count for "2013" is "(9)"
	And the score for the "Clarity of Direction" category for "2014" is "3.91 (+0.80)"
	And the score for the "Clarity of Direction" category for "2013" is "3.11"
	And the Overall Average at the bottom for "2014" is "3.64 (+0.12)"
	And the Overall Average for "2013" is "3.52"
	
	When I click on the Clarity of Direction category
	Then the score for question 1 in the "2014" column is "4.00 (+1.00)"
	And the score for question 1 in the "2013" column is "3.00"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(14)"
	And the Count for "2013" is "(9)"
	And the score for question 1 in the "2014" column is "78.6 (+45.3)"
	And the score for question 1 in the "2013" column is "33.3"
	And the Overall Average at the bottom for "2014" is "65.0 (+4.5)"
	And the Overall Average for "2013" is "60.5"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the Count for "2014" is "(14)"
	And the Count for "2013" is "(9)"
	And the score for the "Clarity of Direction" category for "2014" is "78.6 (+31.9)"
	And the score for the "Clarity of Direction" category for "2013" is "46.7"
	And the Overall Average at the bottom for "2014" is "65.0 (+4.5)"
	And the Overall Average for "2013" is "60.5"
	
	When I click on the Clarity of Direction category
	Then the score for question 1 in the "2014" column is "78.6 (+45.3)"
	And the score for question 1 in the "2013" column is "33.3"

	Now I go to "Functions" > "Logout"