Feature: Check filtered Favorability Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter" in the top right is "14"
	And the Favorable score for question 1 is "78.6%"
	And the Neutral score for question 1 is "14.3%"
	And the Unfavorable score for question 1 is "7.1%"
	And the Overall Average at the bottom has a Favorable score of "65.0%"
	And the Overall Average Neutral score is "21.1%"
	And the Overall Average Unfavorable score is "13.9%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "78.6%"
	And the Neutral score for the "Clarity of Direction" category is "12.9%"
	And the Unfavorable score for the "Clarity of Direction" category is "8.6%"
	And the Overall Average at the bottom has a Favorable score of "65.0%"
	And the Overall Average Neutral score is "21.1%"
	And the Overall Average Unfavorable score is "13.9%"

	When I click on the Clarity of Direction category
	The Favorable score for question 1 is "78.6%"
	And the Neutral score for question 1 is "14.3%"
	And the Unfavorable score for question 1 is "7.1%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	
	Then the number for "Filter" in the top right is "14"
	And the number for "Remainder" in the top right is "16"
	And the Favorable score on top for question 1 is "78.6%"
	And the Neutral score on top for question 1 is "14.3%"
	And the Unfavorable score on top for question 1 is "7.1%"
	And the Favorable score on bottom for question 1 is "87.5%"
	And the Neutral score on bottom for question 1 is "6.2%"
	And the Unfavorable score on bottom for question 1 is "6.2%"
	And the Overall Average at the bottom Favorable score on top is "65.0%"
	And the Overall Average Neutral score on top is "21.1%"
	And the Overall Average Unfavorable score on top is "13.9%"
	And the Overall Average Favorable score on bottom is "71.4%"
	And the Overall Average Neutral score on bottom is "21.0%"
	And the Overall Average Unfavorable score on bottom is "7.6%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "78.6%"
	And the Neutral score on top for the "Clarity of Direction" category is "12.9%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "8.6%"
	And the Favorable score on bottom for the "Clarity of Direction" category is "82.5%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "15.0%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "2.5%"
	And the Overall Average at the bottom Favorable score on top is "65.0%"
	And the Overall Average Neutral score on top is "21.1%"
	And the Overall Average Unfavorable score on top is "13.9%"
	And the Overall Average Favorable score on bottom is "71.4%"
	And the Overall Average Neutral score on bottom is "21.0%"
	And the Overall Average Unfavorable score on bottom is "7.6%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "78.6%"
	And the Neutral score on top for question 1 is "14.3%"
	And the Unfavorable score on top for question 1 is "7.1%"
	And the Favorable score on bottom for question 1 is "87.5%"
	And the Neutral score on bottom for question 1 is "6.2%"
	And the Unfavorable score on bottom for question 1 is "6.2%"
	
	Now I go to "Functions" > "Logout"
