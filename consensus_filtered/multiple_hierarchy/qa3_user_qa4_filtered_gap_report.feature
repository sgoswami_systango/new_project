Feature: Check filtered Gap Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Location" from the "Select a Demographic:" popdown
	And select "SD" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for "Filter Count" in the top right is "14"
	And the Eval Avg for question 1 is "4.00"
	And the Consensus for question 1 is "4.15"
	And the Gap for question 1 is "0.15"
	And the Weighted Gap for question 1 is "3.11"
	And the Overall Average at the bottom has an Eval Avg of "3.64"
	And the Overall Average has a Consensus of "3.98"
	And the Overall Average has a Gap of "0.34"
	And the Overall Average has a Weighted Gap of "6.77"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.91"
	And the Consensus for the "Clarity of Direction" category is "4.18"
	And the Gap for the "Clarity of Direction" category is "0.27"
	And the Weighted Gap for the "Clarity of Direction" category is "5.64"
	And the Overall Average at the bottom has an Eval Avg of "3.64"
	And the Overall Average has a Consensus of "3.98"
	And the Overall Average has a Gap of "0.34"
	And the Overall Average has a Weighted Gap of "6.77"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "4.00"
	And the Consensus for question 1 is "4.15"
	And the Gap for question 1 is "0.15"
	And the Weighted Gap for question 1 is "3.11"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the number for "Filter" in the top right is "14"
	And the number for "Remainder" in the top right is "16"
	And the Eval Avg for question 1 is "4.00 (+0.12)"
	And the Consensus for question 1 is "4.15 (-0.25)"
	And the Gap for question 1 is "0.15 (-0.37)"
	And the Weighted Gap for question 1 is "3.11 (-8.33)"
	And the Overall Average at the bottom has an Eval Avg of "3.64 (-0.19)"
	And the Overall Average has a Consensus of "3.98 (-0.16)"
	And the Overall Average has a Gap of "0.34 (+0.03)"
	And the Overall Average has a Weighted Gap of "6.77 (+0.35)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Remainder" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "3.91 (-0.13)"
	And the Consensus for the "Clarity of Direction" category is "4.18 (-0.12)"
	And the Gap for the "Clarity of Direction" category is "0.27 (+0.01)"
	And the Weighted Gap for the "Clarity of Direction" category is "5.64 (+0.05)"
	And the Overall Average at the bottom has an Eval Avg of "3.64 (-0.19)"
	And the Overall Average has a Consensus of "3.98 (-0.16)"
	And the Overall Average has a Gap of "0.34 (+0.03)"
	And the Overall Average has a Weighted Gap of "6.77 (+0.35)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "4.00 (+0.12)"
	And the Consensus for question 1 is "4.15 (-0.25)"
	And the Gap for question 1 is "0.15 (-0.37)"
	And the Weighted Gap for question 1 is "3.11 (-8.33)"
	
	Now I go to "Functions" > "Logout"