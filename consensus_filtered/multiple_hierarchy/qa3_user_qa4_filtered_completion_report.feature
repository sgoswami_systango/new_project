Feature: Check filtered Completion Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
	
	Then I click on "Filter Data" > "New Filter"
	And select "Custom Management Categories" from the "Select a Demographic:" popdown
	And select "Below Manager" from the "Select Your Option" popdown
	And I click "Submit Filter"
	Then the page finishes loading
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Female" in the "Invitees" column is "14"
	And the figure for "Female" in the "Respondents" column is "14(100.0%)"
	And the figure for "Female" in the "Completions" column is "14(100.0%)"
	And the figure for the "Totals" row in the "Invitees" column is "27"
	And the figure for the "Totals" row in the "Respondents" column is "27(100.0%)"
	And the figure for the "Totals" row in the "Completions" column is "27(100.0%)"

	When I am on the "By Time" tab
	Then the figure for "Wednesday, September 3, 2014" in the "Respondents" column is "4"
	And the figure for "Wednesday, September 3, 2014" in the "% Responded" column is "14.8%"
	And the figure for "Wednesday, September 3, 2014" in the "% Invited" column is "14.8%"
	And the figure for the "Totals" row in the "Respondents" column is "27"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "100.0%"
	
	When I click on "Wednesday, September 3, 2014"
	Then the figure for "12:00 PM" in the "Respondents" column is "2"
	And the figure for "12:00 PM" in the "% Responded" column is "7.4%"
	And the figure for "12:00 PM" in the "% Invited" column is "7.4%"
	
	Now I go to "Functions" > "Logout"