from features.support.lib.utils import *


@step('I am on the "Benchmark Report" page')
def verify_page(step):
    time.sleep(2)
    assert get_browser().is_text_present("Benchmark Report")
    assert 'benchmark' in get_browser().url
