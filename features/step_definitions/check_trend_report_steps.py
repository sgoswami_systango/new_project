from features.support.lettuce_env import *
from features.support.lib.fill_survey_helper import *

def get_question_score_details_in_evaluation(index):
    return get_browser().find_by_css('.report-table .data.standalone')[index].text

def get_category_score_details_in_evaluation(index):
    return get_browser().find_by_css('.report-table .data.category')[index].text

def get_question_score_details_in_favoribility(index):
    return get_browser().find_by_css('.report-table')[2].find_by_css('.data.standalone')[index].text

def get_category_score_details_in_favoribility(index):
    return get_browser().find_by_css('.report-table')[3].find_by_css('.data.category')[index].text

@step('the 2012 score for question 5 for "([^"]*)" is "([^"]*)"')
def verify_2012_score_for_question5(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(4)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(4)

@step('the 2012 score for question 10 for "([^"]*)" is "([^"]*)"')
def verify_2012_score_for_question5(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(9)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(9)

@step('the 2012 score for question 12 for "([^"]*)" is "([^"]*)"')
def verify_2012_score_for_question12(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(11)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(11)

@step('the 2012 score for question 40 for "([^"]*)" is "([^"]*)"')
def verify_2012_score_for_question40(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(39)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(39)

@step('the 2012 score for question 58 for "([^"]*)" is "([^"]*)"')
def verify_2012_score_for_question58(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(57)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(57)

@step('the 2011 score for question 5 for "([^"]*)" is "([^"]*)"')
def verify_2011_score_for_question5(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(4)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(4)

@step('the 2011 score for question 10 for "([^"]*)" is "([^"]*)"')
def verify_2011_score_for_question10(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(9)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(9)

@step('the 2011 score for question 12 for "([^"]*)" is "([^"]*)"')
def verify_2011_score_for_question12(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(11)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(11)

@step('the 2011 score for question 40 for "([^"]*)" is "([^"]*)"')
def verify_2011_score_for_question40(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(39)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(39)

@step('the 2011 score for question 58 for "([^"]*)" is "([^"]*)"')
def verify_2011_score_for_question58(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(57)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(57)

@step('the 2009 score for question 5 for "([^"]*)" is "([^"]*)"')
def verify_2009_score_for_question5(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(4)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(4)

@step('the 2009 score for question 10 for "([^"]*)" is "([^"]*)"')
def verify_2009_score_for_question10(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(9)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(9)

@step('the 2009 score for question 40 for "([^"]*)" is "([^"]*)"')
def verify_2009_score_for_question40(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(39)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(39)

@step('the 2009 score for question 58 for "([^"]*)" is "([^"]*)"')
def verify_2009_score_for_question58(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_question_score_details_in_evaluation(57)
    elif tab_type == "Favorability":
      assert score in get_question_score_details_in_favoribility(57)

@step('the Overall Average at the bottom has a 2012 score of "([^"]*)"')
def verify_2012_average(step, score):
    assert score in get_question_score_details_in_evaluation(61)

@step('the Overall Average at the bottom has a 2011 score of "([^"]*)"')
def verify_2011_average(step, score):
    assert score in get_question_score_details_in_evaluation(61)

@step('the Overall Average at the bottom has a 2009 score of "([^"]*)"')
def verify_2009_average(step, score):
    assert score in get_question_score_details_in_evaluation(61)

@step('the 2012 score for Clarity of Direction for "([^"]*)" is "([^"]*)"')
def verify_2012_score_for_clarity_of_direction(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_category_score_details_in_evaluation(0)
    elif tab_type == "Favorability":
      assert score in get_category_score_details_in_favoribility(0)

@step('the 2011 score for Clarity of Direction for "([^"]*)" is "([^"]*)"')
def verify_2011_score_for_clarity_of_direction(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_category_score_details_in_evaluation(0)
    elif tab_type == "Favorability":
      assert score in get_category_score_details_in_favoribility(0)

@step('the 2009 score for Clarity of Direction for "([^"]*)" is "([^"]*)"')
def verify_2009_score_for_clarity_of_direction(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_category_score_details_in_evaluation(0)
    elif tab_type == "Favorability":
      assert score in get_category_score_details_in_favoribility(0)

@step('the 2012 score for Learning and Development for "([^"]*)" is "([^"]*)"')
def verify_2012_score_for_learning_and_development(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_category_score_details_in_evaluation(10)
    elif tab_type == "Favorability":
      assert score in get_category_score_details_in_favoribility(10)

@step('the 2011 score for Learning and Development for "([^"]*)" is "([^"]*)"')
def verify_2011_score_for_learning_and_development(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_category_score_details_in_evaluation(10)
    elif tab_type == "Favorability":
      assert score in get_category_score_details_in_favoribility(10)

@step('the 2009 score for Learning and Development for "([^"]*)" is "([^"]*)"')
def verify_2009_score_for_learning_and_development(step, tab_type, score):
    if tab_type == "Evaluation":
      assert score in get_category_score_details_in_evaluation(10)
    elif tab_type == "Favorability":
      assert score in get_category_score_details_in_favoribility(10)

@step('I go into the "Favorability" tab')
def click_categories_tab(step):
    get_browser().find_by_id('ui-id-3').click()

@step('I go into the "Questions" tab')
def click_categories_tab(step):
    time.sleep(5)
    get_browser().find_by_id('ui-id-7').click()


