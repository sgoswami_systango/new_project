from features.support.lettuce_env import *
from features.support.lib.fill_survey_helper import *


@step('I am logged into "([^"]*)" as user "([^"]*)"')
def login_to_perceptyx(step, url, username):
    password = username  # Password and username are same
    login_to_url(url, username, password)
    assert get_browser().is_text_present(
        'Click Here to Access Manager Toolkit')
    set_user(username)
    set_filtered(False)
    time.sleep(5)


@step('I click the "([^"]*)" link under "([^"]*)"')
def click_link(step, link, menu_name):
    browser = get_browser()
    assert browser.find_link_by_text(
        menu_name), "%s menu is not found" % (menu_name)
    browser.find_link_by_text(menu_name).click()
    assert browser.find_link_by_partial_text(
        link), "%s link is not found" % (link)
    browser.find_link_by_partial_text(link).click()
    time.sleep(3)


@step('there is a "([^"]*)" tab in top left corner')
def verify_tab(step, tab_name):
    assert get_browser().find_link_by_text(tab_name)


@step('I am on the "([^"]*)" tab')
def when_i_am_on_the_group_tab(step, group):
    tabs = get_browser().find_link_by_partial_text(group)
    if group == 'Categories':
        set_tab_link(group)
    elif group == 'Questions':
        set_tab_link(group)
    set_clarity_of_direction_checked_value(False)
    for tab in tabs:
        if tab.text == group:
            tab.click()
    time.sleep(5)


@step('there is a "([^"]*)" tab in top right corner')
def verify_tab(step, tab_name):
    assert get_browser().find_link_by_partial_text(tab_name)


@step(u'Now I go to "([^"]*)" > "([^"]*)"')
def now_i_go_to_group1_group2(step, group1, group2):
    browser = get_browser()
    assert browser.find_link_by_text(
        group1), "%s menu is not found" % (group1)
    browser.find_link_by_text(group1).click()
    assert browser.find_link_by_partial_text(
        group2), "%s link is not found" % (group2)
    browser.find_link_by_partial_text(group2).click()


@step('I click the "([^"]*)" link')
def click_on_link(step, menu):
    browser = get_browser()
    assert browser.find_link_by_text(menu), "%s menu is not found" % (menu)
    browser.find_link_by_text(menu).click()


@step(u'I click on the Clarity of Direction category')
def click_on_the_clarity_of_direction_category(step):
    try:
        links = get_browser().find_by_css(
            '.data.category.qid-1 .name.average .name')
        if links == []:
            # import pdb;pdb.set_trace()
            links = get_browser().find_by_css(
                '.categories .category.q_1 .name')
        if links[0].text:
            links[0].click()
        elif links[1].text:
            links[1].click()
        elif links[2].text:
            links[2].click()
        elif links[3].text:
            links[3].click()
        elif links[4].text:
            links[4].click()
        set_clarity_of_direction_checked_value(True)
    except Exception, e:
        for i in range(1, 26):
            try:
                elements = get_browser().find_by_css(
                    '#DataTables_Table_%s .category .name' % (i))
                if elements[0].text:
                    elements[0].click()
                    set_clarity_of_direction_checked_value(True)

            except Exception, e:
                continue


@step('I click "([^"]*)" link under "([^"]*)"')
def click_link(step, link, menu_name):
    browser = get_browser()
    assert browser.find_link_by_text(
        menu_name), "%s menu is not found" % (menu_name)
    browser.find_link_by_text(menu_name).click()
    assert browser.find_link_by_partial_text(
        link), "%s link is not found" % (link)
    browser.find_link_by_partial_text(link).click()

    # Then I click on "Filter Data" > "New Filter"
    # And select "Location" from the "Select a Demographic:" popdown
    # And select "SD" from "Select Your Option(s):" popdown
    # And I click "Submit Filter"


@step('I click on "([^"]*)" > "([^"]*)"')
def click_on_link_filter(step, menu_name, link):
    browser = get_browser()
    assert browser.find_link_by_text(
        "Filter"), "%s menu is not found" % ("Filter")
    browser.find_link_by_text("Filter").click()
    assert browser.find_link_by_partial_text(
        link), "%s link is not found" % (link)
    browser.find_link_by_partial_text(link).click()

    set_filtered(True)
    time.sleep(2)


@step('select "([^"]*)" from the "Select a Demographic:" popdown')
def select_group1_from_the_Select_a_Demographic_popdown(step, group1):
    browser = get_browser()
    # assert browser.is_text_present(group2)
    elements = browser.find_by_name('q').find_by_tag("option")
    for i in range(1, 18):
        if elements[i].text == group1:
            elements[i].click()
            break
    time.sleep(2)


@step('I click "Submit Filter"')
def click_submit_filter(step):
    get_browser().find_by_css('.ui-dialog-buttonset .submit_btn').first.click()
    time.sleep(2)


@step(u'select "([^"]*)" from the "([^"]*)" popdown in the top right')
def select_group1_from_the_group2_popdown_in_the_top_right(step, group1, group2):
    element = get_browser().find_by_name('compared_to').find_by_tag("option")
    if element[0].text == group1:
        element[0].click()
    else:
        element[1].click()
    time.sleep(3)


@step('the score for question 1 in the "([^"]*)" column is "([^"]*)"')
def verify_score_for_question_1_in_the_group(step, group, score):
    # import pdb;pdb.set_trace()
    browser = get_browser()
    questions_tab_row = browser.find_by_css(
        '.data.standalone.q_00_01 .calc-value')
    categories_tab_row = browser.find_by_css('.data.sub.q_00_01 .calc-value')
    if group == '2014':
        pass
        # import pdb;pdb.set_trace()
    elif group == "LPL Financial":
        if get_user() == 'qa1':
            # score1_found represents score at Questions tab
            score1_found = questions_tab_row.first.text
            # score2_found represents score at Categories tab
            score2_found = categories_tab_row.first.text
        else:
            # score1_found represents score at Questions tab
            score1_found = questions_tab_row[
                1].text + questions_tab_row[2].text
            # score2_found represents score at Categories tab
            score2_found = categories_tab_row[
                1].text + categories_tab_row[2].text
        assert score1_found == score or score2_found == score

    elif group == "Perceptyx Benchmarks":
        if get_filtered() == True:
            if get_user() == 'qa1':
                total_percent_at_question_tab = questions_tab_row[
                    3].text + questions_tab_row.last.text
                total_percent_at_categories_tab = categories_tab_row[
                    3].text + categories_tab_row.last.text
            else:
                total_percent_at_question_tab = questions_tab_row[
                    3].text + questions_tab_row[4].text
                total_percent_at_categories_tab = categories_tab_row[
                    3].text + categories_tab_row[4].text
        else:
            if get_user() == 'qa1':
                total_percent_at_question_tab = questions_tab_row[
                    1].text + questions_tab_row.last.text
                total_percent_at_categories_tab = categories_tab_row[
                    1].text + categories_tab_row.last.text
            else:
                total_percent_at_question_tab = questions_tab_row[
                    3].text + questions_tab_row[4].text
                total_percent_at_categories_tab = categories_tab_row[
                    3].text + categories_tab_row[4].text
        assert (total_percent_at_question_tab == score) or (
            total_percent_at_categories_tab == score)


@step('select "([^"]*)" from the "Select Your Option" popdown')
def select_group1_from_the_Select_Your_Option_popdown(step, group1):
    browser = get_browser()
    elements = browser.find_by_name('q0')
    if browser.find_by_name('q')[0].value == '01_15_07':
        elements[0].click()
    else:
        elements[43].click()
    time.sleep(3)


@step('the page finishes loading')
def the_page_finishes_loading(step):
    time.sleep(1)
    for i in range(1, 5):
        if get_browser().is_text_present("Filter:") == True:
            break
        time.sleep(1)
    time.sleep(2)


@step(u'select "([^"]*)" from the "([^"]*)" popdown in the top right')
def select_group1_from_the_group2_popdown_in_the_top_right(step, group1, group2):
    element = get_browser().find_by_name('compared_to').find_by_tag("option")
    if element[0].text == group1:
        element[0].click()
    elif element[1].text == group1:
        element[1].click()
    elif element[2].text == group1:
        element[2].click()
    elif element[3].text == group1:
        element[3].click()
    time.sleep(3)


@step(u'the number for "([^"]*)" in the top right is "([^"]*)"')
def the_number_for_group1_in_the_top_right_is_group2(step, group1, group2):
    if get_filtered() != True :
        if group1 == 'Filter':
            elements = get_browser().find_by_css('.legend .description')
            for i in [0, 1, 2, 4]:
                if elements[i].text:
                    value = elements[i].find_by_tag('span').first.text[1:5]
                    break
            assert value == group2
        elif group1 == 'Filter Count':
            value = get_browser().find_by_css('.legend .count').first.text
            assert value == group2
        else:
            elements = get_browser().find_by_css('.legend .description')
            for i in [3, 5]:
                if elements[i].text:
                    value = elements[i].find_by_tag('span').first.text[1:5]
                    break
            assert value == group2
    else:
        if group1 == 'Filter':
            element = get_browser().find_by_css('.segment-count-text')
            value = element.first.text
            assert value == group2
        elif group1 == 'Filter Count':
            value = get_browser().find_by_css('.legend .count').first.text
            assert value == group2
        else:
            elements = get_browser().find_by_css('.legend .sub-bar .description')
            for i in [0, 1]:
                if elements[i].text:
                    value = elements[i].find_by_tag('span').first.text[1:3]
                    break
            assert value == group2
