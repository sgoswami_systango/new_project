from features.support.lettuce_env import *
from features import application_path


def fetch_urls():
    urls = os.path.join(application_path, 'features/fixtures/urls.yml')
    open_yaml = open(urls, 'r')
    url_dict = yaml.load(open_yaml)
    return url_dict


def fill_survey_form(range, id_count):
    for i in xrange(range):
        get_browser().find_by_id("Q0%s_0%s_e1" % (id_count, i)).click()
        get_browser().find_by_id("Q0%s_0%s_i1" % (id_count, i)).click()


def login(username, password):
    get_browser().visit(fetch_urls()['urls']['perceptyx_url'])
    assert 'perceptyx' in get_browser().url
    for text in ['Username', 'Password']:
        assert get_browser().is_text_present(text)
    get_browser().fill('username', username)
    get_browser().fill('password', password)
    get_browser().find_by_name('Submit').click()
    time.sleep(3)
    assert get_browser().is_text_present(
        'Click Here to Access Manager Toolkit')


def wait_with_interval_until_timeout(timeout, interval, content):
    print datetime.datetime.now()
    if timeout < interval:
        return
    current_timestamp = datetime.datetime.now()
    timeout_with_timestamp = current_timestamp + \
        datetime.timedelta(seconds=timeout)
    while datetime.datetime.now() <= timeout_with_timestamp:
        time.sleep(interval)
        if is_content_in_page(content):
            break
        wait_with_interval_until_timeout(timeout, interval, content)


def login_to_url(url, username, password):
    browser = get_browser()
    browser.visit(url)
    assert url in browser.url
    if not browser.find_link_by_partial_text('Logout'):
        for text in ['Username', 'Password']:
            assert browser.is_text_present(text)
        browser.fill('username', username)
        browser.fill('password', password)
        browser.find_by_name('Submit').click()
