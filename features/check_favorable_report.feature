Feature: Check Favorable report

Background:
       Given I am logged in to perceptyx
       And I click on the "Favorability Report" link
       Then I am on the "Favorability Report" page



Scenario: I can check favorable report
          Given there is both a "Questions" and "Categories" tab in top left corner
          And there is a "No Comparison" and "Compared to 2011 Trend" tab in top right corner
          When I am on the "Questions" tab
          Then the Fav score for question 14 is "62.9%"
          And the Fav score for question 21 is "69.6%"
          And the Fav score for question 22 is "63.1%"
          And the Fav score for question 39 is "54.5%"
          And the Fav score for question 59 is "43.9%"
          And the Neutral score for question 14 is "22.0%"
          And the Neutral score for question 21 is "15.1%"
          And the Neutral score for question 22 is "18.5%"
          And the Neutral score for question 39 is "27.1%"
          And the Neutral score for question 59 is "34.6%"
          And the Unfav score for question 14 is "15.1%"
          And the Unfav score for question 21 is "15.3%"
          And the Unfav score for question 22 is "18.4%"
          And the Unfav score for question 39 is "18.4%"
          And the Unfav score for question 59 is "21.5%"
          And the Overall Average at the bottom has a Fav score of "68.0%"
          And the Overall Average Neutral score is "20.6%"
          And the Overall Average Unfav score is "11.5%"
          When I go into the "Categories" tab
          Then the Fav score for Values is "67.0%"
          And the Neutral Score for Values is "23.9%"
          And the Unfav Score for Values is "9.1%"
          And the Fav score for Teamwork is "73.6%"
          And the Neutral Score for Teamwork is "17.9%"
          And the Unfav Score for Teamwork is "8.5%"
          When I am on the "Questions" tab
          And I go into the "Compared to 2011 Trend" tab
          Then the Trend Fav score for question 14 is "59.4%"
          And the Trend Fav score for question 21 is "63.6%"
          And the Trend Fav score for question 22 is "54.6%"
          And the Trend Fav score for question 39 is "44.1%"
          And the Trend Fav score for question 59 is "32.5%"
          And the Trend Neutral score for question 14 is "23.2%"
          And the Trend Neutral score for question 21 is "17.5%"
          And the Trend Neutral score for question 22 is "20.6%"
          And the Trend Neutral score for question 39 is "28.6%"
          And the Trend Neutral score for question 59 is "38.1%"
          And the Trend Unfav score for question 14 is "17.3%"
          And the Trend Unfav score for question 21 is "18.9%"
          And the Trend Unfav score for question 22 is "24.7%"
          And the Trend Unfav score for question 39 is "27.4%"
          And the Trend Unfav score for question 59 is "29.3%"
          When I go into the "Categories" tab
          Then the Trend Fav score for Values is "52.1%"
          And the Trend Neutral Score for Values is "30.9%"
          And the Trend Unfav Score for Values is "17.0%"
          And the Trend Fav score for Teamwork is "66.9%"
          And the Trend Neutral Score for Teamwork is "21.4%"
          And the Trend Unfav Score for Teamwork is "11.7%"
          Now I go to "Logout"


        
