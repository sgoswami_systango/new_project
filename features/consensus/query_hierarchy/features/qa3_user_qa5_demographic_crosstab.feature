Feature: Check Demographic Crosstab for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.0"
	And the score for question 1 in the "Male" column is "3.9"
	And the Weighted Average on the right for question 1 is "3.9"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "3.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.00"
	And the score for question 1 in the "Male" column is "3.90"
	And the Weighted Average on the right for question 1 is "3.95"
	And the Group Average at the bottom for the "Female" column is "3.86"
	And the Group Average at the bottom for the "Male" column is "3.74"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "79"
	And the score for question 1 in the "Male" column is "75"
	And the Weighted Average on the right for question 1 is "77"
	And the Group Average at the bottom for the "Female" column is "74"
	And the Group Average at the bottom for the "Male" column is "67"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "78.9"
	And the score for question 1 in the "Male" column is "75.0"
	And the Weighted Average on the right for question 1 is "76.9"
	And the Group Average at the bottom for the "Female" column is "74.2"
	And the Group Average at the bottom for the "Male" column is "67.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "78.95"
	And the score for question 1 in the "Male" column is "75.00"
	And the Weighted Average on the right for question 1 is "76.92"
	And the Group Average at the bottom for the "Female" column is "74.21"
	And the Group Average at the bottom for the "Male" column is "67.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0"
	And the score for question 1 in the "Male" column is "5"
	And the Weighted Average on the right for question 1 is "3"
	And the Group Average at the bottom for the "Female" column is "12"
	And the Group Average at the bottom for the "Male" column is "16"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.0"
	And the score for question 1 in the "Male" column is "5.0"
	And the Weighted Average on the right for question 1 is "2.6"
	And the Group Average at the bottom for the "Female" column is "12.4"
	And the Group Average at the bottom for the "Male" column is "16.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.00"
	And the score for question 1 in the "Male" column is "5.00"
	And the Weighted Average on the right for question 1 is "2.56"
	And the Group Average at the bottom for the "Female" column is "12.42"
	And the Group Average at the bottom for the "Male" column is "16.00"

	Now I go to "Functions" > "Logout"

# Categories
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.1"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "3.7"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.0"
	And the score for question 1 in the "Male" column is "3.9"
	And the Weighted Average on the right for question 1 is "3.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.09"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.02"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.06"
	And the Group Average at the bottom for the "Female" column is "3.86"
	And the Group Average at the bottom for the "Male" column is "3.74"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.00"
	And the score for question 1 in the "Male" column is "3.90"
	And the Weighted Average on the right for question 1 is "3.95"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "87"
	And the score for the "Clarity of Direction" category in the "Male" column is "79"
	And the Weighted Average on the right for the "Clarity of Direction" category is "83"
	And the Group Average at the bottom for the "Female" column is "74"
	And the Group Average at the bottom for the "Male" column is "67"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "79"
	And the score for question 1 in the "Male" column is "75"
	And the Weighted Average on the right for question 1 is "77"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "87.4"
	And the score for the "Clarity of Direction" category in the "Male" column is "79.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "83.1"
	And the Group Average at the bottom for the "Female" column is "74.2"
	And the Group Average at the bottom for the "Male" column is "67.0"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "78.9"
	And the score for question 1 in the "Male" column is "75.0"
	And the Weighted Average on the right for question 1 is "76.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "87.37"
	And the score for the "Clarity of Direction" category in the "Male" column is "79.00"
	And the Weighted Average on the right for the "Clarity of Direction" category is "83.08"
	And the Group Average at the bottom for the "Female" column is "74.21"
	And the Group Average at the bottom for the "Male" column is "67.00"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "78.95"
	And the score for question 1 in the "Male" column is "75.00"
	And the Weighted Average on the right for question 1 is "76.92"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Female" column is "12"
	And the Group Average at the bottom for the "Male" column is "16"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0"
	And the score for question 1 in the "Male" column is "5"
	And the Weighted Average on the right for question 1 is "3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.2"
	And the score for the "Clarity of Direction" category in the "Male" column is "9.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.7"
	And the Group Average at the bottom for the "Female" column is "12.4"
	And the Group Average at the bottom for the "Male" column is "16.0"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.0"
	And the score for question 1 in the "Male" column is "5.0"
	And the Weighted Average on the right for question 1 is "2.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.21"
	And the score for the "Clarity of Direction" category in the "Male" column is "9.00"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.67"
	And the Group Average at the bottom for the "Female" column is "12.42"
	And the Group Average at the bottom for the "Male" column is "16.00"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.00"
	And the score for question 1 in the "Male" column is "5.00"
	And the Weighted Average on the right for question 1 is "2.56"

	Now I go to "Functions" > "Logout"

# Questions + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.0 (3.6)"
	And the score for question 1 in the "Male" column is "3.9 (3.6)"
	And the Weighted Average on the right for question 1 is "3.9"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.5)"
	And the Group Average at the bottom for the "Male" column is "3.7 (3.7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4.00 (3.64)"
	And the score for question 1 in the "Male" column is "3.90 (3.62)"
	And the Weighted Average on the right for question 1 is "3.95"
	And the Group Average at the bottom for the "Female" column is "3.86 (3.50)"
	And the Group Average at the bottom for the "Male" column is "3.74 (3.74)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "79 (71)"
	And the score for question 1 in the "Male" column is "75 (62)"
	And the Weighted Average on the right for question 1 is "77"
	And the Group Average at the bottom for the "Female" column is "74 (58)"
	And the Group Average at the bottom for the "Male" column is "67 (67)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "78.9 (71.4)"
	And the score for question 1 in the "Male" column is "75.0 (62.5)"
	And the Weighted Average on the right for question 1 is "76.9"
	And the Group Average at the bottom for the "Female" column is "74.2 (58.4)"
	And the Group Average at the bottom for the "Male" column is "67.0 (67.1)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "78.95 (71.43)"
	And the score for question 1 in the "Male" column is "75.00 (62.50)"
	And the Weighted Average on the right for question 1 is "76.92"
	And the Group Average at the bottom for the "Female" column is "74.21 (58.41)"
	And the Group Average at the bottom for the "Male" column is "67.00 (67.08)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0 (14)"
	And the score for question 1 in the "Male" column is "5 (12)"
	And the Weighted Average on the right for question 1 is "3"
	And the Group Average at the bottom for the "Female" column is "12 (21)"
	And the Group Average at the bottom for the "Male" column is "16 (16)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.0 (14.3)"
	And the score for question 1 in the "Male" column is "5.0 (12.5)"
	And the Weighted Average on the right for question 1 is "2.6"
	And the Group Average at the bottom for the "Female" column is "12.4 (20.7)"
	And the Group Average at the bottom for the "Male" column is "16.0 (16.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.00 (14.29)"
	And the score for question 1 in the "Male" column is "5.00 (12.50)"
	And the Weighted Average on the right for question 1 is "2.56"
	And the Group Average at the bottom for the "Female" column is "12.42 (20.71)"
	And the Group Average at the bottom for the "Male" column is "16.00 (16.25)"

	Now I go to "Functions" > "Logout"

# Categories + trend
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.1 (3.9)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.0 (4.0)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.5)"
	And the Group Average at the bottom for the "Male" column is "3.7 (3.7)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.0 (3.6)"
	And the score for question 1 in the "Male" column is "3.9 (3.6)"
	And the Weighted Average on the right for question 1 is "3.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.09 (3.87)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.02 (4.03)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.06"
	And the Group Average at the bottom for the "Female" column is "3.86 (3.50)"
	And the Group Average at the bottom for the "Male" column is "3.74 (3.74)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4.00 (3.64)"
	And the score for question 1 in the "Male" column is "3.90 (3.62)"
	And the Weighted Average on the right for question 1 is "3.95"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "87 (76)"
	And the score for the "Clarity of Direction" category in the "Male" column is "79 (79)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "83"
	And the Group Average at the bottom for the "Female" column is "74 (58)"
	And the Group Average at the bottom for the "Male" column is "67 (67)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "79 (71)"
	And the score for question 1 in the "Male" column is "75 (62)"
	And the Weighted Average on the right for question 1 is "77"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "87.4 (75.7)"
	And the score for the "Clarity of Direction" category in the "Male" column is "79.0 (78.8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "83.1"
	And the Group Average at the bottom for the "Female" column is "74.2 (58.4)"
	And the Group Average at the bottom for the "Male" column is "67.0 (67.1)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "78.9 (71.4)"
	And the score for question 1 in the "Male" column is "75.0 (62.5)"
	And the Weighted Average on the right for question 1 is "76.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "87.37 (75.71)"
	And the score for the "Clarity of Direction" category in the "Male" column is "79.00 (78.75)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "83.08"
	And the Group Average at the bottom for the "Female" column is "74.21 (58.41)"
	And the Group Average at the bottom for the "Male" column is "67.00 (67.08)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "78.95 (71.43)"
	And the score for question 1 in the "Male" column is "75.00 (62.50)"
	And the Weighted Average on the right for question 1 is "76.92"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4 (9)"
	And the score for the "Clarity of Direction" category in the "Male" column is "9 (9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Female" column is "12 (21)"
	And the Group Average at the bottom for the "Male" column is "16 (16)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0 (14)"
	And the score for question 1 in the "Male" column is "5 (12)"
	And the Weighted Average on the right for question 1 is "3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.2 (8.6)"
	And the score for the "Clarity of Direction" category in the "Male" column is "9.0 (8.8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.7"
	And the Group Average at the bottom for the "Female" column is "12.4 (20.7)"
	And the Group Average at the bottom for the "Male" column is "16.0 (16.2)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.0 (14.3)"
	And the score for question 1 in the "Male" column is "5.0 (12.5)"
	And the Weighted Average on the right for question 1 is "2.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.21 (8.57)"
	And the score for the "Clarity of Direction" category in the "Male" column is "9.00 (8.75)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.67"
	And the Group Average at the bottom for the "Female" column is "12.42 (20.71)"
	And the Group Average at the bottom for the "Male" column is "16.00 (16.25)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.00 (14.29)"
	And the score for question 1 in the "Male" column is "5.00 (12.50)"
	And the Weighted Average on the right for question 1 is "2.56"

	Now I go to "Functions" > "Logout"