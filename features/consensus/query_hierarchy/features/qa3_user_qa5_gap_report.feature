Feature: Check Gap Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click on the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "3.95"
	And the Consensus for question 1 is "4.19"
	And the Gap for question 1 is "0.24"
	And the Weighted Gap for question 1 is "5.03"
	And the Overall Average at the bottom has an Eval Avg of "3.80"
	And the Overall Average has a Consensus of "3.91"
	And the Overall Average has a Gap of "0.11"
	And the Overall Average has a Weighted Gap of "2.15"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "4.06"
	And the Consensus for the "Clarity of Direction" category is "4.07"
	And the Gap for the "Clarity of Direction" category is "0.01"
	And the Weighted Gap for the "Clarity of Direction" category is "0.20"
	And the Overall Average at the bottom has an Eval Avg of "3.80"
	And the Overall Average has a Consensus of "3.91"
	And the Overall Average has a Gap of "0.11"
	And the Overall Average has a Weighted Gap of "2.15"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.95"
	And the Consensus for question 1 is "4.19"
	And the Gap for question 1 is "0.24"
	And the Weighted Gap for question 1 is "5.03"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Eval Avg for question 1 is "3.95 (+0.13)"
	And the Consensus for question 1 is "4.19 (+0.04)"
	And the Gap for question 1 is "0.24 (-0.09)"
	And the Weighted Gap for question 1 is "5.03 (-1.82)"
	And the Overall Average at the bottom has an Eval Avg of "3.80 (-0.06)"
	And the Overall Average has a Consensus of "3.91 (-0.10)"
	And the Overall Average has a Gap of "0.11 (-0.04)"
	And the Overall Average has a Weighted Gap of "2.15 (-0.86)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "4.06 (+0.05)"
	And the Consensus for the "Clarity of Direction" category is "4.07 (-0.08)"
	And the Gap for the "Clarity of Direction" category is "0.01 (-0.13)"
	And the Weighted Gap for the "Clarity of Direction" category is "0.20 (-2.71)"
	And the Overall Average at the bottom has an Eval Avg of "3.80 (-0.06)"
	And the Overall Average has a Consensus of "3.91 (-0.10)"
	And the Overall Average has a Gap of "0.11 (-0.04)"
	And the Overall Average has a Weighted Gap of "2.15 (-0.86)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.95 (+0.13)"
	And the Consensus for question 1 is "4.19 (+0.04)"
	And the Gap for question 1 is "0.24 (-0.09)"
	And the Weighted Gap for question 1 is "5.03 (-1.82)"

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Eval Avg for question 1 is "3.95 (+0.31)"
	And the Consensus for question 1 is "4.19 (+0.02)"
	And the Gap for question 1 is "0.24 (-0.29)"
	And the Weighted Gap for question 1 is "5.03 (-6.02)"
	And the Overall Average at the bottom has an Eval Avg of "3.80(+0.21)"
	And the Overall Average has a Consensus of "3.91 (0.00)"
	And the Overall Average has a Gap of "0.11 (-0.21)"
	And the Overall Average has a Weighted Gap of "2.15 (-4.11)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "4.06 (+0.13)"
	And the Consensus for the "Clarity of Direction" category is "4.07 (-0.07)"
	And the Gap for the "Clarity of Direction" category is "0.01 (-0.20)"
	And the Weighted Gap for the "Clarity of Direction" category is "0.20 (-4.15)"
	And the Overall Average at the bottom has an Eval Avg of "3.80 (+0.21)"
	And the Overall Average has a Consensus of "3.91 (0.00)"
	And the Overall Average has a Gap of "0.11 (-0.21)"
	And the Overall Average has a Weighted Gap of "2.15 (-4.11)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.95 (+0.31)"
	And the Consensus for question 1 is "4.19 (+0.02)"
	And the Gap for question 1 is "0.24 (-0.29)"
	And the Weighted Gap for question 1 is "5.03 (-6.02)"

	Now I go to "Functions" > "Logout"