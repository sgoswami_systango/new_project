Feature: Check Trend Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 1 in the "My View" column is "76.9%"
	And the score for question 1 in the "LPL Financial" column is "75.5%(-1.4)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-8.3)"

	When I am on the "Categories" tab
	Then the score for question 1 in the "My View" column is "76.9%"
	Then the score for question 1 in the "LPL Financial" column is "75.5%(-1.4)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-8.3)"
	
	Now I go to "Functions" > "Logout"