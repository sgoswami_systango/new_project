Feature: Check Trend Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the score for question 1 in the "2014" column is "3.95 (+0.31)"
	And the score for question 1 in the "2013" column is "3.64 (+0.14)"
	And the score for question 1 in the "2012" column is "3.50"
	And the Overall Average at the bottom for "2014" is "3.80 (+0.21)"
	And the Overall Average for "2013" is "3.59 (-0.14)"
	And the Overall Average for "2012" is "3.73"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the score for the "Clarity of Direction" category for "2014" is "4.06 (+0.13)"
	And the score for the "Clarity of Direction" category for "2013" is "3.93 (+0.09)"
	And the score for the "Clarity of Direction" category for "2012" is "3.84"
	And the Overall Average at the bottom for "2014" is "3.80 (+0.21)"
	And the Overall Average for "2013" is "3.59 (-0.14)"
	And the Overall Average for "2012" is "3.73"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "3.95 (+0.31)"
	And the score for question 1 in the "2013" column is "3.64 (+0.14)"
	And the score for question 1 in the "2012" column is "3.50"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the score for question 1 in the "2014" column is "76.9 (+8.7)"
	And the score for question 1 in the "2013" column is "68.2 (+3.2)"
	And the score for question 1 in the "2012" column is "65.0"
	And the Overall Average at the bottom for "2014" is "70.5 (+8.9)"
	And the Overall Average for "2013" is "61.6 (-7.1)"
	And the Overall Average for "2012" is "68.7"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the score for the "Clarity of Direction" category for "2014" is "83.1 (+6.3)"
	And the score for the "Clarity of Direction" category for "2013" is "76.8 (+2.8)"
	And the score for the "Clarity of Direction" category for "2012" is "74.0"
	And the Overall Average at the bottom for "2014" is "70.5 (+8.9)"
	And the Overall Average for "2013" is "61.6 (-7.1)"
	And the Overall Average for "2012" is "68.7"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "76.9 (+8.7)"
	And the score for question 1 in the "2013" column is "68.2 (+3.2)"
	And the score for question 1 in the "2012" column is "65.0"

	Now I go to "Functions" > "Logout"