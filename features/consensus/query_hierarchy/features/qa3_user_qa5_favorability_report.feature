Feature: Check Favorability Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa5"
	And I click on the "Favorability Report" link
	Then I am on the "Favorability Report" page
	
@wip		
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for question 1 is "76.9%"
	And the Neutral score for question 1 is "20.5%"
	And the Unfavorable score for question 1 is "2.6%"
	And the Overall Average at the bottom has a Favorable score of "70.5%"
	And the Overall Average Neutral score is "15.2%"
	And the Overall Average Unfavorable score is "14.3%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "83.1%"
	And the Neutral score for the "Clarity of Direction" category is "10.3%"
	And the Unfavorable score for the "Clarity of Direction" category is "6.7%"
	And the Overall Average at the bottom has a Favorable score of "70.5%"
	And the Overall Average Neutral score is "15.2%"
	And the Overall Average Unfavorable score is "14.3%"

	When I click on the Clarity of Direction category
	The Favorable score for question 1 is "76.9%"
	And the Neutral score for question 1 is "20.5%"
	And the Unfavorable score for question 1 is "2.6%"
	
# Compared to Org

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for question 1 is "76.9%"
	And the Neutral score on top for question 1 is "20.5%"
	And the Unfavorable score on top for question 1 is "2.6%"
	And the Favorable score on bottom for question 1 is "75.5%"
	And the Neutral score on bottom for question 1 is "15.4%"
	And the Unfavorable score on bottom for question 1 is "9.1%"
	And the Overall Average at the bottom Favorable score on top is "70.5%"
	And the Overall Average Neutral score on top is "15.2%"
	And the Overall Average Unfavorable score on top is "14.3%"
	And the Overall Average Favorable score on bottom is "72.3%"
	And the Overall Average Neutral score on bottom is "17.1%"
	And the Overall Average Unfavorable score on bottom is "10.6%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "83.1%"
	And the Neutral score on top for the "Clarity of Direction" category is "10.3%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "6.7%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "80.2%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "13.6%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "6.2%"
	And the Overall Average at the bottom Favorable score on top is "70.5%"
	And the Overall Average Neutral score on top is "15.2%"
	And the Overall Average Unfavorable score on top is "14.3%"
	And the Overall Average Favorable score on bottom is "72.3%"
	And the Overall Average Neutral score on bottom is "17.1%"
	And the Overall Average Unfavorable score on bottom is "10.6%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "76.9%"
	And the Neutral score on top for question 1 is "20.5%"
	And the Unfavorable score on top for question 1 is "2.6%"
	And the Favorable score on bottom for question 1 is "75.5%"
	And the Neutral score on bottom for question 1 is "15.4%"
	And the Unfavorable score on bottom for question 1 is "9.1%"

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for question 1 is "76.9%"
	And the Neutral score on top for question 1 is "20.5%"
	And the Unfavorable score on top for question 1 is "2.6%"
	And the Favorable score on bottom for question 1 is "68.2%"
	And the Neutral score on bottom for question 1 is "18.2%"
	And the Unfavorable score on bottom for question 1 is "13.6%"
	And the Overall Average at the bottom Favorable score on top is "70.5%"
	And the Overall Average Neutral score on top is "15.2%"
	And the Overall Average Unfavorable score on top is "14.3%"
	And the Overall Average Favorable score on bottom is "61.6%"
	And the Overall Average Neutral score on bottom is "19.3%"
	And the Overall Average Unfavorable score on bottom is "19.1%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "83.1%"
	And the Neutral score on top for the "Clarity of Direction" category is "10.3%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "6.7%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "76.8%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "14.5%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "8.6%"
	And the Overall Average at the bottom Favorable score on top is "70.5%"
	And the Overall Average Neutral score on top is "15.2%"
	And the Overall Average Unfavorable score on top is "14.3%"
	And the Overall Average Favorable score on bottom is "61.6%"
	And the Overall Average Neutral score on bottom is "19.3%"
	And the Overall Average Unfavorable score on bottom is "19.1%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "76.9%"
	And the Neutral score on top for question 1 is "20.5%"
	And the Unfavorable score on top for question 1 is "2.6%"
	And the Favorable score on bottom for question 1 is "68.2%"
	And the Neutral score on bottom for question 1 is "18.2%"
	And the Unfavorable score on bottom for question 1 is "13.6%"

	Now I go to "Functions" > "Logout"
