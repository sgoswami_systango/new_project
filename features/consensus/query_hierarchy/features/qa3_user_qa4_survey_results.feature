Feature: Check Survey Results for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab

	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "2"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "6.7%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "3"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "10.0%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "20"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "66.7%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "5"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "16.7%"
	And the "Mean" under question 1 is "3.93 (±0.73)"
	And the figure under question 1 for "Mean" in the "Number" column is "30"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0(38)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%(1.3%)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Segment Density" column is "0.0%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "2(238)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "6.7%(7.9%)"
	And the figure under question 1 for "2. Disagree" in the "Segment Density" column is "0.8%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "3(466)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "10.0%(15.4%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Segment Density" column is "0.6%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "20(1769)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "66.7%(58.4%)"
	And the figure under question 1 for "4. Agree" in the "Segment Density" column is "1.1%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "5(518)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "16.7%(17.1%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Segment Density" column is "1.0%"
	And the "Mean" under question 1 is "3.93 (±0.73)"
	And the figure under question 1 for "Mean" in the "Number" column is "30(3029)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	When I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0(1)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%(7.7%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "2(2)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "6.7%(15.4%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "3(5)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "10.0%(38.5%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "20(4)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "66.7%(30.8%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "5(1)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "16.7%(7.7%)"
	And the "Mean" under question 1 is "3.93 (±0.73)"
	And the figure under question 1 for "Mean" in the "Number" column is "30(13)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"