Feature: Check Trend Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 1 in the "LPL Financial" column is "75.5%"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-6.9)"

	When I am on the "Categories" tab
	Then the score for question 1 in the "LPL Financial" column is "75.5%"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-6.9)"
	
	Now I go to "Functions" > "Logout"