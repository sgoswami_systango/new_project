Feature: Check Demographic Crosstab for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.8"
	And the score for question 1 in the "Male" column is "3.8"
	And the Weighted Average on the right for question 1 is "3.8"
	And the Group Average at the bottom for the "Female" column is "3.8"
	And the Group Average at the bottom for the "Male" column is "3.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.80"
	And the score for question 1 in the "Male" column is "3.84"
	And the Weighted Average on the right for question 1 is "3.82"
	And the Group Average at the bottom for the "Female" column is "3.83"
	And the Group Average at the bottom for the "Male" column is "3.88"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "74"
	And the score for question 1 in the "Male" column is "76"
	And the Weighted Average on the right for question 1 is "75"
	And the Group Average at the bottom for the "Female" column is "72"
	And the Group Average at the bottom for the "Male" column is "73"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "74.3"
	And the score for question 1 in the "Male" column is "76.4"
	And the Weighted Average on the right for question 1 is "75.5"
	And the Group Average at the bottom for the "Female" column is "71.8"
	And the Group Average at the bottom for the "Male" column is "72.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "74.30"
	And the score for question 1 in the "Male" column is "76.42"
	And the Weighted Average on the right for question 1 is "75.50"
	And the Group Average at the bottom for the "Female" column is "71.82"
	And the Group Average at the bottom for the "Male" column is "72.64"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "10"
	And the score for question 1 in the "Male" column is "9"
	And the Weighted Average on the right for question 1 is "9"
	And the Group Average at the bottom for the "Female" column is "11"
	And the Group Average at the bottom for the "Male" column is "10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "9.5"
	And the score for question 1 in the "Male" column is "8.8"
	And the Weighted Average on the right for question 1 is "9.1"
	And the Group Average at the bottom for the "Female" column is "11.0"
	And the Group Average at the bottom for the "Male" column is "10.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "9.52"
	And the score for question 1 in the "Male" column is "8.80"
	And the Weighted Average on the right for question 1 is "9.11"
	And the Group Average at the bottom for the "Female" column is "11.02"
	And the Group Average at the bottom for the "Male" column is "10.34"

	Now I go to "Functions" > "Logout"

# Categories
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "4"
	And the score for the Clarity of Direction category in the Male column is "4"
	And the Weighted Average on the right for the Clarity of Direction category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "4.0"
	And the score for the Clarity of Direction category in the Male column is "4.0"
	And the Weighted Average on the right for the Clarity of Direction category is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.8"
	And the Group Average at the bottom for the "Male" column is "3.9"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.8"
	And the score for question 1 in the "Male" column is "3.8"
	And the Weighted Average on the right for question 1 is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "3.99"
	And the score for the Clarity of Direction category in the Male column is "4.02"
	And the Weighted Average on the right for the Clarity of Direction category is "4.01"
	And the Group Average at the bottom for the "Female" column is "3.83"
	And the Group Average at the bottom for the "Male" column is "3.88"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.80"
	And the score for question 1 in the "Male" column is "3.84"
	And the Weighted Average on the right for question 1 is "3.82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "80"
	And the score for the Clarity of Direction category in the Male column is "80"
	And the Weighted Average on the right for the Clarity of Direction category is "80"
	And the Group Average at the bottom for the "Female" column is "72"
	And the Group Average at the bottom for the "Male" column is "73"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "74"
	And the score for question 1 in the "Male" column is "76"
	And the Weighted Average on the right for question 1 is "75"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "80.0"
	And the score for the Clarity of Direction category in the Male column is "80.4"
	And the Weighted Average on the right for the Clarity of Direction category is "80.2"
	And the Group Average at the bottom for the "Female" column is "71.8"
	And the Group Average at the bottom for the "Male" column is "72.6"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "74.3"
	And the score for question 1 in the "Male" column is "76.4"
	And the Weighted Average on the right for question 1 is "75.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "80.05"
	And the score for the Clarity of Direction category in the Male column is "80.35"
	And the Weighted Average on the right for the Clarity of Direction category is "80.22"
	And the Group Average at the bottom for the "Female" column is "71.82"
	And the Group Average at the bottom for the "Male" column is "72.64"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "74.30"
	And the score for question 1 in the "Male" column is "76.42"
	And the Weighted Average on the right for question 1 is "75.50"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "6"
	And the score for the Clarity of Direction category in the Male column is "6"
	And the Weighted Average on the right for the Clarity of Direction category is "6"
	And the Group Average at the bottom for the "Female" column is "11"
	And the Group Average at the bottom for the "Male" column is "10"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "10"
	And the score for question 1 in the "Male" column is "9"
	And the Weighted Average on the right for question 1 is "9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "6.2"
	And the score for the Clarity of Direction category in the Male column is "6.2"
	And the Weighted Average on the right for the Clarity of Direction category is "6.2"
	And the Group Average at the bottom for the "Female" column is "11.0"
	And the Group Average at the bottom for the "Male" column is "10.3"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "9.5"
	And the score for question 1 in the "Male" column is "8.8"
	And the Weighted Average on the right for question 1 is "9.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "6.20"
	And the score for the Clarity of Direction category in the Male column is "6.18"
	And the Weighted Average on the right for the Clarity of Direction category is "6.19"
	And the Group Average at the bottom for the "Female" column is "11.02"
	And the Group Average at the bottom for the "Male" column is "10.34"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "9.52"
	And the score for question 1 in the "Male" column is "8.80"
	And the Weighted Average on the right for question 1 is "9.11"

	Now I go to "Functions" > "Logout"

# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.8 (3.7)"
	And the score for question 1 in the "Male" column is "3.8 (3.8)"
	And the Weighted Average on the right for question 1 is "3.8"
	And the Group Average at the bottom for the "Female" column is "3.8 (3.7)"
	And the Group Average at the bottom for the "Male" column is "3.9 (3.8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.80 (3.66)"
	And the score for question 1 in the "Male" column is "3.84 (3.82)"
	And the Weighted Average on the right for question 1 is "3.82"
	And the Group Average at the bottom for the "Female" column is "3.83 (3.73)"
	And the Group Average at the bottom for the "Male" column is "3.88 (3.79)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "74 (68)"
	And the score for question 1 in the "Male" column is "76 (75)"
	And the Weighted Average on the right for question 1 is "75"
	And the Group Average at the bottom for the "Female" column is "72 (68)"
	And the Group Average at the bottom for the "Male" column is "73 (70)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "74.3 (67.5)"
	And the score for question 1 in the "Male" column is "76.4 (75.0)"
	And the Weighted Average on the right for question 1 is "75.5"
	And the Group Average at the bottom for the "Female" column is "71.8 (67.8)"
	And the Group Average at the bottom for the "Male" column is "72.6 (69.6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "74.30 (67.50)"
	And the score for question 1 in the "Male" column is "76.42 (74.97)"
	And the Weighted Average on the right for question 1 is "75.50"
	And the Group Average at the bottom for the "Female" column is "71.82 (67.78)"
	And the Group Average at the bottom for the "Male" column is "72.64 (69.56)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "10 (12)"
	And the score for question 1 in the "Male" column is "9 (9)"
	And the Weighted Average on the right for question 1 is "9"
	And the Group Average at the bottom for the "Female" column is "11 (13)"
	And the Group Average at the bottom for the "Male" column is "10 (12)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "9.5 (11.6)"
	And the score for question 1 in the "Male" column is "8.8 (9.0)"
	And the Weighted Average on the right for question 1 is "9.1"
	And the Group Average at the bottom for the "Female" column is "11.0 (13.0)"
	And the Group Average at the bottom for the "Male" column is "10.3 (12.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "9.52 (11.56)"
	And the score for question 1 in the "Male" column is "8.80 (8.96)"
	And the Weighted Average on the right for question 1 is "9.11"
	And the Group Average at the bottom for the "Female" column is "11.02 (13.03)"
	And the Group Average at the bottom for the "Male" column is "10.34 (12.33)"

	Now I go to "Functions" > "Logout"

# Categories + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "4 (4)"
	And the score for the Clarity of Direction category in the Male column is "4 (4)"
	And the Weighted Average on the right for the Clarity of Direction category is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "4.0 (3.9)"
	And the score for the Clarity of Direction category in the Male column is "4.0 (4.0)"
	And the Weighted Average on the right for the Clarity of Direction category is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.8 (3.7)"
	And the Group Average at the bottom for the "Male" column is "3.9 (3.8)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.8 (3.7)"
	And the score for question 1 in the "Male" column is "3.8 (3.8)"
	And the Weighted Average on the right for question 1 is "3.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "3.99 (3.88)"
	And the score for the Clarity of Direction category in the Male column is "4.02 (4.00)"
	And the Weighted Average on the right for the Clarity of Direction category is "4.01"
	And the Group Average at the bottom for the "Female" column is "3.83 (3.73)"
	And the Group Average at the bottom for the "Male" column is "3.88 (3.79)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.80 (3.66)"
	And the score for question 1 in the "Male" column is "3.84 (3.82)"
	And the Weighted Average on the right for question 1 is "3.82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "80 (76)"
	And the score for the Clarity of Direction category in the Male column is "80 (79)"
	And the Weighted Average on the right for the Clarity of Direction category is "80"
	And the Group Average at the bottom for the "Female" column is "72 (68)"
	And the Group Average at the bottom for the "Male" column is "73 (70)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "74 (68)"
	And the score for question 1 in the "Male" column is "76 (75)"
	And the Weighted Average on the right for question 1 is "75"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "80.0 (75.9)"
	And the score for the Clarity of Direction category in the Male column is "80.4 (79.2)"
	And the Weighted Average on the right for the Clarity of Direction category is "80.2"
	And the Group Average at the bottom for the "Female" column is "71.8 (67.8)"
	And the Group Average at the bottom for the "Male" column is "72.6 (69.6)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "74.3 (67.5)"
	And the score for question 1 in the "Male" column is "76.4 (75.0)"
	And the Weighted Average on the right for question 1 is "75.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "80.05 (75.88)"
	And the score for the Clarity of Direction category in the Male column is "80.35 (79.22)"
	And the Weighted Average on the right for the Clarity of Direction category is "80.22"
	And the Group Average at the bottom for the "Female" column is "71.82 (67.78)"
	And the Group Average at the bottom for the "Male" column is "72.64 (69.56)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "74.30 (67.50)"
	And the score for question 1 in the "Male" column is "76.42 (74.97)"
	And the Weighted Average on the right for question 1 is "75.50"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "6 (8)"
	And the score for the Clarity of Direction category in the Male column is "6 (7)"
	And the Weighted Average on the right for the Clarity of Direction category is "6"
	And the Group Average at the bottom for the "Female" column is "11 (13)"
	And the Group Average at the bottom for the "Male" column is "10 (12)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "10 (12)"
	And the score for question 1 in the "Male" column is "9 (9)"
	And the Weighted Average on the right for question 1 is "9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "6.2 (7.5)"
	And the score for the Clarity of Direction category in the Male column is "6.2 (6.9)"
	And the Weighted Average on the right for the Clarity of Direction category is "6.2"
	And the Group Average at the bottom for the "Female" column is "11.0 (13.0)"
	And the Group Average at the bottom for the "Male" column is "10.3 (12.3)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "9.5 (11.6)"
	And the score for question 1 in the "Male" column is "8.8 (9.0)"
	And the Weighted Average on the right for question 1 is "9.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the Female column is "6.20 (7.50)"
	And the score for the Clarity of Direction category in the Male column is "6.18 (6.93)"
	And the Weighted Average on the right for the Clarity of Direction category is "6.19"
	And the Group Average at the bottom for the "Female" column is "11.02 (13.03)"
	And the Group Average at the bottom for the "Male" column is "10.34 (12.33)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "9.52 (11.56)"
	And the score for question 1 in the "Male" column is "8.80 (8.96)"
	And the Weighted Average on the right for question 1 is "9.11"

	Now I go to "Functions" > "Logout"