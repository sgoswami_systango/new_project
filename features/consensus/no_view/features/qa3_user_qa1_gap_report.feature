Feature: Check Gap Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to 2013 Trend" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "3.82"
	And the Consensus for question 1 is "4.15"
	And the Gap for question 1 is "0.33"
	And the Weighted Gap for question 1 is "6.85"
	And the Overall Average at the bottom has an Eval Avg of "3.86"
	And the Overall Average has a Consensus of "4.01"
	And the Overall Average has a Gap of "0.15"
	And the Overall Average has a Weighted Gap of "3.01"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the Clarity of Direction category is "4.01"
	And the Consensus for the Clarity of Direction category is "4.15"
	And the Gap for the Clarity of Direction category is "0.14"
	And the Weighted Gap for the Clarity of Direction category is "2.91"
	And the Overall Average at the bottom has an Eval Avg of "3.86"
	And the Overall Average has a Consensus of "4.01"
	And the Overall Average has a Gap of "0.15"
	And the Overall Average has a Weighted Gap of "3.01"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 2 is "3.79"
	And the Consensus for question 2 is "4.13"
	And the Gap for question 2 is "0.34"
	And the Weighted Gap for question 2 is "7.02"
	
	When I am on the "Questions" tab
	And I am on the "Compared to 2013 Trend" tab
	Then the Eval Avg for question 3 is "4.32 (+0.05)"
	And the Consensus for question 3 is "4.28 (+0.02)"
	And the Gap for question 3 is "-0.04 (-0.03)"
	And the Weighted Gap for question 3 is "-0.86 (-0.65)"
	And the Overall Average at the bottom has an Eval Avg of "3.86 (+0.09)"
	And the Overall Average has a Consensus of "4.01 (+0.03)"
	And the Overall Average has a Gap of "0.15 (-0.06)"
	And the Overall Average has a Weighted Gap of "3.01 (-1.17)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to 2013 Trend" tab
	Then the Eval Avg for the Clarity of Direction category is "4.01 (+0.07)"
	And the Consensus for the Clarity of Direction category is "4.15 (+0.02)"
	And the Gap for the Clarity of Direction category is "0.14 (-0.05)"
	And the Weighted Gap for the Clarity of Direction category is "2.91 (-1.01)"
	And the Overall Average at the bottom has an Eval Avg of "3.86 (+0.09)"
	And the Overall Average has a Consensus of "4.01 (+0.03)"
	And the Overall Average has a Gap of "0.15 (-0.06)"
	And the Overall Average has a Weighted Gap of "3.01 (-1.17)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 4 is "4.05 (+0.07)"
	And the Consensus for question 4 is "4.11 (+0.04)"
	And the Gap for question 4 is "0.06 (-0.03)"
	And the Weighted Gap for question 4 is "1.23 (-0.60)"
	
	Now I go to "Functions" > "Logout"