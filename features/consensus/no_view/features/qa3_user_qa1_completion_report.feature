Feature: Check Completion Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Female" in the "Invitees" column is "1,465"
	And the figure for "Female" in the "Respondents" column is "1,329(90.7%)"
	And the figure for "Female" in the "Completions" column is "1,306(89.1%)"
	And the figure for the "Totals" row in the "Invitees" column is "3,367"
	And the figure for the "Totals" row in the "Respondents" column is "3,041(90.3%)"
	And the figure for the "Totals" row in the "Completions" column is "3,004(89.2%)"

	When I am on the "By Time" tab
	Then the figure for "Wednesday, September 3, 2014" in the "Respondents" column is "633"
	And the figure for "Wednesday, September 3, 2014" in the "% Responded" column is "20.8%"
	And the figure for "Wednesday, September 3, 2014" in the "% Invited" column is "18.8%"
	And the figure for the "Totals" row in the "Respondents" column is "3,042"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "90.3%"	
	
	When I click on "Wednesday, September 3, 2014"
	Then the figure for "08:00 PM" in the "Respondents" column is "34"
	And the figure for "08:00 PM" in the "% Responded" column is "1.1%"
	And the figure for "08:00 PM" in the "% Invited" column is "1.0%"
	
	Now I go to "Functions" > "Logout"

