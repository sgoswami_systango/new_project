Feature: Check Favorability Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page

@wip		
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to 2013 Trend" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for question 1 is "75.5%"
	And the Neutral score for question 1 is "15.4%"
	And the Unfavorable score for question 1 is "9.1%"
	And the Overall Average at the bottom has a Favorable score of "72.3%"
	And the Overall Average Neutral score is "17.1%"
	And the Overall Average Unfavorable score is "10.6%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "80.2%"
	And the Neutral score for the "Clarity of Direction" category is "13.6%"
	And the Unfavorable score for the "Clarity of Direction" category is "6.2%"
	And the Overall Average at the bottom has a Favorable score of "72.3%"
	And the Overall Average Neutral score is "17.1%"
	And the Overall Average Unfavorable score is "10.6%"

	When I click on the Effective Management category
	Then the Favorable score for question 6 is "77.6%"
	And the Neutral score for question 6 is "11.7%"
	And the Unfavorable score for question 6 is "10.8%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to 2013 Trend" tab
	Then the Favorable score on top for question 1 is "75.5%"
	And the Neutral score on top for question 1 is "15.4%"
	And the Unfavorable score on top for question 1 is "9.1%"
	And the Favorable score on bottom for question 1 is "71.6%"
	And the Neutral score on bottom for question 1 is "18.3%"
	And the Unfavorable score on bottom for question 1 is "10.1%"
	And the Overall Average at the bottom Favorable score on top is "72.3%"
	And the Overall Average Neutral score on top is "17.1%"
	And the Overall Average Unfavorable score on top is "10.6%"
	And the Overall Average Favorable score on bottom is "68.8%"
	And the Overall Average Neutral score on bottom is "18.6%"
	And the Overall Average Unfavorable score on bottom is "12.6%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to 2013 Trend" tab
	Then the Favorable score on top for the "Clarity of Direction" category is "80.2%"
	And the Neutral score on top for the "Clarity of Direction" category is "13.6%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "6.2%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "77.7%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "15.1%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "7.2%"
	And the Overall Average at the bottom Favorable score on top is "72.3%"
	And the Overall Average Neutral score on top is "17.1%"
	And the Overall Average Unfavorable score on top is "10.6%"
	And the Overall Average Favorable score on bottom is "68.8%"
	And the Overall Average Neutral score on bottom is "18.6%"
	And the Overall Average Unfavorable score on bottom is "12.6%"

	When I click on the Effective Management category
	Then the Favorable score on top for question 6 is "77.6%"
	And the Neutral score on top for question 6 is "11.7%"
	And the Unfavorable score on top for question 6 is "10.8%"
	And the Favorable score on bottom for question 6 is "76.4%"
	And the Neutral score on bottom for question 6 is "13.5%"
	And the Unfavorable score on bottom for question 6 is "10.1%"
	
	Now I go to "Functions" > "Logout"
