Feature: Check Survey Results for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to 2013 Trend" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "38"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.3%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "238"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "7.9%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "466"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "15.4%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "1769"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "58.4%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "518"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "17.1%"
	And the "Mean" under question 1 is "3.82 (±0.85)"
	And the figure under question 1 for "Mean" in the "Number" column is "3029"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to 2013 Trend" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "38(41)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.3%(1.5%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "238(237)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "7.9%(8.6%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "466(501)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "15.4%(18.3%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "1769(1566)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "58.4%(57.1%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "518(398)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "17.1%(14.5%)"
	And the "Mean" under question 1 is "3.82 (±0.85)"
	And the figure under question 1 for "Mean" in the "Number" column is "3029(2743)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"