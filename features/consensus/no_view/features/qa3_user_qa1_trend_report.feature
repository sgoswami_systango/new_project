Feature: Check Trend Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa1"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the score for "question 1" in the "2014" column is "3.82 (+0.08)"
	And the score for "question 1" in the "2013" column is "3.74 (+0.26)"
	And the score for "question 1" in the "2012" column is "3.48"
	And the Overall Average for "2014" is "3.86 (+0.09)"
	And the Overall Average for "2013" is "3.77 (+0.12)"
	And the Overall Average for "2012" is "3.65"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the score for the "Clarity of Direction" category for "2014" is "4.01 (+0.07)"
	And the score for the "Clarity of Direction" category for "2013" is "3.94 (+0.20)"
	And the score for the "Clarity of Direction" category for "2012" is "3.74"
	And the Overall Average for "2014" is "3.86 (+0.09)"
	And the Overall Average for "2013" is "3.77 (+0.12)"
	And the Overall Average for "2012" is "3.65"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "3.82 (+0.08)"
	And the score for "question 1" in the "2013" column is "3.74 (+0.26)"
	And the score for "question 1" in the "2012" column is "3.48"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the score for "question 1" in the "2014" column is "75.5 (+3.9)"
	And the score for "question 1" in the "2013" column is "71.6 (+13.3)"
	And the score for "question 1" in the "2012" column is "58.3"
	And the Overall Average for "2014" is "72.3 (+3.5)"
	And the Overall Average for "2013" is "68.8 (+4.2)"
	And the Overall Average for "2012" is "64.6"
	
	When I am on the Categories tab
	And I am on the "Favorability" tab
	Then the score for the "Clarity of Direction" category for "2014" is "80.2 (+2.5)"
	And the score for the "Clarity of Direction" category for "2013" is "77.7 (+8.9)"
	And the score for the "Clarity of Direction" category for "2012" is "68.8"
	And the Overall Average for "2014" is "72.3 (+3.5)"
	And the Overall Average for "2013" is "68.8 (+4.2)"
	And the Overall Average for "2012" is "64.6"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "75.5 (+3.9)"
	And the score for "question 1" in the "2013" column is "71.6 (+13.3)"
	And the score for "question 1" in the "2012" column is "58.3"

	Now I go to "Functions" > "Logout"