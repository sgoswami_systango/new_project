Feature: Check Trend Report for user with query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the score for question 1 in the "2014" column is "3.50 (-0.27)"
	And the score for question 1 in the "2013" column is "3.77 (+0.02)"
	And the score for question 1 in the "2012" column is "3.75"
	And the Overall Average at the bottom for "2014" is "3.63 (-0.22)"
	And the Overall Average for "2013" is "3.85 (0.00)"
	And the Overall Average for "2012" is "3.85"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the score for the "Clarity of Direction" category for "2014" is "3.69 (-0.23)"
	And the score for the "Clarity of Direction" category for "2013" is "3.92 (+0.04)"
	And the score for the "Clarity of Direction" category for "2012" is "3.88"
	And the Overall Average at the bottom for "2014" is "3.63 (-0.22)"
	And the Overall Average for "2013" is "3.85 (0.00)"
	And the Overall Average for "2012" is "3.85"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "3.50 (-0.27)"
	And the score for question 1 in the "2013" column is "3.77 (+0.02)"
	And the score for question 1 in the "2012" column is "3.75"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the score for question 1 in the "2014" column is "56.2 (-18.2)"
	And the score for question 1 in the "2013" column is "74.4 (+7.4)"
	And the score for question 1 in the "2012" column is "67.0"
	And the Overall Average at the bottom for "2014" is "61.6 (-12.8)"
	And the Overall Average for "2013" is "74.4 (+2.9)"
	And the Overall Average for "2012" is "71.5"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the score for the "Clarity of Direction" category for "2014" is "65.6 (-14.2)"
	And the score for the "Clarity of Direction" category for "2013" is "79.8 (+5.0)"
	And the score for the "Clarity of Direction" category for "2012" is "74.8"
	And the Overall Average at the bottom for "2014" is "61.6 (-12.8)"
	And the Overall Average for "2013" is "74.4 (+2.9)"
	And the Overall Average for "2012" is "71.5"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "56.2 (-18.2)"
	And the score for question 1 in the "2013" column is "74.4 (+7.4)"
	And the score for question 1 in the "2012" column is "67.0"

	Now I go to "Functions" > "Logout"