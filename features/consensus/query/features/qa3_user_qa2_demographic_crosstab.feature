Feature: Check Demographic Crosstab for user with query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page

Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "3"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.8"
	And the score for question 1 in the "Male" column is "3.3"
	And the Weighted Average on the right for question 1 is "3.5"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "3.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.83"
	And the score for question 1 in the "Male" column is "3.30"
	And the Weighted Average on the right for question 1 is "3.50"
	And the Group Average at the bottom for the "Female" column is "3.90"
	And the Group Average at the bottom for the "Male" column is "3.48"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "71"
	And the score for question 1 in the "Male" column is "48"
	And the Weighted Average on the right for question 1 is "56"
	And the Group Average at the bottom for the "Female" column is "73"
	And the Group Average at the bottom for the "Male" column is "55"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "70.8"
	And the score for question 1 in the "Male" column is "47.5"
	And the Weighted Average on the right for question 1 is "56.2"
	And the Group Average at the bottom for the "Female" column is "72.7"
	And the Group Average at the bottom for the "Male" column is "55.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "70.83"
	And the score for question 1 in the "Male" column is "47.50"
	And the Weighted Average on the right for question 1 is "56.25"
	And the Group Average at the bottom for the "Female" column is "72.67"
	And the Group Average at the bottom for the "Male" column is "55.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0"
	And the score for question 1 in the "Male" column is "20"
	And the Weighted Average on the right for question 1 is "12"
	And the Group Average at the bottom for the "Female" column is "9"
	And the Group Average at the bottom for the "Male" column is "16"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.0"
	And the score for question 1 in the "Male" column is "20.0"
	And the Weighted Average on the right for question 1 is "12.5"
	And the Group Average at the bottom for the "Female" column is "8.9"
	And the Group Average at the bottom for the "Male" column is "15.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.00"
	And the score for question 1 in the "Male" column is "20.00"
	And the Weighted Average on the right for question 1 is "12.50"
	And the Group Average at the bottom for the "Female" column is "8.92"
	And the Group Average at the bottom for the "Male" column is "15.90"

	Now I go to "Functions" > "Logout"

# Categories
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "3"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "3"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.9"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.6"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.7"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "3.5"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.8"
	And the score for question 1 in the "Male" column is "3.3"
	And the Weighted Average on the right for question 1 is "3.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.88"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.58"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.69"
	And the Group Average at the bottom for the "Female" column is "3.90"
	And the Group Average at the bottom for the "Male" column is "3.48"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.83"
	And the score for question 1 in the "Male" column is "3.30"
	And the Weighted Average on the right for question 1 is "3.50"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "72"
	And the score for the "Clarity of Direction" category in the "Male" column is "62"
	And the Weighted Average on the right for the "Clarity of Direction" category is "66"
	And the Group Average at the bottom for the "Female" column is "73"
	And the Group Average at the bottom for the "Male" column is "55"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "71"
	And the score for question 1 in the "Male" column is "48"
	And the Weighted Average on the right for question 1 is "56"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "72.5"
	And the score for the "Clarity of Direction" category in the "Male" column is "61.5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "65.6"
	And the Group Average at the bottom for the "Female" column is "72.7"
	And the Group Average at the bottom for the "Male" column is "55.0"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "70.8"
	And the score for question 1 in the "Male" column is "47.5"
	And the Weighted Average on the right for question 1 is "56.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "72.50"
	And the score for the "Clarity of Direction" category in the "Male" column is "61.50"
	And the Weighted Average on the right for the "Clarity of Direction" category is "65.62"
	And the Group Average at the bottom for the "Female" column is "72.67"
	And the Group Average at the bottom for the "Male" column is "55.00"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "70.83"
	And the score for question 1 in the "Male" column is "47.50"
	And the Weighted Average on the right for question 1 is "56.25"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "5"
	And the score for the "Clarity of Direction" category in the "Male" column is "14"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10"
	And the Group Average at the bottom for the "Female" column is "9"
	And the Group Average at the bottom for the "Male" column is "16"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0"
	And the score for question 1 in the "Male" column is "20"
	And the Weighted Average on the right for question 1 is "12"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "5.0"
	And the score for the "Clarity of Direction" category in the "Male" column is "13.5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.3"
	And the Group Average at the bottom for the "Female" column is "8.9"
	And the Group Average at the bottom for the "Male" column is "15.9"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.0"
	And the score for question 1 in the "Male" column is "20.0"
	And the Weighted Average on the right for question 1 is "12.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "5.00"
	And the score for the "Clarity of Direction" category in the "Male" column is "13.50"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.31"
	And the Group Average at the bottom for the "Female" column is "8.92"
	And the Group Average at the bottom for the "Male" column is "15.90"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.00"
	And the score for question 1 in the "Male" column is "20.00"
	And the Weighted Average on the right for question 1 is "12.50"

	Now I go to "Functions" > "Logout"

# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner


	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "3 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "3 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.8 (3.9)"
	And the score for question 1 in the "Male" column is "3.8 (3.9)"
	And the Weighted Average on the right for question 1 is "3.5"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.9)"
	And the Group Average at the bottom for the "Male" column is "3.5 (3.8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.83 (3.93)"
	And the score for question 1 in the "Male" column is "3.30 (3.69)"
	And the Weighted Average on the right for question 1 is "3.50"
	And the Group Average at the bottom for the "Female" column is "3.90 (3.93)"
	And the Group Average at the bottom for the "Male" column is "3.48 (3.81)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "71 (86)"
	And the score for question 1 in the "Male" column is "48 (69)"
	And the Weighted Average on the right for question 1 is "56"
	And the Group Average at the bottom for the "Female" column is "73 (74)"
	And the Group Average at the bottom for the "Male" column is "55 (75)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "70.8 (86.2)"
	And the score for question 1 in the "Male" column is "47.5 (68.9)"
	And the Weighted Average on the right for question 1 is "56.2"
	And the Group Average at the bottom for the "Female" column is "72.7 (74.1)"
	And the Group Average at the bottom for the "Male" column is "55.0 (74.6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "70.83 (86.21)"
	And the score for question 1 in the "Male" column is "47.50 (68.85)"
	And the Weighted Average on the right for question 1 is "56.25"
	And the Group Average at the bottom for the "Female" column is "72.67 (74.09)"
	And the Group Average at the bottom for the "Male" column is "55.00 (74.61)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0 (3)"
	And the score for question 1 in the "Male" column is "20 (5)"
	And the Weighted Average on the right for question 1 is "12"
	And the Group Average at the bottom for the "Female" column is "9 (7)"
	And the Group Average at the bottom for the "Male" column is "16 (8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.0 (3.4)"
	And the score for question 1 in the "Male" column is "20.0 (4.9)"
	And the Weighted Average on the right for question 1 is "12.5"
	And the Group Average at the bottom for the "Female" column is "8.9 (7.4)"
	And the Group Average at the bottom for the "Male" column is "15.9 (8.4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "0.00 (3.45)"
	And the score for question 1 in the "Male" column is "20.00 (4.92)"
	And the Weighted Average on the right for question 1 is "12.50"
	And the Group Average at the bottom for the "Female" column is "8.92 (7.40)"
	And the Group Average at the bottom for the "Male" column is "15.90 (8.42)"

	Now I go to "Functions" > "Logout"

#Categories + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "3 (4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4 (4)"
	And the score for question 1 in the "Male" column is "3 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.9 (4.1)"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.6 (3.8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.7"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.9)"
	And the Group Average at the bottom for the "Male" column is "3.5 (3.8)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.8 (3.9)"
	And the score for question 1 in the "Male" column is "3.3 (3.7)"
	And the Weighted Average on the right for question 1 is "3.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.88 (4.09)"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.58 (3.84)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.69"
	And the Group Average at the bottom for the "Female" column is "3.90 (3.93)"
	And the Group Average at the bottom for the "Male" column is "3.48 (3.81)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.83 (3.93)"
	And the score for question 1 in the "Male" column is "3.30 (3.69)"
	And the Weighted Average on the right for question 1 is "3.50"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "72 (87)"
	And the score for the "Clarity of Direction" category in the "Male" column is "62 (76)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "66"
	And the Group Average at the bottom for the "Female" column is "73 (74)"
	And the Group Average at the bottom for the "Male" column is "55 (75)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "71 (86)"
	And the score for question 1 in the "Male" column is "48 (69)"
	And the Weighted Average on the right for question 1 is "56"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "72.5 (86.9)"
	And the score for the "Clarity of Direction" category in the "Male" column is "61.5 (76.4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "65.6"
	And the Group Average at the bottom for the "Female" column is "72.7 (74.1)"
	And the Group Average at the bottom for the "Male" column is "55.0 (74.6)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "70.8 (86.2)"
	And the score for question 1 in the "Male" column is "47.5 (68.9)"
	And the Weighted Average on the right for question 1 is "56.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "72.50 (86.90)"
	And the score for the "Clarity of Direction" category in the "Male" column is "61.50 (76.39)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "65.62"
	And the Group Average at the bottom for the "Female" column is "72.67 (74.09)"
	And the Group Average at the bottom for the "Male" column is "55.00 (74.61)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "70.83 (86.21)"
	And the score for question 1 in the "Male" column is "47.50 (68.85)"
	And the Weighted Average on the right for question 1 is "56.25"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "5 (2)"
	And the score for the "Clarity of Direction" category in the "Male" column is "14 (5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10"
	And the Group Average at the bottom for the "Female" column is "9 (7)"
	And the Group Average at the bottom for the "Male" column is "16 (8)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0 (3)"
	And the score for question 1 in the "Male" column is "20 (5)"
	And the Weighted Average on the right for question 1 is "12"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "5.0 (2.1)"
	And the score for the "Clarity of Direction" category in the "Male" column is "13.5 (4.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.3"
	And the Group Average at the bottom for the "Female" column is "8.9 (7.4)"
	And the Group Average at the bottom for the "Male" column is "15.9 (8.4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.0 (3.4)"
	And the score for question 1 in the "Male" column is "20.0 (4.9)"
	And the Weighted Average on the right for question 1 is "12.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "5.00 (2.07)"
	And the score for the "Clarity of Direction" category in the "Male" column is "13.50 (4.92)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.31"
	And the Group Average at the bottom for the "Female" column is "8.92 (7.40)"
	And the Group Average at the bottom for the "Male" column is "15.90 (8.42)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "0.00 (3.45)"
	And the score for question 1 in the "Male" column is "20.00 (4.92)"
	And the Weighted Average on the right for question 1 is "12.50"
	
	Now I go to "Functions" > "Logout"
