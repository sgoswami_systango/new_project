Feature: Check Survey Results for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab

	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "8"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "12.5%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "20"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "31.2%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "32"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "50.0%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "4"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "6.2%"
	And the "Mean" under question 1 is "3.50 (±0.79)"
	And the figure under question 1 for "Mean" in the "Number" column is "64"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0(38)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%(1.3%)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Segment Density" column is "0.0%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "8(238)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "12.5%(7.9%)"
	And the figure under question 1 for "2. Disagree" in the "Segment Density" column is "3.4%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "20(466)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "31.2%(15.4%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Segment Density" column is "4.3%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "32(1769)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "50.0%(58.4%)"
	And the figure under question 1 for "4. Agree" in the "Segment Density" column is "1.8%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "4(518)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "6.2%(17.1%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Segment Density" column is "0.8%"
	And the "Mean" under question 1 is "3.50 (±0.79)"
	And the figure under question 1 for "Mean" in the "Number" column is "64(3029)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	When I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "0(0)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "0.0%(0.0%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "8(4)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "12.5%(4.4%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "20(19)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "31.2%(21.1%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "32(61)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "50.0%(67.8%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "4(6)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "6.2%(6.7%)"
	And the "Mean" under question 1 is "3.50 (±0.79)"
	And the figure under question 1 for "Mean" in the "Number" column is "64(90)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"