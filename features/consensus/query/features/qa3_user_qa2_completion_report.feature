Feature: Check Completion Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Female" in the "Invitees" column is "33"
	And the figure for "Female" in the "Respondents" column is "24(72.7%)"
	And the figure for "Female" in the "Completions" column is "24(72.7%)"
	And the figure for the "Totals" row in the "Invitees" column is "93"
	And the figure for the "Totals" row in the "Respondents" column is "64(68.8%)"
	And the figure for the "Totals" row in the "Completions" column is "64(68.8%)"
	
	When I am on the "By Time" tab
	Then the figure for "Wednesday, September 3, 2014" in the "Respondents" column is "9"
	And the figure for "Wednesday, September 3, 2014" in the "% Responded" column is "14.1%"
	And the figure for "Wednesday, September 3, 2014" in the "% Invited" column is "9.7%"
	And the figure for the "Totals" row in the "Respondents" column is "64"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "68.8%"	
	
	When I click on "Wednesday, September 3, 2014"
	Then the figure for "1:00 PM" in the "Respondents" column is "4"
	And the figure for "1:00 PM" in the "% Responded" column is "6.2%"
	And the figure for "1:00 PM" in the "% Invited" column is "4.3%"

	Now I go to "Functions" > "Logout"

