Feature: Check Trend Report for user with query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 1 in the "My View" column is "56.2%"
	And the score for question 1 in the "LPL Financial" column is "75.5%(19.3)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(12.4)"

	When I am on the "Categories" tab
	Then the score for question 1 in the "My View" column is "56.2%"
	Then the score for question 1 in the "LPL Financial" column is "75.5%(19.3)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(12.4)"
	
	Now I go to "Functions" > "Logout"