Feature: Check Gap Report for user with query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "3.50"
	And the Consensus for question 1 is "4.21"
	And the Gap for question 1 is "0.71"
	And the Weighted Gap for question 1 is "14.95"
	And the Overall Average at the bottom has an Eval Avg of "3.63"
	And the Overall Average has a Consensus of "4.03"
	And the Overall Average has a Gap of "0.40"
	And the Overall Average has a Weighted Gap of "8.06"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.69"
	And the Consensus for the "Clarity of Direction" category is "4.16"
	And the Gap for the "Clarity of Direction" category is "0.47"
	And the Weighted Gap for the "Clarity of Direction" category is "9.78"
	And the Overall Average at the bottom has an Eval Avg of "3.63"
	And the Overall Average has a Consensus of "4.03"
	And the Overall Average has a Gap of "0.40"
	And the Overall Average has a Weighted Gap of "8.06"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.50"
	And the Consensus for question 1 is "4.21"
	And the Gap for question 1 is "0.71"
	And the Weighted Gap for question 1 is "14.95"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Eval Avg for question 1 is "3.50 (-0.32)"
	And the Consensus for question 1 is "4.21 (+0.06)"
	And the Gap for question 1 is "0.71 (+0.38)"
	And the Weighted Gap for question 1 is "14.95 (+8.10)"
	And the Overall Average at the bottom has an Eval Avg of "3.63 (-0.23)"
	And the Overall Average has a Consensus of "4.03 (+0.02)"
	And the Overall Average has a Gap of "0.40 (+0.25)"
	And the Overall Average has a Weighted Gap of "8.06 (+5.05)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "3.69 (-0.32)"
	And the Consensus for the "Clarity of Direction" category is "4.16 (+0.01)"
	And the Gap for the "Clarity of Direction" category is "0.47 (+0.33)"
	And the Weighted Gap for the "Clarity of Direction" category is "9.78 (+6.87)"
	And the Overall Average at the bottom has an Eval Avg of "3.63 (-0.23)"
	And the Overall Average has a Consensus of "4.03 (+0.02)"
	And the Overall Average has a Gap of "0.40 (+0.25)"
	And the Overall Average has a Weighted Gap of "8.06 (+5.05)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.50 (-0.32)"
	And the Consensus for question 1 is "4.21 (+0.06)"
	And the Gap for question 1 is "0.71 (+0.38)"
	And the Weighted Gap for question 1 is "14.95 (+8.10)"

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Eval Avg for question 1 is "3.50 (-0.27)"
	And the Consensus for question 1 is "4.21 (-0.16)"
	And the Gap for question 1 is "0.71 (+0.11)"
	And the Weighted Gap for question 1 is "14.95 (+1.84)"
	And the Overall Average at the bottom has an Eval Avg of "3.63 (-0.22)"
	And the Overall Average has a Consensus of "4.03 (-0.07)"
	And the Overall Average has a Gap of "0.40 (+0.15)"
	And the Overall Average has a Weighted Gap of "8.06 (+2.94)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "3.69 (-0.23)"
	And the Consensus for the "Clarity of Direction" category is "4.16 (-0.14)"
	And the Gap for the "Clarity of Direction" category is "0.47 (+0.09)"
	And the Weighted Gap for the "Clarity of Direction" category is "9.78 (+1.61)"
	And the Overall Average at the bottom has an Eval Avg of "3.63 (-0.22)"
	And the Overall Average has a Consensus of "4.03 (-0.07)"
	And the Overall Average has a Gap of "0.40 (+0.15)"
	And the Overall Average has a Weighted Gap of "8.06 (+2.94)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.50 (-0.27)"
	And the Consensus for question 1 is "4.21 (-0.16)"
	And the Gap for question 1 is "0.71 (+0.11)"
	And the Weighted Gap for question 1 is "14.95 (+1.84)"

	Now I go to "Functions" > "Logout"