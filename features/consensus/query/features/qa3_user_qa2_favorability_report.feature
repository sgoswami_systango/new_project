Feature: Check Favorability Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa2"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page

Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for question 1 is "56.2%"
	And the Neutral score for question 1 is "31.2%"
	And the Unfavorable score for question 1 is "12.5%"
	And the Overall Average at the bottom has a Favorable score of "61.6%"
	And the Overall Average Neutral score is "25.1%"
	And the Overall Average Unfavorable score is "13.3%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "65.6%"
	And the Neutral score for the "Clarity of Direction" category is "24.1%"
	And the Unfavorable score for the "Clarity of Direction" category is "10.3%"
	And the Overall Average at the bottom has a Favorable score of "61.6%"
	And the Overall Average Neutral score is "25.1%"
	And the Overall Average Unfavorable score is "13.3%"

	When I click on the Clarity of Direction category
	Then the Favorable score for question 1 is "56.2%"
	And the Neutral score for question 1 is "31.2%"
	And the Unfavorable score for question 1 is "12.5%"
	
# Compared to Org

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for question 1 is "56.2%"
	And the Neutral score on top for question 1 is "31.2%"
	And the Unfavorable score on top for question 1 is "12.5%"
	And the Favorable score on bottom for question 1 is "75.5%"
	And the Neutral score on bottom for question 1 is "15.4%"
	And the Unfavorable score on bottom for question 1 is "9.1%"
	And the Overall Average at the bottom Favorable score on top is "61.6%"
	And the Overall Average Neutral score on top is "25.1%"
	And the Overall Average Unfavorable score on top is "13.3%"
	And the Overall Average Favorable score on bottom is "72.3%"
	And the Overall Average Neutral score on bottom is "17.1%"
	And the Overall Average Unfavorable score on bottom is "10.6%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "65.6%"
	And the Neutral score on top for the "Clarity of Direction" category is "24.1%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "10.3%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "80.2%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "13.6%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "6.2%"
	And the Overall Average at the bottom Favorable score on top is "61.6%"
	And the Overall Average Neutral score on top is "25.1%"
	And the Overall Average Unfavorable score on top is "13.3%"
	And the Overall Average Favorable score on bottom is "72.3%"
	And the Overall Average Neutral score on bottom is "17.1%"
	And the Overall Average Unfavorable score on bottom is "10.6%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "56.2%"
	And the Neutral score on top for question 1 is "31.2%"
	And the Unfavorable score on top for question 1 is "12.5%"
	And the Favorable score on bottom for question 1 is "75.5%"
	And the Neutral score on bottom for question 1 is "15.4%"
	And the Unfavorable score on bottom for question 1 is "9.1%"

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for question 1 is "56.2%"
	And the Neutral score on top for question 1 is "31.2%"
	And the Unfavorable score on top for question 1 is "12.5%"
	And the Favorable score on bottom for question 1 is "74.4%"
	And the Neutral score on bottom for question 1 is "21.1%"
	And the Unfavorable score on bottom for question 1 is "4.4%"
	And the Overall Average at the bottom Favorable score on top is "61.6%"
	And the Overall Average Neutral score on top is "25.1%"
	And the Overall Average Unfavorable score on top is "13.3%"
	And the Overall Average Favorable score on bottom is "74.4%"
	And the Overall Average Neutral score on bottom is "17.5%"
	And the Overall Average Unfavorable score on bottom is "8.1%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "65.6%"
	And the Neutral score on top for the "Clarity of Direction" category is "24.1%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "10.3%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "79.8%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "16.2%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "4.0%"
	And the Overall Average at the bottom Favorable score on top is "61.6%"
	And the Overall Average Neutral score on top is "25.1%"
	And the Overall Average Unfavorable score on top is "13.3%"
	And the Overall Average Favorable score on bottom is "74.4%"
	And the Overall Average Neutral score on bottom is "17.5%"
	And the Overall Average Unfavorable score on bottom is "8.1%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "56.2%"
	And the Neutral score on top for question 1 is "31.2%"
	And the Unfavorable score on top for question 1 is "12.5%"
	And the Favorable score on bottom for question 1 is "74.4%"
	And the Neutral score on bottom for question 1 is "21.1%"
	And the Unfavorable score on bottom for question 1 is "4.4%"

	Now I go to "Functions" > "Logout"