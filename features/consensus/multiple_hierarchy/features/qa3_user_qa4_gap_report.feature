Feature: Check Gap Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "3.93"
	And the Consensus for question 1 is "4.27"
	And the Gap for question 1 is "0.34"
	And the Weighted Gap for question 1 is "7.26"
	And the Overall Average at the bottom has an Eval Avg of "3.74"
	And the Overall Average has a Consensus of "4.05"
	And the Overall Average has a Gap of "0.31"
	And the Overall Average has a Weighted Gap of "6.28"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.98"
	And the Consensus for the "Clarity of Direction" category is "4.24"
	And the Gap for the "Clarity of Direction" category is "0.26"
	And the Weighted Gap for the "Clarity of Direction" category is "5.51"
	And the Overall Average at the bottom has an Eval Avg of "3.74"
	And the Overall Average has a Consensus of "4.05"
	And the Overall Average has a Gap of "0.31"
	And the Overall Average has a Weighted Gap of "6.28"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.93"
	And the Consensus for question 1 is "4.27"
	And the Gap for question 1 is "0.34"
	And the Weighted Gap for question 1 is "7.26"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Eval Avg for question 1 is "3.93 (+0.11)"
	And the Consensus for question 1 is "4.27 (+0.12)"
	And the Gap for question 1 is "0.34 (+0.01)"
	And the Weighted Gap for question 1 is "7.26 (+0.41)"
	And the Overall Average at the bottom has an Eval Avg of "3.74 (-0.12)"
	And the Overall Average has a Consensus of "4.05 (+0.04)"
	And the Overall Average has a Gap of "0.31 (+0.16)"
	And the Overall Average has a Weighted Gap of "6.28 (+3.27)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "3.98 (-0.03)"
	And the Consensus for the "Clarity of Direction" category is "4.24 (+0.09)"
	And the Gap for the "Clarity of Direction" category is "0.26 (+0.12)"
	And the Weighted Gap for the "Clarity of Direction" category is "5.51 (+2.60)"
	And the Overall Average at the bottom has an Eval Avg of "3.74 (-0.12)"
	And the Overall Average has a Consensus of "4.05 (+0.04)"
	And the Overall Average has a Gap of "0.31 (+0.16)"
	And the Overall Average has a Weighted Gap of "6.28 (+3.27)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.93 (+0.11)"
	And the Consensus for question 1 is "4.27 (+0.12)"
	And the Gap for question 1 is "0.34 (+0.01)"
	And the Weighted Gap for question 1 is "7.26 (+0.41)"

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Eval Avg for question 1 is "3.93 (+0.78)"
	And the Consensus for question 1 is "4.27 (+0.30)"
	And the Gap for question 1 is "0.34 (-0.48)"
	And the Weighted Gap for question 1 is "7.26 (-9.02)"
	And the Overall Average at the bottom has an Eval Avg of "3.74 (+0.25)"
	And the Overall Average has a Consensus of "4.05 (+0.18)"
	And the Overall Average has a Gap of "0.31 (-0.07)"
	And the Overall Average has a Weighted Gap of "6.28 (-1.07)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "3.98 (+0.69)"
	And the Consensus for the "Clarity of Direction" category is "4.24 (+0.43)"
	And the Gap for the "Clarity of Direction" category is "0.26 (-0.26)"
	And the Weighted Gap for the "Clarity of Direction" category is "5.51 (-4.40)"
	And the Overall Average at the bottom has an Eval Avg of "3.74 (+0.25)"
	And the Overall Average has a Consensus of "4.05 (+0.18)"
	And the Overall Average has a Gap of "0.31 (-0.07)"
	And the Overall Average has a Weighted Gap of "6.28 (-1.07)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.93 (+0.78)"
	And the Consensus for question 1 is "4.27 (+0.30)"
	And the Gap for question 1 is "0.34 (-0.48)"
	And the Weighted Gap for question 1 is "7.26 (-9.02)"

	Now I go to "Functions" > "Logout"