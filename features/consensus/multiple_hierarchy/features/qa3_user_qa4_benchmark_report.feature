Feature: Check Benchmark Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
		
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 1 in the "My View" column is "83.3%"
	And the score for question 1 in the "LPL Financial" column is "75.5%(-7.8)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-14.7)"

	When I am on the "Categories" tab
	Then the score for question 1 in the "My View" column is "83.3%"
	Then the score for question 1 in the "LPL Financial" column is "75.5%(-7.8)"
	And the score for question 1 in the "Perceptyx Benchmarks" column is "68.6%(-14.7)"
	
	Now I go to "Functions" > "Logout"