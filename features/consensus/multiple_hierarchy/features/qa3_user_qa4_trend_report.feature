Feature: Check Trend Report for user with a multiple-hierachy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the score for question 1 in the "2014" column is "3.93 (+0.78)"
	And the score for question 1 in the "2013" column is "3.15"
	And the Overall Average at the bottom for "2014" is "3.74 (+0.25)"
	And the Overall Average for "2013" is "3.49"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the score for the "Clarity of Direction" category for "2014" is "3.98 (+0.69)"
	And the score for the "Clarity of Direction" category for "2013" is "3.29"
	And the Overall Average at the bottom for "2014" is "3.74 (+0.25)"
	And the Overall Average for "2013" is "3.49"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "3.93 (+0.78)"
	And the score for question 1 in the "2013" column is "3.15"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the score for question 1 in the "2014" column is "83.3 (+44.8)"
	And the score for question 1 in the "2013" column is "38.5"
	And the Overall Average at the bottom for "2014" is "68.4 (+9.9)"
	And the Overall Average for "2013" is "58.5"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the score for the "Clarity of Direction" category for "2014" is "80.7 (+29.9)"
	And the score for the "Clarity of Direction" category for "2013" is "50.8"
	And the Overall Average at the bottom for "2014" is "68.4 (+9.9)"
	And the Overall Average for "2013" is "58.5"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "83.3 (+44.8)"
	And the score for question 1 in the "2013" column is "38.5"

	Now I go to "Functions" > "Logout"