Feature: Check Demographic Crosstab for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa4"
	And I click the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.8"
	And the score for question 1 in the "Male" column is "4.1"
	And the Weighted Average on the right for question 1 is "3.9"
	And the Group Average at the bottom for the "Female" column is "3.7"
	And the Group Average at the bottom for the "Male" column is "3.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.81"
	And the score for question 1 in the "Male" column is "4.07"
	And the Weighted Average on the right for question 1 is "3.93"
	And the Group Average at the bottom for the "Female" column is "3.74"
	And the Group Average at the bottom for the "Male" column is "3.74"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "88"
	And the score for question 1 in the "Male" column is "79"
	And the Weighted Average on the right for question 1 is "83"
	And the Group Average at the bottom for the "Female" column is "68"
	And the Group Average at the bottom for the "Male" column is "69"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "87.5"
	And the score for question 1 in the "Male" column is "78.6"
	And the Weighted Average on the right for question 1 is "83.3"
	And the Group Average at the bottom for the "Female" column is "67.6"
	And the Group Average at the bottom for the "Male" column is "69.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "87.50"
	And the score for question 1 in the "Male" column is "78.57"
	And the Weighted Average on the right for question 1 is "83.33"
	And the Group Average at the bottom for the "Female" column is "67.62"
	And the Group Average at the bottom for the "Male" column is "69.29"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "12"
	And the score for question 1 in the "Male" column is "0"
	And the Weighted Average on the right for question 1 is "7"
	And the Group Average at the bottom for the "Female" column is "10"
	And the Group Average at the bottom for the "Male" column is "11"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "12.5"
	And the score for question 1 in the "Male" column is "0.0"
	And the Weighted Average on the right for question 1 is "6.7"
	And the Group Average at the bottom for the "Female" column is "10.4"
	And the Group Average at the bottom for the "Male" column is "10.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "12.50"
	And the score for question 1 in the "Male" column is "0.00"
	And the Weighted Average on the right for question 1 is "6.67"
	And the Group Average at the bottom for the "Female" column is "10.38"
	And the Group Average at the bottom for the "Male" column is "10.71"

	Now I go to "Functions" > "Logout"

# Categories

Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.0"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.7"
	And the Group Average at the bottom for the "Male" column is "3.7"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.8"
	And the score for question 1 in the "Male" column is "4.1"
	And the Weighted Average on the right for question 1 is "3.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.99"
	And the score for the "Clarity of Direction" category in the "Male" column is "3.97"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.98"
	And the Group Average at the bottom for the "Female" column is "3.74"
	And the Group Average at the bottom for the "Male" column is "3.74"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.81"
	And the score for question 1 in the "Male" column is "4.07"
	And the Weighted Average on the right for question 1 is "3.93"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "81"
	And the score for the "Clarity of Direction" category in the "Male" column is "80"
	And the Weighted Average on the right for the "Clarity of Direction" category is "81"
	And the Group Average at the bottom for the "Female" column is "68"
	And the Group Average at the bottom for the "Male" column is "69"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "88"
	And the score for question 1 in the "Male" column is "79"
	And the Weighted Average on the right for question 1 is "83"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "81.2"
	And the score for the "Clarity of Direction" category in the "Male" column is "80.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80.7"
	And the Group Average at the bottom for the "Female" column is "67.6"
	And the Group Average at the bottom for the "Male" column is "69.3"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "87.5"
	And the score for question 1 in the "Male" column is "78.6"
	And the Weighted Average on the right for question 1 is "83.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "81.25"
	And the score for the "Clarity of Direction" category in the "Male" column is "80.00"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80.67"
	And the Group Average at the bottom for the "Female" column is "67.62"
	And the Group Average at the bottom for the "Male" column is "69.29"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "87.50"
	And the score for question 1 in the "Male" column is "78.57"
	And the Weighted Average on the right for question 1 is "83.33"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "6"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5"
	And the Group Average at the bottom for the "Female" column is "10"
	And the Group Average at the bottom for the "Male" column is "11"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "12"
	And the score for question 1 in the "Male" column is "7"
	And the Weighted Average on the right for question 1 is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "6.2"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.3"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5.3"
	And the Group Average at the bottom for the "Female" column is "10.4"
	And the Group Average at the bottom for the "Male" column is "10.7"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "12.5"
	And the score for question 1 in the "Male" column is "0.0"
	And the Weighted Average on the right for question 1 is "6.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "6.25"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.29"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5.33"
	And the Group Average at the bottom for the "Female" column is "10.38"
	And the Group Average at the bottom for the "Male" column is "10.71"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "12.50"
	And the score for question 1 in the "Male" column is "0.00"
	And the Weighted Average on the right for question 1 is "6.67"

	Now I go to "Functions" > "Logout"

# Questions + trend

Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "4"
	And the score for question 1 in the "Client Support Services" column is "4 (3)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "4"
	And the Group Average at the bottom for the "Client Support Services" column is "4 (3)"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "4.0"
	And the score for question 1 in the "Client Support Services" column is "3.9 (2.9)"
	And the Weighted Average on the right for question 1 is "3.9"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "3.8"
	And the Group Average at the bottom for the "Client Support Services" column is "3.7 (3.5)"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "4.00"
	And the score for question 1 in the "Client Support Services" column is "3.90 (2.88)"
	And the Weighted Average on the right for question 1 is "3.93"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "3.76"
	And the Group Average at the bottom for the "Client Support Services" column is "3.73 (3.50)"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "90"
	And the score for question 1 in the "Client Support Services" column is "80 (25)"
	And the Weighted Average on the right for question 1 is "83"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "64"
	And the Group Average at the bottom for the "Client Support Services" column is "71 (60)"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "90.0"
	And the score for question 1 in the "Client Support Services" column is "80.0 (25.0)"
	And the Weighted Average on the right for question 1 is "83.3"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "63.8"
	And the Group Average at the bottom for the "Client Support Services" column is "70.7 (59.7)"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "90.00"
	And the score for question 1 in the "Client Support Services" column is "80.00 (25.00)"
	And the Weighted Average on the right for question 1 is "83.33"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "63.80"
	And the Group Average at the bottom for the "Client Support Services" column is "70.70 (59.72)"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "0"
	And the score for question 1 in the "Client Support Services" column is "10 (25)"
	And the Weighted Average on the right for question 1 is "7"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "10"
	And the Group Average at the bottom for the "Client Support Services" column is "11 (16)"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "0.0"
	And the score for question 1 in the "Client Support Services" column is "10.0 (25.0)"
	And the Weighted Average on the right for question 1 is "6.7"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "10.0"
	And the Group Average at the bottom for the "Client Support Services" column is "10.8 (15.6)"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "0.00"
	And the score for question 1 in the "Client Support Services" column is "10.00 (25.00)"
	And the Weighted Average on the right for question 1 is "6.67"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "10.00"
	And the Group Average at the bottom for the "Client Support Services" column is "10.80 (15.56)"

	Now I go to "Functions" > "Logout"

# Categories + trend

Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "4"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "4 (3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "4"
	And the Group Average at the bottom for the "Client Support Services" column is "4 (3)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "4"
	And the score for question 1 in the "Client Support Services" column is "4 (3)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "4.0"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "4.0 (3.0)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "3.8"
	And the Group Average at the bottom for the "Client Support Services" column is "3.7 (3.5)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "4.0"
	And the score for question 1 in the "Client Support Services" column is "3.9 (2.9)"
	And the Weighted Average on the right for question 1 is "3.9"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "4.04"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "3.95 (3.02)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.98"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "3.76"
	And the Group Average at the bottom for the "Client Support Services" column is "3.73 (3.50)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "4.00"
	And the score for question 1 in the "Client Support Services" column is "3.90 (2.88)"
	And the Weighted Average on the right for question 1 is "3.93"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "76"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "83 (42)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "81"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "64"
	And the Group Average at the bottom for the "Client Support Services" column is "71 (60)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "90"
	And the score for question 1 in the "Client Support Services" column is "80 (25)"
	And the Weighted Average on the right for question 1 is "83"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "76.0"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "83.0 (42.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80.7"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "63.8"
	And the Group Average at the bottom for the "Client Support Services" column is "70.7 (59.7)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "90.0"
	And the score for question 1 in the "Client Support Services" column is "80.0 (25.0)"
	And the Weighted Average on the right for question 1 is "83.3"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "76.00"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "83.00 (42.50)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80.67"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "63.80"
	And the Group Average at the bottom for the "Client Support Services" column is "70.70 (59.72)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "90.00"
	And the score for question 1 in the "Client Support Services" column is "80.00 (25.00)"
	And the Weighted Average on the right for question 1 is "83.33"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "0"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "8 (25)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "10"
	And the Group Average at the bottom for the "Client Support Services" column is "11 (16)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "0"
	And the score for question 1 in the "Client Support Services" column is "10 (25)"
	And the Weighted Average on the right for question 1 is "7"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "0.0"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "8.0 (25.0)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5.3"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "10.0"
	And the Group Average at the bottom for the "Client Support Services" column is "10.8 (15.6)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "0.0"
	And the score for question 1 in the "Client Support Services" column is "10.0 (25.0)"
	And the Weighted Average on the right for question 1 is "6.7"

	When I select "Business Group" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "C&CS - Governance, Risk & Compliance" column is "0.00"
	And the score for the "Clarity of Direction" category in the "Client Support Services" column is "8.00 (25.00)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5.33"
	And the Group Average at the bottom for the "C&CS - Governance, Risk & Compliance" column is "10.00"
	And the Group Average at the bottom for the "Client Support Services" column is "10.80 (15.56)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "C&CS - Governance, Risk & Compliance" column is "0.00"
	And the score for question 1 in the "Client Support Services" column is "10.00 (25.00)"
	And the Weighted Average on the right for question 1 is "6.67"

	Now I go to "Functions" > "Logout"