Feature: Check Gap Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "3.97"
	And the Consensus for question 1 is "4.19"
	And the Gap for question 1 is "0.22"
	And the Weighted Gap for question 1 is "4.61"
	And the Overall Average at the bottom has an Eval Avg of "3.97"
	And the Overall Average has a Consensus of "4.03"
	And the Overall Average has a Gap of "0.06"
	And the Overall Average has a Weighted Gap of "1.21"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "4.17"
	And the Consensus for the "Clarity of Direction" category is "4.20"
	And the Gap for the "Clarity of Direction" category is "0.03"
	And the Weighted Gap for the "Clarity of Direction" category is "0.63"
	And the Overall Average at the bottom has an Eval Avg of "3.97"
	And the Overall Average has a Consensus of "4.03"
	And the Overall Average has a Gap of "0.06"
	And the Overall Average has a Weighted Gap of "1.21"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.97"
	And the Consensus for question 1 is "4.19"
	And the Gap for question 1 is "0.22"
	And the Weighted Gap for question 1 is "4.61"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Eval Avg for question 1 is "3.97 (+0.15)"
	And the Consensus for question 1 is "4.19 (+0.04)"
	And the Gap for question 1 is "0.22 (-0.11)"
	And the Weighted Gap for question 1 is "4.61 (-2.24)"
	And the Overall Average at the bottom has an Eval Avg of "3.97 (+0.11)"
	And the Overall Average has a Consensus of "4.03 (+0.02)"
	And the Overall Average has a Gap of "0.06 (-0.09)"
	And the Overall Average has a Weighted Gap of "1.21 (-1.80)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "4.17 (+0.16)"
	And the Consensus for the "Clarity of Direction" category is "4.20 (+0.05)"
	And the Gap for the "Clarity of Direction" category is "0.03 (-0.11)"
	And the Weighted Gap for the "Clarity of Direction" category is "0.63 (-2.28)"
	And the Overall Average at the bottom has an Eval Avg of "3.97 (+0.11)"
	And the Overall Average has a Consensus of "4.03 (+0.02)"
	And the Overall Average has a Gap of "0.06 (-0.09)"
	And the Overall Average has a Weighted Gap of "1.21 (-1.80)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.97 (+0.15)"
	And the Consensus for question 1 is "4.19 (+0.04)"
	And the Gap for question 1 is "0.22 (-0.11)"
	And the Weighted Gap for question 1 is "4.61 (-2.24)"

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Eval Avg for question 1 is "3.97 (+0.18)"
	And the Consensus for question 1 is "4.19 (-0.10)"
	And the Gap for question 1 is "0.22 (-0.28)"
	And the Weighted Gap for question 1 is "4.61 (-6.11)"
	And the Overall Average at the bottom has an Eval Avg of "3.97 (+0.02)"
	And the Overall Average has a Consensus of "4.03 (0.00)"
	And the Overall Average has a Gap of "0.06 (-0.02)"
	And the Overall Average has a Weighted Gap of "1.21 (-0.40)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Eval Avg for the "Clarity of Direction" category is "4.17 (+0.09)"
	And the Consensus for the "Clarity of Direction" category is "4.20 (0.00)"
	And the Gap for the "Clarity of Direction" category is "0.03 (-0.09)"
	And the Weighted Gap for the "Clarity of Direction" category is "0.63 (-1.89)"
	And the Overall Average at the bottom has an Eval Avg of "3.97 (+0.02)"
	And the Overall Average has a Consensus of "4.03 (0.00)"
	And the Overall Average has a Gap of "0.06 (-0.02)"
	And the Overall Average has a Weighted Gap of "1.21 (-0.40)"
	
	When I click on the Clarity of Direction category
	Then the Eval Avg for question 1 is "3.97 (+0.18)"
	And the Consensus for question 1 is "4.19 (-0.10)"
	And the Gap for question 1 is "0.22 (-0.28)"
	And the Weighted Gap for question 1 is "4.61 (-6.11)"

	Now I go to "Functions" > "Logout"