Feature: Check Survey Results for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "No Comparison" tab

	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "2"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.5%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "7"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "5.2%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "13"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "9.6%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "84"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "62.2%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "29"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "21.5%"
	And the "Mean" under question 1 is "3.97 (±0.81)"
	And the figure under question 1 for "Mean" in the "Number" column is "135"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%"

	When I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "2(38)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.5%(1.3%)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Segment Density" column is "5.3%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "7(238)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "5.2%(7.9%)"
	And the figure under question 1 for "2. Disagree" in the "Segment Density" column is "2.9%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "13(466)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "9.6%(15.4%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Segment Density" column is "2.8%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "84(1769)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "62.2%(58.4%)"
	And the figure under question 1 for "4. Agree" in the "Segment Density" column is "4.7%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "29(518)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "21.5%(17.1%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Segment Density" column is "5.6%"
	And the "Mean" under question 1 is "3.97 (±0.81)"
	And the figure under question 1 for "Mean" in the "Number" column is "135(3029)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	When I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "2(0)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.5%(0.0%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "7(4)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "5.2%(8.5%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "13(6)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "9.6%(12.8%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "84(33)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "62.2%(70.2%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "29(4)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "21.5%(8.5%)"
	And the "Mean" under question 1 is "3.97 (±0.81)"
	And the figure under question 1 for "Mean" in the "Number" column is "135(47)"
	And the figure under question 1 for "Mean" in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"