Feature: Check Demographic Crosstab for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.9"
	And the score for question 1 in the "Male" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.95"
	And the score for question 1 in the "Male" column is "3.99"
	And the Weighted Average on the right for question 1 is "3.97"
	And the Group Average at the bottom for the "Female" column is "3.92"
	And the Group Average at the bottom for the "Male" column is "4.02"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86"
	And the score for question 1 in the "Male" column is "82"
	And the Weighted Average on the right for question 1 is "84"
	And the Group Average at the bottom for the "Female" column is "76"
	And the Group Average at the bottom for the "Male" column is "78"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86.2"
	And the score for question 1 in the "Male" column is "81.8"
	And the Weighted Average on the right for question 1 is "83.7"
	And the Group Average at the bottom for the "Female" column is "76.1"
	And the Group Average at the bottom for the "Male" column is "77.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86.21"
	And the score for question 1 in the "Male" column is "81.82"
	And the Weighted Average on the right for question 1 is "83.70"
	And the Group Average at the bottom for the "Female" column is "76.06"
	And the Group Average at the bottom for the "Male" column is "77.53"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "7"
	And the score for question 1 in the "Male" column is "6"
	And the Weighted Average on the right for question 1 is "7"
	And the Group Average at the bottom for the "Female" column is "9"
	And the Group Average at the bottom for the "Male" column is "10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "6.9"
	And the score for question 1 in the "Male" column is "6.5"
	And the Weighted Average on the right for question 1 is "6.7"
	And the Group Average at the bottom for the "Female" column is "8.6"
	And the Group Average at the bottom for the "Male" column is "9.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "6.90"
	And the score for question 1 in the "Male" column is "6.49"
	And the Weighted Average on the right for question 1 is "6.67"
	And the Group Average at the bottom for the "Female" column is "8.56"
	And the Group Average at the bottom for the "Male" column is "9.90"

	Now I go to "Functions" > "Logout"

# Categories
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4"
	And the Group Average at the bottom for the "Male" column is "4"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4"
	And the score for question 1 in the "Male" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.2"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.2"
	And the Group Average at the bottom for the "Female" column is "3.9"
	And the Group Average at the bottom for the "Male" column is "4.0"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.9"
	And the score for question 1 in the "Male" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.20"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.16"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.17"
	And the Group Average at the bottom for the "Female" column is "3.92"
	And the Group Average at the bottom for the "Male" column is "4.02"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.95"
	And the score for question 1 in the "Male" column is "3.99"
	And the Weighted Average on the right for question 1 is "3.97"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "92"
	And the score for the "Clarity of Direction" category in the "Male" column is "83"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87"
	And the Group Average at the bottom for the "Female" column is "76"
	And the Group Average at the bottom for the "Male" column is "78"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86"
	And the score for question 1 in the "Male" column is "82"
	And the Weighted Average on the right for question 1 is "84"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "91.7"
	And the score for the "Clarity of Direction" category in the "Male" column is "83.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.0"
	And the Group Average at the bottom for the "Female" column is "76.1"
	And the Group Average at the bottom for the "Male" column is "77.5"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86.2"
	And the score for question 1 in the "Male" column is "81.8"
	And the Weighted Average on the right for question 1 is "83.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "91.72"
	And the score for the "Clarity of Direction" category in the "Male" column is "83.38"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86.96"
	And the Group Average at the bottom for the "Female" column is "76.06"
	And the Group Average at the bottom for the "Male" column is "77.53"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86.21"
	And the score for question 1 in the "Male" column is "81.82"
	And the Weighted Average on the right for question 1 is "83.70"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3"
	And the score for the "Clarity of Direction" category in the "Male" column is "5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "9"
	And the Group Average at the bottom for the "Male" column is "10"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "7"
	And the score for question 1 in the "Male" column is "6"
	And the Weighted Average on the right for question 1 is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.1"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Female" column is "8.6"
	And the Group Average at the bottom for the "Male" column is "9.9"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "6.9"
	And the score for question 1 in the "Male" column is "6.5"
	And the Weighted Average on the right for question 1 is "6.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.10"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.94"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.15"
	And the Group Average at the bottom for the "Female" column is "8.56"
	And the Group Average at the bottom for the "Male" column is "9.90"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "6.90"
	And the score for question 1 in the "Male" column is "6.49"
	And the Weighted Average on the right for question 1 is "6.67"

	Now I go to "Functions" > "Logout"

# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "4 (3)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.9 (3.4)"
	And the score for question 1 in the "Male" column is "4.0 (3.9)"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.8)"
	And the Group Average at the bottom for the "Male" column is "4.0 (4.0)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "3.95 (3.44)"
	And the score for question 1 in the "Male" column is "3.99 (3.87)"
	And the Weighted Average on the right for question 1 is "3.97"
	And the Group Average at the bottom for the "Female" column is "3.92 (3.81)"
	And the Group Average at the bottom for the "Male" column is "4.02 (3.99)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86 (56)"
	And the score for question 1 in the "Male" column is "82 (84)"
	And the Weighted Average on the right for question 1 is "84"
	And the Group Average at the bottom for the "Female" column is "76 (77)"
	And the Group Average at the bottom for the "Male" column is "78 (77)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86.2 (55.6)"
	And the score for question 1 in the "Male" column is "81.8 (84.2)"
	And the Weighted Average on the right for question 1 is "83.7"
	And the Group Average at the bottom for the "Female" column is "76.1 (76.5)"
	And the Group Average at the bottom for the "Male" column is "77.5 (77.5)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "86.21 (55.56)"
	And the score for question 1 in the "Male" column is "81.82 (84.21)"
	And the Weighted Average on the right for question 1 is "83.70"
	And the Group Average at the bottom for the "Female" column is "76.06 (76.54)"
	And the Group Average at the bottom for the "Male" column is "77.53 (77.45)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "7 (11)"
	And the score for question 1 in the "Male" column is "6 (8)"
	And the Weighted Average on the right for question 1 is "7"
	And the Group Average at the bottom for the "Female" column is "9 (9)"
	And the Group Average at the bottom for the "Male" column is "10 (9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "6.9 (11.1)"
	And the score for question 1 in the "Male" column is "6.5 (7.9)"
	And the Weighted Average on the right for question 1 is "6.7"
	And the Group Average at the bottom for the "Female" column is "8.6 (9.4)"
	And the Group Average at the bottom for the "Male" column is "9.9 (9.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Female" column is "6.90 (11.11)"
	And the score for question 1 in the "Male" column is "6.49 (7.89)"
	And the Weighted Average on the right for question 1 is "6.67"
	And the Group Average at the bottom for the "Female" column is "8.56 (9.38)"
	And the Group Average at the bottom for the "Male" column is "9.90 (9.34)"

	Now I go to "Functions" > "Logout"

# Categories + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "4 (4)"
	And the Group Average at the bottom for the "Male" column is "4 (4)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "4 (3)"
	And the score for question 1 in the "Male" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.2 (3.8)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.2 (4.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.2"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.8)"
	And the Group Average at the bottom for the "Male" column is "4.0 (4.0)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.9 (3.4)"
	And the score for question 1 in the "Male" column is "4.0 (3.9)"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "4.20 (3.80)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.16 (4.15)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.17"
	And the Group Average at the bottom for the "Female" column is "3.92 (3.81)"
	And the Group Average at the bottom for the "Male" column is "4.02 (3.99)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "3.95 (3.44)"
	And the score for question 1 in the "Male" column is "3.99 (3.87)"
	And the Weighted Average on the right for question 1 is "3.97"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "92 (76)"
	And the score for the "Clarity of Direction" category in the "Male" column is "83 (87)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87"
	And the Group Average at the bottom for the "Female" column is "76 (77)"
	And the Group Average at the bottom for the "Male" column is "78 (77)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86 (56)"
	And the score for question 1 in the "Male" column is "82 (84)"
	And the Weighted Average on the right for question 1 is "84"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "91.7 (75.6)"
	And the score for the "Clarity of Direction" category in the "Male" column is "83.4 (86.8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.0"
	And the Group Average at the bottom for the "Female" column is "76.1 (76.5)"
	And the Group Average at the bottom for the "Male" column is "77.5 (77.5)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86.2 (55.6)"
	And the score for question 1 in the "Male" column is "81.8 (84.2)"
	And the Weighted Average on the right for question 1 is "83.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "91.72 (75.56)"
	And the score for the "Clarity of Direction" category in the "Male" column is "83.38 (86.84)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86.96"
	And the Group Average at the bottom for the "Female" column is "76.06 (76.54)"
	And the Group Average at the bottom for the "Male" column is "77.53 (77.45)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "86.21 (55.56)"
	And the score for question 1 in the "Male" column is "81.82 (84.21)"
	And the Weighted Average on the right for question 1 is "83.70"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3 (7)"
	And the score for the "Clarity of Direction" category in the "Male" column is "5 (5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Female" column is "9 (9)"
	And the Group Average at the bottom for the "Male" column is "10 (9)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "7 (11)"
	And the score for question 1 in the "Male" column is "6 (8)"
	And the Weighted Average on the right for question 1 is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.1 (6.7)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.9 (4.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Female" column is "8.6 (9.4)"
	And the Group Average at the bottom for the "Male" column is "9.9 (9.3)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "6.9 (11.1)"
	And the score for question 1 in the "Male" column is "6.5 (7.9)"
	And the Weighted Average on the right for question 1 is "6.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Female" column is "3.10 (6.67)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4.94 (4.74)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.15"
	And the Group Average at the bottom for the "Female" column is "8.56 (9.38)"
	And the Group Average at the bottom for the "Male" column is "9.90 (9.34)"

	When I click on the Clarity of Direction category
	Then the score for question 1 in the "Female" column is "6.90 (11.11)"
	And the score for question 1 in the "Male" column is "6.49 (7.89)"
	And the Weighted Average on the right for question 1 is "6.67"

	Now I go to "Functions" > "Logout"