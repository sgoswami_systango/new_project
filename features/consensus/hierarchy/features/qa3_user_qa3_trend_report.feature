Feature: Check Trend Report for user with a hierachy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the score for question 1 in the "2014" column is "3.97 (+0.18)"
	And the score for question 1 in the "2013" column is "3.79"
	And the Overall Average at the bottom for "2014" is "3.97 (+0.02)"
	And the Overall Average for "2013" is "3.95"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the score for the "Clarity of Direction" category for "2014" is "4.17 (+0.09)"
	And the score for the "Clarity of Direction" category for "2013" is "4.08"
	And the Overall Average at the bottom for "2014" is "3.97 (+0.02)"
	And the Overall Average for "2013" is "3.95"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "3.97 (+0.18)"
	And the score for question 1 in the "2013" column is "3.79"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the score for question 1 in the "2014" column is "83.7 (+5.0)"
	And the score for question 1 in the "2013" column is "78.7"
	And the Overall Average at the bottom for "2014" is "76.9 (-0.4)"
	And the Overall Average for "2013" is "77.3"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the score for the "Clarity of Direction" category for "2014" is "87.0 (+2.3)"
	And the score for the "Clarity of Direction" category for "2013" is "84.7"
	And the Overall Average at the bottom for "2014" is "76.9 (-0.4)"
	And the Overall Average for "2013" is "77.3"
	
	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "2014" column is "83.7 (+5.0)"
	And the score for question 1 in the "2013" column is "78.7"

	Now I go to "Functions" > "Logout"