Feature: Check Favorability Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa3/" as user "qa3"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page
@wip	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for question 1 is "83.7%"
	And the Neutral score for question 1 is "9.6%"
	And the Unfavorable score for question 1 is "6.7%"
	And the Overall Average at the bottom has a Favorable score of "76.9%"
	And the Overall Average Neutral score is "13.8%"
	And the Overall Average Unfavorable score is "9.3%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "87.0%"
	And the Neutral score for the "Clarity of Direction" category is "8.9%"
	And the Unfavorable score for the "Clarity of Direction" category is "4.1%"
	And the Overall Average at the bottom has a Favorable score of "76.9%"
	And the Overall Average Neutral score is "13.8%"
	And the Overall Average Unfavorable score is "9.3%"

	When I click on the Clarity of Direction category
	Then the Favorable score for question 1 is "83.7%"
	And the Neutral score for question 1 is "9.6%"
	And the Unfavorable score for question 1 is "6.7%"
	
# Compared to Org

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for question 1 is "83.7%"
	And the Neutral score on top for question 1 is "9.6%"
	And the Unfavorable score on top for question 1 is "6.7%"
	And the Favorable score on bottom for question 1 is "75.5%"
	And the Neutral score on bottom for question 1 is "15.4%"
	And the Unfavorable score on bottom for question 1 is "9.1%"
	And the Overall Average at the bottom Favorable score on top is "76.9%"
	And the Overall Average Neutral score on top is "13.8%"
	And the Overall Average Unfavorable score on top is "9.3%"
	And the Overall Average Favorable score on bottom is "72.3%"
	And the Overall Average Neutral score on bottom is "17.1%"
	And the Overall Average Unfavorable score on bottom is "10.6%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "87.0%"
	And the Neutral score on top for the "Clarity of Direction" category is "8.9%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "4.1%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "80.2%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "13.6%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "6.2%"
	And the Overall Average at the bottom Favorable score on top is "76.9%"
	And the Overall Average Neutral score on top is "13.8%"
	And the Overall Average Unfavorable score on top is "9.3%"
	And the Overall Average Favorable score on bottom is "72.3%"
	And the Overall Average Neutral score on bottom is "17.1%"
	And the Overall Average Unfavorable score on bottom is "10.6%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "83.7%"
	And the Neutral score on top for question 1 is "9.6%"
	And the Unfavorable score on top for question 1 is "6.7%"
	And the Favorable score on bottom for question 1 is "75.5%"
	And the Neutral score on bottom for question 1 is "15.4%"
	And the Unfavorable score on bottom for question 1 is "9.1%"

	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for question 1 is "83.7%"
	And the Neutral score on top for question 1 is "9.6%"
	And the Unfavorable score on top for question 1 is "6.7%"
	And the Favorable score on bottom for question 1 is "78.7%"
	And the Neutral score on bottom for question 1 is "12.8%"
	And the Unfavorable score on bottom for question 1 is "8.5%"
	And the Overall Average at the bottom Favorable score on top is "76.9%"
	And the Overall Average Neutral score on top is "13.8%"
	And the Overall Average Unfavorable score on top is "9.3%"
	And the Overall Average Favorable score on bottom is "77.3%"
	And the Overall Average Neutral score on bottom is "13.4%"
	And the Overall Average Unfavorable score on bottom is "9.3%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "2013 Trend" from the "Compared to:" popdown in the top right
	Then the Favorable score on top for the "Clarity of Direction" category is "87.0%"
	And the Neutral score on top for the "Clarity of Direction" category is "8.9%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "4.1%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "84.7%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "10.2%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "5.1%"
	And the Overall Average at the bottom Favorable score on top is "76.9%"
	And the Overall Average Neutral score on top is "13.8%"
	And the Overall Average Unfavorable score on top is "9.3%"
	And the Overall Average Favorable score on bottom is "77.3%"
	And the Overall Average Neutral score on bottom is "13.4%"
	And the Overall Average Unfavorable score on bottom is "9.3%"

	When I click on the Clarity of Direction category
	Then the Favorable score on top for question 1 is "83.7%"
	And the Neutral score on top for question 1 is "9.6%"
	And the Unfavorable score on top for question 1 is "6.7%"
	And the Favorable score on bottom for question 1 is "78.7%"
	And the Neutral score on bottom for question 1 is "12.8%"
	And the Unfavorable score on bottom for question 1 is "8.5%"

	Now I go to "Functions" > "Logout"
