Feature: Check Trend report

Background:
       Given I am logged into perceptyx 
       And I click on "Trend Report" report link
       Then I am on the "Trend Report" report page


Scenario: I can check Trend report
          Given there is both "Questions" and "Categories" tab in top left corner
          And there is both "Evaluation" and "Favorability" tab in top right corner

          When I am on the "Questions" tab
          Then the 2012 score for question 5 for "Evaluation" is "3.95 (+9.42%)"
          And the 2012 score for question 10 for "Evaluation" is "3.63 (+9.01%)"
          And the 2012 score for question 12 for "Evaluation" is "3.51 (+11.43%)"
          And the 2012 score for question 40 for "Evaluation" is "3.59 (+5.59%)"
          And the 2012 score for question 58 for "Evaluation" is "4.31 (+1.41%)"
          And the 2011 score for question 5 for "Evaluation" is "3.61 (-1.10%)"
          And the 2011 score for question 10 for "Evaluation" is "3.33 (+0.60%)"
          And the 2011 score for question 12 for "Evaluation" is "3.15"
          And the 2011 score for question 40 for "Evaluation" is "3.40 (+2.41%)"
          And the 2011 score for question 58 for "Evaluation" is "4.25 (-2.30%)"
          And the 2009 score for question 5 for "Evaluation" is "3.65"
          And the 2009 score for question 10 for "Evaluation" is "3.31"
          And the 2009 score for question 40 for "Evaluation" is "3.32"
          And the 2009 score for question 58 for "Evaluation" is "4.35"
          And the Overall Average at the bottom has a 2012 score of "3.77 (+3.57%)"
          And the Overall Average at the bottom has a 2011 score of "3.64 (0.00%)"
          And the Overall Average at the bottom has a 2009 score of "3.64"
          When I go into the "Categories" tab
          Then the 2012 score for Clarity of Direction for "Evaluation" is "3.80 (+4.11%)"
          And the 2011 Score for Clarity of Direction for "Evaluation" is "3.65 (+6.41%)"
          And the 2009 Score for Clarity of Direction for "Evaluation" is "3.43"
          And the 2012 score for Learning and Development for "Evaluation" is "3.40 (+3.66%)"
          And the 2011 Score for Learning and Development for "Evaluation" is "3.28 (+0.31%)"
          And the 2009 Score for Learning and Development for "Evaluation" is "3.27"
          When I go into the "Favorability" tab
          And I go into the "Questions" tab
          Then the 2012 score for question 5 for "Favorability" is "77.7 (+25.7%)"
          And the 2012 score for question 10 for "Favorability" is "59.4 (+26.4%)"
          And the 2012 score for question 12 for "Favorability" is "55.8 (+29.2%)"
          And the 2012 score for question 40 for "Favorability" is "63.2 (+16.0%)"
          And the 2012 score for question 58 for "Favorability" is "86.7 (+2.2%)"
          And the 2011 score for question 5 for "Favorability" is "61.8 (0.0%)"
          And the 2011 score for question 10 for "Favorability" is "47.0 (-0.6%)"
          And the 2011 score for question 12 for "Favorability" is "43.2"
          And the 2011 score for question 40 for "Favorability" is "54.5 (+6.7%)"
          And the 2011 score for question 58 for "Favorability" is "84.8 (-3.9%)"
          And the 2009 score for question 5 for "Favorability" is "61.8"
          And the 2009 score for question 10 for "Favorability" is "47.3"
          And the 2009 score for question 40 for "Favorability" is "51.1"
          And the 2009 score for question 58 for "Favorability" is "88.2"
          When I go into the "Categories" tab
          Then the 2012 score for Clarity of Direction for "Favorability" is "71.0 (+9.2%)"
          And the 2011 Score for Clarity of Direction for "Favorability" is "65.0 (+17.8%)"
          And the 2009 Score for Clarity of Direction for "Favorability" is "55.2"
          And the 2012 score for Learning and Development for "Favorability" is "51.7 (+8.6%)"
          And the 2011 Score for Learning and Development for "Favorability" is "47.6 (-0.8%)"
          And the 2009 Score for Learning and Development for "Favorability" is "48"
          Now I go to "Logout"
