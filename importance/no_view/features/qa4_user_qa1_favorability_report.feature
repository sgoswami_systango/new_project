Feature: Check Favorability Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa1"
	And I click the "Favorability Report" link
	Then I am on the "Favorability Report" page
	
Scenario: I can check Favorability Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to 2013 Trend" tab in top right corner

	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the number for Organization at the top right is "(1136)"
	And the Favorable score for question 1 is "85.4%"
	And the Neutral score for question 1 is "10.1%"
	And the Unfavorable score for question 1 is "4.6%"
	And the Overall Average at the bottom has a Favorable score of "76.4%"
	And the Overall Average Neutral score is "15.4%"
	And the Overall Average Unfavorable score is "8.2%"

	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Favorable score for the "Clarity of Direction" category is "78.1%"
	And the Neutral score for the "Clarity of Direction" category is "13.8%"
	And the Unfavorable score for the "Clarity of Direction" category is "8.1%"
	And the Overall Average at the bottom has a Favorable score of "76.4%"
	And the Overall Average Neutral score is "15.4%"
	And the Overall Average Unfavorable score is "8.2%"

	When I click on the "Clarity of Direction" category
	Then The Favorable score for question 1 is "85.4%"
	And the Neutral score for question 1 is "10.1%"
	And the Unfavorable score for question 1 is "4.6%"
	
	When I am on the "Questions" tab
	And I am on the "Compared to 2013 Trend" tab
	Then the number for "Organization" is "(1136)"
	And the number for "2013 Trend" is "(1127)"
	And the Favorable score on top for question 1 is "85.4%"
	And the Neutral score on top for question 1 is "10.1%"
	And the Unfavorable score on top for question 1 is "4.6%"
	And the Favorable score on bottom for question 1 is "86.8%"
	And the Neutral score on bottom for question 1 is "9.8%"
	And the Unfavorable score on bottom for question 1 is "3.3%"
	And the Overall Average at the bottom Favorable score on top is "76.4%"
	And the Overall Average Neutral score on top is "15.4%"
	And the Overall Average Unfavorable score on top is "8.2%"
	And the Overall Average Favorable score on bottom is "77.9%"
	And the Overall Average Neutral score on bottom is "15.0%"
	And the Overall Average Unfavorable score on bottom is "7.0%"
	
	When I am on the "Categories" tab
	And I am on the "Compared to 2013 Trend" tab
	Then the Favorable score on top for the "Clarity of Direction" category is "78.1%"
	And the Neutral score on top for the "Clarity of Direction" category is "13.8%"
	And the Unfavorable score on top for the "Clarity of Direction" category is "8.1%"
	Then the Favorable score on bottom for the "Clarity of Direction" category is "82.1%"
	And the Neutral score on bottom for the "Clarity of Direction" category is "12.5%"
	And the Unfavorable score on bottom for the "Clarity of Direction" category is "5.4%"
	And the Overall Average at the bottom Favorable score on top is "76.4%"
	And the Overall Average Neutral score on top is "15.4%"
	And the Overall Average Unfavorable score on top is "8.2%"
	And the Overall Average Favorable score on bottom is "77.9%"
	And the Overall Average Neutral score on bottom is "15.0%"
	And the Overall Average Unfavorable score on bottom is "7.0%"

	When I click on the "Clarity of Direction" category
	Then the Favorable score on top for question 1 is "85.4%"
	And the Neutral score on top for question 1 is "10.1%"
	And the Unfavorable score on top for question 1 is "4.6%"
	And the Favorable score on bottom for question 1 is "86.8%"
	And the Neutral score on bottom for question 1 is "9.8%"
	And the Unfavorable score on bottom for question 1 is "3.3%"
	
	Now I go to "Functions" > "Logout"
