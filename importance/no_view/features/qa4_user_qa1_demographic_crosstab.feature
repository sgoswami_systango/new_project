Feature: Check Demographic Crosstab for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa1"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(617)"
	And the first number under "Female" is "(519)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "3.9"
	And the Group Average at the bottom for the "Female" column is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.09"
	And the score for question 1 in the "Female" column is "4.15"
	And the Weighted Average on the right for question 1 is "4.12"
	And the Group Average at the bottom for the "Male" column is "3.92"
	And the Group Average at the bottom for the "Female" column is "4.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "84"
	And the score for question 1 in the "Female" column is "87"
	And the Weighted Average on the right for question 1 is "85"
	And the Group Average at the bottom for the "Male" column is "75"
	And the Group Average at the bottom for the "Female" column is "79"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.9"
	And the score for question 1 in the "Female" column is "87.1"
	And the Weighted Average on the right for question 1 is "85.4"
	And the Group Average at the bottom for the "Male" column is "74.6"
	And the Group Average at the bottom for the "Female" column is "78.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.93"
	And the score for question 1 in the "Female" column is "87.10"
	And the Weighted Average on the right for question 1 is "85.37"
	And the Group Average at the bottom for the "Male" column is "74.59"
	And the Group Average at the bottom for the "Female" column is "78.53"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "5"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.6"
	And the Group Average at the bottom for the "Male" column is "9.0"
	And the Group Average at the bottom for the "Female" column is "7.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.08"
	And the score for question 1 in the "Female" column is "3.97"
	And the Weighted Average on the right for question 1 is "4.58"
	And the Group Average at the bottom for the "Male" column is "8.99"
	And the Group Average at the bottom for the "Female" column is "7.34"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.09"
	And the score for question 1 in the "Female" column is "4.11"
	And the Weighted Average on the right for question 1 is "4.10"
	And the Group Average at the bottom for the "Male" column is "4.32"
	And the Group Average at the bottom for the "Female" column is "4.34"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81"
	And the score for question 1 in the "Female" column is "84"
	And the Weighted Average on the right for question 1 is "82"
	And the Group Average at the bottom for the "Male" column is "90"
	And the Group Average at the bottom for the "Female" column is "91"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81.3"
	And the score for question 1 in the "Female" column is "83.5"
	And the Weighted Average on the right for question 1 is "82.3"
	And the Group Average at the bottom for the "Male" column is "89.6"
	And the Group Average at the bottom for the "Female" column is "91.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81.31"
	And the score for question 1 in the "Female" column is "83.53"
	And the Weighted Average on the right for question 1 is "82.32"
	And the Group Average at the bottom for the "Male" column is "89.63"
	And the Group Average at the bottom for the "Female" column is "91.02"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "-1"
	And the Weighted Average on the right for question 1 is "0"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "-0.8"
	And the Weighted Average on the right for question 1 is "-0.4"
	And the Group Average at the bottom for the "Male" column is "8.6"
	And the Group Average at the bottom for the "Female" column is "7.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "-0.82"
	And the Weighted Average on the right for question 1 is "-0.41"
	And the Group Average at the bottom for the "Male" column is "8.64"
	And the Group Average at the bottom for the "Female" column is "7.38"


# Categories
@wip
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(617)"
	And the number directly below "Female" is "(519)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "3.9"
	And the Group Average at the bottom for the "Female" column is "4.0"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.91"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.02"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.96"
	And the Group Average at the bottom for the "Male" column is "3.92"
	And the Group Average at the bottom for the "Female" column is "4.00"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.09"
	And the score for question 1 in the "Female" column is "4.15"
	And the Weighted Average on the right for question 1 is "4.12"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75"
	And the score for the "Clarity of Direction" category in the "Female" column is "81"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78"
	And the Group Average at the bottom for the "Male" column is "75"
	And the Group Average at the bottom for the "Female" column is "79"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "84"
	And the score for question 1 in the "Female" column is "87"
	And the Weighted Average on the right for question 1 is "85"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75.4"
	And the score for the "Clarity of Direction" category in the "Female" column is "81.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.1"
	And the Group Average at the bottom for the "Male" column is "74.6"
	And the Group Average at the bottom for the "Female" column is "78.5"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.9"
	And the score for question 1 in the "Female" column is "87.1"
	And the Weighted Average on the right for question 1 is "85.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75.37"
	And the score for the "Clarity of Direction" category in the "Female" column is "81.45"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.12"
	And the Group Average at the bottom for the "Male" column is "74.59"
	And the Group Average at the bottom for the "Female" column is "78.53"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.93"
	And the score for question 1 in the "Female" column is "87.10"
	And the Weighted Average on the right for question 1 is "85.37"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9"
	And the score for the "Clarity of Direction" category in the "Female" column is "7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.1"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.8"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.1"
	And the Group Average at the bottom for the "Male" column is "9.0"
	And the Group Average at the bottom for the "Female" column is "7.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.10"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.80"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.06"
	And the Group Average at the bottom for the "Male" column is "8.99"
	And the Group Average at the bottom for the "Female" column is "7.34"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.08"
	And the score for question 1 in the "Female" column is "3.97"
	And the Weighted Average on the right for question 1 is "4.58"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.3"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.27"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.25"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.26"
	And the Group Average at the bottom for the "Male" column is "4.32"
	And the Group Average at the bottom for the "Female" column is "4.34"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.09"
	And the score for question 1 in the "Female" column is "4.11"
	And the Weighted Average on the right for question 1 is "4.10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88"
	And the score for the "Clarity of Direction" category in the "Female" column is "89"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "90"
	And the Group Average at the bottom for the "Female" column is "91"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81"
	And the score for question 1 in the "Female" column is "84"
	And the Weighted Average on the right for question 1 is "82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87.6"
	And the score for the "Clarity of Direction" category in the "Female" column is "89.0"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88.2"
	And the Group Average at the bottom for the "Male" column is "89.6"
	And the Group Average at the bottom for the "Female" column is "91.0"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81.3"
	And the score for question 1 in the "Female" column is "83.5"
	And the Weighted Average on the right for question 1 is "82.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87.62"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.99"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88.24"
	And the Group Average at the bottom for the "Male" column is "89.63"
	And the Group Average at the bottom for the "Female" column is "91.02"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81.31"
	And the score for question 1 in the "Female" column is "83.53"
	And the Weighted Average on the right for question 1 is "82.32"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8"
	And the score for the "Clarity of Direction" category in the "Female" column is "5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "-1"
	And the Weighted Average on the right for question 1 is "0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7.7"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.4"
	And the Group Average at the bottom for the "Male" column is "8.6"
	And the Group Average at the bottom for the "Female" column is "7.4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "-0.8"
	And the Weighted Average on the right for question 1 is "-0.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7.69"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.89"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.39"
	And the Group Average at the bottom for the "Male" column is "8.64"
	And the Group Average at the bottom for the "Female" column is "7.38"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "-0.82"
	And the Weighted Average on the right for question 1 is "-0.41"



# Questions + trend
@wip
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.1 (4.2)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "3.9 (4.0)"
	And the Group Average at the bottom for the "Female" column is "4.0 (4.0)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.09 (4.14)"
	And the score for question 1 in the "Female" column is "4.15 (4.19)"
	And the Weighted Average on the right for question 1 is "4.12"
	And the Group Average at the bottom for the "Male" column is "3.92 (3.98)"
	And the Group Average at the bottom for the "Female" column is "4.00 (4.02)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "84 (85)"
	And the score for question 1 in the "Female" column is "87 (89)"
	And the Weighted Average on the right for question 1 is "85"
	And the Group Average at the bottom for the "Male" column is "75 (77)"
	And the Group Average at the bottom for the "Female" column is "79 (80)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.9 (85.2)"
	And the score for question 1 in the "Female" column is "87.1 (89.2)"
	And the Weighted Average on the right for question 1 is "85.4"
	And the Group Average at the bottom for the "Male" column is "74.6 (76.8)"
	And the Group Average at the bottom for the "Female" column is "78.5 (79.6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.93 (85.21)"
	And the score for question 1 in the "Female" column is "87.10 (89.21)"
	And the Weighted Average on the right for question 1 is "85.37"
	And the Group Average at the bottom for the "Male" column is "74.59 (76.80)"
	And the Group Average at the bottom for the "Female" column is "78.53 (79.58)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5 (4)"
	And the score for question 1 in the "Female" column is "4 (3)"
	And the Weighted Average on the right for question 1 is "5"
	And the Group Average at the bottom for the "Male" column is "9 (8)"
	And the Group Average at the bottom for the "Female" column is "7 (6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.1 (3.8)"
	And the score for question 1 in the "Female" column is "4.0 (2.6)"
	And the Weighted Average on the right for question 1 is "4.6"
	And the Group Average at the bottom for the "Male" column is "9.0 (7.6)"
	And the Group Average at the bottom for the "Female" column is "7.3 (6.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.08 (3.81)"
	And the score for question 1 in the "Female" column is "3.97 (2.64)"
	And the Weighted Average on the right for question 1 is "4.58"
	And the Group Average at the bottom for the "Male" column is "8.99 (7.60)"
	And the Group Average at the bottom for the "Female" column is "7.34 (6.22)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.09 (4.05)"
	And the score for question 1 in the "Female" column is "4.11 (4.12)"
	And the Weighted Average on the right for question 1 is "4.10"
	And the Group Average at the bottom for the "Male" column is "4.32 (4.30)"
	And the Group Average at the bottom for the "Female" column is "4.34 (4.32)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81 (80)"
	And the score for question 1 in the "Female" column is "84 (83)"
	And the Weighted Average on the right for question 1 is "82"
	And the Group Average at the bottom for the "Male" column is "90 (90)"
	And the Group Average at the bottom for the "Female" column is "91 (91)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81.3 (79.9)"
	And the score for question 1 in the "Female" column is "83.5 (83.0)"
	And the Weighted Average on the right for question 1 is "82.3"
	And the Group Average at the bottom for the "Male" column is "89.6 (89.7)"
	And the Group Average at the bottom for the "Female" column is "91.0 (91.0)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "81.31 (79.88)"
	And the score for question 1 in the "Female" column is "83.53 (83.04)"
	And the Weighted Average on the right for question 1 is "82.32"
	And the Group Average at the bottom for the "Male" column is "89.63 (89.73)"
	And the Group Average at the bottom for the "Female" column is "91.02 (90.99)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0 (-2)"
	And the score for question 1 in the "Female" column is "-1 (-1)"
	And the Weighted Average on the right for question 1 is "0"
	And the Group Average at the bottom for the "Male" column is "9 (7)"
	And the Group Average at the bottom for the "Female" column is "7 (6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0 (-1.8)"
	And the score for question 1 in the "Female" column is "-0.8 (-1.4)"
	And the Weighted Average on the right for question 1 is "-0.4"
	And the Group Average at the bottom for the "Male" column is "8.6 (6.9)"
	And the Group Average at the bottom for the "Female" column is "7.4 (6.5)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00 (-1.82)"
	And the score for question 1 in the "Female" column is "-0.82 (-1.44)"
	And the Weighted Average on the right for question 1 is "-0.41"
	And the Group Average at the bottom for the "Male" column is "8.64 (6.88)"
	And the Group Average at the bottom for the "Female" column is "7.38 (6.48)"


# Categories + trend
@wip
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9 (4.0)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.0 (4.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "3.9 (4.0)"
	And the Group Average at the bottom for the "Female" column is "4.0 (4.0)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.1 (4.2)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.91 (4.03)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.02 (4.07)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.96"
	And the Group Average at the bottom for the "Male" column is "3.92 (3.98)"
	And the Group Average at the bottom for the "Female" column is "4.00 (4.02)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.09 (4.14)"
	And the score for question 1 in the "Female" column is "4.15 (4.19)"
	And the Weighted Average on the right for question 1 is "4.12"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75 (81)"
	And the score for the "Clarity of Direction" category in the "Female" column is "81 (84)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78"
	And the Group Average at the bottom for the "Male" column is "75 (77)"
	And the Group Average at the bottom for the "Female" column is "79 (80)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "84 (85)"
	And the score for question 1 in the "Female" column is "87 (89)"
	And the Weighted Average on the right for question 1 is "85"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75.4 (80.7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "81.4 (84.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.1"
	And the Group Average at the bottom for the "Male" column is "74.6 (76.8)"
	And the Group Average at the bottom for the "Female" column is "78.5 (79.6)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.9 (85.2)"
	And the score for question 1 in the "Female" column is "87.1 (89.2)"
	And the Weighted Average on the right for question 1 is "85.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75.37 (80.68)"
	And the score for the "Clarity of Direction" category in the "Female" column is "81.45 (84.14)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "78.12"
	And the Group Average at the bottom for the "Male" column is "74.59 (76.80)"
	And the Group Average at the bottom for the "Female" column is "78.53 (79.58)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.93 (85.21)"
	And the score for question 1 in the "Female" column is "87.10 (89.21)"
	And the Weighted Average on the right for question 1 is "85.37"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9 (6)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8"
	And the Group Average at the bottom for the "Male" column is "9 (8)"
	And the Group Average at the bottom for the "Female" column is "7 (6)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5 (4)"
	And the score for question 1 in the "Female" column is "4 (3)"
	And the Weighted Average on the right for question 1 is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.1 (6.1)"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.8 (4.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.1"
	And the Group Average at the bottom for the "Male" column is "9.0 (7.6)"
	And the Group Average at the bottom for the "Female" column is "7.3 (6.2)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.1 (3.8)"
	And the score for question 1 in the "Female" column is "4.0 (2.6)"
	And the Weighted Average on the right for question 1 is "4.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.10 (6.06)"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.80 (4.46)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.06"
	And the Group Average at the bottom for the "Male" column is "8.99 (7.60)"
	And the Group Average at the bottom for the "Female" column is "7.34 (6.22)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.08 (3.81)"
	And the score for question 1 in the "Female" column is "3.97 (2.64)"
	And the Weighted Average on the right for question 1 is "4.58"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.3 (4.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.2 (4.2)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.3"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.27 (4.23)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.25 (4.24)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.26"
	And the Group Average at the bottom for the "Male" column is "4.32 (4.30)"
	And the Group Average at the bottom for the "Female" column is "4.34 (4.32)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.09 (4.05)"
	And the score for question 1 in the "Female" column is "4.11 (4.12)"
	And the Weighted Average on the right for question 1 is "4.10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88 (86)"
	And the score for the "Clarity of Direction" category in the "Female" column is "89 (89)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "90 (90)"
	And the Group Average at the bottom for the "Female" column is "91 (91)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81 (80)"
	And the score for question 1 in the "Female" column is "84 (83)"
	And the Weighted Average on the right for question 1 is "82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87.6 (86.4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "89.0 (88.8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88.2"
	And the Group Average at the bottom for the "Male" column is "89.6 (89.7)"
	And the Group Average at the bottom for the "Female" column is "91.0 (91.0)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81.3 (79.9)"
	And the score for question 1 in the "Female" column is "83.5 (83.0)"
	And the Weighted Average on the right for question 1 is "82.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87.62 (86.43)"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.99 (88.77)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88.24"
	And the Group Average at the bottom for the "Male" column is "89.63 (89.73)"
	And the Group Average at the bottom for the "Female" column is "91.02 (90.99)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "81.31 (79.88)"
	And the score for question 1 in the "Female" column is "83.53 (83.04)"
	And the Weighted Average on the right for question 1 is "82.32"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "5 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6"
	And the Group Average at the bottom for the "Male" column is "9 (7)"
	And the Group Average at the bottom for the "Female" column is "7 (6)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0 (-2)"
	And the score for question 1 in the "Female" column is "-1 (-1)"
	And the Weighted Average on the right for question 1 is "0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7.7 (4.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.9 (3.6)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.4"
	And the Group Average at the bottom for the "Male" column is "8.6 (6.9)"
	And the Group Average at the bottom for the "Female" column is "7.4 (6.5)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0 (-1.8)"
	And the score for question 1 in the "Female" column is "-0.8 (-1.4)"
	And the Weighted Average on the right for question 1 is "-0.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7.69 (4.23)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.89 (3.60)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.39"
	And the Group Average at the bottom for the "Male" column is "8.64 (6.88)"
	And the Group Average at the bottom for the "Female" column is "7.38 (6.48)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00 (-1.82)"
	And the score for question 1 in the "Female" column is "-0.82 (-1.44)"
	And the Weighted Average on the right for question 1 is "-0.41"



	Now I go to "Functions" > "Logout"