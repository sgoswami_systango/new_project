Feature: Check Trend Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa1"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(1136)"
	And the "Count" for the "2013" column is "(1127)"
	And the "Count" for the "2012" column is "(1136)"
	And the "Count" for the "2011" column is "(1158)"	
	And the score for "question 1" in the "2014" column is "4.12 (-0.04)"
	And the score for "question 1" in the "2013" column is "4.16 (+0.03)"
	And the score for "question 1" in the "2012" column is "4.13"
	And there is no score for "question 1" in the "2011" column
	And the Overall Average at the bottom for "2014" is "3.98 (-0.02)"
	And the Overall Average for "2013" is "4.00 (+0.05)"
	And the Overall Average for "2012" is "3.95 (+0.05)"
	And the Overall Average for "2011" is "3.90"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(1136)"
	And the "Count" for the "2013" column is "(1127)"
	And the "Count" for the "2012" column is "(1136)"
	And the "Count" for the "2011" column is "(1158)"	
	And the score for the "Clarity of Direction" category for "2014" is "3.96 (-0.09)"
	And the score for the "Clarity of Direction" category for "2013" is "4.05 (+0.10)"
	And the score for the "Clarity of Direction" category for "2012" is "3.95 (+0.14)"
	And the score for the "Clarity of Direction" category for "2011" is "3.81"
	And the Overall Average at the bottom for "2014" is "3.98 (-0.02)"
	And the Overall Average for "2013" is "4.00 (+0.05)"
	And the Overall Average for "2012" is "3.95 (+0.05)"
	And the Overall Average for "2011" is "3.90"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.12 (-0.04)"
	And the score for "question 1" in the "2013" column is "4.16 (+0.03)"
	And the score for "question 1" in the "2012" column is "4.13"
	And there is no score for "question 1" in the "2011" column
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(1136)"
	And the "Count" for the "2013" column is "(1127)"
	And the "Count" for the "2012" column is "(1136)"
	And the "Count" for the "2011" column is "(1158)"	
	And the score for "question 1" in the "2014" column is "85.4 (-1.4)"
	And the score for "question 1" in the "2013" column is "86.8 (0.0)"
	And the score for "question 1" in the "2012" column is "86.8"
	And there is no score for "question 1" in the "2011" column
	And the Overall Average at the bottom for "2014" is "77.6 (-0.3)"
	And the Overall Average for "2013" is "77.9 (+1.5)"
	And the Overall Average for "2012" is "76.4 (+2.3)"
	And the Overall Average for "2011" is "74.1"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(1136)"
	And the "Count" for the "2013" column is "(1127)"
	And the "Count" for the "2012" column is "(1136)"
	And the "Count" for the "2011" column is "(1158)"	
	And the score for the "Clarity of Direction" category for "2014" is "78.1 (-4.0)"
	And the score for the "Clarity of Direction" category for "2013" is "82.1 (+3.1)"
	And the score for the "Clarity of Direction" category for "2012" is "79.0 (+6.6)"
	And the score for the "Clarity of Direction" category for "2011" is "72.4"
	And the Overall Average at the bottom for "2014" is "77.6 (-0.3)"
	And the Overall Average for "2013" is "77.9 (+1.5)"
	And the Overall Average for "2012" is "76.4 (+2.3)"
	And the Overall Average for "2011" is "74.1"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "85.4 (-1.4)"
	And the score for "question 1" in the "2013" column is "86.8 (0.0)"
	And the score for "question 1" in the "2012" column is "86.8"
	And there is no score for question 1 in the "2011" column

	Now I go to "Functions" > "Logout"
