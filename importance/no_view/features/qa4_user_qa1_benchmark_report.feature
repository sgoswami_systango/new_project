Feature: Check Trend Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa1"
	And I click on the "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 2 in the "Mitchell" column is "77.2%"
	And the score for question 2 in the "Perceptyx overall benchmark" column is "73.6%(-3.6)"
	And the score for question 2 in the "Perceptyx software/technology benchmark" column is "75.4%(-1.8)"

	When I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the "Mitchell" column is "78.1%"
	And the score for question 2 in the "Mitchell" column is "77.2%"
	And the score for question 2 in the "Perceptyx overall benchmark" column is "73.6%(-3.6)"
	And the score for question 2 in the "Perceptyx software/technology benchmark" column is "75.4%(-1.8)"

	Now I go to "Functions" > "Logout"