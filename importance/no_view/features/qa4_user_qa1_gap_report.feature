Feature: Check Gap Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa1"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to 2013 Trend" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "4.12"
	And the Importance for question 1 is "4.10"
	And the Gap for question 1 is "-0.02"
	And the Weighted Gap for question 1 is "-0.41"
	And the Overall Average at the bottom has an Eval Avg of "3.95"
	And the Overall Average has a Importance of "4.33"
	And the Overall Average has a Gap of "0.38"
	And the Overall Average has a Weighted Gap of "8.23"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.96"
	And the Importance for the "Clarity of Direction" category is "4.26"
	And the Gap for the "Clarity of Direction" category is "0.30"
	And the Weighted Gap for the "Clarity of Direction" category is "6.39"
	And the Overall Average at the bottom has an Eval Avg of "3.95"
	And the Overall Average has a Importance of "4.33"
	And the Overall Average has a Gap of "0.38"
	And the Overall Average has a Weighted Gap of "8.23"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.12"
	And the Importance for question 1 is "4.10"
	And the Gap for question 1 is "-0.02"
	And the Weighted Gap for question 1 is "-0.41"
	
	When I am on the "Questions" tab
	And I am on the "Compared to 2013 Trend" tab
	Then the number for "Organization" in the upper right is "(1136)"
	And the number for "2013 Trend" in the upper right is "(1127)"
	And the Eval Avg for question 1 is "4.12 (-0.04)"
	And the Importance for question 1 is "4.10 (+0.02)"
	And the Gap for question 1 is "-0.02 (+0.06)"
	And the Weighted Gap for question 1 is "-0.41 (+1.22)"
	And the Overall Average at the bottom has an Eval Avg of "3.95 (-0.05)"
	And the Overall Average has a Importance of "4.33 (+0.02)"
	And the Overall Average has a Gap of "0.38 (+0.07)"
	And the Overall Average has a Weighted Gap of "8.23 (+1.55)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to 2013 Trend" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.96 (-0.09)"
	And the Importance for the "Clarity of Direction" category is "4.26 (+0.02)"
	And the Gap for the "Clarity of Direction" category is "0.30 (+0.11)"
	And the Weighted Gap for the "Clarity of Direction" category is "6.39 (+2.36)"
	And the Overall Average at the bottom has an Eval Avg of "3.95 (-0.05)"
	And the Overall Average has a Importance of "4.33 (+0.02)"
	And the Overall Average has a Gap of "0.38 (+0.07)"
	And the Overall Average has a Weighted Gap of "8.23 (+1.55)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.12 (-0.04)"
	And the Importance for question 1 is "4.10 (+0.02)"
	And the Gap for question 1 is "-0.02 (+0.06)"
	And the Weighted Gap for question 1 is "-0.41 (+1.22)"
	
	Now I go to "Functions" > "Logout"