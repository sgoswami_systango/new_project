Feature: Check Completion Report for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa1"
	And I click on the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Male" in the "Invitees" column is "695"
	And the figure for "Male" in the "Respondents" column is "617(88.8%)"
	And the figure for "Male" in the "Completions" column is "605(87.1%)"
	And the figure for the "Totals" row in the "Invitees" column is "1,261"
	And the figure for the "Totals" row in the "Respondents" column is "1,136(90.1%)"
	And the figure for the "Totals" row in the "Completions" column is "1,102(87.4%)"

	When I am on the "By Time" tab
	Then the figure for "Monday, September 15, 2014" in the "Respondents" column is "196"
	And the figure for "Monday, September 15, 2014" in the "% Responded" column is "17.3%"
	And the figure for "Monday, September 15, 2014" in the "% Invited" column is "15.5%"
	And the figure for the "Totals" row in the "Respondents" column is "1,136"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "90.1%"
	
	When I click on "Monday, September 15, 2014"
	Then the figure for "07:00 PM" in the "Respondents" column is "32"
	And the figure for "07:00 PM" in the "% Responded" column is "2.8%"
	And the figure for "07:00 PM" in the "% Invited" column is "2.5%"
	
	Now I go to "Functions" > "Logout"