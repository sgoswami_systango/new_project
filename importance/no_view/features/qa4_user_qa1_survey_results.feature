Feature: Check Survey Results for user with no view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa1"
	And I click on the "Survey Results" link under "Reference Reports"
	Then I am on the "Survey Results" page
	
Scenario: I can check Survey Results
	Given there is a "No Comparison" tab in top right corner
	And there is a "Compared to 2013" tab in top right corner
	
	When I am on the "No Comparison" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "11"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.0%"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "40"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "3.6%"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "112"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "10.1%"
	And the figure under question 1 for "4. Agree" in the "Number" column is "596"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "53.5%"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "355"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "31.9%"
	And the "Eval Mean" under question 1 is "4.12 (±0.80)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Number" column is "1114"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Percent" column is "100.0%"
	And the figure under question 1 for "1. Not Important" in the "Number" column is "3"
	And the figure under question 1 for "1. Not Important" in the "Percent" column is "0.3%"
	And the figure under question 1 for "2. Little Importance" in the "Number" column is "28"
	And the figure under question 1 for "2. Little Importance" in the "Percent" column is "2.5%"
	And the figure under question 1 for "3. Somewhat Important" in the "Number" column is "166"
	And the figure under question 1 for "3. Somewhat Important" in the "Percent" column is "14.9%"
	And the figure under question 1 for "4. Very Important" in the "Number" column is "575"
	And the figure under question 1 for "4. Very Important" in the "Percent" column is "51.6%"
	And the figure under question 1 for "5. Extremely Important" in the "Number" column is "342"
	And the figure under question 1 for "5. Extremely Important" in the "Percent" column is "30.7%"
	And the "Imp Mean" under question 1 is "4.10 (±0.76)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Number" column is "1114"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Percent" column is "100.0%"


	When I am on the "Compared to 2013 Trend" tab
	Then the figure under question 1 for "1. Strongly Disagree" in the "Number" column is "11(6)"
	And the figure under question 1 for "1. Strongly Disagree" in the "Percent" column is "1.0%(0.5%)"
	And the figure under question 1 for "2. Disagree" in the "Number" column is "40(31)"
	And the figure under question 1 for "2. Disagree" in the "Percent" column is "3.6%(2.8%)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Number" column is "112(109)"
	And the figure under question 1 for "3. Neither under Agree Nor Disagree" in the "Percent" column is "10.1%(9.8%)"
	And the figure under question 1 for "4. Agree" in the "Number" column is "596(599)"
	And the figure under question 1 for "4. Agree" in the "Percent" column is "53.5%(54.0%)"
	And the figure under question 1 for "5. Strongly Agree" in the "Number" column is "355(365)"
	And the figure under question 1 for "5. Strongly Agree" in the "Percent" column is "31.9%(32.9%)"
	And the "Eval Mean" under question 1 is "4.12 (±0.80)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Number" column is "1114(1110)"
	And the figure to the right of "Eval Mean" under the Evaluation section of question 1 in the "Percent" column is "100.0%(100.0%)"
	And the figure under question 1 for "1. Not Important" in the "Number" column is "3(7)"
	And the figure under question 1 for "1. Not Important" in the "Percent" column is "0.3%(0.6%)"
	And the figure under question 1 for "2. Little Importance" in the "Number" column is "28(26)"
	And the figure under question 1 for "2. Little Importance" in the "Percent" column is "2.5%(2.3%)"
	And the figure under question 1 for "3. Somewhat Important" in the "Number" column is "166(176)"
	And the figure under question 1 for "3. Somewhat Important" in the "Percent" column is "14.9%(15.9%)"
	And the figure under question 1 for "4. Very Important" in the "Number" column is "575(561)"
	And the figure under question 1 for "4. Very Important" in the "Percent" column is "51.6%(50.5%)"
	And the figure under question 1 for "5. Extremely Important" in the "Number" column is "342(340)"
	And the figure under question 1 for "5. Extremely Important" in the "Percent" column is "30.7%(30.6%)"
	And the "Imp Mean" under question 1 is "4.10 (±0.76)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Number" column is "1114(1110)"
	And the figure to the right of "Imp Mean" under the Importance section of question 1 in the "Percent" column is "100.0%(100.0%)"

	Now I go to "Functions" > "Logout"