Feature: Check Demographic Crosstab for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(32)"
	And the first number under "Female" is "(86)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.3"
	And the score for question 1 in the "Female" column is "4.2"
	And the Weighted Average on the right for question 1 is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.1"
	And the Group Average at the bottom for the "Female" column is "3.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.34"
	And the score for question 1 in the "Female" column is "4.17"
	And the Weighted Average on the right for question 1 is "4.22"
	And the Group Average at the bottom for the "Male" column is "4.08"
	And the Group Average at the bottom for the "Female" column is "3.95"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "97"
	And the score for question 1 in the "Female" column is "91"
	And the Weighted Average on the right for question 1 is "92"
	And the Group Average at the bottom for the "Male" column is "81"
	And the Group Average at the bottom for the "Female" column is "78"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96.9"
	And the score for question 1 in the "Female" column is "90.7"
	And the Weighted Average on the right for question 1 is "92.4"
	And the Group Average at the bottom for the "Male" column is "81.1"
	And the Group Average at the bottom for the "Female" column is "78.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96.88"
	And the score for question 1 in the "Female" column is "90.70"
	And the Weighted Average on the right for question 1 is "92.37"
	And the Group Average at the bottom for the "Male" column is "81.07"
	And the Group Average at the bottom for the "Female" column is "78.14"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "2"
	And the Weighted Average on the right for question 1 is "2"
	And the Group Average at the bottom for the "Male" column is "6"
	And the Group Average at the bottom for the "Female" column is "9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "2.3"
	And the Weighted Average on the right for question 1 is "1.7"
	And the Group Average at the bottom for the "Male" column is "6.2"
	And the Group Average at the bottom for the "Female" column is "8.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "2.33"
	And the Weighted Average on the right for question 1 is "1.69"
	And the Group Average at the bottom for the "Male" column is "6.16"
	And the Group Average at the bottom for the "Female" column is "8.57"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.0"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.4"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.03"
	And the score for question 1 in the "Female" column is "4.06"
	And the Weighted Average on the right for question 1 is "4.05"
	And the Group Average at the bottom for the "Male" column is "4.39"
	And the Group Average at the bottom for the "Female" column is "4.28"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "78"
	And the score for question 1 in the "Female" column is "81"
	And the Weighted Average on the right for question 1 is "81"
	And the Group Average at the bottom for the "Male" column is "91"
	And the Group Average at the bottom for the "Female" column is "89"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "78.1"
	And the score for question 1 in the "Female" column is "81.4"
	And the Weighted Average on the right for question 1 is "80.5"
	And the Group Average at the bottom for the "Male" column is "91.0"
	And the Group Average at the bottom for the "Female" column is "89.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "78.12"
	And the score for question 1 in the "Female" column is "81.40"
	And the Weighted Average on the right for question 1 is "80.51"
	And the Group Average at the bottom for the "Male" column is "90.98"
	And the Group Average at the bottom for the "Female" column is "89.24"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-6"
	And the score for question 1 in the "Female" column is "-2"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "7"
	And the Group Average at the bottom for the "Female" column is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-6.2"
	And the score for question 1 in the "Female" column is "-2.2"
	And the Weighted Average on the right for question 1 is "-3.4"
	And the Group Average at the bottom for the "Male" column is "6.8"
	And the Group Average at the bottom for the "Female" column is "7.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-6.25"
	And the score for question 1 in the "Female" column is "-2.23"
	And the Weighted Average on the right for question 1 is "-3.44"
	And the Group Average at the bottom for the "Male" column is "6.80"
	And the Group Average at the bottom for the "Female" column is "7.06"

	Now I go to "Functions" > "Logout"

# Categories
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(32)"
	And the number directly below "Female" is "(86)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.1"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.1"
	And the Group Average at the bottom for the "Female" column is "3.9"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.3"
	And the score for question 1 in the "Female" column is "4.2"
	And the Weighted Average on the right for question 1 is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.14"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.09"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.10"
	And the Group Average at the bottom for the "Male" column is "4.08"
	And the Group Average at the bottom for the "Female" column is "3.95"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.34"
	And the score for question 1 in the "Female" column is "4.17"
	And the Weighted Average on the right for question 1 is "4.22"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88"
	And the score for the "Clarity of Direction" category in the "Female" column is "88"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "81"
	And the Group Average at the bottom for the "Female" column is "78"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "97"
	And the score for question 1 in the "Female" column is "91"
	And the Weighted Average on the right for question 1 is "92"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "87.8"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.9"
	And the Group Average at the bottom for the "Male" column is "81.1"
	And the Group Average at the bottom for the "Female" column is "78.1"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96.9"
	And the score for question 1 in the "Female" column is "90.7"
	And the Weighted Average on the right for question 1 is "92.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.28"
	And the score for the "Clarity of Direction" category in the "Female" column is "87.79"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.92"
	And the Group Average at the bottom for the "Male" column is "81.07"
	And the Group Average at the bottom for the "Female" column is "78.14"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96.88"
	And the score for question 1 in the "Female" column is "90.70"
	And the Weighted Average on the right for question 1 is "92.37"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5"
	And the Group Average at the bottom for the "Male" column is "6"
	And the Group Average at the bottom for the "Female" column is "9"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "2"
	And the Weighted Average on the right for question 1 is "2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9"
	And the score for the "Clarity of Direction" category in the "Female" column is "5.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.9"
	And the Group Average at the bottom for the "Male" column is "6.2"
	And the Group Average at the bottom for the "Female" column is "8.6"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "2.3"
	And the Weighted Average on the right for question 1 is "1.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.91"
	And the score for the "Clarity of Direction" category in the "Female" column is "5.23"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.87"
	And the Group Average at the bottom for the "Male" column is "6.16"
	And the Group Average at the bottom for the "Female" column is "8.57"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "2.33"
	And the Weighted Average on the right for question 1 is "1.69"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.1"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.4"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.0"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.14"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.15"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.15"
	And the Group Average at the bottom for the "Male" column is "4.39"
	And the Group Average at the bottom for the "Female" column is "4.28"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.03"
	And the score for question 1 in the "Female" column is "4.06"
	And the Weighted Average on the right for question 1 is "4.05"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "84"
	And the score for the "Clarity of Direction" category in the "Female" column is "86"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86"
	And the Group Average at the bottom for the "Male" column is "91"
	And the Group Average at the bottom for the "Female" column is "89"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "78"
	And the score for question 1 in the "Female" column is "81"
	And the Weighted Average on the right for question 1 is "81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "83.6"
	And the score for the "Clarity of Direction" category in the "Female" column is "86.3"
	And the Weighted Average on the right for the "Clarity of Direction" category is "85.6"
	And the Group Average at the bottom for the "Male" column is "91.0"
	And the Group Average at the bottom for the "Female" column is "89.2"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "78.1"
	And the score for question 1 in the "Female" column is "81.4"
	And the Weighted Average on the right for question 1 is "80.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "83.59"
	And the score for the "Clarity of Direction" category in the "Female" column is "86.34"
	And the Weighted Average on the right for the "Clarity of Direction" category is "85.59"
	And the Group Average at the bottom for the "Male" column is "90.98"
	And the Group Average at the bottom for the "Female" column is "89.24"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "78.12"
	And the score for question 1 in the "Female" column is "81.40"
	And the Weighted Average on the right for question 1 is "80.51"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "0"
	And the score for the "Clarity of Direction" category in the "Female" column is "1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1"
	And the Group Average at the bottom for the "Male" column is "7"
	And the Group Average at the bottom for the "Female" column is "7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-6"
	And the score for question 1 in the "Female" column is "-2"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "0.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "1.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1.0"
	And the Group Average at the bottom for the "Male" column is "6.8"
	And the Group Average at the bottom for the "Female" column is "7.1"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-6.2"
	And the score for question 1 in the "Female" column is "-2.2"
	And the Weighted Average on the right for question 1 is "-3.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "0.00"
	And the score for the "Clarity of Direction" category in the "Female" column is "1.25"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1.04"
	And the Group Average at the bottom for the "Male" column is "6.80"
	And the Group Average at the bottom for the "Female" column is "7.06"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-6.25"
	And the score for question 1 in the "Female" column is "-2.23"
	And the Weighted Average on the right for question 1 is "-3.44"

	Now I go to "Functions" > "Logout"


# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.3 (4.0)"
	And the score for question 1 in the "Female" column is "4.2 (3.9)"
	And the Weighted Average on the right for question 1 is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.1 (3.9)"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.34 (4.00)"
	And the score for question 1 in the "Female" column is "4.17 (3.91)"
	And the Weighted Average on the right for question 1 is "4.22"
	And the Group Average at the bottom for the "Male" column is "4.08 (3.90)"
	And the Group Average at the bottom for the "Female" column is "3.95 (3.91)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "97 (89)"
	And the score for question 1 in the "Female" column is "91 (82)"
	And the Weighted Average on the right for question 1 is "92"
	And the Group Average at the bottom for the "Male" column is "81 (77)"
	And the Group Average at the bottom for the "Female" column is "78 (75)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96.9 (88.9)"
	And the score for question 1 in the "Female" column is "90.7 (81.8)"
	And the Weighted Average on the right for question 1 is "92.4"
	And the Group Average at the bottom for the "Male" column is "81.1 (76.9)"
	And the Group Average at the bottom for the "Female" column is "78.1 (75.4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "96.88 (88.89)"
	And the score for question 1 in the "Female" column is "90.70 (81.82)"
	And the Weighted Average on the right for question 1 is "92.37"
	And the Group Average at the bottom for the "Male" column is "81.07 (76.88)"
	And the Group Average at the bottom for the "Female" column is "78.14 (75.37)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0 (0)"
	And the score for question 1 in the "Female" column is "2 (0)"
	And the Weighted Average on the right for question 1 is "2"
	And the Group Average at the bottom for the "Male" column is "6 (6)"
	And the Group Average at the bottom for the "Female" column is "9 (6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0 (0.0)"
	And the score for question 1 in the "Female" column is "2.3 (0.0)"
	And the Weighted Average on the right for question 1 is "1.7"
	And the Group Average at the bottom for the "Male" column is "6.2 (6.3)"
	And the Group Average at the bottom for the "Female" column is "8.6 (6.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00 (0.00)"
	And the score for question 1 in the "Female" column is "2.33 (0.00)"
	And the Weighted Average on the right for question 1 is "1.69"
	And the Group Average at the bottom for the "Male" column is "6.16 (6.27)"
	And the Group Average at the bottom for the "Female" column is "8.57 (6.16)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.0 (3.6)"
	And the score for question 1 in the "Female" column is "4.1 (3.6)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.4 (4.2)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.03 (3.61)"
	And the score for question 1 in the "Female" column is "4.06 (3.64)"
	And the Weighted Average on the right for question 1 is "4.05"
	And the Group Average at the bottom for the "Male" column is "4.39 (4.21)"
	And the Group Average at the bottom for the "Female" column is "4.28 (4.25)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "78 (61)"
	And the score for question 1 in the "Female" column is "81 (55)"
	And the Weighted Average on the right for question 1 is "81"
	And the Group Average at the bottom for the "Male" column is "91 (87)"
	And the Group Average at the bottom for the "Female" column is "89 (89)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "78.1 (61.1)"
	And the score for question 1 in the "Female" column is "81.4 (54.5)"
	And the Weighted Average on the right for question 1 is "80.5"
	And the Group Average at the bottom for the "Male" column is "91.0 (87.1)"
	And the Group Average at the bottom for the "Female" column is "89.2 (88.6)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "78.12 (61.11)"
	And the score for question 1 in the "Female" column is "81.40 (54.55)"
	And the Weighted Average on the right for question 1 is "80.51"
	And the Group Average at the bottom for the "Male" column is "90.98 (87.10)"
	And the Group Average at the bottom for the "Female" column is "89.24 (88.56)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-6 (-7)"
	And the score for question 1 in the "Female" column is "-2 (-5)"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "7 (7)"
	And the Group Average at the bottom for the "Female" column is "7 (7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-6.2 (-7.0)"
	And the score for question 1 in the "Female" column is "-2.2 (-4.9)"
	And the Weighted Average on the right for question 1 is "-3.4"
	And the Group Average at the bottom for the "Male" column is "6.8 (6.5)"
	And the Group Average at the bottom for the "Female" column is "7.1 (7.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-6.25 (-7.04)"
	And the score for question 1 in the "Female" column is "-2.23 (-4.91)"
	And the Weighted Average on the right for question 1 is "-3.44"
	And the Group Average at the bottom for the "Male" column is "6.80 (6.53)"
	And the Group Average at the bottom for the "Female" column is "7.06 (7.22)"

	Now I go to "Functions" > "Logout"

# Categories + trend
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.1 (3.8)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1 (3.8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.1 (3.9)"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.3 (4.0)"
	And the score for question 1 in the "Female" column is "4.2 (3.9)"
	And the Weighted Average on the right for question 1 is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.14 (3.82)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.09 (3.80)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.10"
	And the Group Average at the bottom for the "Male" column is "4.08 (3.90)"
	And the Group Average at the bottom for the "Female" column is "3.95 (3.91)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.34 (4.00)"
	And the score for question 1 in the "Female" column is "4.17 (3.91)"
	And the Weighted Average on the right for question 1 is "4.22"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88 (78)"
	And the score for the "Clarity of Direction" category in the "Female" column is "88 (70)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "81 (77)"
	And the Group Average at the bottom for the "Female" column is "78 (75)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "97 (89)"
	And the score for question 1 in the "Female" column is "91 (82)"
	And the Weighted Average on the right for question 1 is "92"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.3 (77.8)"
	And the score for the "Clarity of Direction" category in the "Female" column is "87.8 (70.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.9"
	And the Group Average at the bottom for the "Male" column is "81.1 (76.9)"
	And the Group Average at the bottom for the "Female" column is "78.1 (75.4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96.9 (88.9)"
	And the score for question 1 in the "Female" column is "90.7 (81.8)"
	And the Weighted Average on the right for question 1 is "92.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.28 (77.78)"
	And the score for the "Clarity of Direction" category in the "Female" column is "87.79 (70.45)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.92"
	And the Group Average at the bottom for the "Male" column is "81.07 (76.88)"
	And the Group Average at the bottom for the "Female" column is "78.14 (75.37)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "96.88 (88.89)"
	And the score for question 1 in the "Female" column is "90.70 (81.82)"
	And the Weighted Average on the right for question 1 is "92.37"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "5 (7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5"
	And the Group Average at the bottom for the "Male" column is "6 (6)"
	And the Group Average at the bottom for the "Female" column is "9 (6)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0 (0)"
	And the score for question 1 in the "Female" column is "2 (0)"
	And the Weighted Average on the right for question 1 is "2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.9 (6.9)"
	And the score for the "Clarity of Direction" category in the "Female" column is "5.2 (6.8)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.9"
	And the Group Average at the bottom for the "Male" column is "6.2 (6.3)"
	And the Group Average at the bottom for the "Female" column is "8.6 (6.2)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0 (0.0)"
	And the score for question 1 in the "Female" column is "2.3 (0.0)"
	And the Weighted Average on the right for question 1 is "1.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.91 (6.94)"
	And the score for the "Clarity of Direction" category in the "Female" column is "5.23 (6.82)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.87"
	And the Group Average at the bottom for the "Male" column is "6.16 (6.27)"
	And the Group Average at the bottom for the "Female" column is "8.57 (6.16)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00 (0.00)"
	And the score for question 1 in the "Female" column is "2.33 (0.00)"
	And the Weighted Average on the right for question 1 is "1.69"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.1 (3.9)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1 (4.0)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.4 (4.2)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.0 (3.6)"
	And the score for question 1 in the "Female" column is "4.1 (3.6)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.14 (3.93)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.15 (4.05)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.15"
	And the Group Average at the bottom for the "Male" column is "4.39 (4.21)"
	And the Group Average at the bottom for the "Female" column is "4.28 (4.25)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.03 (3.61)"
	And the score for question 1 in the "Female" column is "4.06 (3.64)"
	And the Weighted Average on the right for question 1 is "4.05"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "84 (72)"
	And the score for the "Clarity of Direction" category in the "Female" column is "86 (77)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "86"
	And the Group Average at the bottom for the "Male" column is "91 (87)"
	And the Group Average at the bottom for the "Female" column is "89 (89)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "78 (61)"
	And the score for question 1 in the "Female" column is "81 (55)"
	And the Weighted Average on the right for question 1 is "81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "83.6 (72.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "86.3 (77.3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "85.6"
	And the Group Average at the bottom for the "Male" column is "91.0 (87.1)"
	And the Group Average at the bottom for the "Female" column is "89.2 (88.6)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "78.1 (61.1)"
	And the score for question 1 in the "Female" column is "81.4 (54.5)"
	And the Weighted Average on the right for question 1 is "80.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "83.59 (72.22)"
	And the score for the "Clarity of Direction" category in the "Female" column is "86.34 (77.27)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "85.59"
	And the Group Average at the bottom for the "Male" column is "90.98 (87.10)"
	And the Group Average at the bottom for the "Female" column is "89.24 (88.56)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "78.12 (61.11)"
	And the score for question 1 in the "Female" column is "81.40 (54.55)"
	And the Weighted Average on the right for question 1 is "80.51"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "0 (2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "1 (5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1"
	And the Group Average at the bottom for the "Male" column is "7 (7)"
	And the Group Average at the bottom for the "Female" column is "7 (7)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-6 (-7)"
	And the score for question 1 in the "Female" column is "-2 (-5)"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "0.0 (2.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "1.2 (5.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1.0"
	And the Group Average at the bottom for the "Male" column is "6.8 (6.5)"
	And the Group Average at the bottom for the "Female" column is "7.1 (7.2)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-6.2 (-7.0)"
	And the score for question 1 in the "Female" column is "-2.2 (-4.9)"
	And the Weighted Average on the right for question 1 is "-3.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "0.00 (2.16)"
	And the score for the "Clarity of Direction" category in the "Female" column is "1.25 (5.06)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "1.04"
	And the Group Average at the bottom for the "Male" column is "6.80 (6.53)"
	And the Group Average at the bottom for the "Female" column is "7.06 (7.22)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-6.25 (-7.04)"
	And the score for question 1 in the "Female" column is "-2.23 (-4.91)"
	And the Weighted Average on the right for question 1 is "-3.44"



	Now I go to "Functions" > "Logout"