Feature: Check Gap Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "4.22"
	And the Importance for question 1 is "4.05"
	And the Gap for question 1 is "-0.17"
	And the Weighted Gap for question 1 is "-3.44"
	And the Overall Average at the bottom has an Eval Avg of "3.98"
	And the Overall Average has a Importance of "4.31"
	And the Overall Average has a Gap of "0.33"
	And the Overall Average has a Weighted Gap of "7.11"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "4.10"
	And the Importance for the "Clarity of Direction" category is "4.15"
	And the Gap for the "Clarity of Direction" category is "0.05"
	And the Weighted Gap for the "Clarity of Direction" category is "1.04"
	And the Overall Average at the bottom has an Eval Avg of "3.98"
	And the Overall Average has a Importance of "4.31"
	And the Overall Average has a Gap of "0.33"
	And the Overall Average has a Weighted Gap of "7.11"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.22"
	And the Importance for question 1 is "4.05"
	And the Gap for question 1 is "-0.17"
	And the Weighted Gap for question 1 is "-3.44"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the number for "Your view" in the upper right is "(118)"
	And the number for "Organization" in the upper right is "(1136)"
	And the Eval Avg for question 1 is "4.22 (+0.10)"
	And the Importance for question 1 is "4.05 (-0.05)"
	And the Gap for question 1 is "-0.17 (-0.15)"
	And the Weighted Gap for question 1 is "-3.44 (-3.03)"
	And the Overall Average at the bottom has an Eval Avg of "3.98 (+0.03)"
	And the Overall Average has a Importance of "4.31 (-0.02)"
	And the Overall Average has a Gap of "0.33 (-0.05)"
	And the Overall Average has a Weighted Gap of "7.11 (-1.12)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the Eval Avg for the "Clarity of Direction" category is "4.10 (+0.14)"
	And the Importance for the "Clarity of Direction" category is "4.15 (-0.11)"
	And the Gap for the "Clarity of Direction" category is "0.05 (-0.25)"
	And the Weighted Gap for the "Clarity of Direction" category is "1.04 (-5.35)"
	And the Overall Average at the bottom has an Eval Avg of "3.98 (+0.03)"
	And the Overall Average has a Importance of "4.31 (-0.02)"
	And the Overall Average has a Gap of "0.33 (-0.05)"
	And the Overall Average has a Weighted Gap of "7.11 (-1.12)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.22 (+0.10)"
	And the Importance for question 1 is "4.05 (-0.05)"
	And the Gap for question 1 is "-0.17 (-0.15)"
	And the Weighted Gap for question 1 is "-3.44 (-3.03)"
	
	Now I go to "Functions" > "Logout"