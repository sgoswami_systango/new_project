Feature: Check Completion Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click on the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Male" in the "Invitees" column is "41"
	And the figure for "Male" in the "Respondents" column is "32(78.0%)"
	And the figure for "Male" in the "Completions" column is "32(78.0%)"
	And the figure for the "Totals" row in the "Invitees" column is "131"
	And the figure for the "Totals" row in the "Respondents" column is "118(90.1%)"
	And the figure for the "Totals" row in the "Completions" column is "118(90.1%)"

	When I am on the "By Time" tab
	Then the figure for "Monday, September 15, 2014" in the "Respondents" column is "22"
	And the figure for "Monday, September 15, 2014" in the "% Responded" column is "18.6%"
	And the figure for "Monday, September 15, 2014" in the "% Invited" column is "16.8%"
	And the figure for the "Totals" row in the "Respondents" column is "118"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "90.1%"
	
	When I click on "Monday, September 15, 2014"
	Then the figure for "12:00 PM" in the "Respondents" column is "5"
	And the figure for "12:00 PM" in the "% Responded" column is "4.2%"
	And the figure for "12:00 PM" in the "% Invited" column is "3.8%"
	
	Now I go to "Functions" > "Logout"