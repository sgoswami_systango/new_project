Feature: Check Trend Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(118)"
	And the "Count" for the "2013" column is "(30)"
	And the "Count" for the "2012" column is "(20)"
	And the score for "question 1" in the "2014" column is "4.22 (+0.25)"
	And the score for "question 1" in the "2013" column is "3.97 (-0.13)"
	And the score for "question 1" in the "2012" column is "4.10"
	And the Overall Average at the bottom for "2014" is "3.99 (+0.09)"
	And the Overall Average for "2013" is "3.90 (-0.10)"
	And the Overall Average for "2012" is "4.00"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(118)"
	And the "Count" for the "2013" column is "(30)"
	And the "Count" for the "2012" column is "(20)"
	And the score for the "Clarity of Direction" category for "2014" is "4.10 (+0.29)"
	And the score for the "Clarity of Direction" category for "2013" is "3.81 (-0.20)"
	And the score for the "Clarity of Direction" category for "2012" is "4.01"
	And the Overall Average at the bottom for "2014" is "3.99 (+0.09)"
	And the Overall Average for "2013" is "3.90 (-0.10)"
	And the Overall Average for "2012" is "4.00"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.22 (+0.25)"
	And the score for "question 1" in the "2013" column is "3.97 (-0.13)"
	And the score for "question 1" in the "2012" column is "4.10"
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(118)"
	And the "Count" for the "2013" column is "(30)"
	And the "Count" for the "2012" column is "(20)"
	And the score for "question 1" in the "2014" column is "92.4 (+6.2)"
	And the score for "question 1" in the "2013" column is "86.2 (-8.8)"
	And the score for "question 1" in the "2012" column is "95.0"
	And the Overall Average at the bottom for "2014" is "79.3 (+3.0)"
	And the Overall Average for "2013" is "76.3 (-4.3)"
	And the Overall Average for "2012" is "80.6"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(118)"
	And the "Count" for the "2013" column is "(30)"
	And the "Count" for the "2012" column is "(20)"
	And the score for the "Clarity of Direction" category for "2014" is "87.9 (+12.9)"
	And the score for the "Clarity of Direction" category for "2013" is "75.0 (-11.2)"
	And the score for the "Clarity of Direction" category for "2012" is "86.2"
	And the Overall Average at the bottom for "2014" is "79.3 (+3.0)"
	And the Overall Average for "2013" is "76.3 (-4.3)"
	And the Overall Average for "2012" is "80.6"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "92.4 (+6.2)"
	And the score for "question 1" in the "2013" column is "86.2 (-8.8)"
	And the score for "question 1" in the "2012" column is "95.0"

	Now I go to "Functions" > "Logout"