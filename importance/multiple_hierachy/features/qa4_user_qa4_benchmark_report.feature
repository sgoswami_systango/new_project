Feature: Check Benchmark Report for user with a multiple-hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa4"
	And I click on the "Benchmark Report" link under "Comparison Reports"
	Then I am on the "Benchmark Report" page
	
Scenario: I can check Benchmark Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
	When I am on the "Questions" tab
	Then the score for question 2 in the "My View" column is "83.1%"
	And the score for question 2 in the "Mitchell" column is "77.2%(-5.9)"
	And the score for question 2 in the "Perceptyx overall benchmark" column is "73.6%(-9.5)"
	And the score for question 2 in the "Perceptyx software/technology benchmark" column is "75.4%(-7.7)"

	When I am on the "Categories" tab
	Then the score for the Clarity of Direction category in the "My View" column is "87.9%"
	And the score for the Clarity of Direction category in the "Mitchell" column is "78.1%"
	And the score for question 2 in the "My View" column is "83.1%"
	And the score for question 2 in the "Mitchell" column is "77.2%(-5.9)"
	And the score for question 2 in the "Perceptyx overall benchmark" column is "73.6%(-9.5)"
	And the score for question 2 in the "Perceptyx software/technology benchmark" column is "75.4%(-7.7)"

	Now I go to "Functions" > "Logout"