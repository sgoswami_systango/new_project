Feature: Check Demographic Crosstab for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(332)"
	And the first number under "Female" is "(132)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Male" column is "3.9"
	And the Group Average at the bottom for the "Female" column is "3.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.06"
	And the score for question 1 in the "Female" column is "4.02"
	And the Weighted Average on the right for question 1 is "4.05"
	And the Group Average at the bottom for the "Male" column is "3.88"
	And the Group Average at the bottom for the "Female" column is "3.87"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83"
	And the score for question 1 in the "Female" column is "84"
	And the Weighted Average on the right for question 1 is "84"
	And the Group Average at the bottom for the "Male" column is "73"
	And the Group Average at the bottom for the "Female" column is "73"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.5"
	And the score for question 1 in the "Female" column is "83.7"
	And the Weighted Average on the right for question 1 is "83.6"
	And the Group Average at the bottom for the "Male" column is "72.8"
	And the Group Average at the bottom for the "Female" column is "72.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.49"
	And the score for question 1 in the "Female" column is "83.72"
	And the Weighted Average on the right for question 1 is "83.55"
	And the Group Average at the bottom for the "Male" column is "72.80"
	And the Group Average at the bottom for the "Female" column is "72.67"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "6"
	And the score for question 1 in the "Female" column is "5"
	And the Weighted Average on the right for question 1 is "5"
	And the Group Average at the bottom for the "Male" column is "10"
	And the Group Average at the bottom for the "Female" column is "10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.5"
	And the score for question 1 in the "Female" column is "5.4"
	And the Weighted Average on the right for question 1 is "5.5"
	And the Group Average at the bottom for the "Male" column is "10.2"
	And the Group Average at the bottom for the "Female" column is "9.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.50"
	And the score for question 1 in the "Female" column is "5.43"
	And the Weighted Average on the right for question 1 is "5.48"
	And the Group Average at the bottom for the "Male" column is "10.23"
	And the Group Average at the bottom for the "Female" column is "9.92"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.4"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.14"
	And the score for question 1 in the "Female" column is "3.98"
	And the Weighted Average on the right for question 1 is "4.10"
	And the Group Average at the bottom for the "Male" column is "4.35"
	And the Group Average at the bottom for the "Female" column is "4.33"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83"
	And the score for question 1 in the "Female" column is "79"
	And the Weighted Average on the right for question 1 is "82"
	And the Group Average at the bottom for the "Male" column is "90"
	And the Group Average at the bottom for the "Female" column is "90"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "82.6"
	And the score for question 1 in the "Female" column is "79.1"
	And the Weighted Average on the right for question 1 is "81.6"
	And the Group Average at the bottom for the "Male" column is "89.7"
	And the Group Average at the bottom for the "Female" column is "89.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "82.57"
	And the score for question 1 in the "Female" column is "79.07"
	And the Weighted Average on the right for question 1 is "81.58"
	And the Group Average at the bottom for the "Male" column is "89.74"
	And the Group Average at the bottom for the "Female" column is "89.88"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "2"
	And the score for question 1 in the "Female" column is "-1"
	And the Weighted Average on the right for question 1 is "1"
	And the Group Average at the bottom for the "Male" column is "10"
	And the Group Average at the bottom for the "Female" column is "10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "1.7"
	And the score for question 1 in the "Female" column is "-0.8"
	And the Weighted Average on the right for question 1 is "1.0"
	And the Group Average at the bottom for the "Male" column is "10.2"
	And the Group Average at the bottom for the "Female" column is "10.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "1.66"
	And the score for question 1 in the "Female" column is "-0.80"
	And the Weighted Average on the right for question 1 is "1.02"
	And the Group Average at the bottom for the "Male" column is "10.22"
	And the Group Average at the bottom for the "Female" column is "9.96"


# Categories
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(332)"
	And the number directly below "Female" is "(132)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.8"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.8"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.8"
	And the Group Average at the bottom for the "Male" column is "3.9"
	And the Group Average at the bottom for the "Female" column is "3.9"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.80"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.78"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.79"
	And the Group Average at the bottom for the "Male" column is "3.88"
	And the Group Average at the bottom for the "Female" column is "3.87"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.06"
	And the score for question 1 in the "Female" column is "4.02"
	And the Weighted Average on the right for question 1 is "4.05"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "71"
	And the score for the "Clarity of Direction" category in the "Female" column is "70"
	And the Weighted Average on the right for the "Clarity of Direction" category is "71"
	And the Group Average at the bottom for the "Male" column is "73"
	And the Group Average at the bottom for the "Female" column is "73"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83"
	And the score for question 1 in the "Female" column is "84"
	And the Weighted Average on the right for question 1 is "84"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "71.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "70.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "70.9"
	And the Group Average at the bottom for the "Male" column is "72.8"
	And the Group Average at the bottom for the "Female" column is "72.7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.5"
	And the score for question 1 in the "Female" column is "83.7"
	And the Weighted Average on the right for question 1 is "83.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "71.25"
	And the score for the "Clarity of Direction" category in the "Female" column is "70.16"
	And the Weighted Average on the right for the "Clarity of Direction" category is "70.94"
	And the Group Average at the bottom for the "Male" column is "72.80"
	And the Group Average at the bottom for the "Female" column is "72.67"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.49"
	And the score for question 1 in the "Female" column is "83.72"
	And the Weighted Average on the right for question 1 is "83.55"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "12"
	And the score for the "Clarity of Direction" category in the "Female" column is "11"
	And the Weighted Average on the right for the "Clarity of Direction" category is "11"
	And the Group Average at the bottom for the "Male" column is "10"
	And the Group Average at the bottom for the "Female" column is "10"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "6"
	And the score for question 1 in the "Female" column is "5"
	And the Weighted Average on the right for question 1 is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "11.7"
	And the score for the "Clarity of Direction" category in the "Female" column is "10.9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "11.5"
	And the Group Average at the bottom for the "Male" column is "10.2"
	And the Group Average at the bottom for the "Female" column is "9.9"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.5"
	And the score for question 1 in the "Female" column is "5.4"
	And the Weighted Average on the right for question 1 is "5.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "11.70"
	And the score for the "Clarity of Direction" category in the "Female" column is "10.85"
	And the Weighted Average on the right for the "Clarity of Direction" category is "11.46"
	And the Group Average at the bottom for the "Male" column is "10.23"
	And the Group Average at the bottom for the "Female" column is "9.92"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.50"
	And the score for question 1 in the "Female" column is "5.43"
	And the Weighted Average on the right for question 1 is "5.48"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.3"
	And the Group Average at the bottom for the "Male" column is "4.4"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.0"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.30"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.22"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.27"
	And the Group Average at the bottom for the "Male" column is "4.35"
	And the Group Average at the bottom for the "Female" column is "4.33"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.14"
	And the score for question 1 in the "Female" column is "3.98"
	And the Weighted Average on the right for question 1 is "4.10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88"
	And the score for the "Clarity of Direction" category in the "Female" column is "87"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "90"
	And the Group Average at the bottom for the "Female" column is "90"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83"
	And the score for question 1 in the "Female" column is "79"
	And the Weighted Average on the right for question 1 is "82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.1"
	And the score for the "Clarity of Direction" category in the "Female" column is "87.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.9"
	And the Group Average at the bottom for the "Male" column is "89.7"
	And the Group Average at the bottom for the "Female" column is "89.9"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "82.6"
	And the score for question 1 in the "Female" column is "79.1"
	And the Weighted Average on the right for question 1 is "81.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.15"
	And the score for the "Clarity of Direction" category in the "Female" column is "87.40"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.94"
	And the Group Average at the bottom for the "Male" column is "89.74"
	And the Group Average at the bottom for the "Female" column is "89.88"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "82.57"
	And the score for question 1 in the "Female" column is "79.07"
	And the Weighted Average on the right for question 1 is "81.58"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "11"
	And the score for the "Clarity of Direction" category in the "Female" column is "9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10"
	And the Group Average at the bottom for the "Male" column is "10"
	And the Group Average at the bottom for the "Female" column is "10"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "2"
	And the score for question 1 in the "Female" column is "-1"
	And the Weighted Average on the right for question 1 is "1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "10.8"
	And the score for the "Clarity of Direction" category in the "Female" column is "9.3"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.2"
	And the Group Average at the bottom for the "Male" column is "10.2"
	And the Group Average at the bottom for the "Female" column is "10.0"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "1.7"
	And the score for question 1 in the "Female" column is "-0.8"
	And the Weighted Average on the right for question 1 is "1.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "10.75"
	And the score for the "Clarity of Direction" category in the "Female" column is "9.28"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.25"
	And the Group Average at the bottom for the "Male" column is "10.22"
	And the Group Average at the bottom for the "Female" column is "9.96"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "1.66"
	And the score for question 1 in the "Female" column is "-0.80"
	And the Weighted Average on the right for question 1 is "1.02"



# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.0 (4.1)"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Male" column is "3.9 (3.9)"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.06 (4.07)"
	And the score for question 1 in the "Female" column is "4.02 (4.06)"
	And the Weighted Average on the right for question 1 is "4.05"
	And the Group Average at the bottom for the "Male" column is "3.88 (3.93)"
	And the Group Average at the bottom for the "Female" column is "3.87 (3.91)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83 (83)"
	And the score for question 1 in the "Female" column is "84 (85)"
	And the Weighted Average on the right for question 1 is "84"
	And the Group Average at the bottom for the "Male" column is "73 (75)"
	And the Group Average at the bottom for the "Female" column is "73 (75)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.5 (83.2)"
	And the score for question 1 in the "Female" column is "83.7 (84.5)"
	And the Weighted Average on the right for question 1 is "83.6"
	And the Group Average at the bottom for the "Male" column is "72.8 (74.8)"
	And the Group Average at the bottom for the "Female" column is "72.7 (75.5)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83.49 (83.20)"
	And the score for question 1 in the "Female" column is "83.72 (84.51)"
	And the Weighted Average on the right for question 1 is "83.55"
	And the Group Average at the bottom for the "Male" column is "72.80 (74.84)"
	And the Group Average at the bottom for the "Female" column is "72.67 (75.47)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "6 (5)"
	And the score for question 1 in the "Female" column is "5 (2)"
	And the Weighted Average on the right for question 1 is "5"
	And the Group Average at the bottom for the "Male" column is "10 (9)"
	And the Group Average at the bottom for the "Female" column is "10 (7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.5 (5.0)"
	And the score for question 1 in the "Female" column is "5.4 (2.1)"
	And the Weighted Average on the right for question 1 is "5.5"
	And the Group Average at the bottom for the "Male" column is "10.2 (8.7)"
	And the Group Average at the bottom for the "Female" column is "9.9 (7.1)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.50 (4.99)"
	And the score for question 1 in the "Female" column is "5.43 (2.11)"
	And the Weighted Average on the right for question 1 is "5.48"
	And the Group Average at the bottom for the "Male" column is "10.23 (8.66)"
	And the Group Average at the bottom for the "Female" column is "9.92 (7.07)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.0 (4.1)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.4 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.14 (4.09)"
	And the score for question 1 in the "Female" column is "3.98 (4.06)"
	And the Weighted Average on the right for question 1 is "4.10"
	And the Group Average at the bottom for the "Male" column is "4.35 (4.29)"
	And the Group Average at the bottom for the "Female" column is "4.33 (4.28)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83 (81)"
	And the score for question 1 in the "Female" column is "79 (82)"
	And the Weighted Average on the right for question 1 is "82"
	And the Group Average at the bottom for the "Male" column is "90 (89)"
	And the Group Average at the bottom for the "Female" column is "90 (89)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "82.6 (80.8)"
	And the score for question 1 in the "Female" column is "79.1 (82.4)"
	And the Weighted Average on the right for question 1 is "81.6"
	And the Group Average at the bottom for the "Male" column is "89.7 (89.1)"
	And the Group Average at the bottom for the "Female" column is "89.9 (88.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "82.57 (80.84)"
	And the score for question 1 in the "Female" column is "79.07 (82.39)"
	And the Weighted Average on the right for question 1 is "81.58"
	And the Group Average at the bottom for the "Male" column is "89.74 (89.06)"
	And the Group Average at the bottom for the "Female" column is "89.88 (88.90)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "2 (0)"
	And the score for question 1 in the "Female" column is "-1 (0)"
	And the Weighted Average on the right for question 1 is "1"
	And the Group Average at the bottom for the "Male" column is "10 (8)"
	And the Group Average at the bottom for the "Female" column is "10 (8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "1.7 (0.4)"
	And the score for question 1 in the "Female" column is "-0.8 (0.0)"
	And the Weighted Average on the right for question 1 is "1.0"
	And the Group Average at the bottom for the "Male" column is "10.2 (7.7)"
	And the Group Average at the bottom for the "Female" column is "10.0 (7.9)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "1.66 (0.41)"
	And the score for question 1 in the "Female" column is "-0.80 (0.00)"
	And the Weighted Average on the right for question 1 is "1.02"
	And the Group Average at the bottom for the "Male" column is "10.22 (7.72)"
	And the Group Average at the bottom for the "Female" column is "9.96 (7.92)"


# Categories + trend
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.8 (4.0)"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.8 (3.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.8"
	And the Group Average at the bottom for the "Male" column is "3.9 (3.9)"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.0 (4.1)"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.80 (3.95)"
	And the score for the "Clarity of Direction" category in the "Female" column is "3.78 (3.89)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.79"
	And the Group Average at the bottom for the "Male" column is "3.88 (3.93)"
	And the Group Average at the bottom for the "Female" column is "3.87 (3.91)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.06 (4.07)"
	And the score for question 1 in the "Female" column is "4.02 (4.06)"
	And the Weighted Average on the right for question 1 is "4.05"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "71 (77)"
	And the score for the "Clarity of Direction" category in the "Female" column is "70 (77)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "71"
	And the Group Average at the bottom for the "Male" column is "73 (75)"
	And the Group Average at the bottom for the "Female" column is "73 (75)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83 (83)"
	And the score for question 1 in the "Female" column is "84 (85)"
	And the Weighted Average on the right for question 1 is "84"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "71.3 (77.4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "70.2 (77.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "70.9"
	And the Group Average at the bottom for the "Male" column is "72.8 (74.8)"
	And the Group Average at the bottom for the "Female" column is "72.7 (75.5)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.5 (83.2)"
	And the score for question 1 in the "Female" column is "83.7 (84.5)"
	And the Weighted Average on the right for question 1 is "83.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "71.25 (77.36)"
	And the score for the "Clarity of Direction" category in the "Female" column is "70.16 (77.46)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "70.94"
	And the Group Average at the bottom for the "Male" column is "72.80 (74.84)"
	And the Group Average at the bottom for the "Female" column is "72.67 (75.47)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83.49 (83.20)"
	And the score for question 1 in the "Female" column is "83.72 (84.51)"
	And the Weighted Average on the right for question 1 is "83.55"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "12 (8)"
	And the score for the "Clarity of Direction" category in the "Female" column is "11 (7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "11"
	And the Group Average at the bottom for the "Male" column is "10 (9)"
	And the Group Average at the bottom for the "Female" column is "10 (7)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "6 (5)"
	And the score for question 1 in the "Female" column is "5 (2)"
	And the Weighted Average on the right for question 1 is "5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "11.7 (7.7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "10.9 (6.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "11.5"
	And the Group Average at the bottom for the "Male" column is "10.2 (8.7)"
	And the Group Average at the bottom for the "Female" column is "9.9 (7.1)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.5 (5.0)"
	And the score for question 1 in the "Female" column is "5.4 (2.1)"
	And the Weighted Average on the right for question 1 is "5.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "11.70 (7.74)"
	And the score for the "Clarity of Direction" category in the "Female" column is "10.85 (6.51)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "11.46"
	And the Group Average at the bottom for the "Male" column is "10.23 (8.66)"
	And the Group Average at the bottom for the "Female" column is "9.92 (7.07)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.50 (4.99)"
	And the score for question 1 in the "Female" column is "5.43 (2.11)"
	And the Weighted Average on the right for question 1 is "5.48"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.3 (4.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.2 (4.2)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.3"
	And the Group Average at the bottom for the "Male" column is "4.4 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1 (4.1)"
	And the score for question 1 in the "Female" column is "4.0 (4.1)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.30 (4.24)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.22 (4.18)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.27"
	And the Group Average at the bottom for the "Male" column is "4.35 (4.29)"
	And the Group Average at the bottom for the "Female" column is "4.33 (4.28)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.14 (4.09)"
	And the score for question 1 in the "Female" column is "3.98 (4.06)"
	And the Weighted Average on the right for question 1 is "4.10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88 (86)"
	And the score for the "Clarity of Direction" category in the "Female" column is "87 (87)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "90 (89)"
	And the Group Average at the bottom for the "Female" column is "90 (89)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83 (81)"
	And the score for question 1 in the "Female" column is "79 (82)"
	And the Weighted Average on the right for question 1 is "82"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.1 (86.3)"
	And the score for the "Clarity of Direction" category in the "Female" column is "87.4 (87.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.9"
	And the Group Average at the bottom for the "Male" column is "89.7 (89.1)"
	And the Group Average at the bottom for the "Female" column is "89.9 (88.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "82.6 (80.8)"
	And the score for question 1 in the "Female" column is "79.1 (82.4)"
	And the Weighted Average on the right for question 1 is "81.6"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.15 (86.29)"
	And the score for the "Clarity of Direction" category in the "Female" column is "87.40 (87.15)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.94"
	And the Group Average at the bottom for the "Male" column is "89.74 (89.06)"
	And the Group Average at the bottom for the "Female" column is "89.88 (88.90)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "82.57 (80.84)"
	And the score for question 1 in the "Female" column is "79.07 (82.39)"
	And the Weighted Average on the right for question 1 is "81.58"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "11 (6)"
	And the score for the "Clarity of Direction" category in the "Female" column is "9 (6)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10"
	And the Group Average at the bottom for the "Male" column is "10 (8)"
	And the Group Average at the bottom for the "Female" column is "10 (8)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "2 (0)"
	And the score for question 1 in the "Female" column is "-1 (0)"
	And the Weighted Average on the right for question 1 is "1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "10.8 (6.1)"
	And the score for the "Clarity of Direction" category in the "Female" column is "9.3 (6.0)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.2"
	And the Group Average at the bottom for the "Male" column is "10.2 (7.7)"
	And the Group Average at the bottom for the "Female" column is "10.0 (7.9)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "1.7 (0.4)"
	And the score for question 1 in the "Female" column is "-0.8 (0.0)"
	And the Weighted Average on the right for question 1 is "1.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "10.75 (6.15)"
	And the score for the "Clarity of Direction" category in the "Female" column is "9.28 (6.06)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "10.25"
	And the Group Average at the bottom for the "Male" column is "10.22 (7.72)"
	And the Group Average at the bottom for the "Female" column is "9.96 (7.92)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "1.66 (0.41)"
	And the score for question 1 in the "Female" column is "-0.80 (0.00)"
	And the Weighted Average on the right for question 1 is "1.02"



	Now I go to "Functions" > "Logout"