Feature: Check Gap Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "4.05"
	And the Importance for question 1 is "4.10"
	And the Gap for question 1 is "0.05"
	And the Weighted Gap for question 1 is "1.02"
	And the Overall Average at the bottom has an Eval Avg of "3.88"
	And the Overall Average has a Importance of "4.35"
	And the Overall Average has a Gap of "0.47"
	And the Overall Average has a Weighted Gap of "10.22"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.79"
	And the Importance for the "Clarity of Direction" category is "4.27"
	And the Gap for the "Clarity of Direction" category is "0.48"
	And the Weighted Gap for the "Clarity of Direction" category is "10.25"
	And the Overall Average at the bottom has an Eval Avg of "3.88"
	And the Overall Average has a Importance of "4.35"
	And the Overall Average has a Gap of "0.47"
	And the Overall Average has a Weighted Gap of "10.22"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.05"
	And the Importance for question 1 is "4.10"
	And the Gap for question 1 is "0.05"
	And the Weighted Gap for question 1 is "1.02"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the number for "Your view" in the upper right is "(464)"
	And the number for "Organization" in the upper right is "(1136)"
	And the Eval Avg for question 1 is "4.05 (-0.07)"
	And the Importance for question 1 is "4.10 (0.00)"
	And the Gap for question 1 is "0.05 (+0.07)"
	And the Weighted Gap for question 1 is "1.02 (+1.43)"
	And the Overall Average at the bottom has an Eval Avg of "3.88 (-0.07)"
	And the Overall Average has a Importance of "4.35 (+0.02)"
	And the Overall Average has a Gap of "0.47 (+0.09)"
	And the Overall Average has a Weighted Gap of "10.22 (+1.99)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the Eval Avg for the "Clarity of Direction" category is "3.79 (-0.17)"
	And the Importance for the "Clarity of Direction" category is "4.27 (+0.01)"
	And the Gap for the "Clarity of Direction" category is "0.48 (+0.18)"
	And the Weighted Gap for the "Clarity of Direction" category is "10.25 (+3.86)"
	And the Overall Average at the bottom has an Eval Avg of "3.88 (-0.07)"
	And the Overall Average has a Importance of "4.35 (+0.02)"
	And the Overall Average has a Gap of "0.47 (+0.09)"
	And the Overall Average has a Weighted Gap of "10.22 (+1.99)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.05 (-0.07)"
	And the Importance for question 1 is "4.10 (0.00)"
	And the Gap for question 1 is "0.05 (+0.07)"
	And the Weighted Gap for question 1 is "1.02 (+1.43)"
	
	Now I go to "Functions" > "Logout"