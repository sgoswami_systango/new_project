Feature: Check Trend Report for user with a query-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa2"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(464)"
	And the "Count" for the "2013" column is "(531)"
	And the "Count" for the "2012" column is "(502)"
	And the "Count" for the "2011" column is "(548)"	
	And the score for "question 1" in the "2014" column is "4.05 (-0.02)"
	And the score for "question 1" in the "2013" column is "4.07 (+0.01)"
	And the score for "question 1" in the "2012" column is "4.06"
	And there is no score for "question 1" in the "2011" column
	And the Overall Average at the bottom for "2014" is "3.92 (-0.01)"
	And the Overall Average for "2013" is "3.93 (+0.07)"
	And the Overall Average for "2012" is "3.86 (-0.02)"
	And the Overall Average for "2011" is "3.88"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(464)"
	And the "Count" for the "2013" column is "(531)"
	And the "Count" for the "2012" column is "(502)"
	And the "Count" for the "2011" column is "(548)"	
	And the score for the "Clarity of Direction" category for "2014" is "3.79 (-0.15)"
	And the score for the "Clarity of Direction" category for "2013" is "3.94 (+0.14)"
	And the score for the "Clarity of Direction" category for "2012" is "3.80 (+0.10)"
	And the score for the "Clarity of Direction" category for "2011" is "3.70"
	And the Overall Average at the bottom for "2014" is "3.92 (-0.01)"
	And the Overall Average for "2013" is "3.93 (+0.07)"
	And the Overall Average for "2012" is "3.86 (-0.02)"
	And the Overall Average for "2011" is "3.88"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.05 (-0.02)"
	And the score for "question 1" in the "2013" column is "4.07 (+0.01)"
	And the score for "question 1" in the "2012" column is "4.06"
	And there is no score for question 1 in the "2011" column
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(464)"
	And the "Count" for the "2013" column is "(531)"
	And the "Count" for the "2012" column is "(502)"
	And the "Count" for the "2011" column is "(548)"	
	And the score for "question 1" in the "2014" column is "83.6 (0.0)"
	And the score for "question 1" in the "2013" column is "83.6 (-1.2)"
	And the score for "question 1" in the "2012" column is "84.8"
	And there is no score for question 1 in the "2011" column
	And the Overall Average at the bottom for "2014" is "74.7 (-0.3)"
	And the Overall Average for "2013" is "75.0 (+1.8)"
	And the Overall Average for "2012" is "73.2 (+0.3)"
	And the Overall Average for "2011" is "72.9"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(464)"
	And the "Count" for the "2013" column is "(531)"
	And the "Count" for the "2012" column is "(502)"
	And the "Count" for the "2011" column is "(548)"	
	And the score for the "Clarity of Direction" category for "2014" is "70.9 (-6.5)"
	And the score for the "Clarity of Direction" category for "2013" is "77.4 (+4.5)"
	And the score for the "Clarity of Direction" category for "2012" is "72.9 (+5.6)"
	And the score for the "Clarity of Direction" category for "2011" is "67.3"
	And the Overall Average at the bottom for "2014" is "74.7 (-0.3)"
	And the Overall Average for "2013" is "75.0 (+1.8)"
	And the Overall Average for "2012" is "73.2 (+0.3)"
	And the Overall Average for "2011" is "72.9"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "83.6 (0.0)"
	And the score for "question 1" in the "2013" column is "83.6 (-1.2)"
	And the score for "question 1" in the "2012" column is "84.8"
	And there is no score for "question 1" in the "2011" column

	Now I go to "Functions" > "Logout"