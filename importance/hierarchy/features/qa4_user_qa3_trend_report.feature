Feature: Check Trend Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(119)"
	And the "Count" for the "2013" column is "(111)"
	And the "Count" for the "2012" column is "(109)"
	And the "Count" for the "2011" column is "(18)"	
	And the score for "question 1" in the "2014" column is "4.14 (-0.09)"
	And the score for "question 1" in the "2013" column is "4.23 (+0.13)"
	And the score for "question 1" in the "2012" column is "4.10"
	And there is no score for "question 1" in the "2011" column
	And the Overall Average at the bottom for "2014" is "4.05 (-0.04)"
	And the Overall Average for "2013" is "4.09 (+0.04)"
	And the Overall Average for "2012" is "4.05 (+0.33)"
	And the Overall Average for "2011" is "3.72"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(119)"
	And the "Count" for the "2013" column is "(111)"
	And the "Count" for the "2012" column is "(109)"
	And the "Count" for the "2011" column is "(18)"	
	And the score for the "Clarity of Direction" category for "2014" is "3.99 (-0.13)"
	And the score for the "Clarity of Direction" category for "2013" is "4.12 (+0.11)"
	And the score for the "Clarity of Direction" category for "2012" is "4.01 (+0.21)"
	And the score for the "Clarity of Direction" category for "2011" is "3.80"
	And the Overall Average at the bottom for "2014" is "4.05 (-0.04)"
	And the Overall Average for "2013" is "4.09 (+0.04)"
	And the Overall Average for "2012" is "4.05 (+0.33)"
	And the Overall Average for "2011" is "3.72"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.14 (-0.09)"
	And the score for "question 1" in the "2013" column is "4.23 (+0.13)"
	And the score for "question 1" in the "2012" column is "4.10"
	And there is no score for "question 1" in the "2011" column
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(119)"
	And the "Count" for the "2013" column is "(111)"
	And the "Count" for the "2012" column is "(109)"
	And the "Count" for the "2011" column is "(18)"	
	And the score for "question 1" in the "2014" column is "86.4 (-1.9)"
	And the score for "question 1" in the "2013" column is "88.3 (+2.2)"
	And the score for "question 1" in the "2012" column is "86.1"
	And there is no score for "question 1" in the "2011" column
	And the Overall Average at the bottom for "2014" is "80.4 (-1.4)"
	And the Overall Average for "2013" is "81.8 (+0.8)"
	And the Overall Average for "2012" is "81.0 (+12.9)"
	And the Overall Average for "2011" is "68.1"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(119)"
	And the "Count" for the "2013" column is "(111)"
	And the "Count" for the "2012" column is "(109)"
	And the "Count" for the "2011" column is "(18)"	
	And the score for the "Clarity of Direction" category for "2014" is "80.5 (-5.1)"
	And the score for the "Clarity of Direction" category for "2013" is "85.6 (+3.9)"
	And the score for the "Clarity of Direction" category for "2012" is "81.7 (+3.3)"
	And the score for the "Clarity of Direction" category for "2011" is "78.4"
	And the Overall Average at the bottom for "2014" is "80.4 (-1.4)"
	And the Overall Average for "2013" is "81.8 (+0.8)"
	And the Overall Average for "2012" is "81.0 (+12.9)"
	And the Overall Average for "2011" is "68.1"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "86.4 (-1.9)"
	And the score for "question 1" in the "2013" column is "88.3 (+2.2)"
	And the score for "question 1" in the "2012" column is "86.1"
	And there is no score for "question 1" in the "2011" column

	Now I go to "Functions" > "Logout"