Feature: Check Completion Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click on the "Completion Report" link under "Reference Reports"
	Then I am on the "Completion Report" page
@wip	
Scenario: I can check Completion Report
	Given there is a "By Demographic" tab in top left corner
	And there is a "By Time" tab in top left corner
	
	When I am on the "By Demographic" tab
	And I choose "Gender" from the View Type popdown
	Then the figure for "Male" in the "Invitees" column is "96"
	And the figure for "Male" in the "Respondents" column is "93(96.9%)"
	And the figure for "Male" in the "Completions" column is "92(95.8%)"
	And the figure for the "Totals" row in the "Invitees" column is "123"
	And the figure for the "Totals" row in the "Respondents" column is "119(96.7%)"
	And the figure for the "Totals" row in the "Completions" column is "118(95.9%)"

	When I am on the "By Time" tab
	Then the figure for "Monday, September 15, 2014" in the "Respondents" column is "16"
	And the figure for "Monday, September 15, 2014" in the "% Responded" column is "13.4%"
	And the figure for "Monday, September 15, 2014" in the "% Invited" column is "13.0%"
	And the figure for the "Totals" row in the "Respondents" column is "119"
	And the figure for the "Totals" row in the "% Responded" column is "100.0%"
	And the figure for the "Totals" row in the "% Invited" column is "96.7%"
	
	When I click on "Monday, September 15, 2014"
	Then the figure for "12:00 PM" in the "Respondents" column is "2"
	And the figure for "12:00 PM" in the "% Responded" column is "1.7%"
	And the figure for "12:00 PM" in the "% Invited" column is "1.6%"
	
	Now I go to "Functions" > "Logout"