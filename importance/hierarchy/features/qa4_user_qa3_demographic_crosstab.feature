Feature: Check Demographic Crosstab for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(93)"
	And the first number under "Female" is "(26)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.3"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.0"
	And the Group Average at the bottom for the "Female" column is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.09"
	And the score for question 1 in the "Female" column is "4.31"
	And the Weighted Average on the right for question 1 is "4.14"
	And the Group Average at the bottom for the "Male" column is "3.99"
	And the Group Average at the bottom for the "Female" column is "4.16"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83"
	And the score for question 1 in the "Female" column is "100"
	And the Weighted Average on the right for question 1 is "86"
	And the Group Average at the bottom for the "Male" column is "77"
	And the Group Average at the bottom for the "Female" column is "87"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "82.6"
	And the score for question 1 in the "Female" column is "100.0"
	And the Weighted Average on the right for question 1 is "86.4"
	And the Group Average at the bottom for the "Male" column is "77.3"
	And the Group Average at the bottom for the "Female" column is "86.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "82.61"
	And the score for question 1 in the "Female" column is "100.00"
	And the Weighted Average on the right for question 1 is "86.44"
	And the Group Average at the bottom for the "Male" column is "77.30"
	And the Group Average at the bottom for the "Female" column is "86.81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5"
	And the score for question 1 in the "Female" column is "0"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.4"
	And the score for question 1 in the "Female" column is "0.0"
	And the Weighted Average on the right for question 1 is "4.2"
	And the Group Average at the bottom for the "Male" column is "8.6"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.43"
	And the score for question 1 in the "Female" column is "0.00"
	And the Weighted Average on the right for question 1 is "4.24"
	And the Group Average at the bottom for the "Male" column is "8.57"
	And the Group Average at the bottom for the "Female" column is "4.29"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.0"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.98"
	And the score for question 1 in the "Female" column is "4.08"
	And the Weighted Average on the right for question 1 is "4.00"
	And the Group Average at the bottom for the "Male" column is "4.34"
	And the Group Average at the bottom for the "Female" column is "4.34"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80"
	And the score for question 1 in the "Female" column is "85"
	And the Weighted Average on the right for question 1 is "81"
	And the Group Average at the bottom for the "Male" column is "91"
	And the Group Average at the bottom for the "Female" column is "91"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80.4"
	And the score for question 1 in the "Female" column is "84.6"
	And the Weighted Average on the right for question 1 is "81.4"
	And the Group Average at the bottom for the "Male" column is "91.1"
	And the Group Average at the bottom for the "Female" column is "91.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80.43"
	And the score for question 1 in the "Female" column is "84.62"
	And the Weighted Average on the right for question 1 is "81.36"
	And the Group Average at the bottom for the "Male" column is "91.12"
	And the Group Average at the bottom for the "Female" column is "90.99"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2"
	And the score for question 1 in the "Female" column is "-5"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "8"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2.2"
	And the score for question 1 in the "Female" column is "-4.7"
	And the Weighted Average on the right for question 1 is "-2.8"
	And the Group Average at the bottom for the "Male" column is "7.6"
	And the Group Average at the bottom for the "Female" column is "3.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2.19"
	And the score for question 1 in the "Female" column is "-4.69"
	And the Weighted Average on the right for question 1 is "-2.80"
	And the Group Average at the bottom for the "Male" column is "7.60"
	And the Group Average at the bottom for the "Female" column is "3.91"


# Categories

Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(93)"
	And the number directly below "Female" is "(26)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.0"
	And the Group Average at the bottom for the "Female" column is "4.2"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1"
	And the score for question 1 in the "Female" column is "4.3"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.96"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.06"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.99"
	And the Group Average at the bottom for the "Male" column is "3.99"
	And the Group Average at the bottom for the "Female" column is "4.16"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.09"
	And the score for question 1 in the "Female" column is "4.31"
	And the Weighted Average on the right for question 1 is "4.14"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "78"
	And the score for the "Clarity of Direction" category in the "Female" column is "88"
	And the Weighted Average on the right for the "Clarity of Direction" category is "81"
	And the Group Average at the bottom for the "Male" column is "77"
	And the Group Average at the bottom for the "Female" column is "87"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83"
	And the score for question 1 in the "Female" column is "100"
	And the Weighted Average on the right for question 1 is "86"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "78.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80.5"
	And the Group Average at the bottom for the "Male" column is "77.3"
	And the Group Average at the bottom for the "Female" column is "86.8"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "82.6"
	And the score for question 1 in the "Female" column is "100.0"
	And the Weighted Average on the right for question 1 is "86.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "78.26"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.46"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80.51"
	And the Group Average at the bottom for the "Male" column is "77.30"
	And the Group Average at the bottom for the "Female" column is "86.81"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "82.61"
	And the score for question 1 in the "Female" column is "100.00"
	And the Weighted Average on the right for question 1 is "86.44"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9"
	And the score for the "Clarity of Direction" category in the "Female" column is "7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8"
	And the Group Average at the bottom for the "Male" column is "9"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5"
	And the score for question 1 in the "Female" column is "0"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8.7"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.3"
	And the Group Average at the bottom for the "Male" column is "8.6"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.4"
	And the score for question 1 in the "Female" column is "0.0"
	And the Weighted Average on the right for question 1 is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8.70"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.73"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.26"
	And the Group Average at the bottom for the "Male" column is "8.57"
	And the Group Average at the bottom for the "Female" column is "4.29"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.43"
	And the score for question 1 in the "Female" column is "0.00"
	And the Weighted Average on the right for question 1 is "4.24"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.2"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.3"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.0"
	And the score for question 1 in the "Female" column is "4.1"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.21"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.27"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.22"
	And the Group Average at the bottom for the "Male" column is "4.34"
	And the Group Average at the bottom for the "Female" column is "4.34"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.98"
	And the score for question 1 in the "Female" column is "4.08"
	And the Weighted Average on the right for question 1 is "4.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87"
	And the score for the "Clarity of Direction" category in the "Female" column is "89"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "91"
	And the Group Average at the bottom for the "Female" column is "91"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80"
	And the score for question 1 in the "Female" column is "85"
	And the Weighted Average on the right for question 1 is "81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "89.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.5"
	And the Group Average at the bottom for the "Male" column is "91.1"
	And the Group Average at the bottom for the "Female" column is "91.0"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80.4"
	And the score for question 1 in the "Female" column is "84.6"
	And the Weighted Average on the right for question 1 is "81.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.96"
	And the score for the "Clarity of Direction" category in the "Female" column is "89.42"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.50"
	And the Group Average at the bottom for the "Male" column is "91.12"
	And the Group Average at the bottom for the "Female" column is "90.99"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80.43"
	And the score for question 1 in the "Female" column is "84.62"
	And the Weighted Average on the right for question 1 is "81.36"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5"
	And the Group Average at the bottom for the "Male" column is "8"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2"
	And the score for question 1 in the "Female" column is "-5"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.5"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.9"
	And the Group Average at the bottom for the "Male" column is "7.6"
	And the Group Average at the bottom for the "Female" column is "3.9"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2.2"
	And the score for question 1 in the "Female" column is "-4.7"
	And the Weighted Average on the right for question 1 is "-2.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.26"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.48"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.85"
	And the Group Average at the bottom for the "Male" column is "7.60"
	And the Group Average at the bottom for the "Female" column is "3.91"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2.19"
	And the score for question 1 in the "Female" column is "-4.69"
	And the Weighted Average on the right for question 1 is "-2.80"



# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.1 (4.3)"
	And the score for question 1 in the "Female" column is "4.3 (4.2)"
	And the Weighted Average on the right for question 1 is "4.1"
	And the Group Average at the bottom for the "Male" column is "4.0 (4.1)"
	And the Group Average at the bottom for the "Female" column is "4.2 (4.1)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.09 (4.26)"
	And the score for question 1 in the "Female" column is "4.31 (4.15)"
	And the Weighted Average on the right for question 1 is "4.14"
	And the Group Average at the bottom for the "Male" column is "3.99 (4.08)"
	And the Group Average at the bottom for the "Female" column is "4.16 (4.12)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "83 (88)"
	And the score for question 1 in the "Female" column is "100 (88)"
	And the Weighted Average on the right for question 1 is "86"
	And the Group Average at the bottom for the "Male" column is "77 (81)"
	And the Group Average at the bottom for the "Female" column is "87 (86)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "82.6 (88.2)"
	And the score for question 1 in the "Female" column is "100.0 (88.5)"
	And the Weighted Average on the right for question 1 is "86.4"
	And the Group Average at the bottom for the "Male" column is "77.3 (80.5)"
	And the Group Average at the bottom for the "Female" column is "86.8 (86.1)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "82.61 (88.24)"
	And the score for question 1 in the "Female" column is "100.00 (88.46)"
	And the Weighted Average on the right for question 1 is "86.44"
	And the Group Average at the bottom for the "Male" column is "77.30 (80.53)"
	And the Group Average at the bottom for the "Female" column is "86.81 (86.10)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5 (2)"
	And the score for question 1 in the "Female" column is "0 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "9 (6)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.4 (2.4)"
	And the score for question 1 in the "Female" column is "0.0 (3.8)"
	And the Weighted Average on the right for question 1 is "4.2"
	And the Group Average at the bottom for the "Male" column is "8.6 (6.3)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "5.43 (2.35)"
	And the score for question 1 in the "Female" column is "0.00 (3.85)"
	And the Weighted Average on the right for question 1 is "4.24"
	And the Group Average at the bottom for the "Male" column is "8.57 (6.26)"
	And the Group Average at the bottom for the "Female" column is "4.29 (4.34)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.0 (4.0)"
	And the score for question 1 in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for question 1 is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "3.98 (4.00)"
	And the score for question 1 in the "Female" column is "4.08 (4.08)"
	And the Weighted Average on the right for question 1 is "4.00"
	And the Group Average at the bottom for the "Male" column is "4.34 (4.31)"
	And the Group Average at the bottom for the "Female" column is "4.34 (4.27)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80 (79)"
	And the score for question 1 in the "Female" column is "85 (88)"
	And the Weighted Average on the right for question 1 is "81"
	And the Group Average at the bottom for the "Male" column is "91 (91)"
	And the Group Average at the bottom for the "Female" column is "91 (89)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80.4 (78.8)"
	And the score for question 1 in the "Female" column is "84.6 (88.5)"
	And the Weighted Average on the right for question 1 is "81.4"
	And the Group Average at the bottom for the "Male" column is "91.1 (90.7)"
	And the Group Average at the bottom for the "Female" column is "91.0 (89.3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "80.43 (78.82)"
	And the score for question 1 in the "Female" column is "84.62 (88.46)"
	And the Weighted Average on the right for question 1 is "81.36"
	And the Group Average at the bottom for the "Male" column is "91.12 (90.73)"
	And the Group Average at the bottom for the "Female" column is "90.99 (89.33)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2 (-5)"
	And the score for question 1 in the "Female" column is "-5 (-1)"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "8 (5)"
	And the Group Average at the bottom for the "Female" column is "4 (3)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2.2 (-5.2)"
	And the score for question 1 in the "Female" column is "-4.7 (-1.4)"
	And the Weighted Average on the right for question 1 is "-2.8"
	And the Group Average at the bottom for the "Male" column is "7.6 (5.0)"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-2.19 (-5.20)"
	And the score for question 1 in the "Female" column is "-4.69 (-1.43)"
	And the Weighted Average on the right for question 1 is "-2.80"
	And the Group Average at the bottom for the "Male" column is "7.60 (4.96)"
	And the Group Average at the bottom for the "Female" column is "3.91 (3.20)"


# Categories + trend
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.0 (4.1)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "4.0 (4.1)"
	And the Group Average at the bottom for the "Female" column is "4.2 (4.1)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.1 (4.3)"
	And the score for question 1 in the "Female" column is "4.3 (4.2)"
	And the Weighted Average on the right for question 1 is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.96 (4.13)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.06 (4.11)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "3.99"
	And the Group Average at the bottom for the "Male" column is "3.99 (4.08)"
	And the Group Average at the bottom for the "Female" column is "4.16 (4.12)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.09 (4.26)"
	And the score for question 1 in the "Female" column is "4.31 (4.15)"
	And the Weighted Average on the right for question 1 is "4.14"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "78 (85)"
	And the score for the "Clarity of Direction" category in the "Female" column is "88 (88)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "81"
	And the Group Average at the bottom for the "Male" column is "77 (81)"
	And the Group Average at the bottom for the "Female" column is "87 (86)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "83 (88)"
	And the score for question 1 in the "Female" column is "100 (88)"
	And the Weighted Average on the right for question 1 is "86"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "78.3 (84.7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.5 (88.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80.5"
	And the Group Average at the bottom for the "Male" column is "77.3 (80.5)"
	And the Group Average at the bottom for the "Female" column is "86.8 (86.1)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "82.6 (88.2)"
	And the score for question 1 in the "Female" column is "100.0 (88.5)"
	And the Weighted Average on the right for question 1 is "86.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "78.26 (84.71)"
	And the score for the "Clarity of Direction" category in the "Female" column is "88.46 (88.46)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "80.51"
	And the Group Average at the bottom for the "Male" column is "77.30 (80.53)"
	And the Group Average at the bottom for the "Female" column is "86.81 (86.10)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "82.61 (88.24)"
	And the score for question 1 in the "Female" column is "100.00 (88.46)"
	And the Weighted Average on the right for question 1 is "86.44"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9 (5)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7 (3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8"
	And the Group Average at the bottom for the "Male" column is "9 (6)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5 (2)"
	And the score for question 1 in the "Female" column is "0 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8.7 (5.0)"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.7 (2.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.3"
	And the Group Average at the bottom for the "Male" column is "8.6 (6.3)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.4 (2.4)"
	And the score for question 1 in the "Female" column is "0.0 (3.8)"
	And the Weighted Average on the right for question 1 is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "8.70 (5.00)"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.73 (2.88)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.26"
	And the Group Average at the bottom for the "Male" column is "8.57 (6.26)"
	And the Group Average at the bottom for the "Female" column is "4.29 (4.34)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "5.43 (2.35)"
	And the score for question 1 in the "Female" column is "0.00 (3.85)"
	And the Weighted Average on the right for question 1 is "4.24"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.2 (4.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.3 (4.2)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.3 (4.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.0 (4.0)"
	And the score for question 1 in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for question 1 is "4.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.21 (4.18)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.27 (4.21)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.22"
	And the Group Average at the bottom for the "Male" column is "4.34 (4.31)"
	And the Group Average at the bottom for the "Female" column is "4.34 (4.27)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "3.98 (4.00)"
	And the score for question 1 in the "Female" column is "4.08 (4.08)"
	And the Weighted Average on the right for question 1 is "4.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87 (84)"
	And the score for the "Clarity of Direction" category in the "Female" column is "89 (92)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "88"
	And the Group Average at the bottom for the "Male" column is "91 (91)"
	And the Group Average at the bottom for the "Female" column is "91 (89)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80 (79)"
	And the score for question 1 in the "Female" column is "85 (88)"
	And the Weighted Average on the right for question 1 is "81"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87.0 (84.4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "89.4 (92.3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.5"
	And the Group Average at the bottom for the "Male" column is "91.1 (90.7)"
	And the Group Average at the bottom for the "Female" column is "91.0 (89.3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80.4 (78.8)"
	And the score for question 1 in the "Female" column is "84.6 (88.5)"
	And the Weighted Average on the right for question 1 is "81.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "86.96 (84.41)"
	And the score for the "Clarity of Direction" category in the "Female" column is "89.42 (92.31)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "87.50"
	And the Group Average at the bottom for the "Male" column is "91.12 (90.73)"
	And the Group Average at the bottom for the "Female" column is "90.99 (89.33)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "80.43 (78.82)"
	And the score for question 1 in the "Female" column is "84.62 (88.46)"
	And the Weighted Average on the right for question 1 is "81.36"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5 (1)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (2)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "5"
	And the Group Average at the bottom for the "Male" column is "8 (5)"
	And the Group Average at the bottom for the "Female" column is "4 (3)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2 (-5)"
	And the score for question 1 in the "Female" column is "-5 (-1)"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.3 (1.0)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.5 (2.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.9"
	And the Group Average at the bottom for the "Male" column is "7.6 (5.0)"
	And the Group Average at the bottom for the "Female" column is "3.9 (3.2)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2.2 (-5.2)"
	And the score for question 1 in the "Female" column is "-4.7 (-1.4)"
	And the Weighted Average on the right for question 1 is "-2.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "5.26 (1.04)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.48 (2.10)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.85"
	And the Group Average at the bottom for the "Male" column is "7.60 (4.96)"
	And the Group Average at the bottom for the "Female" column is "3.91 (3.20)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-2.19 (-5.20)"
	And the score for question 1 in the "Female" column is "-4.69 (-1.43)"
	And the Weighted Average on the right for question 1 is "-2.80"



	Now I go to "Functions" > "Logout"