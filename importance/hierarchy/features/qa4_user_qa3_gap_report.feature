Feature: Check Gap Report for user with a hierarchy-based view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa3"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparsion" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "4.14"
	And the Importance for question 1 is "4.00"
	And the Gap for question 1 is "-0.14"
	And the Weighted Gap for question 1 is "-2.80"
	And the Overall Average at the bottom has an Eval Avg of "4.02"
	And the Overall Average has a Importance of "4.34"
	And the Overall Average has a Gap of "0.32"
	And the Overall Average has a Weighted Gap of "6.94"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "3.99"
	And the Importance for the "Clarity of Direction" category is "4.22"
	And the Gap for the "Clarity of Direction" category is "0.23"
	And the Weighted Gap for the "Clarity of Direction" category is "4.85"
	And the Overall Average at the bottom has an Eval Avg of "4.02"
	And the Overall Average has a Importance of "4.34"
	And the Overall Average has a Gap of "0.32"
	And the Overall Average has a Weighted Gap of "6.94"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.14"
	And the Importance for question 1 is "4.00"
	And the Gap for question 1 is "-0.14"
	And the Weighted Gap for question 1 is "-2.80"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the number for "Your view" in the upper right is "(119)"
	And the number for "Organization" in the upper right is "(1136)"
	And the Eval Avg for question 1 is "4.14 (+0.02)"
	And the Importance for question 1 is "4.00 (-0.10)"
	And the Gap for question 1 is "-0.14 (-0.12)"
	And the Weighted Gap for question 1 is "-2.80 (-2.39)"
	And the Overall Average at the bottom has an Eval Avg of "4.02 (+0.07)"
	And the Overall Average has a Importance of "4.34 (+0.01)"
	And the Overall Average has a Gap of "0.32 (-0.06)"
	And the Overall Average has a Weighted Gap of "6.94 (-1.29)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the Eval Avg for the "Clarity of Direction" category is "3.99 (+0.03)"
	And the Importance for the "Clarity of Direction" category is "4.22 (-0.04)"
	And the Gap for the "Clarity of Direction" category is "0.23 (-0.07)"
	And the Weighted Gap for the "Clarity of Direction" category is "4.85 (-1.54)"
	And the Overall Average at the bottom has an Eval Avg of "4.02 (+0.07)"
	And the Overall Average has a Importance of "4.34 (+0.01)"
	And the Overall Average has a Gap of "0.32 (-0.06)"
	And the Overall Average has a Weighted Gap of "6.94 (-1.29)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.14 (+0.02)"
	And the Importance for question 1 is "4.00 (-0.10)"
	And the Gap for question 1 is "-0.14 (-0.12)"
	And the Weighted Gap for question 1 is "-2.80 (-2.39)"
	
	Now I go to "Functions" > "Logout"