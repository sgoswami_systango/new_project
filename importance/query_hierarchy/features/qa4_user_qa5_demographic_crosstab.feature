Feature: Check Demographic Crosstab for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa5"
	And I click on the "Demographic Crosstab" link under "Comparison Reports"
	Then I am on the "Demographic Crosstab" page
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	
# Questions

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the first number under "Male" is "(27)"
	And the first number under "Female" is "(18)"
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.2"
	And the score for question 1 in the "Female" column is "4.5"
	And the Weighted Average on the right for question 1 is "4.3"
	And the Group Average at the bottom for the "Male" column is "3.8"
	And the Group Average at the bottom for the "Female" column is "4.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.22"
	And the score for question 1 in the "Female" column is "4.50"
	And the Weighted Average on the right for question 1 is "4.33"
	And the Group Average at the bottom for the "Male" column is "3.77"
	And the Group Average at the bottom for the "Female" column is "4.10"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "89"
	And the score for question 1 in the "Female" column is "89"
	And the Weighted Average on the right for question 1 is "89"
	And the Group Average at the bottom for the "Male" column is "69"
	And the Group Average at the bottom for the "Female" column is "83"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "88.9"
	And the score for question 1 in the "Female" column is "88.9"
	And the Weighted Average on the right for question 1 is "88.9"
	And the Group Average at the bottom for the "Male" column is "68.7"
	And the Group Average at the bottom for the "Female" column is "82.5"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "88.89"
	And the score for question 1 in the "Female" column is "88.89"
	And the Weighted Average on the right for question 1 is "88.89"
	And the Group Average at the bottom for the "Male" column is "68.68"
	And the Group Average at the bottom for the "Female" column is "82.54"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "0"
	And the Weighted Average on the right for question 1 is "0"
	And the Group Average at the bottom for the "Male" column is "12"
	And the Group Average at the bottom for the "Female" column is "8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "0.0"
	And the Weighted Average on the right for question 1 is "0.0"
	And the Group Average at the bottom for the "Male" column is "11.6"
	And the Group Average at the bottom for the "Female" column is "8.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "0.00"
	And the Weighted Average on the right for question 1 is "0.00"
	And the Group Average at the bottom for the "Male" column is "11.64"
	And the Group Average at the bottom for the "Female" column is "8.25"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.0"
	And the score for question 1 in the "Female" column is "4.4"
	And the Weighted Average on the right for question 1 is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.04"
	And the score for question 1 in the "Female" column is "4.39"
	And the Weighted Average on the right for question 1 is "4.18"
	And the Group Average at the bottom for the "Male" column is "4.28"
	And the Group Average at the bottom for the "Female" column is "4.42"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "70"
	And the score for question 1 in the "Female" column is "89"
	And the Weighted Average on the right for question 1 is "78"
	And the Group Average at the bottom for the "Male" column is "88"
	And the Group Average at the bottom for the "Female" column is "93"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "70.4"
	And the score for question 1 in the "Female" column is "88.9"
	And the Weighted Average on the right for question 1 is "77.8"
	And the Group Average at the bottom for the "Male" column is "88.3"
	And the Group Average at the bottom for the "Female" column is "92.7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "70.37"
	And the score for question 1 in the "Female" column is "88.89"
	And the Weighted Average on the right for question 1 is "77.78"
	And the Group Average at the bottom for the "Male" column is "88.25"
	And the Group Average at the bottom for the "Female" column is "92.70"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-4"
	And the score for question 1 in the "Female" column is "-2"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "11"
	And the Group Average at the bottom for the "Female" column is "7"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-3.6"
	And the score for question 1 in the "Female" column is "-2.4"
	And the Weighted Average on the right for question 1 is "-3.1"
	And the Group Average at the bottom for the "Male" column is "10.9"
	And the Group Average at the bottom for the "Female" column is "7.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-3.64"
	And the score for question 1 in the "Female" column is "-2.41"
	And the Weighted Average on the right for question 1 is "-3.13"
	And the Group Average at the bottom for the "Male" column is "10.91"
	And the Group Average at the bottom for the "Female" column is "7.07"

# Categories
	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the number directly below "Male" is "(27)"
	And the number directly below "Female" is "(18)"
	And the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "3.8"
	And the Group Average at the bottom for the "Female" column is "4.1"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.2"
	And the score for question 1 in the "Female" column is "4.5"
	And the Weighted Average on the right for question 1 is "4.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.97"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.11"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.03"
	And the Group Average at the bottom for the "Male" column is "3.77"
	And the Group Average at the bottom for the "Female" column is "4.10"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.22"
	And the score for question 1 in the "Female" column is "4.50"
	And the Weighted Average on the right for question 1 is "4.33"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76"
	And the score for the "Clarity of Direction" category in the "Female" column is "79"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77"
	And the Group Average at the bottom for the "Male" column is "69"
	And the Group Average at the bottom for the "Female" column is "83"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "89"
	And the score for question 1 in the "Female" column is "89"
	And the Weighted Average on the right for question 1 is "89"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75.9"
	And the score for the "Clarity of Direction" category in the "Female" column is "79.2"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77.2"
	And the Group Average at the bottom for the "Male" column is "68.7"
	And the Group Average at the bottom for the "Female" column is "82.5"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "88.9"
	And the score for question 1 in the "Female" column is "88.9"
	And the Weighted Average on the right for question 1 is "88.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75.93"
	And the score for the "Clarity of Direction" category in the "Female" column is "79.17"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77.22"
	And the Group Average at the bottom for the "Male" column is "68.68"
	And the Group Average at the bottom for the "Female" column is "82.54"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "88.89"
	And the score for question 1 in the "Female" column is "88.89"
	And the Weighted Average on the right for question 1 is "88.89"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9"
	And the score for the "Clarity of Direction" category in the "Female" column is "8"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Male" column is "12"
	And the Group Average at the bottom for the "Female" column is "8"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0"
	And the score for question 1 in the "Female" column is "0"
	And the Weighted Average on the right for question 1 is "0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "8.3"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.9"
	And the Group Average at the bottom for the "Male" column is "11.6"
	And the Group Average at the bottom for the "Female" column is "8.3"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0"
	And the score for question 1 in the "Female" column is "0.0"
	And the Weighted Average on the right for question 1 is "0.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.26"
	And the score for the "Clarity of Direction" category in the "Female" column is "8.33"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.89"
	And the Group Average at the bottom for the "Male" column is "11.64"
	And the Group Average at the bottom for the "Female" column is "8.25"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00"
	And the score for question 1 in the "Female" column is "0.00"
	And the Weighted Average on the right for question 1 is "0.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4"
	And the score for the "Clarity of Direction" category in the "Female" column is "4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4"
	And the Group Average at the bottom for the "Female" column is "4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4"
	And the score for question 1 in the "Female" column is "4"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.3"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.4"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.3"
	And the Group Average at the bottom for the "Male" column is "4.3"
	And the Group Average at the bottom for the "Female" column is "4.4"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.0"
	And the score for question 1 in the "Female" column is "4.4"
	And the Weighted Average on the right for question 1 is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.30"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.42"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.34"
	And the Group Average at the bottom for the "Male" column is "4.28"
	And the Group Average at the bottom for the "Female" column is "4.42"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.04"
	And the score for question 1 in the "Female" column is "4.39"
	And the Weighted Average on the right for question 1 is "4.18"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88"
	And the score for the "Clarity of Direction" category in the "Female" column is "92"
	And the Weighted Average on the right for the "Clarity of Direction" category is "89"
	And the Group Average at the bottom for the "Male" column is "88"
	And the Group Average at the bottom for the "Female" column is "93"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "70"
	And the score for question 1 in the "Female" column is "89"
	And the Weighted Average on the right for question 1 is "78"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.0"
	And the score for the "Clarity of Direction" category in the "Female" column is "91.7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "89.4"
	And the Group Average at the bottom for the "Male" column is "88.3"
	And the Group Average at the bottom for the "Female" column is "92.7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "70.4"
	And the score for question 1 in the "Female" column is "88.9"
	And the Weighted Average on the right for question 1 is "77.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87.96"
	And the score for the "Clarity of Direction" category in the "Female" column is "91.67"
	And the Weighted Average on the right for the "Clarity of Direction" category is "89.44"
	And the Group Average at the bottom for the "Male" column is "88.25"
	And the Group Average at the bottom for the "Female" column is "92.70"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "70.37"
	And the score for question 1 in the "Female" column is "88.89"
	And the Weighted Average on the right for question 1 is "77.78"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7"
	And the score for the "Clarity of Direction" category in the "Female" column is "7"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Male" column is "11"
	And the Group Average at the bottom for the "Female" column is "7"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-4"
	And the score for question 1 in the "Female" column is "-2"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7.1"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.9"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.7"
	And the Group Average at the bottom for the "Male" column is "10.9"
	And the Group Average at the bottom for the "Female" column is "7.1"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-3.6"
	And the score for question 1 in the "Female" column is "-2.4"
	And the Weighted Average on the right for question 1 is "-3.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7.10"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.85"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.73"
	And the Group Average at the bottom for the "Male" column is "10.91"
	And the Group Average at the bottom for the "Female" column is "7.07"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-3.64"
	And the score for question 1 in the "Female" column is "-2.41"
	And the Weighted Average on the right for question 1 is "-3.13"


# Questions + trend
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.2 (4.1)"
	And the score for question 1 in the "Female" column is "4.5 (4.2)"
	And the Weighted Average on the right for question 1 is "4.3"
	And the Group Average at the bottom for the "Male" column is "3.8 (3.9)"
	And the Group Average at the bottom for the "Female" column is "4.1 (4.0)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.22 (4.12)"
	And the score for question 1 in the "Female" column is "4.50 (4.19)"
	And the Weighted Average on the right for question 1 is "4.33"
	And the Group Average at the bottom for the "Male" column is "3.77 (3.93)"
	And the Group Average at the bottom for the "Female" column is "4.10 (3.98)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "89 (84)"
	And the score for question 1 in the "Female" column is "89 (90)"
	And the Weighted Average on the right for question 1 is "89"
	And the Group Average at the bottom for the "Male" column is "69 (75)"
	And the Group Average at the bottom for the "Female" column is "83 (78)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "88.9 (84.4)"
	And the score for question 1 in the "Female" column is "88.9 (89.8)"
	And the Weighted Average on the right for question 1 is "88.9"
	And the Group Average at the bottom for the "Male" column is "68.7 (74.6)"
	And the Group Average at the bottom for the "Female" column is "82.5 (78.2)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "88.89 (84.42)"
	And the score for question 1 in the "Female" column is "88.89 (89.83)"
	And the Weighted Average on the right for question 1 is "88.89"
	And the Group Average at the bottom for the "Male" column is "68.68 (74.57)"
	And the Group Average at the bottom for the "Female" column is "82.54 (78.18)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0 (1)"
	And the score for question 1 in the "Female" column is "0 (3)"
	And the Weighted Average on the right for question 1 is "0"
	And the Group Average at the bottom for the "Male" column is "12 (8)"
	And the Group Average at the bottom for the "Female" column is "8 (7)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.0 (1.3)"
	And the score for question 1 in the "Female" column is "0.0 (3.4)"
	And the Weighted Average on the right for question 1 is "0.0"
	And the Group Average at the bottom for the "Male" column is "11.6 (8.1)"
	And the Group Average at the bottom for the "Female" column is "8.3 (7.4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "0.00 (1.30)"
	And the score for question 1 in the "Female" column is "0.00 (3.39)"
	And the Weighted Average on the right for question 1 is "0.00"
	And the Group Average at the bottom for the "Male" column is "11.64 (8.09)"
	And the Group Average at the bottom for the "Female" column is "8.25 (7.41)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.0 (3.9)"
	And the score for question 1 in the "Female" column is "4.4 (4.1)"
	And the Weighted Average on the right for question 1 is "4.2"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.4 (4.4)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "4.04 (3.88)"
	And the score for question 1 in the "Female" column is "4.39 (4.14)"
	And the Weighted Average on the right for question 1 is "4.18"
	And the Group Average at the bottom for the "Male" column is "4.28 (4.31)"
	And the Group Average at the bottom for the "Female" column is "4.42 (4.35)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "70 (73)"
	And the score for question 1 in the "Female" column is "89 (82)"
	And the Weighted Average on the right for question 1 is "78"
	And the Group Average at the bottom for the "Male" column is "88 (91)"
	And the Group Average at the bottom for the "Female" column is "93 (92)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "70.4 (72.7)"
	And the score for question 1 in the "Female" column is "88.9 (82.2)"
	And the Weighted Average on the right for question 1 is "77.8"
	And the Group Average at the bottom for the "Male" column is "88.3 (91.2)"
	And the Group Average at the bottom for the "Female" column is "92.7 (91.8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "70.37 (72.73)"
	And the score for question 1 in the "Female" column is "88.89 (82.20)"
	And the Weighted Average on the right for question 1 is "77.78"
	And the Group Average at the bottom for the "Male" column is "88.25 (91.24)"
	And the Group Average at the bottom for the "Female" column is "92.70 (91.77)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-4 (-5)"
	And the score for question 1 in the "Female" column is "-2 (-1)"
	And the Weighted Average on the right for question 1 is "-3"
	And the Group Average at the bottom for the "Male" column is "11 (8)"
	And the Group Average at the bottom for the "Female" column is "7 (8)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-3.6 (-4.7)"
	And the score for question 1 in the "Female" column is "-2.4 (-1.0)"
	And the Weighted Average on the right for question 1 is "-3.1"
	And the Group Average at the bottom for the "Male" column is "10.9 (8.2)"
	And the Group Average at the bottom for the "Female" column is "7.1 (8.0)"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And check "Show trend score:" in the top middle
	And I am on the "Questions" tab
	Then the score for question 1 in the "Male" column is "-3.64 (-4.66)"
	And the score for question 1 in the "Female" column is "-2.41 (-1.03)"
	And the Weighted Average on the right for question 1 is "-3.13"
	And the Group Average at the bottom for the "Male" column is "10.91 (8.19)"
	And the Group Average at the bottom for the "Female" column is "7.07 (8.05)"

# Categories + trend
@wip	
Scenario: I can check Demographic Crosstab Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.0 (4.0)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.1 (4.1)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.0"
	And the Group Average at the bottom for the "Male" column is "3.8 (3.9)"
	And the Group Average at the bottom for the "Female" column is "4.1 (4.0)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.2 (4.1)"
	And the score for question 1 in the "Female" column is "4.5 (4.2)"
	And the Weighted Average on the right for question 1 is "4.3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "3.97 (4.04)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.11 (4.05)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.03"
	And the Group Average at the bottom for the "Male" column is "3.77 (3.93)"
	And the Group Average at the bottom for the "Female" column is "4.10 (3.98)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.22 (4.12)"
	And the score for question 1 in the "Female" column is "4.50 (4.19)"
	And the Weighted Average on the right for question 1 is "4.33"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "76 (82)"
	And the score for the "Clarity of Direction" category in the "Female" column is "79 (84)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77"
	And the Group Average at the bottom for the "Male" column is "69 (75)"
	And the Group Average at the bottom for the "Female" column is "83 (78)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "89 (84)"
	And the score for question 1 in the "Female" column is "89 (90)"
	And the Weighted Average on the right for question 1 is "89"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75.9 (82.5)"
	And the score for the "Clarity of Direction" category in the "Female" column is "79.2 (83.7)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77.2"
	And the Group Average at the bottom for the "Male" column is "68.7 (74.6)"
	And the Group Average at the bottom for the "Female" column is "82.5 (78.2)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "88.9 (84.4)"
	And the score for question 1 in the "Female" column is "88.9 (89.8)"
	And the Weighted Average on the right for question 1 is "88.9"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "75.93 (82.47)"
	And the score for the "Clarity of Direction" category in the "Female" column is "79.17 (83.69)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "77.22"
	And the Group Average at the bottom for the "Male" column is "68.68 (74.57)"
	And the Group Average at the bottom for the "Female" column is "82.54 (78.18)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "88.89 (84.42)"
	And the score for question 1 in the "Female" column is "88.89 (89.83)"
	And the Weighted Average on the right for question 1 is "88.89"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "8 (6)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "9"
	And the Group Average at the bottom for the "Male" column is "12 (8)"
	And the Group Average at the bottom for the "Female" column is "8 (7)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0 (1)"
	And the score for question 1 in the "Female" column is "0 (3)"
	And the Weighted Average on the right for question 1 is "0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.3 (3.9)"
	And the score for the "Clarity of Direction" category in the "Female" column is "8.3 (5.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.9"
	And the Group Average at the bottom for the "Male" column is "11.6 (8.1)"
	And the Group Average at the bottom for the "Female" column is "8.3 (7.4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.0 (1.3)"
	And the score for question 1 in the "Female" column is "0.0 (3.4)"
	And the Weighted Average on the right for question 1 is "0.0"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Evaluation by % Unfavorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "9.26 (3.90)"
	And the score for the "Clarity of Direction" category in the "Female" column is "8.33 (5.93)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "8.89"
	And the Group Average at the bottom for the "Male" column is "11.64 (8.09)"
	And the Group Average at the bottom for the "Female" column is "8.25 (7.41)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "0.00 (1.30)"
	And the score for question 1 in the "Female" column is "0.00 (3.39)"
	And the Weighted Average on the right for question 1 is "0.00"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4 (4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4"
	And the Group Average at the bottom for the "Male" column is "4 (4)"
	And the Group Average at the bottom for the "Female" column is "4 (4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4 (4)"
	And the score for question 1 in the "Female" column is "4 (4)"
	And the Weighted Average on the right for question 1 is "4"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.3 (4.2)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.4 (4.3)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.3"
	And the Group Average at the bottom for the "Male" column is "4.3 (4.3)"
	And the Group Average at the bottom for the "Female" column is "4.4 (4.4)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.0 (3.9)"
	And the score for question 1 in the "Female" column is "4.4 (4.1)"
	And the Weighted Average on the right for question 1 is "4.2"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by Mean" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "4.30 (4.20)"
	And the score for the "Clarity of Direction" category in the "Female" column is "4.42 (4.26)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "4.34"
	And the Group Average at the bottom for the "Male" column is "4.28 (4.31)"
	And the Group Average at the bottom for the "Female" column is "4.42 (4.35)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "4.04 (3.88)"
	And the score for question 1 in the "Female" column is "4.39 (4.14)"
	And the Weighted Average on the right for question 1 is "4.18"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88 (87)"
	And the score for the "Clarity of Direction" category in the "Female" column is "92 (88)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "89"
	And the Group Average at the bottom for the "Male" column is "88 (91)"
	And the Group Average at the bottom for the "Female" column is "93 (92)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "70 (73)"
	And the score for question 1 in the "Female" column is "89 (82)"
	And the Weighted Average on the right for question 1 is "78"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "88.0 (86.7)"
	And the score for the "Clarity of Direction" category in the "Female" column is "91.7 (87.9)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "89.4"
	And the Group Average at the bottom for the "Male" column is "88.3 (91.2)"
	And the Group Average at the bottom for the "Female" column is "92.7 (91.8)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "70.4 (72.7)"
	And the score for question 1 in the "Female" column is "88.9 (82.2)"
	And the Weighted Average on the right for question 1 is "77.8"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Importance by % Favorable" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "87.96 (86.69)"
	And the score for the "Clarity of Direction" category in the "Female" column is "91.67 (87.92)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "89.44"
	And the Group Average at the bottom for the "Male" column is "88.25 (91.24)"
	And the Group Average at the bottom for the "Female" column is "92.70 (91.77)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "70.37 (72.73)"
	And the score for question 1 in the "Female" column is "88.89 (82.20)"
	And the Weighted Average on the right for question 1 is "77.78"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "0" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7 (3)"
	And the score for the "Clarity of Direction" category in the "Female" column is "7 (4)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "7"
	And the Group Average at the bottom for the "Male" column is "11 (8)"
	And the Group Average at the bottom for the "Female" column is "7 (8)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-4 (-5)"
	And the score for question 1 in the "Female" column is "-2 (-1)"
	And the Weighted Average on the right for question 1 is "-3"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "1" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7.1 (3.4)"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.9 (4.5)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.7"
	And the Group Average at the bottom for the "Male" column is "10.9 (8.2)"
	And the Group Average at the bottom for the "Female" column is "7.1 (8.0)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-3.6 (-4.7)"
	And the score for question 1 in the "Female" column is "-2.4 (-1.0)"
	And the Weighted Average on the right for question 1 is "-3.1"

	When I select "Gender" from the "Demographic Selection" popdown in the top left corner
	And select "Weighted Gap" from the "Type of Score:" popdown in the top middle
	And select "2" from the "Number of Decimal Places:" popdown in the top middle	
	And I check "Show trend score:" in the top middle
	And I am on the "Categories" tab
	Then the score for the "Clarity of Direction" category in the "Male" column is "7.10 (3.36)"
	And the score for the "Clarity of Direction" category in the "Female" column is "6.85 (4.47)"
	And the Weighted Average on the right for the "Clarity of Direction" category is "6.73"
	And the Group Average at the bottom for the "Male" column is "10.91 (8.19)"
	And the Group Average at the bottom for the "Female" column is "7.07 (8.05)"

	When I click on the "Clarity of Direction" category
	Then the score for question 1 in the "Male" column is "-3.64 (-4.66)"
	And the score for question 1 in the "Female" column is "-2.41 (-1.03)"
	And the Weighted Average on the right for question 1 is "-3.13"



	Now I go to "Functions" > "Logout"