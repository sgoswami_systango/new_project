Feature: Check Gap Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa5"
	And I click the "Gap Report" link
	Then I am on the "Gap Report" page
	
Scenario: I can check Gap Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "No Comparison" tab in top right corner
	And there is a "Compared to:" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for question 1 is "4.33"
	And the Importance for question 1 is "4.18"
	And the Gap for question 1 is "-0.15"
	And the Weighted Gap for question 1 is "-3.13"
	And the Overall Average at the bottom has an Eval Avg of "3.90"
	And the Overall Average has a Importance of "4.34"
	And the Overall Average has a Gap of "0.44"
	And the Overall Average has a Weighted Gap of "9.55"
	
	When I am on the "Categories" tab
	And I am on the "No Comparison" tab
	Then the Eval Avg for the "Clarity of Direction" category is "4.03"
	And the Importance for the "Clarity of Direction" category is "4.34"
	And the Gap for the "Clarity of Direction" category is "0.31"
	And the Weighted Gap for the "Clarity of Direction" category is "6.73"
	And the Overall Average at the bottom has an Eval Avg of "3.90"
	And the Overall Average has a Importance of "4.34"
	And the Overall Average has a Gap of "0.44"
	And the Overall Average has a Weighted Gap of "9.55"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.33"
	And the Importance for question 1 is "4.18"
	And the Gap for question 1 is "-0.15"
	And the Weighted Gap for question 1 is "-3.13"
	
	When I am on the "Questions" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the number for "Your view" in the upper right is "(45)"
	And the number for "Organization" in the upper right is "(1136)"
	And the Eval Avg for question 1 is "4.33 (+0.21)"
	And the Importance for question 1 is "4.18 (+0.08)"
	And the Gap for question 1 is "-0.15 (-0.13)"
	And the Weighted Gap for question 1 is "-3.13 (-2.72)"
	And the Overall Average at the bottom has an Eval Avg of "3.90 (-0.05)"
	And the Overall Average has a Importance of "4.34 (+0.01)"
	And the Overall Average has a Gap of "0.44 (+0.06)"
	And the Overall Average has a Weighted Gap of "9.55 (+1.32)"
	
	When I am on the "Categories" tab
	And I am on the "Compared to:" tab
	And select "Organization" from the "Compared to:" popdown
	Then the Eval Avg for the "Clarity of Direction" category is "4.03 (+0.07)"
	And the Importance for the "Clarity of Direction" category is "4.34 (+0.08)"
	And the Gap for the "Clarity of Direction" category is "0.31 (+0.01)"
	And the Weighted Gap for the "Clarity of Direction" category is "6.73 (+0.34)"
	And the Overall Average at the bottom has an Eval Avg of "3.90 (-0.05)"
	And the Overall Average has a Importance of "4.34 (+0.01)"
	And the Overall Average has a Gap of "0.44 (+0.06)"
	And the Overall Average has a Weighted Gap of "9.55 (+1.32)"
	
	When I click on the "Clarity of Direction" category
	Then the Eval Avg for question 1 is "4.33 (+0.21)"
	And the Importance for question 1 is "4.18 (+0.08)"
	And the Gap for question 1 is "-0.15 (-0.13)"
	And the Weighted Gap for question 1 is "-3.13 (-2.72)"
	
	Now I go to "Functions" > "Logout"