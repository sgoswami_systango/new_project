Feature: Check Trend Report for user with a blended (query- and hierarchy-based) view

Background:
	Given I am logged into "https://perceptyx.com/qa4/" as user "qa5"
	And I click the "Trend Report" link
	Then I am on the "Trend Report" page

Scenario: I can check Trend Report
	Given there is a "Questions" tab in top left corner
	And there is a "Categories" tab in top left corner
	And there is a "Evaluation" tab in top right corner
	And there is a "Favorability" tab in top right corner
	
	When I am on the "Questions" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(45)"
	And the "Count" for the "2013" column is "(199)"
	And the "Count" for the "2012" column is "(224)"
	And the "Count" for the "2011" column is "(34)"	
	And the score for "question 1" in the "2014" column is "4.33 (+0.17)"
	And the score for "question 1" in the "2013" column is "4.16 (-0.03)"
	And the score for "question 1" in the "2012" column is "4.19"
	And there is no score for "question 1" in the "2011" column
	And the Overall Average at the bottom for "2014" is "3.95 (-0.01)"
	And the Overall Average for "2013" is "3.96 (-0.03)"
	And the Overall Average for "2012" is "3.99 (+0.04)"
	And the Overall Average for "2011" is "3.95"
	
	When I am on the "Categories" tab
	And I am on the "Evaluation" tab
	Then the "Count" for the "2014" column is "(45)"
	And the "Count" for the "2013" column is "(199)"
	And the "Count" for the "2012" column is "(224)"
	And the "Count" for the "2011" column is "(34)"	
	And the score for the "Clarity of Direction" category for "2014" is "4.03 (-0.01)"
	And the score for the "Clarity of Direction" category for "2013" is "4.04 (-0.06)"
	And the score for the "Clarity of Direction" category for "2012" is "4.10 (+0.16)"
	And the score for the "Clarity of Direction" category for "2011" is "3.94"
	And the Overall Average at the bottom for "2014" is "3.95 (-0.01)"
	And the Overall Average for "2013" is "3.96 (-0.03)"
	And the Overall Average for "2012" is "3.99 (+0.04)"
	And the Overall Average for "2011" is "3.95"

	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "4.33 (+0.17)"
	And the score for "question 1" in the "2013" column is "4.16 (-0.03)"
	And the score for "question 1" in the "2012" column is "4.19"
	And there is no score for "question 1" in the "2011" column
	
	When I am on the "Questions" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(45)"
	And the "Count" for the "2013" column is "(199)"
	And the "Count" for the "2012" column is "(224)"
	And the "Count" for the "2011" column is "(34)"	
	And the score for "question 1" in the "2014" column is "88.9 (+1.2)"
	And the score for "question 1" in the "2013" column is "87.7 (-0.1)"
	And the score for "question 1" in the "2012" column is "87.8"
	And there is no score for "question 1" in the "2011" column
	And the Overall Average at the bottom for "2014" is "75.9 (-0.9)"
	And the Overall Average for "2013" is "76.8 (-1.0)"
	And the Overall Average for "2012" is "77.8 (+1.5)"
	And the Overall Average for "2011" is "76.3"
	
	When I am on the "Categories" tab
	And I am on the "Favorability" tab
	Then the "Count" for the "2014" column is "(45)"
	And the "Count" for the "2013" column is "(199)"
	And the "Count" for the "2012" column is "(224)"
	And the "Count" for the "2011" column is "(34)"	
	And the score for the "Clarity of Direction" category for "2014" is "77.2 (-6.0)"
	And the score for the "Clarity of Direction" category for "2013" is "83.2 (-1.9)"
	And the score for the "Clarity of Direction" category for "2012" is "85.1 (+6.3)"
	And the score for the "Clarity of Direction" category for "2011" is "78.8"
	And the Overall Average at the bottom for "2014" is "75.9 (-0.9)"
	And the Overall Average for "2013" is "76.8 (-1.0)"
	And the Overall Average for "2012" is "77.8 (+1.5)"
	And the Overall Average for "2011" is "76.3"
	
	When I click on the "Clarity of Direction" category
	Then the score for "question 1" in the "2014" column is "88.9 (+1.2)"
	And the score for "question 1" in the "2013" column is "87.7 (-0.1)"
	And the score for "question 1" in the "2012" column is "87.8"
	And there is no score for question 1 in the "2011" column

	Now I go to "Functions" > "Logout"